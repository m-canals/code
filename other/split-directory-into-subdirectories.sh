#!/bin/sh

# TODO
#
#  - Make zenity recommended but not required.

TITLE="Arrange files into directories"
TEXT="Files per directory:"

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-h] directory

Move files and directories located at certain directories into directories \
(named 1, 2, ...) located at the same directories in alphabetical order.
Required programs: cat, echo, find, head, mkdir, mktemp, printf, rm, rmdir, \
sort, test, tr, wc, xargs, zenity.

Opions

 -h  Show this message.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Number of files per directory is not a non-negative integer.
 4) Could not change directory.
 5) Some directory could not be created.

Usage as Thunar custom action

  Action             | Condition         | Command
 --------------------+-------------------+-------------------------------------
  Arrange in folders | directory         | $script_name %f
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "There are not enough arguments.\n" >&2 ;;
		4) printf "There are too many arguments.\n" >&2 ;;
		5) zenity_error "Number of files per directory is not a non-negative integer." ;;
		6) zenity_error "Could not change directory." ;;
		7) zenity_error "Some directory could not be created." ;;
	esac
	exit $1
}

zenity_error() {
	zenity --error --title "Error" --no-wrap --text "$1"
}

count_character() {
	tr --complement --delete "$1" |
	wc --bytes
}

trim() {
	local old new
	old=$1
	new=${old#0}
	while test ${#new} -lt ${#old}
	do
		old=$new
		new=${old#0}
	done
	printf '%s' "$new"
}

while getopts ":h" option
do
	case $option in
		h) print_help; exit 0 ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -lt 1
then error 3
elif test $# -gt 1
then error 4
fi

# Get number of files per directory.
fpd=$(zenity --title="$TITLE" --entry --text="$TEXT")
fpd=$(trim "$fpd")

if ! test "$fpd" -eq "$fpd" -a "$fpd" -ge 0 2> /dev/null
then error 5
elif test ${#fpd} -eq 0
then exit 0
fi

# Get files to arrange.
if ! cd "$1"
then error 6
fi

list=$(mktemp)
find -maxdepth 1 -print0 |
sort --zero-terminated > "$list"

# Get number of files to arrange.
files=$(count_character '\0' < "$list")

if test $files -eq 0
then exit 0
fi

# Get number of required directories.
directories=$((files / fpd + (files % fpd > 0)))

# Arrange files.
directory=1
while test $directory -le $directories
do
	if ! mkdir "$directory"
	then
		while test $directory -gt 1
		do
			directory=$((directory-1))
			rmdir "$directoy"
		done
		error 7
	fi
	directory=$((directory+1))
done

exec 3< "$list"
directory=1
while test $directory -le $directories
do
	head --zero-terminated --lines $fpd <&3 |
	xargs --null mv --no-clobber --target-directory "$directory"
	echo $((directory * 100 / directories))
	directory=$((directory+1))
done |
zenity --progress --auto-close
exec 3<&-
rm "$list"

exit 0
