#!/bin/sh

# TODO
#
#  - Sembla que no accepta més de 40449 bytes. Cal separar-ho, primer per
#    signes de puntuació que delimiten sentències i deprés per comes, espais,
#    etc. (si ho féssim per bytes trencaríem sentències i paraules).

API_URL="https://www.softcatala.org/apertium/json/translate"

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "Source language '%s' is not supported.\n" "$2" >&2 ;;
		4) printf "Target language '%s' is not supported.\n" "$2" >&2 ;;
		5) printf "Both languages are the same.\n" >&2 ;;
		6) printf "None of the languages is catalan.\n" >&2 ;;
	esac
	exit $1
}

is_supported() {
	case $1 in
		ca|an|es|pt|fr|en|oc) return 0 ;;
		*) return 1 ;;
	esac
}

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-hm] {-f source-language | -t target-language} [-u user-agent] \
[text]...

Translate short fragments of text (~ 39.5 KiB) by using Softcatalà Apertium-\
based service. If there is no argument text is read from stdin. All leading \
and trailing whitespace characters are removed. Required programs: cat, jq, \
od, printf, test, tr, wget.

Options

 -f language   Set source language. Default: catalan.
 -h            Show this message.
 -m            Mark unknown words with an asterisk.
 -t language   Set target language. Default: catalan.
 -u user-agent Set HTTP user agent. Default: empty string.

Supported languages

 an) Aragonés
 es) Castellano
 en) English
 fr) Français
 oc) Occitan (aranés)
 pt) Português

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Non-supported source language.
 4) Non-supported target language.
 5) Same languages.
 6) None of the languages is catalan.
EOF
}

url_encode() {
	printf '%s' "$1" |
	od --output-duplicates --address-radix='n' --format=x1 |
	tr -d '\n' |
	tr ' [:lower:]' '%[:upper:]' 
}

from=ca
mark_unknown=no
to=ca
user_agent=

while getopts ":f:hmt:u:" option
do
	case $option in
		f) from=$OPTARG ;;
		h) print_help; exit 0 ;;
		m) mark_unknown=yes ;;
		t) to=$OPTARG ;;
		u) user_agent=$OPTARG ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if ! is_supported "$from"
then error 3 "$from"
elif ! is_supported "$to"
then error 4 "$to"
elif test "$from" = "$to"
then error 5
elif test "$from" != "ca" -a "$to" != "ca"
then error 6
fi

if test $# -eq 0
then read_stdin=true
else read_stdin=false
fi

if $read_stdin
then input=$(cat)
else input=$*
fi

query=$(url_encode "$input")
data="langpair=$from%7C$to&markUnknown=$mark_unknown&q=$query"

# It returns an object with the following attributes: responseStatus (HTTP
# status code), responseData (an object with the attribute translatedText) and
# responseDetails.
wget --quiet --output-document - --user-agent "$user_agent" 2>/dev/null \
 --post-data "$data" "$API_URL" |
jq --raw-output '.responseData.translatedText'

exit 0
