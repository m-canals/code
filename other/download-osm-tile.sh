#!/bin/sh

API_URL="https://a.tile.openstreetmap.org"

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-h] latitude longitude zoom-level

Download OpenStreetMap tiles. Required programs: bc, cat, printf, rm, test, \
wget.

Options

 -h  Show this message.
 -u  Set HTTP user agent. Default: 'osm-tile-downloader'.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Not enough arguments.
 4) Too many arguments.
 5) Unable to download file.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "There are not enough arguments.\n" >&2 ;;
		4) printf "There are too many arguments.\n" >&2 ;;
		5) printf "Unable to download file.\n" >&2 ;;
	esac
	exit $1
}

user_agent="osm-tile-downloader"

while getopts ":h" option
do
	case $option in
		h) print_help; exit 0 ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -lt 3
then error 3
elif test $# -eq 3
then latitude=$1 longitude=$2 zoom=$3
else error 4
fi

# https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
# log(tan(x) + 1 / cos(x)) =
# log(sin(x) / cos(lat) + 1 / cos(lat)) =
# log((sin(x) + 1) / cos(x)) =
# log(sin(x) + 1) - log(cos(x))
read x y << EOF
$(printf "%s" "
pi = 4 * a(1)
l = $latitude / 180.0 * pi
n = 2.0 ^ $3
x = ($longitude + 180.0) / 360.0 * n
y = (1.0 - (l(s(l) + 1.0) - l(c(l))) / pi) / 2.0 * n
scale = 0
print x / 1, \" \", y / 1
" | bc -l)
EOF

url=$(printf '%d/%d/%d.png' $3 $x $y)
name=$(printf '%s' "$url" | tr '/' '_')

if ! wget --user-agent "$user_agent" --output-document "$name" "$API_URL/$url"
then
	rm -f "$name"
	error 5
fi

exit 0
