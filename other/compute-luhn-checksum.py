#!/usr/bin/env python3
# https://en.wikipedia.org/wiki/Luhn_algorithm

import multiprocessing

def sum_even_digits(digits, right_offset, left_offset):
	return sum([0, 2, 4, 6, 8, 1, 3, 5, 7, 9][int(digits[i])]
		for i in range(len(digits) - 1 - right_offset, -1 + left_offset, -2))

def sum_odd_digits(digits, right_offset, left_offset):
	return sum(int(digits[i])
		for i in range(len(digits) - 2 - right_offset, -1 + left_offset, -2))

class Luhn(object):
	def __init__(self, digits):
		self.__digits = digits
	def validate(self):
		return self.compute(right_offset = 1) == int(self.__digits[-1])
	def compute(self, right_offset = 0, left_offset = 0):
		# Paral·lelització de l'execució amb finalitat didàctica
		# (segurament és menys eficient pel sobrecost de paral·lelització).
		pool = multiprocessing.Pool(processes = 2)
		# Right-to-left.
		arguments = (self.__digits, right_offset, left_offset)
		even_sum = pool.apply_async(sum_even_digits, arguments)
		odd_sum = pool.apply_async(sum_odd_digits, arguments)
		return 10 - ((even_sum.get() + odd_sum.get()) % 10)

def _parse_arguments():
	def number(argument):
		if not argument.isdigit():
			raise ValueError
		return argument
	parser = ArgumentParser(
		add_help = False,
		description = "Compute or validate Luhn checksums.")
	parser.add_argument('numbers',
		metavar = "NUMBER",
		nargs = '*',
		type = number,
		help = "Compute or validate NUMBER checksum.")
	parser.add_argument('-v', '--validate',
		action = 'store_true',
		dest = 'validate',
		default = False,
		help = "Validate checksum instead of computing it.")
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")
	return parser.parse_args()

def _main():
	arguments = _parse_arguments()
	for number in arguments.numbers:
		luhn = Luhn(number)
		if arguments.validate:
			print("%-5s  %s" %(luhn.validate(), number))
		else:
			print("%d  %s" %(luhn.compute(), number))

if __name__ == "__main__":
	from argparse import ArgumentParser
	_main()

