#!/bin/sh

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-ahv] [link]...

Download audio, subtitle and video files from YouTube. If there is no \
argument links are read from stdin. Required programs: cat, ffmpeg, head, od, \
printf, tail, test, tr, youtube-dl.
 
If it stops working try updating youtube-dl:

 pip3 install --upgrade youtube-dl (if installed as a Python package)

Opions

 -a  Download audio.
 -h  Show this message.
 -s  Download subtitle (requires user interaction).
 -v  Download video.
 -f  Use ffmpeg directly.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
	esac
	exit $1
}

get_lines() {
	head --lines "$1" |
	tail --lines "$2"
}

select_item_from_arguments() {
	local prompt="$1" key_var="$2" val_var="$3"
	shift 3
	if test $# -eq 0
	then
		return 1
	else
		local i=0 value key max 
		for value
		do
			i=$((i+1))
			printf '%d) %s\n' "$i" "$value"
		done
		max=$i
		read -p "$prompt" key
		while ! test "$key" -ge 1 -a "$key" -le $max 2> /dev/null
		do read -p "$prompt" key
		done
		eval "$key_var=$key"
		eval "$val_var=\$$key"
		return 0
	fi
}

url_encode() {
	printf '%s' "$1" |
	od --output-duplicates --address-radix='n' --format=x1 |
	tr -d '\n' |
	tr ' [:lower:]' '%[:upper:]' 
}

download() {
	$DOWNLOAD_VIDEO && download_video "$1"
	$DOWNLOAD_AUDIO && download_audio "$1"
	$DOWNLOAD_SUBTITLE && download_subtitle "$1"
}

download_audio() {
	if $USE_FFMPEG
	then
		local video audio
		video=$(get_filename "$1")
		youtube-dl --format best --merge-output-format mp4 "$1"
		audio=${video%.*}.mp3
		ffmpeg -y -i "file:$video" -vn -acodec libmp3lame "file:$audio"
		$DOWNLOAD_VIDEO || rm "$video"
	else
		local options
		$DOWNLOAD_VIDEO && options=--keep-video
		youtube-dl --format best --merge-output-format mp4 \
		 --extract-audio --audio-format mp3 $options "$1"
	fi
}

download_subtitle() {
	local subtitles languages language language_key formats format_key format
	subtitles=$(get_available_subtitles "$1")
	languages=$(printf '%s' "$subtitles" | cut --delimiter ' ' --fields 1)
	select_item_from_arguments "Language: " language_key language $languages
	if test -n "$language_key"
	then
		formats=$(printf '%s' "$subtitles" | get_lines "$language_key" 1 | cut --delimiter ' ' --fields 2-)
		select_item_from_arguments "Format: " format_key format $formats
		youtube-dl --skip-download --write-sub \
		 --sub-format "$format" --sub-lang "$language" "$1"
	fi
}

download_video() {
	youtube-dl --format best --merge-output-format mp4 "$1"
}

get_available_subtitles() {
	youtube-dl --list-subs "$1" |
	grep --after-context $((0x7FFFFFFFFFFFFFFF)) "^Available subtitles"  |
	tail --lines +3 |
	tr --delete ','
}

get_filename() {
	youtube-dl --get-filename "$1"
}

SITE_URL="https://www.youtube.com"
USE_FFMPEG=false
DOWNLOAD_AUDIO=false
DOWNLOAD_SUBTITLE=false
DOWNLOAD_VIDEO=false

while getopts ":afhsv" option
do
	case $option in
		a) DOWNLOAD_AUDIO=true ;;
		f) USE_FFMPEG=true ;;
		h) print_help; exit 0 ;;
		s) DOWNLOAD_SUBTITLE=true ;;
		v) DOWNLOAD_VIDEO=true ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -eq 0
then read_stdin=true
else read_stdin=false
fi

if $read_stdin
then
	while IFS= read -r link
	do download "$link"
	done
else
	for link
	do download "$link"
	done
fi

exit 0
