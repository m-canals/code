#!/bin/sh

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-h] 'compress' directory file
 $script_name [-h] 'extract' file directory
 $script_name [-h] 'tag' file image-source

Compress files into ODT files, extract files from ODT files or tag ODT files \
with very small images that are downloaded from the source when the files \
are opened. Required programs: cat, printf, realpath, rm, sed, test, unzip, \
xargs, zip.

Options

 -h Show this message.

Examples

 $script_name tag 'file.odt' 'http://localhost:8080/'

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Not enough arguments.
 4) Too many arguments.
 5) Undefined action.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "There are not enough arguments.\n" >&2 ;;
		4) printf "There are too many arguments.\n" >&2 ;;
		5) printf "Action '%s' is not defined.\n" "$2" >&2 ;;
	esac
	exit $1
}

# Compress optimally and recursively files in directory $1 into file $2,
# updating its contents.
compress() {
	local output
	
	if test $# -lt 2
	then error 3
	elif test $# -gt 2
	then error 4
	fi
	
	# Absolute path.
	output=$(realpath --canonicalize-missing --no-symlinks --zero "$2")
	
	(
		cd "$1" &&
		printf '%s' "$output" |
		xargs --null -I {} zip -9ru {} .
	)
}

# Extract file $1 into directory $2, overwritting its contents.
# If extraction directory doesn't exist, it's created.
extract() {
	if test $# -lt 2
	then error 3
	elif test $# -gt 2
	then error 4
	fi
	
	unzip -o "$1" -d "$2"
}

# Tag file (link $2) into file $1.
tag() {
	local hred width height tag dir
	if test $# -lt 2
	then error 3
	elif test $# -gt 2
	then error 4
	fi
	
	href=$2
	width="0.0201in"
	height="0.0201in"
	tag="<draw:frame text:anchor-type=\"page\" text:anchor-page-number=\"1\" svg:width=\"$width\" svg:height=\"$height\" draw:z-index=\"0\"><draw:image xlink:href=\"$href\" xlink:type=\"simple\" xlink:show=\"embed\" xlink:actuate=\"onLoad\"/></draw:frame>"
	tag=$(printf '%s' "$tag" | sed -e 's/[\/&]/\\&/g')
	
	dir=$(mktemp -d)
	unzip -p "$1" content.xml > $dir/content.xml
	sed -i "s/\(<office:text[^>]*>\)/\1$tag/g" $dir/content.xml
	zip -9j "$1" $dir/content.xml
	rm -r "$dir"
}

while getopts ":h" option
do
	case $option in
		h) print_help; exit 0 ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -lt 1
then error 3
fi

action=$1
shift

case $action in
	compress) compress "$@" ;;
	extract) extract "$@" ;;
	tag) tag "$@" ;;
	*) error 5 "$action" ;;
esac

exit 0
