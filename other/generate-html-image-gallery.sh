#!/bin/sh

# TODO
#
# Title, encoding, width, height, css, js, etc.

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-h] [link]...

Generate HTML image galleries. If there is no argument, image links are read \
from stdin. Required programs: cat, printf, sed, test.

Options

 -h            Show this message.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
	esac
	exit $1
}

html_encode() {
	printf '%s' "$1" |
	sed 's/&/\&amp;/g; s/</\&lt;/g; s/>/\&gt;/g; s/"/\&quot;/g'
}

print_element() {
	local link=$(html_encode "$1")
	printf '\t\t<a href="%s"><img width="400" src="%s"></a>\n' "$link" "$link"
}

while getopts ":h" option
do
	case $option in
		h) print_help; exit 0 ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

cat << EOF
<!DOCTYPE html>
<html>
	<head></head>
	<body>
EOF

if test $# -eq 0
then
	while IFS= read -r link
	do print_element "$link"
	done
else
	for link
	do print_element "$link"
	done
fi

cat << EOF
	</body>
</html>
EOF

exit 0
