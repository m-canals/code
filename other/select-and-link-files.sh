#!/bin/sh

# TODO
#
#  - Fix relative linkage error messages (file may be written simultaneously by
#    both xargs commands).

SELECTION="/tmp/$USER-file-selection"
ERROR="/tmp/$USER-linkage-errors"

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-h] 'select' [file]...
 $script_name [-h] 'link' 'absolute' directory
 $script_name [-h] 'link' 'relative' directory
 $script_name [-h] 'link' 'hard' directory

Select files and create absolute, relative or hard links to them. Required \
programs: cat, ln, realpath, rm, which, xargs. Recommended programs: zenity.

Options

 -h  Show this message.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Invalid action.
 4) Invalid link type.
 5) Error file could not be removed before execution.
 6) Error file does exist before execution.
 7) Error file does not exist after execution.
 8) Some file could not be linked.

Usage as Thunar custom actions

  Action          | Condition         | Command
 -----------------+-------------------+----------------------------------------
  Link (absolute) | directory         | $script_name absolute %f
  Link (relative) | directory         | $script_name relative %f
  Link (hard)     | directory         | $script_name hard %f
  Select          | directory or file | $script_name select %F
EOF
}

warning() {
	which zenity > /dev/null &&
	zenity --warning --title "Warning" --text "$1" ||
	printf '%s\n' "$1"
	exit 0
}

show_error() {
	which zenity > /dev/null &&
	zenity --error --title "Error" --text "$1" ||
	printf '%s\n' "$1"
}

show_errors() {
	which zenity > /dev/null &&
	zenity --text-info --title 'Errors' --filename "$1" ||
	cat "$1"
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "Action '%s' is not defined.\n" "$2" >&2 ;;
		4) printf "Invalid link type.\n" "$2" >&2 ;;
		5) show_error "Error file could not be removed before execution." ;;
		6) show_error "Error file does exist before execution." ;;
		7) show_error "Error file does not exist after execution." ;;
		8) show_errors "$ERROR" ;;
	esac
	exit $1
}

select_files() {
	local file
	rm --force -- "$SELECTION"
	for file
	do printf '%s\0' "$file" >> "$SELECTION" 
	done
}

#PERL_CODE='for my $i (1 .. $#ARGV) { printf "%s\0", File::Spec->abs2rel($ARGV[$i], $ARGV[0]); }'

link_to_files() {
	local type="$1" directory="$2"

	if test ! -e "$SELECTION"
	then warning "There are no selected files."
	elif ! rm --force -- "$ERROR"
	then error 5
	elif test -e "$ERROR"
	then error 6
	fi

	# It should be noticed that xargs uses as many arguments per command line as
	# possible, so the command line of the following invocations is not
	# necessarily executed as many times as the number of selected files.
	#
	# Therefore, the fourth form of ln is used:
	#
	#  ln --target-directory DIRECTORY TARGET...

	case $type in
		absolute)
			xargs --null -- \
			ln --target-directory "$directory" --symbolic -- \
			< "$SELECTION" 2> "$ERROR" 1>&2 ;;
		relative)
			#perl -MFile::Spec -e "$PERL_CODE" -- "$directory" \
			xargs -l --null -- \
			realpath --zero --relative-to "$directory" -- \
			< "$SELECTION" 2> "$ERROR" |
			xargs -l --null -- \
			ln --target-directory "$directory" --symbolic -- \
			2> "$ERROR" 1>&2 ;;
		hard)
			xargs --null -- \
			ln --target-directory "$directory" -- \
			< "$SELECTION" 2> "$ERROR" 1>&2 ;;
		*)
			error 4 ;;
	esac

	if test ! -e "$ERROR"
	then error 7
	elif test $(stat --format '%s' "$ERROR") -gt 0
	then error 8
	fi
}

while getopts ":h" option
do
	case $option in
		h) print_help; exit 0 ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

action=$1
shift

case $action in
	select) select_files "$@" ;;
	link) link_to_files "$@" ;;
	*) error 3
esac

exit 0
