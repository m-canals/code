#!/usr/bin/perl

sub getAttributeValue {
	my ($attribute, $attributes) = @_;
	my $value;
	
	# attribute="..." attribute='...'  attribute=...
	if ($attributes =~ /$attribute="([^"]*)"/i) {
	   return $1;
	} elsif ($attributes =~ /$attribute='([^']*)'/i) {
	   return $1;
	} elsif ($attributes =~ /$attribute=([^ ]*)/i) {
	   return $1;
	}
}

sub getElementReplacement {
	my ($lt, $tag, $attributes, $gt) = @_;
	my $src = "";

	if ($tag =~ /img/i) {
		$link = getAttributeValue("src", $attributes);
	}
	
	$text = $link;
	$text =~ s/>/&gt;/ig;
	$text =~ s/</&lt;/ig;
	
	return "[".$lt."a"." href=\"".$link."\"".$gt.$text.$lt."/a".$gt."]";
}

my $tags = "img";

my %replacements = (
	# <TAG .../>
	# <TAG ...>...</TAG>
	qr{(<)($tags) (.*?)/?(>)(?:.*?\1/\2\4)?}i => 'getElementReplacement($1, $2, $3, $4)'
);

my @lines = <>;

foreach my $line (@lines) {
	foreach my $regex (keys(%replacements)) {
		$line =~ s/$regex/$replacements{$regex}/gee;
	}
	
	print($line);
}
