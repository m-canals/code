#!/bin/sh

# TODO
#
#  - Sembla que no accepta més de 5405 bytes. Cal separar-ho, primer per signes
#    de puntuació que delimiten sentències i deprés per comes, espais, etc. (si
#    ho féssim per bytes trencaríem sentències i paraules).

API_URL="https://translate.googleapis.com/translate_a/single"

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-f source-language] [-h] [-t target-language] [-u user-agent] \
[text]...

Translate short fragments of text (~ 5 KiB) by using Google Translate service. \
If there is no argument text is read from stdin. All leading and trailing \
whitespace characters are removed. Required programs: cat, jq, od, printf, \
tr, test, wget.

Options

 -f source-language  Set source language. Default: english.
 -h                  Show this message.
 -t target-language  Set target language. Default: english.
 -u user-agent       Set HTTP user agent. Default: empty string.

Supported languages

 - https://cloud.google.com/translate/docs/languages

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
	esac
	exit $1
}

url_encode() {
	printf '%s' "$1" |
	od --output-duplicates --address-radix='n' --format=x1 |
	tr -d '\n' |
	tr ' [:lower:]' '%[:upper:]' 
}

from=en
to=en
user_agent=

while getopts ":f:ht:u:" option
do
	case $option in
		h) print_help; exit 0 ;;
		f) from=$OPTARG ;;
		t) to=$OPTARG ;;
		u) user_agent=$OPTARG ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -eq 0
then read_stdin=true
else read_stdin=false
fi

if $read_stdin
then input=$(cat)
else input=$*
fi

from=$(url_encode "$from")
to=$(url_encode "$to")
input=$(url_encode "$input")
url=
query="client=gtx&sl=$from&tl=$to&dt=t&q=$input"

# Returns an array whose first element is an array of fragments or null if the
# translation fails. Each fragment is an array whose first two elements are the
# translated fragment and the source fragment.
wget --quiet --output-document - --user-agent "$4" "$API_URL?$query" \
 2> /dev/null |
jq --join-output 'if .[0] == null then "" else .[0][][0] end'

if ! $read_stdin
then printf '\n'
fi

exit 0
