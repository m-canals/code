#!/usr/bin/env python2
# -*- encoding: utf-8 -*-

URL_FORMAT = 'http://www.ccma.cat/Comu/standalone/tv3_programacio_canal-tv3/contenidor/divgraella_tv3_%d/0/0'

from HTMLParser import HTMLParser

class TV3ScheduleParser(HTMLParser):
	def __init__(self):
		HTMLParser.__init__(self)
		self.programes = list()
		self.programa = None
		self.ambit = None
		self.ambits = list()

	def handle_starttag(self, tag, attributes):
		attributes = dict(attributes)
		if tag == 'div' and 'class' in attributes:
			self.ambit = attributes['class']
			self.ambits.append(self.ambit)
		elif tag == 'li':
			self.programa = {
				'hora': attributes['data-date'],
				'imatge': None,
				'informació': [],
				'descripció': []
			}
		elif tag == 'img' and self.ambit == 'imatge-programa':
			self.programa['imatge'] = attributes['src']
			
	def handle_endtag(self, tag):
		if tag == 'div' and len(self.ambits) > 0:
			self.ambit = self.ambits.pop()
		elif tag == 'li':
			self.programes.append(self.programa)
			self.programa = None

	def handle_data(self, data):
		data = data.strip()
		if self.ambit == 'informacio-programa':
			self.programa['informació'].append(data)
		elif self.ambit == 'codis':
			self.programa['descripció'].append(data)

def _parse_arguments():
	parser = ArgumentParser(add_help = False, description =
		"Get TV3 schedule.")
	parser.add_argument('-c', '--color',
		action = 'store_false', default = True,
		help = "Disable colored output.")
	parser.add_argument('-d', '--day', metavar = "DAY",
		type = int, choices = range(1, 11), default = 1,
		help = "Show schedule for DAY (1-10).")
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")
	return parser.parse_args()

def _main():
	arguments = _parse_arguments()
	stream = urlopen(URL_FORMAT % arguments.day)
	html = stream.read() 
	html = html.decode('utf-8')
	parser = TV3ScheduleParser()
	parser.feed(html)
	for programa in parser.programes:
		if len(programa['informació']) > 1:
			informacio = programa['informació'][0] + ' - ' + \
			             ' '.join(programa['informació'][1:])
		else:
			informacio = ' '.join(programa['informació'])
		if programa is not parser.programes[0]:
			print
		if arguments.color:
			print '\033[02m' + programa['hora'] + '\033[0m', \
			      '\033[04m' + informacio + '\033[0m'
			print '. '.join(programa['descripció'])
		else:
			print programa['hora'], ' '.join(programa['informació'])
			print '. '.join(programa['descripció'])
			
if __name__ == "__main__":
	from argparse import ArgumentParser
	from urllib import urlopen
	_main()

