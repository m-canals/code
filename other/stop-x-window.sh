#!/bin/sh

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-h]

Stop and continue X window processes. Required programs: awk, cat, kill, \
printf, test, xwininfo.

Options

 -h  Show this message.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Too many arguments.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "There are too many arguments.\n" >&2 ;;
	esac
	exit $1
}

while getopts ":h" option
do
	case $option in
		h) print_help; exit 0 ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -gt 0
then error 3
fi

pid=$(xwininfo -wm | awk '$0 ~ "Process id" {print $3}')
state=$(awk '$1 ~ "State:" {print $2}' /proc/$pid/status)

case $state in
	T) kill -s 18 $pid ;; # SIGCONT
	*) kill -s 19 $pid ;; # SIGSTOP
esac

exit 0
