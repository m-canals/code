#!/bin/sh

# TODO
#
#  - Make it work with multiple outputs.
#  - Variable increment.

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-h]
 $script_name [-h] +
 $script_name [-h] -
 $script_name [-h] 1-20

Change screen brightness level. Required programs: cat, cut, grep, printf, \
test, which, xrandr. Recommended programs: zenity.

Options

 -h         Show this message.
 -o output  Set output (see xrandr output).

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Too many arguments.
 4) Invalid argument.
 5) Failed to change brightness level.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "There are too many arguments.\n" >&2 ;;
		4) printf "Invalid argument.\n" >&2 ;;
		5) printf "Failed to change brightness level.\n" >&2 ;;
		6) printf "GUI dialog requires zenity.\n" >&2 ;;
	esac
	exit $1
}

# Represent $1-decimal floating point number $2 as an integer.
to_pseudo_integer() {
	printf "scale = 0; %s * 10 ^ %d / 1\n" "$2" "$1" | bc
}

# Represent integer $2 as a $1-decimal floating point number.
from_pseudo_integer() {
	printf "scale = %d; %s / 10 ^ %d;\n" "$1" "$2" "$1" | bc
}

# Format depends on value. It prints at least one decimal for values between
# MINIMUM and MAXIMUM.
get_brightness() {
	xrandr --verbose |
	grep "Brightness" |
	cut --delimiter ' ' --field 2
}

set_brightness() {
	xrandr --output "$2" --brightness "$1" || error 5
}

ask_value() {
	local old_value value new_value
	old_value=$(get_brightness)
	value=$(to_pseudo_integer 1 "$old_value")
	value=$((value < MINIMUM ? 1 : value > MAXIMUM ? 20 : value))
	zenity --title "Brightness" --scale --print-partial --ok-label "Done" \
	--value $value --min-value $MINIMUM --max-value $MAXIMUM |
	while read value
	do
		new_value=$(from_pseudo_integer 1 $value)
		set_brightness "$new_value" "$1"
		printf "Changed brightness level from %s to %s.\n" \
		 "$old_value" "$new_value" >&2
		old_value=$new_value
	done
}

increase_value() {
	local old_value value new_value
	old_value=$(get_brightness)
	value=$(to_pseudo_integer 1 "$old_value")
	value=$((value + $1))
	value=$((value < MINIMUM ? 1 : value > MAXIMUM ? 20 : value))
	new_value=$(from_pseudo_integer 1 $value)
	set_brightness "$new_value" "$2"
	printf "Changed brightness level from %s to %s.\n" \
	 "$old_value" "$new_value" >&2
}

set_value() {
	local old_value value new_value
	old_value=$(get_brightness)
	value=$1
	value=$((value < MINIMUM ? 1 : value > MAXIMUM ? 20 : value))
	new_value=$(from_pseudo_integer 1 $value)
	set_brightness "$new_value" "$2"
	printf "Changed brightness level from %s to %s.\n" \
	 "$old_value" "$new_value" >&2
}

MINIMUM=1
MAXIMUM=20

output=eDP-1

while getopts ':ho:' option
do
	case $option in
		h) print_help; exit 0 ;;
		o) output=$OPTARG ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -gt 1
then error 3
fi

case $1 in
    +) increase_value 1 "$output" ;;
    -) increase_value -1 "$output" ;;
    [1-9]|1[0-9]|20) set_value $1 "$output" ;;
    '') which zenity > /dev/null && ask_value "$output" || error 6 ;;
    *) error 4 ;;
esac

exit 0
