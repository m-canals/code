#!/bin/sh

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-Lh] [-A max-amplitude] [-P max-pitch] [-S max-speed] \
[-a min-amplitude] [-p min-pitch] [-s min-speed] [-v voice-name] [text]...

Spell out text. If there is no argument text is read from stdin. If an \
unavailable voice is used it does nothing. Required programs: cat, espeak, 	
expr, printf, test, xxd.

Options

 -A max-amplitude  Set maximum amplitude. Default: 10.
 -P max-pitch      Set maximum pitch. Default: 50.
 -S max-speed      Set maximum speed. Default: 160.
 -a min-amplitude  Set minimum amplitude. Default: 10.
 -h                Show this message.
 -l                Assume locale unaware 'expr'.
 -p min-pitch      Set minimum pitch. Default: 50.
 -s min-speed      Set minimum speed. Default: 160.
 -v voice-name     Set voice name. Default: 'default'.

Available voices

 espeak --voices

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
	esac
	exit $1
}

get_random_integer() {
	# Between - 2 ^ 63 and 2 ^ 63 - 1 (2 ^ 64 distinct values).
	local value=$((0x$(xxd -ps -l 8 /dev/urandom) % ($2 - $1 + 1)))
	if test "$value" -lt 0
	then echo $(($1 - value))
	else echo $(($1 + value))
	fi
}

# https://en.wikipedia.org/wiki/UTF-8#Description
utf8_character_length() {
	local value=$(printf '%d' "'$1")
	if test $value -ge 240
	then echo 4
	elif test $value -ge 224
	then echo 3
	elif test $value -ge 192
	then echo 2
	else echo 1
	fi
}

spell_out_string() {
	local input="$1" min_amplitude="$2" max_amplitude="$3" min_pitch="$4" \
	 max_pitch="$5" min_speed="$6" max_speed="$7" voice="$8" use_locale=$9
	local input_length=$(expr length + "$input")
	local i=1 amplitude pitch speed length string
	while test $i -le $input_length
	do
			amplitude=$(get_random_integer $min_amplitude $max_amplitude)
			pitch=$(get_random_integer $min_pitch $max_pitch)
			speed=$(get_random_integer $min_speed $max_speed)
			if $use_locale
			then
				string=$(expr substr + "$input" $i 1)
				i=$((i+1))
			else
				first_byte=$(expr substr + "$input" $i 1)
				length=$(utf8_character_length "$first_byte")
				string=$(expr substr + "$1" $i $length)
				i=$((i+length))
			fi
			espeak -a $amplitude -p $pitch -s $speed -v "$voice" -x --punct "$string" 2>/dev/null
	done
}

max_amplitude=10
max_pitch=50
max_speed=160
min_amplitude=10
min_pitch=50
min_speed=160
use_locale=true
voice='default'

while getopts ":A:P:S:a:hlp:s:v:" option
do
	case $option in
		A) max_amplitude=$OPTARG ;;
		P) max_pitch=$OPTARG ;;
		S) max_speed=$OPTARG ;;
		a) min_amplitude=$OPTARG ;;
		h) print_help; exit 0 ;;
		l) use_locale=false ;;
		p) min_pitch=$OPTARG ;;
		s) min_speed=$OPTARG ;;
		v) voice=$OPTARG ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -eq 0
then read_stdin=true
else read_stdin=false
fi

if $read_stdin
then
	while IFS= read -r line
	do spell_out_string "$line" $min_amplitude $max_amplitude \
	 $min_pitch $max_pitch $min_speed $max_speed $voice $use_locale
	done
else spell_out_string "$*" $min_amplitude $max_amplitude $min_pitch $max_pitch \
 $min_speed $max_speed $voice $use_locale
fi

exit 0
