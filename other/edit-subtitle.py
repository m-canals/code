#!/usr/bin/env python3

# Example:
#
#  edit-subtitle.py -d 8859 -f 184 -t -2 |
#  edit-subtitle.py -f 460 -t -2

class SubRipTextSubtitleTime():
	def __init__(self, value = 0):
		if type(value) == str:
			value = value.replace(',', '.')
			components = value.split(":")
			second = float(components[-1])
			self.time = second
			if len(components) >= 2:
				minute = float(components[-2])
				self.time += minute * 60
			if len(components) >= 3:
				hour = float(components[-3])
				self.time += hour * 60 * 60
			if len(components) >= 4:
				raise ValueError
		else:
			self.time = float(value)
	
	def hour(self):
		return (abs(self.time) // 60) // 60
	
	def minute(self):
		return (abs(self.time) // 60) %  60
	
	def second(self):
		return (abs(self.time) %  60)
	
	def __float__(self):
		return self.time
	
	def __iadd__(self, time):
		self.time += float(time)
		return self
	
	def __imul__(self, time):
		self.time *= float(time)
		return self
	
	def __le__(self, time):
		return self.time <= float(time)
	
	def __ge__(self, time):
		return self.time >= float(time)
	
	def __sub__(self, time):
		return SubRipTextSubtitleTime(self.time - float(time))
	
	def __add__(self, time):
		return SubRipTextSubtitleTime(self.time + float(time))
	
	def __str__(self):
		sign = "-" * (self.time < 0)
		string = "%s%02d:%02d:%06.3f" %(
			sign, self.hour(), self.minute(), self.second())
		return string.replace('.', ',')

class SubRipTextSubtitle():
	def __init__(self,
			start = SubRipTextSubtitleTime(),
			end = SubRipTextSubtitleTime(),
			text = list()):
		self.start = start
		self.end = end
		self.text = text
	
	def __str__(self):
		text = "\n".join(self.text)
		return "%s --> %s\n%s\n" %(self.start, self.end, text)
	
	def delay(self, time, speed):
		duration = self.end - self.start
		self.start *= speed
		self.start += time
		self.end = self.start + duration

class SubRipTextSyntaxError(Exception):
	def __init__(self, lineno, *args):
		super().__init__(args)
		self.lineno = lineno

class SubRipTextFile():
	def __init__(self, subtitles = list()):
		self.subtitles = subtitles
	
	def __str__(self):
		return "".join("%d\n%s\n" %(counter + 1, subtitle)
			for counter, subtitle in enumerate(self.subtitles))
	
	def remove(self, condition):
		preserved = list()
		removed = list()
		for counter, subtitle in enumerate(self.subtitles):
			if condition(counter + 1, subtitle):
				removed.append(subtitle)
			else:
				preserved.append(subtitle)
		self.subtitles = preserved
		return removed
	
	def add(self, subtitles):
		self.subtitles.extend(subtitles)
		self.subtitles.sort(key = lambda subtitle: subtitle.start.time)
	
	def delay(self, condition, time, speed):
		for counter, subtitle in enumerate(self.subtitles):
			if condition(counter + 1, subtitle):
				subtitle.delay(time, speed)
		self.subtitles.sort(key = lambda subtitle: subtitle.start.time)
	
	def read_from(self, stream):
		subtitles = list()
		lines = list()
		
		for lineno, line in enumerate(stream):
			line = line.strip("\r\n\ufeff") # CR, LF, unicode BOM
			if line == "":
				if len(lines) >= 2:
					try:
						counter = int(lines[0])
					except ValueError:
						raise SubRipTextSyntaxError(lineno - len(lines) + 1)
					times = lines[1].split("-->")
					if len(times) != 2:
						raise SubRipTextSyntaxError(lineno - len(lines) + 2)
					try:
						start = SubRipTextSubtitleTime(times[0])
						end = SubRipTextSubtitleTime(times[1])
					except ValueError:
						raise SubRipTextSyntaxError(lineno - len(lines) + 2)
					text = lines[2:]
					subtitle = SubRipTextSubtitle(start, end, text)
					subtitles.append(subtitle)
				lines.clear()
			else:
				lines.append(line)
		
		was_not_empty = len(self.subtitles) > 0
		self.subtitles += subtitles
		if was_not_empty:
			self.subtitles.sort(key = lambda subtitle: subtitle.start.time)
	
	def write_to(self, stream):
		stream.write(str(self))

def _parse_arguments():
	def CounterOrTime(argument):
		try:
			return int(argument)
		except:
			return SubRipTextSubtitleTime(argument)
	
	parser = ArgumentParser(
		add_help = False,
		description = "Translate and scale SRT subtitles.")
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")
	parser.add_argument('-i', '--input',
		metavar = 'INPUT-FILE',
		default = '/dev/stdin',
		dest = 'input_file',
		help = "Set input file.")
	parser.add_argument('-o', '--output',
		metavar = 'OUTPUT-FILE',
		default = '/dev/stdout',
		dest = 'output_file',
		help = "Set output file.")
	parser.add_argument('-d', '--decode',
		metavar = 'INPUT-ENCODING',
		default = 'utf-8',
		choices = ('8859', 'utf-8'),
		dest = 'input_encoding', 
		help = "Set input encoding.")
	parser.add_argument('-e', '--encode',
		metavar = 'OUTPUT-ENCODING',
		default = 'utf-8',
		choices = ('8859', 'utf-8'),
		dest = 'output_encoding', 
		help = "Set output encoding.")
	parser.add_argument('-f', '--first',
		metavar = 'FIRST-SUBTITLE',
		default = float('-inf'),
		type = CounterOrTime,
		dest = 'first_subtitle', 
		help = "Set counter or start time of the first subtitle to "
		       "modify.")
	parser.add_argument('-l', '--last',
		metavar = 'LAST-SUBTITLE',
		default = float('inf'),
		type = CounterOrTime,
		dest = 'last_subtitle', 
		help = "Set counter or start time of the last subtitle to "
		       "modify.")
	parser.add_argument('-t', '--translate',
		metavar = 'TRANSLATION-CONSTANT',
		default = 0,
		dest = 'translation_constant', 
		help = "Add TRANSLATION-CONSTANT to start and end times of "
		       "modified subtitles (after scaling).")
	parser.add_argument('-s', '--scale',
		metavar = 'SCALING-FACTOR',
		default = 1,
		dest = 'scaling_factor', 
		help = "Multiply start and end times of modified subtitles by "
		       "SCALING-FACTOR (before translation).")
		
	arguments = parser.parse_args()
	
	if type(arguments.first_subtitle) == int:
		arguments.first_counter = arguments.first_subtitle
		arguments.first_start = float('-inf')
	else:
		arguments.first_counter = float('-inf')
		arguments.first_start = arguments.first_subtitle
	
	if type(arguments.last_subtitle) == int:
		arguments.last_counter = arguments.last_subtitle
		arguments.last_start = float('inf')
	else:
		arguments.last_counter = float('inf')
		arguments.last_start = arguments.last_subtitle
	
	def condition (counter, subtitle):
		return (counter >= arguments.first_counter
		    and subtitle.start >= arguments.first_start
		    and counter <= arguments.last_counter
		    and subtitle.start <= arguments.last_start)
	
	arguments.condition = condition
	
	return arguments

def _main():
	arguments = _parse_arguments()
	
	with open(arguments.input_file,
			encoding = arguments.input_encoding) as stream:
		srt_file = SubRipTextFile()
		
		try:
			srt_file.read_from(stream)
		except SubRipTextSyntaxError as error:
			print("Syntax error at line %d." % error.lineno, file = stderr)
			exit(1)
		
		srt_file.delay(
			arguments.condition,
			arguments.translation_constant,
			arguments.scaling_factor)
	
	with open(arguments.output_file, 'w',
			encoding = arguments.output_encoding) as stream:
		srt_file.write_to(stream)

if __name__ == '__main__':
	from argparse import ArgumentParser
	from sys import stderr
	_main()
