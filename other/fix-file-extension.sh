#!/bin/sh

# TODO
#
# - Caldria assignar una extensió a alguns tipus, perquè en el cas dels fitxers
#   d'algun dels tipus que no tenen extensió s'utilitza el subtipus i es
#   modifica una extensió que és vàlida; per exemple: en el cas dels fitxers de
#   tipus «sh» s'utilitza el subtipus «x-shellscript».
#
# - Es podria afegir una opció per a canviar l'assignació d'un tipus.

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-hr] [file]...

Rename files according to their MIME types. Required programs: awk, cat, cut, \
file, mv, printf, test. Required files: /etc/mime.types.

Options

 -h  Show this message.
 -r  Replace extension instead of appending it.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
	esac
	exit $1
}

append() {
	mv "$1" "$1.$2"
}

append_or_replace() {
	local old_filename new_filename
	striped_filename=${1%.*}
	if test ${#striped_filename} -eq 0
	then append "$1" "$2" # .x -> .x.y
	else mv "$1" "$striped_filename.$2" # x.y -> x.z
	fi
	
}

rename=append

while getopts ":hr" option
do
	case $option in
		h) print_help; exit 0 ;;
		r) rename=append_or_replace ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

for file
do
	# Returns the MIME type and subtype (separated with a slash).
	mime=$(file --brief --mime-type "$file")
	# Returns an extension or an empty string.
	extension=$(awk --assign mime=$mime '$1 ~ mime {print $2}' /etc/mime.types)
	# If no extension is found the subtype is used; if it is empty the file is
	# ignored.
	if test "${#extension}" -eq 0
	then
		extension=$(printf '%s' "$mime" | cut --delimiter '/' --field 2-)
		if test "${#extension}" -ne 0
		then $rename "$file" "$extension"    
		fi
	fi
done

exit 0
