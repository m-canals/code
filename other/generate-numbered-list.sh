#!/bin/sh

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-h] [-S string-separator] [-p string-prefix] \
[-s string-suffix] first-number last-number [substring]...

Generate numbered lists where each item consists of the given substrings \
joined by the corresponding item number. Required programs: cat, printf, seq, \
test.

Options

 -S string-separator  Set string separator.
 -e                   Equalize number width by padding with leading zeroes.
 -h                   Show this message.
 -p string-prefix     Set string prefix.
 -s string-suffix     Set string suffix. Default: '\n'.

Examples

 $script_name 1 3 'a'
 a
 a
 a
 
 $script_name 1 3 'a' ''
 a1
 a2
 a3
 
 $script_name 1 3 '' 'b'
 1b
 2b
 3b
 
 $script_name 1 3 a b c
 a1b1c
 a2b2c
 a3b3c

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Not enough arguments.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "There are not enough arguments.\n" >&2 ;;
	esac
	exit $1
}

join_substrings() {
	local separator=$1 string substring
	shift 1
	if [ $# -gt 0 ]
	then
		string=$1
		shift 1
	fi
	for substring
	do string=$string$separator$substring
	done
	printf '%s' "$string"
}

string_prefix=''
string_separator=''
string_suffix='\n'
seq_options=''

while getopts ":S:ehp:s:" option
do
	case $option in
		S) string_separator=$OPTARG ;;
		e) seq_options="$seq_options--equal-width" ;;
		h) print_help; exit 0 ;;
		p) string_prefix=$OPTARG ;;
		s) string_suffix=$OPTARG ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -lt 2
then error 3
else
	begin=$1 end=$2
	shift 2
fi

for i in $(seq $seq_options $begin $end)
do
	printf "$string_prefix"
	join_substrings $i "$@"
	printf "$string_suffix"
	if [ $i -ne $end ]
	then printf "$string_separator"
	fi
done

exit 0
