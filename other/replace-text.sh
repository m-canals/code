#!/bin/sh

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-h] keyword replacement [file]

Replace keywords with replacement strings. Files are edited in place. If \
there is no file, input is read from stdin. Required programs: cat, printf, \
sed, test.

Options

 -h  Show this message.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Not enough arguments.
 4) Too many arguments.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "There are not enough arguments.\n" >&2 ;;
		4) printf "There are too many arguments.\n" >&2 ;;
	esac
	exit $1
}

while getopts ":h" option
do
	case $option in
		h) print_help; exit 0 ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -lt 2
then error 3
elif test $# -eq 2
then infile=false
elif test $# -eq 3
then infile=true
else error 4
fi

# Escape '^' and bracket everything else.
keyword=$(printf '%s' "$1" | sed -e 's/[^^]/[&]/g; s/\^/\\^/g')
# Escape '\', '/' and '&'.
replacement=$(printf '%s' "$2" | sed -e 's/[\/&]/\\&/g')

if $infile
then sed -i "s/$keyword/$replacement/g" "$3"
else sed "s/$keyword/$replacement/g"
fi

exit 0
