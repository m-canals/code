#!/usr/bin/env python3

import os
import hashlib
from collections import defaultdict

class DuplicatedFileFinder:

	def __init__(self, follow_links = False):
		self.follow_links = follow_links
	
	def _compute_md5_checksum(self, file_path):
		md5 = hashlib.md5()
		size = 128 * md5.block_size
		
		with open(file_path, 'rb') as file: 
			for chunk in iter(lambda: file.read(size), b''): 
				 md5.update(chunk)
		
		return md5.hexdigest()

	def _walk(self, directory_path):
		walk = os.walk(directory_path, followlinks = self.follow_links)
		
		for name, directories, files in walk:
			for file in files:
				yield os.path.join(name, file)
	
	def _compute_group_id(self, filename):
		return self._compute_md5_checksum(filename)
	
	def find(self, directories):
		groups = defaultdict(list)
		
		for directory in directories:
			for filename in self._walk(directory):
				group_id = self._compute_group_id(filename)
				groups[group_id].append(filename)

		return [group for group in groups.values() if len(group) > 1]			

def __main__():
	arguments = _parse_arguments()
	
	finder = DuplicatedFileFinder(follow_links = arguments.follow_links)
	duplicates = finder.find(arguments.directories)
	
	if arguments.hidden_file == "first":
		for filenames in duplicates:
			filename = filenames.pop(0)
	elif arguments.hidden_file == "last":
		for filenames in duplicates:
			filename = filenames.pop()
	
	for i, duplicate in enumerate(duplicates):
		if i > 0 and arguments.separate_groups:
			stdout.write(arguments.file_terminator)
		for filename in duplicate:
			if arguments.file_terminator == "\n":
				filename = filename.replace("\\", "\\\\").replace("\n", "\\n")
			stdout.write(filename + arguments.file_terminator)

def _parse_arguments():
	parser = ArgumentParser(
		add_help = False,
		description = "Find duplicate files based on MD5 checksum. "
			"It can be used in conjunction with xargs:" \
			"\n\tpython script.py -0 DIR | xargs --null -I {} mv {} $DIR" \
			"\n\tpython script.py -0 DIR | xargs -0 rm")
	parser.add_argument(
		metavar = 'DIRECTORY',
		dest = 'directories',		
		type = str,
		nargs = '*',
		help = "List files from DIRECTORY.")
	parser.add_argument('-0', '--null',
		dest = 'file_terminator',
		required = False,
		default = '\n',
		action = 'store_const',
		const = '\0',
		help = "Use null character as file terminator "
			"instead of newline character.")
	parser.add_argument('-s', '--separate',
		dest = 'separate_groups',
		required = False,
		default = False,
		action = 'store_true',
		help = "Separate groups with file terminator character.")
	parser.add_argument('-H', '--hide-file',
		metavar = 'HIDDEN-FILE',
		dest = 'hidden_file',
		type = str,
		choices = ('none', 'first', 'last'),
		required = False,
		default = 'first',
		help = "Don't show HIDDEN-FILE file of each group.")
	parser.add_argument('-f', '--follow-links',
		dest = 'follow_links',
		action = 'store_true',
		required = False,
		default = False,
		help = "Follow links.")
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")
	
	return parser.parse_args()

if __name__ == '__main__':
	from argparse import ArgumentParser
	from sys import stdout
	__main__()
