#!/bin/sh

# TODO
#
#  - Test thoroughly.
#  - Add minutes.

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-h] [-M maximim-hour] [-d delay] [-m minimum-hour]

Execute commands on X screensaver activation. Required programs: cat, date, \
kill, printf, sleep, test, xscreensaver.

Options

 -M max-hour  Set maximum hour (ignore screensaver activation since this hour).
              Default: 6.
 -c command   Execute command on screensaver activation.
              Default: xfce4-session-logout.
 -d delay     Set delay between screensaver activation and command execution.
 -h           Show this message.
 -m min-hour  Set minimum hour (ignore screensaver activation before this
              hour). Default: 0.
 -u unit      Set delay unit ('s' for seconds, 'm' for minutes, 'h' for hours
              and 'd' for days). Default: 's'.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Too many arguments.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "There are too many arguments.\n" >&2 ;;
		4) printf "The minimum hour is not an integer between 0 and 23.\n" >&2 ;;
		5) printf "The maximum hour is not an integer between 0 and 23.\n" >&2 ;;
	esac
	exit $1
}

command=xfce4-session-logout
min_hour=2
max_hour=6
delay=30
delay_unit=m

while getopts ':M:c:d:hm:u:' option
do
	case $option in
		M) max_hour=$OPTARG ;;
		c) command=$OPTARG ;;
		d) delay=$OPTARG ;;
		h) print_help; exit 0 ;;
		m) min_hour=$OPTARG ;;
		u) delay_unit=$OPTARG ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -gt 0
then error 3
elif ! test "$min_hour" -ge 0 -a "$min_hour" -lt 24 2> /dev/null
then error 4
elif ! test "$max_hour" -ge 0 -a "$max_hour" -lt 24 2> /dev/null
then error 5
fi

# Disable globbing.
set -f

enabled=false
pid=

xscreensaver-command -watch |
while read -r line
do
	set -- $line
	action=$1
	shift
	case $action in
		BLANK)
			if ! $enabled
			then
				hour=$(date -d "$*" '+%k')
				if test '(' $min_hour -le $max_hour \
				     -a '(' $hour -ge $min_hour -a $hour -lt $max_hour ')' ')' \
				     -o '(' $min_hour -gt $max_hour \
				     -a '(' $hour -ge $min_hour -o $hour -lt $max_hour ')' ')'
				then
					printf 'Initiating process... '
					{
						sleep $delay$delay_unit
						printf 'Preparing to ignore termination signal... '
						trap '' 15
						printf 'done.\n'
						$command
					} &
					pid=$!
					enabled=true
					printf 'done (%d).\n' $pid
				fi
			fi ;;
		UNBLANK)
			if $enabled
			then
				printf 'Terminating process (%d)... ' $pid
				kill $pid
				wait $pid
				enabled=false
				printf 'done.\n'
			fi ;;
	esac
done
