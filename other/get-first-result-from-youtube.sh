#!/bin/sh

SITE_URL="https://www.youtube.com"

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-h] [search-query]...

Get the first result from YouTube for each search query. If there is no \
argument search queries are read from stdin. Required programs: cat, grep, \
od, sed, test, tr, wget.

Options

 -h  Show this message.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
	esac
	exit $1
}

escape_sed_replacement() {
	printf '%s' "$1" | sed -e 's/[\/&]/\\&/g'
}

get_results() {
	local query=$(url_encode "$1")
	wget --quiet --output-document - "$SITE_URL/results?search_query=$query" |
	grep --only '/watch?v=[0-9A-Za-z_-]*' |
	sed "s/^.*\$/$ESCAPED_SITE_URL\0/g"
}

get_first_result() {
	get_results "$1" |
	head --lines 1
}

url_encode() {
	printf '%s' "$1" |
	od --output-duplicates --address-radix='n' --format=x1 |
	tr -d '\n' |
	tr ' [:lower:]' '%[:upper:]' 
}

ESCAPED_SITE_URL=$(escape_sed_replacement "$SITE_URL")

while getopts ":h" option
do
	case $option in
		h) print_help; exit 0 ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -eq 0
then read_stdin=true
else read_stdin=false
fi

if $read_stdin
then
	while IFS= read -r line
	do get_first_result "$line"
	done
else
	for argument
	do get_first_result "$argument"
	done
fi

exit 0
