# https://medium.com/@petehouston/streaming-webcam-to-http-using-vlc-dda7259176c9

# TODO
#
#  - Handle VLC errors.
#  - Escape host and port in text message.

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-h]

Stream webcam feed using HTTP. Required programs: cat, kill, printf, test, \
vlc, which. Recommended programs: zenity.

Options

 -h       Show this message.
 -H host  Set HTTP host. Default: 0.0.0.0.
 -p port  Set HTTP port. Default: 8080.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Too many arguments.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "There are too many arguments.\n" >&2 ;;
	esac
	exit $1
}

host="0.0.0.0"
port="8080"

while getopts ":H:hp:" option
do
	case $option in
		H) host=$OPTARG ;;
		h) print_help; exit 0 ;;
		p) port=$OPTARG ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -gt 0
then error 3
fi

input="v4l2:///dev/video0:chroma=mjpg"
boundary="7b3cc56e5f51db803f790dad720ed50a"
http_parameters="mime=multipart/x-mixed-replace;boundary=$boundary"
access="http{$http_parameters},mux=mpjpeg" # dst=$host:$port"
output="#transcode{ec=mjpg}:std{access=$access}"
LABEL='Atura la transmissió'
TEXT="S'està transmetent el senyal de vídeo de la càmera.
Hi podeu accedir mitjançant l'URL \
<a href=\"http://$host:$port\">http://$host:$port</a>."

if which zenity
then
	vlc --intf dummy "$input" --sout "$output" \
      --http-host "$host" --http-port "$port" &
	zenity --info --icon-name camera-web --no-wrap \
	       --ok-label "$LABEL" --text "$TEXT"
	kill -9 $!
else
	vlc --intf dummy "$input" --sout "$output" \
	    --http-host "$host" --http-port "$port"
fi

exit 0
