#!/bin/sh

API_URL="https://translate.google.com/translate_tts"

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-h] [-l language] [-u user-agent] [text]...

Speak (generate an MP3 audio file) very short fragments of text (~ 201 bytes) \
by using Google Text-to-Speech service. If there is no argument text is read \
from stdin. Required programs: cat, od, printf, test, tr, wget.

Options

 -h             Show this message.
 -l language    Set language. Default: 'en'.
 -u user-agent  Set HTTP user agent. Default: empty string.

Supported languages

 - https://cloud.google.com/translate/docs/languages

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Unable to download file.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "Unable to download file.\n" >&2 ;;
	esac
	exit $1
}

url_encode() {
	printf '%s' "$1" |
	od --output-duplicates --address-radix='n' --format=x1 |
	tr -d '\n' |
	tr ' [:lower:]' '%[:upper:]' 
}

language='en'
user_agent=

while getopts ":hl:u:" option
do
	case $option in
		h) print_help; exit 0 ;;
		l) language=$OPTARG ;;
		u) user_agent=$OPTARG ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -eq 0
then read_stdin=true
else read_stdin=false
fi

if $read_stdin
then input=$(cat)
else input=$*
fi

language=$(url_encode "$language")
query=$(url_encode "$input")

if ! wget --output-document - --user-agent "$user_agent" \
 "$API_URL?ie=UTF-8&tl=$language&client=tw-ob&q=$query" #2> /dev/null
then error 3
fi

exit 0
