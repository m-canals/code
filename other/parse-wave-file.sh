#!/bin/sh

# TODO
#
# Complete.
# Read from stdin.

# https://sharkysoft.com/archive/wave/docs/javadocs/lava/riff/wave/doc-files/riffwave-frameset.htm
#
# A chunk id is a sequence of one to four ASCII alphanumeric characters, padded
# on the right with blank characters and with no embedded blanks. An unsigned
# four-byte integer is used to store the size of a chunk.

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-h] file

Parse WAVE files. Required programs: cat, dd, od, printf, stat, test.

Options

 -h  Show this message.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Not enough arguments.
 4) Too many arguments.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "There are not enough arguments.\n" >&2 ;;
		4) printf "There are too many arguments.\n" >&2 ;;
	esac
	exit $1
}

# filepath offset size
get_data() {
	dd if="$1" ibs=1 skip=$2 count=$3 2>/dev/null
}

# filepath offset size
get_size() {
	od -t u4 --address-radix='n' --skip-bytes=$2 --read-bytes=$3 "$1"
}

# filepath
get_file_size() {
	stat --format='%s' "$1"
}

# filepath offset size
parse_wave() {
	local offset stop id size  
	offset=$2
	stop=$(($2+$3))
	# Skip id
	offset=$((offset+4))
	while [ $offset -lt $stop ]
	do
		id=$(get_data "$1" $offset 4) 
		offset=$((offset+4))

		size=$(get_size "$1" $offset 4)
		offset=$((offset+4))

		printf "% $4s<chunk id=\"%s\" size=\"%d\">\n" '' "$id" $size

		case $id in
			'fmt ') ;;
			'fact') printf '%d' $(get_size "$1" $offset 4) ;;
			'data') ;;
			'id3 ') parse_id3 "$1" $offset $size $(($4 + 1)) ;;
		esac

		printf "% $4s</chunk>\n" ''

		offset=$((offset+size))
	done
}

# filepath offset size depth
parse_riff() {
	local id
	id=$(get_data "$1" $2 4)
	printf "% $4s<chunk id=\"%s\">\n" '' "$id"
	case $id in
		WAVE) parse_wave "$1" $2 $3 $(($4 + 1)) ;;
		*) return ;;
	esac
	printf "% $4s</chunk>\n" ''
}

# filepath offset size
parse_id3() {
	local offset stop id size  

	offset=$2
	stop=$(($2+$3))

	get_data "$1" $2 $3 | xxd | head
}

while getopts ":h" option
do
	case $option in
		h) print_help; exit 0 ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -lt 1
then error 3
elif test $# -gt 1
then error 4
fi

file=$1
offset=0
length=$(get_file_size "$file")

printf '<file size="%d">\n' $length

while test $offset -lt $length
do
	id=$(get_data "$file" $offset 4)
	offset=$((offset+4))

	size=$(get_size "$file" $offset 4)
	offset=$((offset+4))

	printf ' <chunk id="%s" size="%d">\n' "$id" $size

	case $id in
		'RIFF') parse_riff "$file" $offset $size 2 ;;
		'id3 ') parse_id3 "$file" $offset $size 2 ;;
		*) ;;
	esac

	printf ' </chunk>\n'

	offset=$((offset+size))
done

printf '</file>\n'
