#!/bin/sh

# TODO
#
#  - Try to use xprop insted of wmctrl.
#    https://specifications.freedesktop.org/wm-spec/1.3/index.html

# windows=$(wmctrl -l | awk -v desktop=$_NET_CURRENT_DESKTOP '$2 == desktop { print $1; }')
# https://github.com/geekless/wmctrl/blob/master/main.c

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-h] border-name

Snap active X window to screen borders. Border names are: top, top-right, \
right, bottom-right, bottom, bottom-left, left and top-left. Required 
programs: cat, printf, test, wmctrl, xprop, xwininfo.

Options

 -h            Show this message.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Not enough arguments.
 4) Too many arguments.
 5) Undefined border name.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "There are not enough arguments.\n" >&2 ;;
		4) printf "There are too many arguments.\n" >&2 ;;
		5) printf "Border name '%s' is not defined\n" "$2" >&2 ;;
	esac
	exit $1
}

while getopts ":h" option
do
	case $option in
		h) print_help; exit 0 ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -lt 1
then error 3
elif test $# -gt 1
then error 4
fi

border_name=$1

# Active window.
read _ active << EOF
$(xprop -root -notype -f '_NET_ACTIVE_WINDOW' '0x' ' $0' '_NET_ACTIVE_WINDOW')
EOF

# Is it not snapped (property is not found)?
if xprop -id "$active" -f 'WINDOW_SNAP' '32cccc' 'WINDOW_SNAP' 'WINDOW_SNAP' | grep 'not found' >/dev/null 
then

# Active window position and size (x, y, width, height).
read x y w h << EOF
$(xwininfo -id $active | grep -e 'Absolute upper-left X' -e 'Absolute upper-left Y' -e 'Width' -e 'Height' | cut -d ':' -f 2 | tr -d ' ' | tr '\n' ' ')
EOF

# Current desktop.
read _ desktop << EOF
$(xprop -root -notype -f '_NET_CURRENT_DESKTOP' '0c' ' $0' '_NET_CURRENT_DESKTOP')
EOF

# Workarea (x, y, width, height).
read _ workarea_x workarea_y workarea_w workarea_h _ <<- EOF
$(xprop -root -notype -f '_NET_WORKAREA' '0c' " \$$((desktop*4))+" '_NET_WORKAREA' | tr --delete ',')
EOF

# Active window border (right, left, top, bottom).
read _ border_r border_l border_t border_b << EOF
$(xprop -id $active -notype -f '_NET_FRAME_EXTENTS' '0c' ' $0 $1 $2 $3' '_NET_FRAME_EXTENTS')
EOF

x=$((x-border_l))
y=$((y-border_t))

# Set custom property and save size and position.
xprop -id "$active" -f 'WINDOW_SNAP' '32cccc' -set 'WINDOW_SNAP' "$x,$y,$w,$h"

# New size and position.
case $border_name in
	top-left)
		x=$workarea_x
		y=$workarea_y
		w=$((workarea_w / 2 - border_r - border_l))
		h=$((workarea_h / 2 - border_t - border_b))
		;;
	top-right)
		x=$((workarea_w / 2))
		y=$workarea_y
		w=$((workarea_w / 2 - border_r - border_l))
		h=$((workarea_h / 2 - border_t - border_b))
		;;
	bottom-left)
		x=$workarea_x
		y=$((workarea_h / 2 + workarea_y))
		w=$((workarea_w / 2 - border_r - border_l))
		h=$((workarea_h / 2 - border_t - border_b))
		;;
	bottom-right)
		x=$((workarea_w / 2))
		y=$((workarea_h / 2 + workarea_y))
		w=$((workarea_w / 2 - border_r - border_l))
		h=$((workarea_h / 2 - border_t - border_b))
		;;
	top)
		x=$workarea_x
		y=$workarea_y
		w=$((workarea_w - border_r - border_l))
		h=$((workarea_h / 2 - border_t - border_b))
		;;
	bottom)
		x=$workarea_x
		y=$((workarea_h / 2 + workarea_y))
		w=$((workarea_w - border_r - border_l))
		h=$((workarea_h / 2 - border_t - border_b))
		;;
	left)
		x=$workarea_x
		y=$workarea_y
		w=$((workarea_w / 2 - border_r - border_l))
		h=$((workarea_h - border_t - border_b))
		;;
	right)
		x=$((workarea_w / 2))
		y=$workarea_y
		w=$((workarea_w / 2 - border_r - border_l))
		h=$((workarea_h - border_t - border_b))
		;;
	*)
		error 5
		;;
esac

# Change size and position (snap it).
wmctrl -r :ACTIVE: -e 0,$x,$y,$w,$h

else

# Restore size and position and remove property (unsnap it).
properties=$(xprop -id "$active" 'WINDOW_SNAP' | cut -d '=' -f 2)
wmctrl -ir "$active" -e "0, $properties"
xprop -id "$active" -remove 'WINDOW_SNAP'

fi

exit 0
