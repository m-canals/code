#!/usr/bin/env python3

from itertools import product

class LexicalError(Exception):
	pass

class SyntacticError(Exception):
	pass

# A boolean expression is one of the following expressions, where F
# and G are boolean expressions, a is an alphabetical character and s
# is a whitespace character. Negation takes precedence over
# conjunction and conjunction takes precedence over disjunction.
# 
#  - Spacing        sF    - Parenthesization   (F)
#  - Negation       -F    - True constant        1
#  - Conjunction    FG    - False constant       0
#  - Disjunction   F+G    - Variable             a
#
# If the expression contains any other type of character LexicalError
# exception is raised. Syntactic validation is done on evaluation.
class BooleanExpression():
	# Es podria millorar aquesta implementació avaluant l'arbre
	# corresponent a l'expressió en lloc d'aplicar regles de
	# substitució.
	
	def __set_symbols(self, left_parenthesis, right_parenthesis,
			negation, conjunction, disjunction, false, true):
		self.__replacements = (
			(left_parenthesis + false       + right_parenthesis , false),
			(left_parenthesis + true        + right_parenthesis , true),
			(negation         + false                           , true),
			(negation         + true                            , false),
			(false            + conjunction + false             , false),
			(false            + conjunction + true              , false),
			(true             + conjunction + false             , false),
			(true             + conjunction + true              , true),
			(false            + disjunction + false             , false),
			(false            + disjunction + true              , true),
			(true             + disjunction + false             , true),
			(true             + disjunction + true              , true))
		self.__constants = (
			false, true)
		self.__symbols   = (
			left_parenthesis, right_parenthesis, negation, conjunction,
			disjunction, false, true)
	
	def __init__(self, expression, left_parenthesis = '(',
			right_parenthesis = ')', negation = '-', conjunction = '',
			disjunction = '+', false = '0', true = '1'):
		self.__set_symbols(left_parenthesis, right_parenthesis, negation,
			conjunction, disjunction, false, true)
		self.variables = set()
		self.expression = str()
		for character in expression:
			if character.isalpha():
				self.variables.add(character)
				self.expression += character
			elif character in self.__symbols:
				self.expression += character
			elif not character.isspace():
				raise LexicalError
		self.variables = list(self.variables)
		self.variables.sort()
	
	def __replace_variables(self, interpretation):
		expression = self.expression
		for variable, value in zip(self.variables, interpretation):
			expression = expression.replace(variable, value)
		return expression
	
	def __replace_subexpression(self, expression):
		for old, new in self.__replacements:
			new_expression = expression.replace(old, new)
			if new_expression != expression:
				return new_expression
		return expression
	
	def __replace_subexpressions(self, expression):
		new_expression = self.__replace_subexpression(expression)
		while new_expression != expression:
			expression = new_expression
			new_expression = self.__replace_subexpression(expression)
		return expression
	
	# An interpretation is a list of 0 and 1, one for each variable 
	# (alphabetical order). If the boolean expression is not
	# well-constructed SyntacticError exception is raised.
	def evaluate(self, interpretation):
		expression = self.__replace_variables(interpretation)
		expression = self.__replace_subexpressions(expression)
		if expression not in self.__constants:
			raise SyntacticError
		return expression

def _parse_arguments():
	parser = ArgumentParser(add_help = False, description =
		"Get the truth table of a boolean expression. "
		"Negation is denoted by -, disjunction is denoted by + and "
		"conjunction is denoted by concatenation. "
		"Double-dash (--) option prevents an expression that starts with "
		"a dash (-) from being treated as an option.")
	parser.add_argument('expression', metavar = 'EXPRESSION', nargs = 1,
		help = 'Expression.')
	parser.add_argument('-h', '--help', action = 'help',
		help = "Show this help message and exit.")
	return parser.parse_args()

def _main():
	try:
		arguments = _parse_arguments()
		expression = BooleanExpression(arguments.expression[0])
		interpretations = product('01', repeat = len(expression.variables))
		for i, interpretation in enumerate(interpretations):
			evaluation = expression.evaluate(interpretation)
			# Endarrerim l'escriptura de les variables per evitar
			# de mostrar-les si hi ha un error sintàctic.
			if i == 0:
				print(* expression.variables)
			print(* interpretation, evaluation)
	except LexicalError as error:
		print("Lexical error.", file = stderr)
		exit(1)
	except SyntacticError as error:
		print("Syntactic error.", file = stderr)
		exit(1)

if __name__ == '__main__':
	from argparse import ArgumentParser
	from sys import stderr
	_main()

