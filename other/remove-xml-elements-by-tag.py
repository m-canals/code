#!/usr/bin/env python3

from lxml import etree

# def replace(element, tag, attributes, text):
# 	element.clear()
# 	element.tag = tag
# 	for name, value in attributes.items():
# 		element.set(name, value)
# 	element.text = text

def remove_elements_by_tag(xml, tags):
	tree = etree.XML(xml)
	for tag in tags:
		for element in tree.xpath(".//%s" % tag):
			element.getparent().remove(element)
	return etree.tostring(tree, encoding = str)

def _parse_arguments():
	def xml_tag(argument):
		for i, character in enumerate(argument):
			if (i == 0 and not character.isalpha() and not character in "_" or
				  i != 0 and not character.isalnum() and not character in "_-."):
				raise ValueError
		return argument
	parser = ArgumentParser(add_help = False, description =
		"Remove XML elements by tag. "
		"Read from stdin and write to stdout.")
	parser.add_argument('tags', metavar = 'TAG',
		type = xml_tag, nargs = '*',
		help = "Tags to be removed (case-sensitive).")
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")
	return parser.parse_args()

def _main():
	arguments = _parse_arguments()
	document = stdin.read()
	try:
		document = remove_elements_by_tag(document, arguments.tags)
	except etree.XMLSyntaxError as error:
		print(error, file = stderr)
		exit(1)
	stdout.write(document)

if __name__ == '__main__':
	from argparse import ArgumentParser
	from sys import stdin, stdout, stderr
	_main()

