#!/usr/bin/env python2

import gtk
import webkit

class WebBrowser:
	def __init__(self, url, script, width, height):
		self.url = url
		self.script = script
		self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		self.window.set_position(gtk.WIN_POS_CENTER_ALWAYS)
		self.window.set_default_size(width, height)
		self.window.connect('delete_event', self.close)
		vbox = gtk.VBox()
		self.url_entry = gtk.Entry()
		self.url_entry.connect('activate', self._url_entry_activate)
		self.scrolled_window = gtk.ScrolledWindow()
		self.webview = webkit.WebView()
		self.scrolled_window.add(self.webview)
		vbox.pack_start(self.url_entry, fill=False, expand=False)
		vbox.pack_start(self.scrolled_window, fill=True, expand=True)
		self.window.add(vbox)
		
	def _url_entry_activate(self, entry):
		self.url = entry.get_text()
		self._load()
		
	def _load(self):
		self.webview.execute_script(self.script)
		self.webview.open(self.url)
		
	def open(self):
		self.url_entry.set_text(self.url)
		self.window.set_title(self.url)
		self.window.show_all()
		self._load()

	def close(self, widget, event, data=None):
		gtk.main_quit()

def _parse_arguments():
	parser = ArgumentParser(add_help = False, description =
		"Browse the web.")
	parser.add_argument('url', metavar = "URL",
		nargs = '?', type = str, default = "localhost",
		help = "Open URL. Default: localhost.")
	parser.add_argument('-s', '--script', metavar = "SCRIPT",
		type = str, default = "",
		help = "Load SCRIPT. Default: none.")
	parser.add_argument('-w', '--width', metavar = "WIDTH",
		type = int, default = 500,
		help = "Set window width.")
	parser.add_argument('-H', '--height', metavar = "HEIGHT",
		type = int, default = 500,
		help = "Set window height.")
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")
	return parser.parse_args()

def _main():
	arguments = _parse_arguments()
	gtk.gdk.threads_init()
	browser = WebBrowser(arguments.url[0], arguments.script,
		arguments.width, arguments.height)
	browser.open()
	gtk.main()

if __name__ == '__main__':
	from argparse import ArgumentParser
	_main()

