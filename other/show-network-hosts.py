#!/usr/bin/env python3

from subprocess import Popen, PIPE
from re import search

class ArpError(Exception):
	pass

class NmapError(Exception):
	pass

def get_mac_address(address, interface):
	try:
		process = Popen(['arp', '-ani', interface, address], stdout = PIPE)
	except FileNotFoundError:
		raise ArpError()
	
	stdout, stderr = process.communicate()
	
	if process.returncode != 0:
		raise ArpError()
	
	stdout = stdout.decode()
	match = search('(([0-9a-f]){2}:){5}([0-9a-f]){2}', stdout)
	
	if match:
		return stdout[match.start():match.end()]
	else:
		return None

def get_network_hosts(network):
	try:
		process = Popen(['nmap', '-sn', '-oG', '-', network], stdout = PIPE)
	except FileNotFoundError:
		raise NmapError()
	
	stdout, stderr = process.communicate()
	
	if process.returncode != 0:
		raise NmapError()
	
	stdout = stdout.decode()
	hosts = list()
	
	for line in stdout.splitlines():
		if not line.startswith('#'):
			host = dict()
			for field in line.split('\t'):
				name, value = field.split(': ', 1)
				if name == 'Host':
					address, name = value.split(' ')
					host['Address'] = address
					host['Name'] = name.strip('()')
					mac = get_mac_address(address, '')
					host['MAC'] = mac if mac else ''
				else:
					host[name] = value
			hosts.append(host)
	
	return hosts

def _parse_arguments():
	parser = ArgumentParser(add_help = False, description =
		"Show network hosts.")
	parser.add_argument('network', metavar = 'NETWORK-ADDRESS',
		nargs = '?', type = str, default = '192.168.1.0/24',
		help = "Network address. Default: 192.168.1.0/24.")
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")
	return parser.parse_args()

def _main():
	arguments = _parse_arguments()
	try:
		hosts = get_network_hosts(arguments.network)
	except ArpError:
		print("Arp command error.", file = stderr)
		exit(1)
	except NmapError:
		print("Nmap command error.", file = stderr)
		exit(1)
	for i, host in enumerate(hosts):
		if i > 0:
			print()
		for key, value in host.items():
			print("%s: %s" %(key, value))

if __name__ == "__main__":
	from argparse import ArgumentParser
	from sys import stderr
	_main()
	
