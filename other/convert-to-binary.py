#!/usr/bin/python3

def decode_integer(s):
	n = 0
	x = 1
	for c in s[::-1]:
		n = n + int(c) * x
		x = x * 2
	return n

def decode_float(s):
	n = 0.0
	x = 2
	for c in s:
		n = n + float(c) / x
		x = x * 2
	return n

def encode_integer(n):
	s = str()
	while n > 1:
		n, r = divmod(n, 2)
		s = str(r) + s
	s = str(n) + s
	return s

def encode_float(n):
	if n == 0.0:
		return '0'
	s = str()
	while n > 0:
		n = n * 2.0
		s = s + str(int(n))
		if n >= 1:
			n = n - 1.0
	return s

def is_unsigned_binary_integer(s):
	for c in s:
		if c != '0' and c != '1':
			return False
	return len(s) > 0

def decode(s):
	s = s.split('.')
	if len(s) > 2:
		raise ValueError
	for x in s:
		if not is_unsigned_binary_integer(x):
			raise ValueError
	f = s[1]
	i = s[0]
	if len(s) == 1:
		return decode_integer(i)
	else:
		return decode_integer(i) + decode_float(f)

def is_unsigned_integer(s):
	for c in s:
		if not c.isdigit():
			return False
	return len(s) > 0

def encode(n):
	s = n.split('.')
	if len(s) > 2:
		raise ValueError
	for x in s:
		if not is_unsigned_integer(x):
			raise ValueError
	f = float(n)
	i = int(f)
	if len(s) == 1:
		return encode_integer(i)
	else:
		return encode_integer(i) + '.' + encode_float(f - i)

def _parse_arguments():
	parser = ArgumentParser(
		add_help = False,
		description = "Convert non-negative real numbers "
		              "from base-10 to base-2 and viceversa.")
	parser.add_argument('numbers',
		metavar = "NUMBER",
		nargs = '*',
		type = str,
		help = "Converted number (/[0-9]+(.[0-9]+)?/).")
	parser.add_argument('-d', '--decode',
		action = 'store_true',
		dest = 'decode',
		default = False,
		help = "Convert from base-2 to base-10.")
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")
	return parser, parser.parse_args()

def _main():
	parser, arguments = _parse_arguments()
	function = decode if arguments.decode else encode
	numbers = list()
	for number in arguments.numbers:
		try:
			numbers.append(function(number))
		except ValueError:
			parser.print_usage()
			print("%s: error: argument NUMBER: invalid %s value: '%s'"
				%(argv[0],
				"non-negative base-2" if arguments.decode else "non-negative base-10",
				 number))
			exit(1)
	print(*numbers, sep = '\n')

if __name__ == "__main__":
	from argparse import ArgumentParser
	from sys import argv
	_main()

