#!/bin/sh

API_URL="http://dinamics.ccma.cat/pvideo/media.jsp"

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-ah] video-url

Get download links of CCMA videos. A video may be referenced by link or \
numeric id. Required programs: cat, jq, printf, test, wget.

Options

 -a  Get all links instead of the best link.
 -h  Show this message.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Not enough arguments.
 4) Too many arguments.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "There are not enough arguments.\n" >&2 ;;
		4) printf "There are too many arguments.\n" >&2 ;;
	esac
	exit $1
}

get_video_id() {
	local id
	id=${1%/*}
	id=${id##*/}
	printf '%d' "$id"
}

all_links=false

while getopts ":ah" option
do
	case $option in
		a) all_links=true ;;
		h) print_help; exit 0 ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -lt 1
then error 3
elif test $# -gt 1
then error 4
fi

id=$(get_video_id "$1")

# {"media": {"url": [{"file": ..., "label": [0-9]+p}, ...], ...}, ...}
if $all_links
then jq_filter='"LINKS", (.media.url[].file)'
else jq_filter='"LINK", (.media.url | max_by(.label | rtrimstr("p") | tonumber).file)'
fi

jq_filter="\"SLUG\", .informacio.slug, \"PROGRAMA\", .informacio.programa, $jq_filter"

wget --quiet --output-document - "$API_URL?idint=$id&media=video" | # &profile=mobile
jq --raw-output "$jq_filter"

exit 0
