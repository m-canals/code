#!/usr/bin/env python3

from subprocess import call
from math import sqrt
from Xlib.display import Display

def _distance(x, y):
	return sqrt(x * x + y * y)

class Beeper():
	DEFAULT_LENGTH = 50
	MIN_LENGTH = 0
	MAX_LENGTH = 65535
	MIN_FREQ = 1
	MAX_FREQ = 19999
	
	def beep(self, frequency, length):
		frequency = min(max(Beeper.MIN_FREQ, int(frequency)), Beeper.MAX_FREQ)
		length = min(max(Beeper.MIN_LENGTH, int(length)), Beeper.MAX_LENGTH)
		call(['beep', '-f', str(frequency), '-l', str(length)])
		return frequency

class DistanceBeeper(Beeper):
	def __init__(self, x, y, radius, length = Beeper.DEFAULT_LENGTH,
			min_freq = Beeper.MIN_FREQ, max_freq = Beeper.MAX_FREQ):
		self.__x = x
		self.__y = y
		self.__max_distance = radius
		self.__min_freq = min_freq
		self.__max_freq = max_freq
		self.__freq_range = self.__max_freq - self.__min_freq
		self.__length = length
	
	def beep(self, x, y):
		distance = _distance(self.__x - x, self.__y - y)
		closeness = 1.0 - distance / self.__max_distance
		freq = closeness * self.__freq_range + self.__min_freq
		return x, y, super().beep(freq, self.__length)

class PointerDistanceBeeper(DistanceBeeper):
	def __init__(self, x = 0.5, y = 0.5, length = Beeper.DEFAULT_LENGTH,
			min_freq = Beeper.MIN_FREQ, max_freq = Beeper.MAX_FREQ):
		display = Display()
		self.__screen = display.screen()
		geometry = self.__screen.root.get_geometry()
		w = geometry.width
		h = geometry.height
		x = w * min(max(0, x), 1)
		y = h * min(max(0, y), 1)
		radius = max(
			_distance(0 - x, 0 - y),
			_distance(0 - x, h - 1 - y),
			_distance(w - 1 - x, 0 - y),
			_distance(w - 1 - x, h - 1 - y))
		super().__init__(x, y, radius, length, min_freq, max_freq)
	
	def get_pointer_position(self):
		pointer = self.__screen.root.query_pointer()
		return pointer.root_x, pointer.root_y
	
	def beep(self):
		x, y = self.get_pointer_position()
		return super().beep(x, y)

def _parse_arguments():
	parser = ArgumentParser(add_help = False, description =
		"Beep.")
	parser.add_argument('-x',
		type = float, default = 0.5,
		help = "Set center x coordinate (distance mode). Default: 0.5.")
	parser.add_argument('-y',
		type = float, default = 0.5,
		help = "Set center y coordinate (distance mode). Default: 0.5.")
	parser.add_argument('-f', '--min-freq',
		type = int, default = Beeper.MIN_FREQ,
		help = "Set minimum frequency. Default: %d." % Beeper.MIN_FREQ)
	parser.add_argument('-F', '--max-freq',
		type = int, default = Beeper.MAX_FREQ,
		help = "Set maximum frequency. Default: %d." % Beeper.MAX_FREQ)
	parser.add_argument('-l', '--length',
		type = int, default = Beeper.DEFAULT_LENGTH,
		help = "Set length. Default: %d." % Beeper.DEFAULT_LENGTH)
	parser.add_argument('-m', '--mode',
		choices = ['distance'], default = 'distance',
		help = "Set mode. Default: circular.")
	parser.add_argument('-v', '--verbose',
		action = 'store_true', default = False,
		help = "Show input data and frequency.")
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")
	return parser.parse_args()

def _main():
	arguments = _parse_arguments()
	if arguments.mode == 'distance':
		beeper = PointerDistanceBeeper(arguments.x, arguments.y,
			arguments.length, arguments.min_freq, arguments.max_freq)
	try:
		while True:
			data = beeper.beep()
			if arguments.verbose:
				print(data)
	except KeyboardInterrupt:
		pass

if __name__ == "__main__":
	from argparse import ArgumentParser
	_main()

