#!/usr/bin/env python2

from gtk.gdk import COLORSPACE_RGB, Pixbuf, get_default_root_window

class Screenshoter():
	def __init__(self):
		self.__root = get_default_root_window()
		self.__root_width, self.__root_height = self.__root.get_size()
	
	def get_root_width(self):
		return self.__root_width
	
	def get_root_height(self):
		return self.__root_height
	
	def take_screenshot(self, x, y, width, height):
		x = min(max(0, x), self.__root_width - 1)
		y = min(max(0, y), self.__root_height - 1)
		width = min(max(1, width), self.__root_width - x)
		height = min(max(1, height), self.__root_height - y)
		pixbuf = Pixbuf(COLORSPACE_RGB, False, 8, width, height)
		return pixbuf.get_from_drawable(self.__root, self.__root.get_colormap(),
			x, y, False, False, width, height)

def _parse_arguments(default_width, default_height):
	parser = ArgumentParser(add_help = False, description =
		"Take a screenshot.")
	parser.add_argument('-o', '--output',
		type = str, default = '/dev/stdout',
		help = "Set output file path.")
	parser.add_argument('-f', '--format',
		type = str, default = 'png',
		help = "Set output file format.")
	parser.add_argument('-x',
		type = int, default = 0,
		help = "Set x coordinate.")
	parser.add_argument('-y',
		type = int, default = 0,
		help = "Set y coordinate.")
	parser.add_argument('-w', '--width',
		type = int, default = default_width,
		help = "Set width.")
	parser.add_argument('-H', '--height',
		type = int, default = default_height,
		help = "Set height.")
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")
	return parser.parse_args()

def _main():
	screenshoter = Screenshoter()
	arguments = _parse_arguments(
		screenshoter.get_root_width(),
		screenshoter.get_root_height())
	try:
		image = screenshoter.take_screenshot(
			arguments.x, arguments.y,
			arguments.width, arguments.height)
		image.save(arguments.output, arguments.format)
	except GError, error:
		print >> stderr, error

if __name__ == '__main__':
	from argparse import ArgumentParser
	from glib import GError
	from sys import stderr
	_main()

