#!/usr/bin/env python3
# https://en.wikipedia.org/wiki/Bijective_numeration#The_bijective_base-26_system

def decode(string):
	number = 0
	factor = 1
	for character in string[::-1]:
		number += (ord(character) - 65 + 1) * factor
		factor *= 26
	return number

def encode(number):
	string = ""
	while number > 0:
		number -= 1
		string += chr(65 + number % 26)
		number //= 26	
	return string[::-1]

def encoded(string):
	return all(65 <= ord(character) <= 90 for character in string)

def _parse_arguments():
	def encoded_or_int(argument):
		argument = argument.upper()
		if encoded(argument):
			return argument
		elif argument.isdigit():
			return int(argument)
		else:
			raise ValueError
	parser = ArgumentParser(add_help = False, description =
		"Convert base-10 numbers to bijective base-26 numbers "
		"(spreadsheet column numbers) and viceversa.")
	parser.add_argument('number', metavar = 'NUMBER',
		nargs = '*', type = encoded_or_int,
		help = "Number to convert. It detects whether it is encoded or not.")
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")
	return parser.parse_args()

def _main():
	arguments = _parse_arguments()
	for number in arguments.number:
		if type(number) is int:
			print(encode(number))
		elif type(number) is str and encoded(number):
			print(decode(number))

if __name__ == '__main__':
	from argparse import ArgumentParser
	_main()

