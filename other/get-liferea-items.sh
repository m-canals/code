#!/bin/sh

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-dfhn]

Enumerate Liferea items in HTML. Required programs: base64, cat, cut, mkfifo, \
mktemp, printf, rm, sed, sqlite3, test.

Options

 -d PATH   Set database directory. Default: ~/.local/share/liferea/liferea.db.
 -f PATH   Set favicons directory. Default: ~/.cache/liferea/favicons.
 -h        Show this message.
 -n TITLE  Set node title (SQL pattern). Default: '%'.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Too many arguments.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "There are too many arguments.\n" >&2 ;;
	esac
	exit $1
}

get_field() {
	printf '%s' "$1" |
	cut -d '=' -f 2- |
	cut -c 2-
}

escape_html_attribute() {
	sed 's/&/&amp;/g;s/"/&quot;/g;'
}

escape_html_text() {
	sed 's/&/&amp;/g;s/</&lt;/g;s/>/&gt;/g'
}

escape_sql_string() {
	sed "s/'/''/g"
}

get_html_image() {
	local id=$1
	if test -f $favicons/$id.png
	then
		icon=$(base64 $favicons/$id.png)
		printf "<img align=\"top\" width=\"16\" height=\"16\" src=\"data:image/png;base64,%s\"/> " "$icon"
	fi
}

database=~/.local/share/liferea/liferea.db
favicons=~/.cache/liferea/favicons
node_title=%

while getopts ":d:f:hn:" option
do
	case $option in
		d) database=$OPTARG ;;
		f) favicons=$OPTARG ;;
		h) print_help; exit 0 ;;
		n) node_title=$(printf '%s' "$OPTARG" | escape_sql_string) ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -gt 0
then error 3
fi

# Setup.
directory=$(mktemp -d)
data="$directory/data"
mkfifo "$data"

# Read input.
{
	sqlite3 -line $database "
		SELECT i.parent_node_id, i.title, i.source
		FROM node n
		JOIN items i ON i.node_id = n.node_id
		WHERE n.title LIKE '$node_title'"
	printf '\n' # Cal perquè el bucle no acabi abans d'escriure el darrer ítem.
} > "$data" &

# Write output.
i=0
exec 3<"$data"
while IFS= read -r line <&3
do
	case $i in
		0) i=1
		   id=$(get_field "$line")
		   image=$(get_html_image "$id") ;;
		1) i=2
		   title=$(get_field "$line" | escape_html_text) ;;
		2) i=3
		   source=$(get_field "$line" | escape_html_attribute) ;;
		3) i=0
		   printf "<p>%s<a href=\"%s\">%s</a></p>\n" "$image" "$source" "$title" ;;
	esac
done

# Cleanup.
exec 3<&-
rm -rf "$directory"
exit 0
