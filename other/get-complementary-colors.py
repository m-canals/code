#!/usr/bin/env python3

from colorsys import hsv_to_rgb

def get_complementary_colors(n, h0 = 0.0, s = 1.0, v = 1.0):
	return (hsv_to_rgb(h0 + i / n, s, v) for i in range(n))

def _parse_arguments():
	def int_greater_than_zero(value):
		value = int(value)
		if value < 1:
			raise ValueError
		return value
	def float_between_zero_and_one(value):
		value = float(value)
		if value < 0 or value > 1:
			raise ValueError
		return value
	parser = ArgumentParser(
		add_help = False,
		description = "Get complementary colors.")
	parser.add_argument('-n', '--number',
		metavar = 'NUM-COLORS',
		dest = 'n',
		type = int_greater_than_zero,
		default = 1,
		help = "Get NUM-COLORS colors.")
	parser.add_argument('-H', '--hue',
		metavar = 'INITIAL-HUE',
		dest = 'h',
		type = float_between_zero_and_one,
		default = 0.0,
		help = "Set first color hue.")
	parser.add_argument('-s', '--saturation',
		metavar = 'SATURATION',
		dest = 's',
		type = float_between_zero_and_one,
		default = 1.0,
		help = "Set color saturation.")
	parser.add_argument('-v', '--value',
		metavar = 'VALUE',
		dest = 'v',
		type = float_between_zero_and_one,
		default = 1.0,
		help = "Set color value.")
	parser.add_argument('-c', '--color',
		dest = 'color',
		action = 'store_true',
		default = False,
		help = "Show color next to value.")
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")
	return parser.parse_args()

def _main():
	arguments = _parse_arguments()
	for color in get_complementary_colors(
			arguments.n, arguments.h, arguments.s, arguments.v):
		r, g, b = map(lambda x: int(255 * x), color)
		if arguments.color:
			print("\033[38;2;%d;%d;%dm■" %(r, g, b), end = '')
			print("\033[0m ", end = '')
		print("#%02x%02x%02x" %(r, g, b))

if __name__ == '__main__':
	from argparse import ArgumentParser
	_main()
