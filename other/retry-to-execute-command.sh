#!/bin/sh

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-hpu] [-M maximum-delay] [-m minimum-delay] [-r retries] command

Try to execute commands until they are executed successfully or the maximum \
number of retries is reached. Required programs: cat, od and printf, sleep, \
test.

Options

 -M maximum-delay  Set maximum delay between two consecutive retries.
 -h                Show this message.
 -m minimum-delay  Set minimum delay between two consecutive retries.
 -p                Pass retry number to executed command.
 -r retries        Set maximum number of retries. Default: infinite.
 -u                Set delay unit ('s' for seconds, 'm' for minutes,
                   'h' for hours and 'd' for days). Default: 's'.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) The number of retries is not a non-negative integer.
 4) The minimum delay is not a non-negative integer.
 5) The maximum delay is not a non-negative integer.
 6) The maximum delay is less than the minimum delay.
 7) Unrecognized delay unit.
 8) Unable to execute the command successfully.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "The number of retries is not a non-negative integer.\n" >&2 ;;
		4) printf "The minimum delay is not a non-negative integer.\n" >&2 ;;
		5) printf "The maximum delay is not a non-negative integer.\n" >&2 ;;
		6) printf "The maximum delay is less than the minimum delay.\n" >&2 ;;
		7) printf "Unrecognized delay unit.\n" >&2 ;;
		8) printf "Unable to execute the command successfully.\n" >&2 ;;
	esac
	exit $1
}

get_random_number() {
	local number
	number=$(od --address-radix n --format d8 --read-bytes 8 /dev/urandom)
	number=$((number & 0x7FFFFFFFFFFFFFFF))
	number=$((number % ($2 - $1 + 1) + $1))
	printf '%d' $number
}

forever=true
max_delay=0
min_delay=0
pass_retry=false
delay_unit=s
retries=1

while getopts ":M:hm:p:r:s:" option
do
	case $option in
		M) max_delay=$OPTARG ;;
		h) print_help ;;
		m) min_delay=$OPTARG ;;
		p) pass_retry=true ;;
		r) forever=false; retries=$OPTARG ;;
		u) delay_unit=$OPTARG ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if ! test "$retries" -ge 0 2> /dev/null
then error 3
elif ! test "$min_delay" -ge 0 2> /dev/null
then error 4
elif ! test "$max_delay" -ge 0 2> /dev/null
then error 5
elif test $max_delay -lt $min_delay
then error 6
fi

case $delay_unit in
	[smhd]) : ;;
	*) error 7 ;;
esac

retry=-1

while $forever || test  $retry -lt $retries
do
	retry=$((retry+1))
	
	if $pass_retry
	then "$@" $retry
	else "$@"
	fi

	if test $? -eq 0
	then exit 0
	fi

	sleep $(get_random_number $min_delay $max_delay)$delay_unit
done

exit 8
