#!/bin/sh

# TODO
#
#  - Extendre-ho a text (.matches és un array amb les correccions).

API_URL='https://www.softcatala.org/languagetool/api/v2/check'

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "There are not enough arguments.\n" >&2 ;;
		4) printf "There are too many arguments.\n" >&2 ;;
	esac
	exit $1
}

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-h] [-u user-agent] word

Spellcheck words by using Softcatalà service. Required programs: cat, column, \
jq, od, printf, test, tr, wget.

Options

 -h                  Show this message.
 -u user-agent       Set HTTP user agent. Default: empty string.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Not enough arguments.
 4) Too many arguments.
EOF
}

url_encode() {
	printf '%s' "$1" |
	od --output-duplicates --address-radix='n' --format=x1 |
	tr -d '\n' |
	tr ' [:lower:]' '%[:upper:]' 
}

user_agent=''

while getopts ":f:hmt:u:" option
do
	case $option in
		h) print_help; exit 0 ;;
		u) user_agent=$OPTARG ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -lt 1
then error 3
elif test $# -eq 1
then text=$1
else error 4
fi

text=$(url_encode "$text")
data="text=$text&language=ca"

wget --quiet --output-document - --user-agent "$user_agent" 2>/dev/null \
 --post-data "$data" "$API_URL" |
jq --raw-output 'if (.matches | length) == 0 then "" else 
 .matches[0].replacements[].value end' |
column

exit 0
