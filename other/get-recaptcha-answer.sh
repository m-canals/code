#!/bin/sh

# Probably discontinued.

API_URL='http://api.recaptcha.net'

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-ahv] challenge-key

Get answers for Google ReCAPTCHA challenges. It asks the user for the \
response to the audio or video challenge. Required programs: cat, cut, \
display, grep, kill, printf, test, vlc, wget.

Options

 -a  Ask for an audio challenge.
 -h  Show this message.
 -v  Set verbose mode.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Not enough arguments.
 4) Too many arguments.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "There are not enough arguments.\n" >&2 ;;
		4) printf "There are too many arguments.\n" >&2 ;;
	esac
	exit $1
}

get_answer() {
	wget --quiet --output-document - \
	 --post-data "recaptcha_challenge_field=$2&recaptcha_response_field=$3" \
	 "$API_URL/noscript?k=$1" |
	grep --only '<textarea rows="[0-9]*" cols="[0-9]*">[^<]*</textarea>' |
	cut --delimiter '<' --field 2 |
	cut --delimiter '>' --field 2
}

get_challenge() {
	wget --quiet --output-document - "$API_URL/noscript?k=$1&is_audio=$2" |
	grep --only '<input type="hidden" name="recaptcha_challenge_field" id="recaptcha_challenge_field" value="[^"]*">' |
	cut --delimiter '"' --field 8
}

get_response() {
	local response
	if $2
	then vlc "$API_URL/image?c=$1" >/dev/null 2>&1 &
	else display "$API_URL/image?c=$1" >/dev/null 2>&1 &
	fi
	read -p "Response: " response
	kill -15 $! >/dev/null 2>&1
	printf '%s' "$response"
}

message() {
	if $VERBOSE
	then printf '%s\n' "$1" >&2
	fi
}

VERBOSE=false
audio=false

while getopts ':ahv' option
do
	case $option in
		h) print_help; exit 0 ;;
		a) audio=true ;;
		v) VERBOSE=true ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -lt 1
then error 3
elif test $# -eq 1
then key=$1
else error 4
fi

answer=

while test ${#answer} -eq 0
do
	challenge=$(get_challenge "$key" "$audio")
	message "Challenge: $challenge"
	response=$(get_response "$challenge" "$audio")
	message "Response: $response"
	answer=$(get_answer "$key" "$challenge" "$response")
	message "Answer: $answer"
done

printf '%s\n' "$answer"
exit 0
