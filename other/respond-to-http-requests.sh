#!/bin/sh

# TODO
#
#  - Decode resource.
#  - Prevent directory traversal.
#  - Read only variables and functions (prevent modification by executed
#    scripts).

# https://en.wikipedia.org/wiki/Url_encode
# https://en.wikipedia.org/wiki/Http_status_code
# https://secure.php.net/manual/en/reserved.variables.server.php
# https://en.wikipedia.org/wiki/Directory_traversal

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-h] [-H host] [-p port] [-r root-directory]

Respond to HTTP requests. Directories are listed, executable files are \
executed (output is used as response) and non-executable files are served. \
Required programs: cat, cut, dirname, file, find, head, mkfifo, mktemp, nc, \
printf, sed, test, xargs.

Options

 -H host            Set host.
 -h                 Show this message.
 -p port            Set port.
 -r root-directory  Set root directory.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Too many arguments.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "There are too many arguments.\n" >&2 ;;
	esac
	exit $1
}

log() {
	printf '%s\n' "$1" >&2
}

parse_http_request_line() {
	read -r METHOD URI PROTOCOL

	RESOURCE=${URI%%\?*}

	if test $(expr index + "$URI" '?') -gt 0
	then QUERY_STRING=${URI#*\?} 
	else QUERY_STRING=
	fi
	
	log ""
	log "Parsing request line..."
	log "Method: $METHOD"
	log "URI: $URI"
	log "Resource: $RESOURCE"
	log "Query string: $QUERY_STRING"
	log "Protocol: $PROTOCOL"
}

parse_http_header_fields() {
	log "Parsing header fields..."

	while IFS=: read -r name value
	do
		case $name in
		?) break ;; # XXX Què fa?
		*) log "Ignoring unknown field: $name" ;;
		esac
	done
}

list_directory() {
	log "Showing directory $RESOURCE ..."
	
	local parenthead
	parent=$(dirname "$RESOURCE")
	
	printf "%s 200 OK\n" "$SERVER_PROTOCOL"
	printf "Content-Type: text/html\n"
	printf '\n'
	printf '<!DOCTYPE html>\n'
	printf '<html>\n'
	printf '<a href="%s">..</a><br/>\n' "$parent"
	
	find ".$RESOURCE" -maxdepth 1 -print0 |
	cut --delimiter '' --fields 2- |
	head --bytes -1 | # Remove last byte.
	sed 's/&/\&amp;/g; s/</\&lt;/g; s/>/\&gt;/g; s/"/\&quot;/g' |
	xargs -0 -I {} printf '<a href="%s">%s</a><br/>\n' '{}' '{}'
	printf '</html>\n'
}

execute_file() {
	log "Executing $RESOURCE ..."
	
	 . ".$RESOURCE"
}

serve_file() {
	log "Serving $RESOURCE ..."
	
	local mimetype
	mimetype=$(file --brief --mime-type ".$RESOURCE")
	
	printf '%s 200 OK\n' "$SERVER_PROTOCOL"
	printf 'Content-Type: %s\n' "$mimetype"
	printf '\n'
	cat ".$RESOURCE"
}

not_found() {
	log "Serving 404 Not Found..."
	
	printf '%s 404 Not Found' "$SERVER_PROTOCOL"
	printf '\n'
}

respond_request() {
	parse_http_request_line
	parse_http_header_fields

	if test -d ".$RESOURCE"
	then list_directory
	elif test -x ".$RESOURCE"
	then execute_file
	elif test -f ".$RESOURCE"
	then serve_file
	else not_found
	fi
}

SERVER_PROTOCOL='HTTP/1.1'
SERVER_PORT=8080
SERVER_HOST=127.0.0.1
DOCUMENT_ROOT=$PWD

while getopts ':a:hp:r:' option
do
	case $option in
		H) SERVER_HOST=$OPTARG ;;
		h) print_help; exit 0 ;;
		p) SERVER_PORT=$OPTARG ;;
		r) DOCUMENT_ROOT=$OPTARG ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -gt 0
then error 3
fi

trap 'rm -fr "$temporary"; exit 0' 1 2 3 15
temporary=$(mktemp --directory)
response="$temporary/response"
mkfifo "$response"
cd "$DOCUMENT_ROOT"

log "Listening on $SERVER_HOST:$SERVER_PORT:$PWD"

while :
do
	nc -l -q 0 -p "$SERVER_PORT" "$SERVER_HOST" < "$response" |
	respond_request > "$response"
done
