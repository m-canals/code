#!/bin/sh

# TODO
#
# - Tot i que s'ha povat bastant, caldria veure que segueix algun estàndard
#   (l'especificació de freedesktop.org és poc clara). 

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-h] file-path

Get thumbnail paths. Required programs: cat, cut, head, md5sum, perl, printf, \
realpath, test.

Options

 -h  Show this message.

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Not enough arguments.
 4) Too many arguments.
 5) Empty file path.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "There are not enough arguments.\n" >&2 ;;
		4) printf "There are too many arguments.\n" >&2 ;;
		5) printf "File path is empty.\n" >&2 ;;
	esac
	exit $1
}

escape_path() {
	perl -pe "s/([^A-Za-z0-9-_.!~*\'()\/:@&=+\\\$,])/sprintf('%%%02X',ord(\$1))/seg"
}

remove_last_byte() {
	head --bytes -1
}

while getopts ":h" option
do
	case $option in
		h) print_help; exit 0 ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -lt 1
then error 3
elif test $# -eq 1
then file=$1
else error 4
fi

if test -z "$file"
then error 5
fi

# https://specifications.freedesktop.org/thumbnail-spec/thumbnail-spec-latest.html#DIRECTORY
if test -n "$XDG_CACHE_HOME"
then thumbnails_directory="$XDG_CACHE_HOME/thumbnails/normal"
else thumbnails_directory="$HOME/.cache/thumbnails/normal"
fi

# https://specifications.freedesktop.org/thumbnail-spec/thumbnail-spec-latest.html#THUMBSAVE
# Symbolic links are not resolved: a file accessed through multiple canonical
# paths has multiple thumbnails. We call 'remove_last_byte' to remove the
# trailing  newline character, since assignment removes all trailing newlines
# and some filename could end like that. We could have also used '--zero'
# option and 'xargs' command.
path=$(realpath --canonicalize-missing --no-symlinks "$file" | remove_last_byte | escape_path "$path")
hash=$(printf 'file://%s' "$path" | md5sum | cut --delimiter ' ' --field 1)
printf '%s\n' "$thumbnails_directory/$hash.png"

exit 0
