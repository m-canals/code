#!/usr/bin/env sh

export TEXTDOMAINDIR="locale"
export TEXTDOMAIN="domain"
export LANGUAGE="ca"

directory=$(dirname "$0")
file=$(basename "$0")
pot_path="$TEXTDOMAINDIR/$LANGUAGE/LC_MESSAGES/$TEXTDOMAIN.pot"
po_path="$TEXTDOMAINDIR/$LANGUAGE/LC_MESSAGES/$TEXTDOMAIN.po"
mo_path="$TEXTDOMAINDIR/$LANGUAGE/LC_MESSAGES/$TEXTDOMAIN.mo"
system_mo_path="/usr/share/locale/$LANGUAGE/LC_MESSAGES/$TEXTDOMAIN.mo"

cd "$directory"
mkdir -p "$TEXTDOMAINDIR/$LANGUAGE/LC_MESSAGES"

# Extracció.
xgettext -k_ -k_N -o "$pot_path" "$file" # No extreu els plurals.

# Inicialització.
# msginit -l "$LANGUAGE" -o "$po_path" -i "$pot_path"

# Compilació.
msgfmt -o "$mo_path" "$po_path"

# Instal·lació.
# install "$mo_path" "$system_mo_path"

# Execució.

alias _=gettext
alias N_=ngettext

printf "$(_ 'Today is %s.\n')" $(date +%D)
printf "$(N_ 'There is %d thing.\n' 'There are %d things.\n' 0)" 0
printf "$(N_ 'There is %d thing.\n' 'There are %d.\n' 1)" 1
printf "$(N_ 'There is %d thing.\n' 'There are %d things.\n' 2)" 2
