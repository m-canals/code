Font Generator
==============

Generate a font from images containing glyph tables.

![Screenshot of a glyph table and its corresponding font.](screenshot.png "Screenshot of a glyph table and its corresponding font.")

You may want to print, fill and scan a template with basic latin and latin supplement glyph tables. When filling the tables keep an eye on the alignment (baseline, cap height, etc.)

There are better alternatives such as [this one](https://pypi.org/project/handwrite/).

Required packages: `python2.7`, `potrace` and `python-fontforge`.

Optional packages: `opencv-python`.

Usage
-----

	sh generate-font.sh

This script does not receive any argument but there are variable definitions that you may want to change.
