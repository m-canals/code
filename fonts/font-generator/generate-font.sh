#!/usr/bin/env sh

input1="input-1.jpg"
input2="input-2.jpg"
output="output.ttf"
name="MyFont"
directory="output"
debug1="debug-1.png"
debug2="debug-2.png"
blacklevel=0.8
codes1=$(seq 32 126)
codes2=$(seq 160 255)
rows=10
columns=10
threshold=10
padding="0.15 0.1 0.15 0.3"

remove_temp_directory() {
	if test -n "$temp_directory"
	then
		rm -r -- "$temp_directory"
	fi
}

# Setup.
if test -z "$directory"
then
	trap remove_temp_directory 2 15
	temp_directory=$(mktemp -d)
	directory=$temp_directory
else
	rm -rf -- "$directory"
	mkdir -p -- "$directory"
fi

mkdir -- "$directory/bmp" "$directory/svg"

# Glyph tables to BMP glyphs.
python crop-table-cells.py $codes1 \
	--input $input1 \
	--output "$directory/bmp" \
	--suffix ".bmp" \
	--debug "$debug1" \
	--threshold "$threshold" \
	--rows "$rows" \
	--columns "$columns" \
	--padding $padding

python crop-table-cells.py $codes2 \
	--input $input2 \
	--output "$directory/bmp" \
	--suffix ".bmp" \
	--debug "$debug2" \
	--threshold "$threshold" \
	--rows "$rows" \
	--columns "$columns" \
	--padding $padding

# BMP glyphs to SVG glyphs.
find "$directory/bmp" -name '*.bmp' -print0 |
xargs -0 potrace --blacklevel "$blacklevel" --svg --

find "$directory/bmp" -name '*.svg' -print0 |
xargs -0 mv --target-directory "$directory/svg" --

# SVG glyphs to TTF font.
python2 generate-font-from-glyphs.py \
	--input "$directory/svg" \
	--output "$output" \
	--name "$name"

# Cleanup.
remove_temp_directory
