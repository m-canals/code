#!/bin/env sh

rows=${1:-0}
columns=${2:-0}

cat << EOF
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
html { height: 100%; }
body { height: 100%; margin: 0px; padding: 2cm; box-sizing: border-box; }
table { border-collapse: collapse; width: 100%; height: 100%; }
td { border: 1mm solid black; }
</style>
</head>
<body>
<table>
EOF

i=0

while test $i -lt $rows
do
	printf '<tr>\n'
	j=0

	while test $j -lt $columns
	do
		printf '<td></td>\n'
		j=$((j+1))
	done
	
	printf '</tr>\n'
	i=$((i+1))
done

cat << EOF
</table>
</body>
</html>
EOF
