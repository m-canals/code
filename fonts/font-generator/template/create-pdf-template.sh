#!/usr/bin/env sh

sh create-html-template.sh "${1:-0}" "${2:-0}" |
wkhtmltopdf --page-size A2 - -
