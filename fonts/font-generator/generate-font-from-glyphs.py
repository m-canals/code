#!/usr/bin/env python2

import fontforge

def create_font_from_files(code_to_files_map,
	name = None, style = 'Regular', encoding = 'UnicodeFull'):
	
	font = fontforge.font()
	
	if encoding != None:
		font.encoding = encoding
	
	if name != None:
		font.familyname = name
		font.fontname = name + "-" + style
		font.fullname = name + " " + style
	
	for code, files in code_to_files_map.items():
		for file in files:
			glyph = font.createChar(code)
			glyph.importOutlines(file)
	
	return font

def _parse_arguments():

	parser = ArgumentParser(
		add_help = False,
		description = "Generate a font from glyph images. Filenames without "
		              "extension are used as character codes, except non-digital "
		              "ones, which are ignored.")
	parser.add_argument('-i', '--input',
		metavar = 'INPUT-DIRECTORY',
		dest = 'input',
		required = False,
		default = path.curdir,
		help = "Load input images from INPUT-DIRECTORY. "
		       "Default: working directory.")
	parser.add_argument('-o', '--output',
		metavar = 'OUTPUT-FILE',
		dest = 'output',
		required = True,
		default = path.curdir,
		help = "Save output font to OUTPUT-FILE. ")
	parser.add_argument('-n', '--name',
		metavar = 'NAME',
		dest = 'name',
		required = False,
		default = None,
		help = "Set font name to NAME. Default: none.")
	parser.add_argument('-s', '--style',
		metavar = 'STYLE',
		dest = 'style',
		required = False,
		default = 'Regular',
		help = "Set font style to STYLE if name is not none. Default: 'Regular'.")
	parser.add_argument('-e', '--encoding',
		metavar = 'ENCODING',
		dest = 'encoding',
		required = False,
		default = 'UnicodeFull',
		help = "Set font encoding to ENCODING. Default: 'UnicodeFull'.")
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")
	
	return parser.parse_args()

def __main__():

	arguments = _parse_arguments()
	name = arguments.name
	style = arguments.style
	encoding = arguments.encoding
	
	files = defaultdict(list)

	for dirpath, dirnames, filenames in walk(arguments.input):
		for filename in filenames:
			basename = filename.rsplit('.', 1)[0]
			if basename.isdigit():
				code = int(basename)
				files[code].append(dirpath + path.sep + filename)
	
	font = create_font_from_files(files, name, style, encoding)
	font.generate(arguments.output)

if __name__ == '__main__':
	from argparse import ArgumentParser
	from os import walk, path
	from collections import defaultdict
	__main__()
