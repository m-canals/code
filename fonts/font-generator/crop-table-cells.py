#!/usr/bin/env python
#
# TODO
#
# 	- Take into account table border width.
# 	- Fix CV box estimation function.

from PIL import Image
from PIL import ImageDraw
from os.path import sep, curdir

def estimate_boxes_with_cv(filename):
	import cv2
	
	image = cv2.imread(filename)
	image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	image = cv2.bilateralFilter(image, 10, 17, 17)
	image = cv2.Canny(image, 50, 100)
	
	contours = cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]
	boxes = list()

	for contour in contours[::-1]:

		# Must have 4 lines (8 coordinates).
		if len(contour) != 4 * 2:
			continue
		
		l = contour[0][0][0]
		t = contour[1][0][1]
		r = contour[4][0][0]
		b = contour[5][0][1]
		w = r - l
		h = b - t

		if w < 0 or h < 0:
			continue

		boxes.append((a[0], a[1], b[0], b[1]))
	
	return boxes

def median(iterable):

	sorted_iterable = sorted(iterable)
	median_index = len(sorted_iterable) // 2
	
	return sorted_iterable[median_index]

def estimate_table_box(image, threshold):

	# Grayscale image and image size.
	image = image.convert('L')
	w, h = image.size
	
	# Return the first point whose intensity difference
	# between the previous point is not below the threshold.
	# If it does not exist, return the first point.
	def f(points):
	
		p = next(points)
		a = image.getpixel(p)
		
		for q in points:
			b = image.getpixel(q)
			
			if a - b >= threshold:
				return q
			else:
				a = b
		
		return p
	
	# Table box.
	l = median(f((x, y) for x in range(0, w, 1))[0] for y in range(h))
	t = median(f((x, y) for y in range(0, h, 1))[1] for x in range(w))
	r = median(f((x, y) for x in range(w - 1, -1, -1))[0] for y in range(h))
	b = median(f((x, y) for y in range(h - 1, -1, -1))[1] for x in range(w))
	
	return l, t, r, b

def estimate_boxes(image, size, padding, threshold):

	# Number of rows and cells.
	n, m = size

	# Table box and size.
	t_l, t_t, t_r, t_b = estimate_table_box(image, threshold)
	t_w = t_r - t_l
	t_h = t_b - t_t

	# Cell padding and size.
	p_l, p_t, p_r, p_b = padding
	c_w = float(t_w) / float(m)
	c_h = float(t_h) / float(n)

	boxes = list()

	for i in range(n):
		for j in range(m):
			# Cell box.
			l_1 = t_l + c_w * j
			t_1 = t_t + c_h * i
			r_1 = l_1 + c_w
			b_1 = t_1 + c_h
			
			# Cell content box.
			l = int(l_1 + c_w * p_l)
			t = int(t_1 + c_h * p_t)
			r = int(r_1 - c_w * p_r)
			b = int(b_1 - c_h * p_b)
			
			boxes.append([l, t, r, b])
	
	return boxes

def _parse_arguments():

	def natural(argument):
	
		value = int(argument)
		
		if value < 0:
			raise ValueError
		else:
			return value
		
	parser = ArgumentParser(
		add_help = False,
		description = "Crop an image of each table cell content "
		              "from an image of a table.")
	parser.add_argument(
		metavar = 'BASENAME',
		dest = 'basenames',		
		type = str,
		nargs = '*',
		help = "Save each output image to a file whose name starts with BASENAME. "
		       "Cells are ordered from left to right and from top to bottom. "
		       "See also --suffix.")
	parser.add_argument('-r', '--rows',
		metavar = 'ROWS',
		dest = 'rows',
		required = False,
		default = 1,
		type = natural,
		help = "Crop ROWS images from each column.")
	parser.add_argument('-c', '--columns',
		metavar = 'COLUMNS',
		dest = 'columns',
		required = False,
		default = 1,
		type = natural,
		help = "Crop COLUMNS cells from each row.")
	parser.add_argument('-p', '--padding',
		metavar = 'PADDING',
		dest = 'padding',
		nargs = 4,
		required = False,
		type = float,
		default = (0.0, 0.0, 0.0, 0.0),
		help = "Shrink crop boxes by PADDING. Relevant values range from 0 to 1 "
		       "and are relative to cell size. Default: 0.")
	parser.add_argument('-t', '--threshold',
		metavar = 'THRESHOLD',
		dest = 'threshold',
		required = False,
		default = 0,
		type = float,
		help = "Detect table borders where the intensity difference is greater or "
		       "equal to THRESHOLD. Relevant values range from 0 to 1 and "
		       "are relative to maximum intensity difference. Default: 0.")
	parser.add_argument('-i', '--input',
		metavar = 'INPUT-FILE',
		dest = 'input',
		required = True,
		help = "Load input image from INPUT-FILE.")
	parser.add_argument('-o', '--output',
		metavar = 'OUTPUT-DIRECTORY',
		dest = 'output',
		required = False,
		default = curdir,
		help = "Save output images in OUTPUT-DIRECTORY. "
		       "Default: working directory.")
	parser.add_argument('-d', '--debug',
		metavar = 'DEBUG-FILE',
		dest = 'debug',
		required = False,
		default = None,
		help = "Draw crop boxes to DEBUG-FILE. Default: none (disabled).")
	parser.add_argument('-f', '--format',
		metavar = 'FORMAT',
		dest = 'format',
		required = False,
		default = 'bmp',
		help = "Save output images using FORMAT. Default: BMP.")
	parser.add_argument('-s', '--suffix',
		metavar = 'SUFFIX',
		dest = 'suffix',
		required = False,
		default = '',
		help = "Append SUFFIX to each output image BASENAME. Default: none.")
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")
	
	return parser.parse_args()

def __main__():
	arguments = _parse_arguments()
	size = (arguments.rows, arguments.columns)
	input = arguments.input
	output = arguments.output
	debug = arguments.debug
	basenames = arguments.basenames
	suffix = arguments.suffix
	format = arguments.format
	padding = arguments.padding
	threshold = arguments.threshold
	
	image = Image.open(input)
	boxes = estimate_boxes(image, size, padding, threshold)
	# boxes = estimate_boxes_with_cv(input)

	for basename, box in zip(basenames, boxes):
		cell = image.crop(box)
		if cell.width > 0 and cell.height > 0:
			path = output + sep + basename + suffix
			cell.save(path, format)

	if debug != None:
		draw = ImageDraw.Draw(image)
		for box in boxes:
			draw.rectangle(box, outline = 'red')
		image.save(debug)

if __name__ == '__main__':
	from argparse import ArgumentParser
	__main__()
