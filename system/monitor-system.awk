#!/usr/bin/awk -f

function GPUTemp() {
	cmd = "nvidia-settings -tq GPUCoreTemp"
	cmd | getline temp
	close(cmd)
	if (temp < 50) color = 39
	else if (temp < 60) color = 33
	else color = 91
	printf "GPU Temperature: \033[%sm%s°C\033[0m\033[K\n", color, temp
}

function CPUTemp() {
	printf "CPU Temperature: "
	cmd = "sensors -A coretemp-isa-*"
	core = 0
	while (cmd | getline) {	 
		if (/^Core/) {
			temp = $3
			gsub(/^\+|\.0°C$/, "", temp)
			if (core > 0) printf ", "
			if ( temp < 50 ) color = 39
			else if ( temp < 60 ) color = 33
			else color = 91
			printf "\033[%sm%s°C\033[0m", color, temp
			core += 1
		}
	}
	close(cmd)
	printf "\033[K\n"
}

function HDDTemp() {
	printf "HDD Temperature: "
	cmd = "hddtemp /dev/sd?"
	OLDFS=FS
	FS = ":"
	disk = 0
	while (cmd | getline) {
		if (! /not available/) {  
			model = $2
			temp = $NF
			sub(/ /, "", model)
			gsub(/[ °C]/, "", temp)
			if (disk > 0) printf ", "
			if ( temp < 40 ) color = 39
			else if ( temp < 50 ) color = 33
			else color = 91
			printf "\033[%sm%s°C\033[0m (%s)", color, temp, model
			disk += 1
		}
	}
	FS = OLDFS
	close(cmd)
	printf "\033[K\n"
}

function CPUFanSpeed() {
	cmd = "sensors -A w83697hf-isa-*"
	while (cmd | getline) {
		if (/^fan2/) {
			speed = $2
			if ( speed >= 1800 ) color = 39
			else if ( speed >= 1700 ) color = 33
			else color = 91
			printf "CPU Fan Speed: \033[%sm%s RPM\033[0m\033[K\n", color, speed
		}
	}
	close(cmd)
}

function memUsage() {
	file = "/proc/meminfo"
	record = 1
	while (getline < file) {
		if (record == 1) total = used = $2
		else if (record < 5) used -= $2
		else {
			used = int(used/1024)
			total = int(total/1024)
			if (used < total - 500) color = 39
			else if (used < total - 250) color = 33
			else color = 91
			printf "Memory Usage: \033[%sm%s MiB\033[0m / %s MiB\033[K\n",
			color, used, total
			break
		}
		record += 1
	}
	close(file)
}

function procMon(records) {
	cmd = "top -b -n 1"
	max_record = records + 7
	record = 0
	while (cmd | getline) {
		record += 1
		if (record < 7) continue
		else if (record == 7) printf "\033[38;5;255;48;5;238m%s\033[K\033[0m\n", $0
		else if (record <= max_record) printf "%s\033[K\n", $0
		else break
	}
	close(cmd)
}

function procUsage() {
	printf "CPU Usage: "
	file = "/proc/stat"
	core = 1
	while (getline < file) {
		if (/^cpu[0-9]/) {
			user = $2
			nice = $3
			sys = $4
			idle = $5
			iowait = $6
			irq = $7
			softirq = $8
			total=(user + nice + sys + idle + iowait + irq + softirq)
			diff_idle = idle - cpu[core]
			diff_total = total - cpu[core + 1]
			cpu[core] = idle
			cpu[core + 1] = total
			usage = int(100 * (diff_total - diff_idle) / diff_total)
			if (core > 1) printf ", "
			if ( usage < 75 ) color = 39
			else if ( usage < 90 ) color = 33
			else color = 91
			printf "\033[%sm%s%%\033[0m", color, usage
			core += 2
		}
	}
	close(file)
	printf "\033[K\n"
}

# TODO Acabar.
function netUsage(iface) {
	file = "/proc/net/dev"
	while (getline < file) {
		if($0 ~ iface) {
			rx_bytes = $2
			tx_bytes = $10
			#interface = interfaces[iface]
			#diff_rx_bytes = rx_bytes - interface[1]
			#diff_tx_bytes = tx_bytes - interface[2]
			#interface[1] = rx_bytes
			#interface[2] = tx_bytes
			#interfaces[iface] = interface[0]
			#printf "Network Usage (%s): ↓ %s (%s) ↑ %s (%s)\033[K\n",
			#iface, rx_bytes, diff_tx_bytes, tx_bytes 
		}
	}
	close(file)
}

# TODO Fer.
function netHosts() {

}

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

function fancyTitle(title) {
	printf "\033[38;5;236;48;5;230m%s\033[K\033[0m\n", title
}

function setup() {
	system("clear")
}

function main() {
	setup()
	while (1) {
		#netUsage("eth0") 
		printf "\033[?25l"
		printf "\033[0;0H"
		#GPUTemp()
		CPUTemp()
		#HDDTemp()
		#CPUFanSpeed()
		memUsage()
		fancyTitle(" :: Proc. Monitor")
		procMon(3)
		procUsage()
		printf "\033[?25h"
		system("sleep 5")
	}
}

BEGIN {
	main()
}
