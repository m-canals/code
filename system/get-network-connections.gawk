#!/usr/bin/gawk -f

# TODO
#
#  - Remove GAWK requirement (strtonum).
#  - Support IPv6 addresses.

function hex2dec(hex) {
	return strtonum("0x" hex)
}

function dotdecimal(input) {
	return hex2dec(substr(input, 7, 2)) "." hex2dec(substr(input, 5, 2)) "." \
	       hex2dec(substr(input, 3, 2)) "." hex2dec(substr(input, 1, 2))
}

function parseline() {
	split($2, local, ":")
	split($3, remote, ":")
	local[3] = dotdecimal(local[1])
	local[4] = hex2dec(local[2])
	remote[3] = dotdecimal(remote[1])
	remote[4] = hex2dec(remote[2])
	# printf "%s %s >>> ", $2, $3
	printf("% 15s % 5s  ->  % 15s % 5s\n",
	 local[3], local[4], remote[3], remote[4])
}

function main() {
	protocol = "tcp"
	
	if (ARGC > 1) {
		protocol = ARGV[1]
	}
	
	if (protocol !~ /^[a-zA-Z0-9]*$/) {
		print "Invalid protocol format."
		exit 1
	}
	
	file = "/proc/net/" protocol
	
	if (system("test -f " file) != 0) {
		print "No file for this protocol."
		exit 1
	}
	
	# Skip header.
	getline < file
	
	while (getline < file) {
		parseline()
	}
	 
	close(file)
}

BEGIN {
	main()
}
