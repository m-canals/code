#!/usr/bin/awk -f

function sample(array,  file, i) {
	file = "/proc/stat";
	i = 0;
	
	while (getline < file && $1 ~ "^cpu") {
		array[i] = $0;
		i++;
	}
	
	close(file);
	return i;
}

function cpu_usage(sample1, sample2, idle,  sample1_arr, sample2_arr,
 sample1_len, sample2_len, sample_len, total, running, i) {
	sample1_len = split(sample1, sample1_arr);
	sample2_len = split(sample2, sample2_arr);
	sample_len = (sample1_len > sample2_len) ? sample2_len : sample1_len;
	
	if (sample_len < 5) {
		return -1;
	}
	
	total = 0;
	for (i = 2; i <= sample_len; i++) {
		total += sample2_arr[i] - sample1_arr[i];
	}
	
	# total - idle
	if (idle == 1) {
		running = total - (sample2_arr[5] - sample1_arr[5]);
	# user + nice + system
	} else {
		running = ((sample2_arr[2] + sample2_arr[3] + sample2_arr[4]) - \
		           (sample1_arr[2] + sample1_arr[3] + sample1_arr[4]));
	}
	
	return int((1000 * running / total + 5) / 10);
}

BEGIN {
	average = 0; # Show also CPU average.
	idle = 0; # Calculate total - idle instead of user + nice + system.
	format = "CPU %d: %d%%\n"
	sample1_len = sample(sample1);
	system("sleep 1");
	sample2_len = sample(sample2);
	sample_len = (sample1_len > sample2_len) ? sample2_len : sample1_len;
	for (i = 1 - average; i < sample_len; i++) {
		percentage = cpu_usage(sample1[i], sample2[i], idle);
		printf(format, i, percentage);
	}
}
