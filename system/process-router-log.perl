#!/usr/bin/perl

use strict;
use warnings;

{
	package RegExp;
	
	our $DecOctet = "2[0-5][0-5]|1[0-9][0-9]|[1-9]?[0-9]";
	our $IPv4Address = "(?:$DecOctet)(?:\.(?:$DecOctet)){3}";
	our $HexOctet = "[0-9A-Fa-f]{2}";
	our $MACAddress = "(?:$HexOctet)(?:\:(?:$HexOctet)){5}";
	our $DateTime = qr{\S\S\S (?: |\d)\d \d\d:\d\d:\d\d};
}

# Known hosts.
my %hosts = (
	'00:00:00:00:00:00' => {
		'name' => 'Example',
		'hostname' => 'example'
	}
);

sub geoiplookup {
	my ($address) = @_;
	
	system("which geoiplookup >/dev/null");
	
	if ($? != 0) {
		return '';
	}
	
	open(CMD, "geoiplookup $address |");

	my ($database, $country);
	
	while (<CMD>) {
		($database, $country) = split(': ', $_, 2);
	}
	
	$country =~ s/\n//;

	return $country;
}

sub kernel_intrusion_handler {
	my ($date, $in, $out, $mac, $src, $dst, $len, $tos, $prec, $ttl, $id, $df, $proto, $spt, $dpt, $window, $res, $syn, $urgp, $mark) = @_;
	
	$df = $df || "";
	$syn = $syn || "";
	
	my $country = geoiplookup($src);
	
	$date = "\033[37m$date\033[0m";
	$out = "\033[31m$out\033[0m";
	$src = "\033[33m$src\033[0m";
	$dpt = "\033[35m$dpt\033[0m";

	print("$date: Intrusion: $in $out $proto $src:$spt $dst:$dpt $country\n");
}

sub dnsmasq_dhcp_operation_handler {
	my ($date, $operation, $interface, $ip_address, $mac_address, $hostname) = @_;
	
	$ip_address = $ip_address || '';
	$hostname = $hostname || '';
	
	$date = "\033[37m$date\033[0m";
	$ip_address = "\033[36m$ip_address\033[0m";
	
	if (exists $hosts{$mac_address}) {
		$mac_address = "\033[32m$mac_address\033[0m";
		
		if (! ($hostname eq $hosts{$mac_address}{'hostname'})) {
			$hostname = "\033[31m$hostname\033[0m";
		}
	} else {
		$mac_address = "\033[31m$mac_address\033[0m";
		$hostname = "\033[31m$hostname\033[0m";
	}
	
	if ($operation eq 'REQUEST') {
		print("$date: $mac_address requests address $ip_address\n")
	} elsif ($operation eq 'NAK') {
		print("$date: address $ip_address is not offered to $mac_address ($hostname)\n")
	} elsif ($operation eq 'DISCOVER') {
		print("$date: $mac_address wants an address\n")
	} elsif ($operation eq 'OFFER') {
		print("$date: address $ip_address is offered to $mac_address\n")
	} elsif ($operation eq 'ACK') {
		print("$date: $mac_address ($hostname) accepts address $ip_address\n")
	} else {
		print("$date: $mac_address is doing an \033[33munknown operation\033[0m ($operation)\n")
	}
}

sub default_handler {
	my ($date, $text) = @_;
	$date = "\033[37m$date\033[0m";
	printf("$date: $text\n");
}

my %handler = (
	qr{^($RegExp::DateTime) (?:\S+)  dnsmasq-dhcp\[\d+\]: DHCP([^ ]*)(\(\S+\)) ($RegExp::IPv4Address)? ?($RegExp::MACAddress) ?(.*)$} => 'dnsmasq_dhcp_operation_handler',
	qr{^($RegExp::DateTime) (?:\S+)  kernel: Intrusion -> IN=(\S*) OUT=(\S*) MAC=(\S*) SRC=(\S*) DST=(\S*) LEN=(\S*) TOS=(\S*) PREC=(\S*) TTL=(\S*) ID=(\S*) (?:(DF)? )?PROTO=(\S*) SPT=(\S*) DPT=(\S*) WINDOW=(\S*) RES=(\S*) (?:(SYN)? )?URGP=(\S*) MARK=(\S*) $} => 'kernel_intrusion_handler',
	qr{^($RegExp::DateTime) (?:\S+)  (.*)$} => 'default_handler'
);

while (<>) {
	foreach my $regex (keys(%handler)) {
		if (my @matches = $_ =~ m/$regex/) {
			eval "$handler{$regex}(\@matches)";
			last;
		}
	}
}
