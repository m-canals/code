#!/usr/bin/awk -f

# Usage: tail -F /var/log/router.log | ./script.awk -W interactive

function getfields(start, stop,  string, i) {
	string = $start

	for (i = start + 1; i <= stop; i++) {
		string = string FS $i
	}
	
	return string
}

function gethostname(mac, hostname, hosts,  name) {
	name = hosts[mac, "name"]
	htname = hosts[mac, "hostname"]
	
	if (name) {
		if (hostname && htname != hostname) {
			name = "\033[1;33m" name "\033[0m" " (" mac ", " hostname ")"
		} else {
			name =  "\033[1;36m" name "\033[0m"
		}
	} else {
		name = "\033[1;31m" "Unknown host" "\033[0m" \
		" (" mac (hostname ? ", " hostname : "") ")"
	}

	return name
}

function dhcp_handler(  operation, mac, ip, hostname, message, name) {
	operation = $6
	
	if (operation ~ /DISCOVER/) {
		if (NF == 7) {
			ip = ""
			mac = $7
		} else if (NF == 8) {
			ip = $7
			mac = $8
		}
		name = gethostname(mac, "", hosts)
		printf "DHCP DISCOVER: %s %s \n", name, ip
	} else if (operation ~ /OFFER/) {
		ip = $7
		mac = $8
		name = gethostname(mac, "", hosts)
		printf "DHCP OFFER: %s %s \n", name, ip
	} else if (operation ~ /REQUEST/) {
		ip = $7
		mac = $8
		name = gethostname(mac, "", hosts)
		printf "DHCP REQUEST: %s %s \n", name, ip
	} else if (operation ~ /ACK/) {
		ip = $7
		mac = $8
		hostname = $9
		name = gethostname(mac, hostname, hosts)
		printf "DHCP ACK: %s %s\n", name, ip
	} else if (operation ~ /NAK/) {
		ip = $7
		mac = $8
		message = getfields(9, NF)
		name = gethostname(mac, "", hosts)
		printf "DHCP NAK: %s %s (%s)\n", name, ip, message
	} else if (operation ~ /RELEASE/) {
		ip = $7
		mac = $8
		message = getfields(9, NF)
		name = gethostname(mac, "", hosts)
		printf "DHCP RELEASE: %s %s (%s)\n", name, ip, message
	} else if (operation ~ /INFORM/) {
		ip = $7
		mac = $8
		name = gethostname(mac, "", hosts)
		printf "DHCP INFORM: %s %s \n", name, ip
	} else if (operation == "DHCP,") {
		printf "DHCP: %s\n", getfields(7, NF)
	} else {
		printf "%s\n", getfields(6, NF)
	} 
}

# Si es declara la variable local «fields» en alguns casos no va.
function geoiplookup(address,  line) {
	# TODO Escapar adreça.
	while (("geoiplookup " address) | getline line) {
		split(line, fields, ": ") 
	}
	
	return fields[2] == "IP Address not found" ? "" : fields[2]
}

function intrusion_handler(  fields, field, i) {
	for (i = 8; i <= NF; i++) {
		split($i, field, "=")
		fields[field[1]] = field[2]
	}
   
	fields["COUNTRY"] = geoiplookup(fields["SRC"])
	printf "Intrusion: %s%s %s \033[1;34m%s\033[0m:%d %s:\033[1;35m%d\033[0m %s\n",
	fields["IN"],
	fields["OUT"] ? " \033[1;31m" fields["OUT"] "\033[0m" : "",
	fields["PROTO"],
	fields["SRC"],
	fields["SPT"],
	fields["DST"],
	fields["DPT"],
	fields["COUNTRY"]
}

BEGIN {
	hosts["00:00:00:00:00:00", "name"] = "Example"
	hosts["00:00:00:00:00:00", "hostname"] = "example"
}

{
	printf "\033[37m%s %2d %s\033[0m: ", $1, $2, $3

	if ($0 ~ /dnsmasq-dhcp/) {
		dhcp_handler()
	} else if ($0 ~ /kernel: Intrusion/) {
		intrusion_handler()
	} else {
		print getfields(5, NF)
	}
}
