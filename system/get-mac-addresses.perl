#!/usr/bin/perl

{
	package RegExp;
	
	our $DecOctet = "2[0-5][0-5]|1[0-9][0-9]|[1-9]?[0-9]";
	our $IPv4Address = "(?:$DecOctet)(?:\.(?:$DecOctet)){3}";
	our $HexOctet = "[0-9A-Fa-f]{2}";
	our $MACAddress = "(?:$HexOctet)(?:\:(?:$HexOctet)){5}";
}

my @hosts;

foreach my $line (<>) {
	if ($line =~ m/^Host: ($RegExp::IPv4Address) \((.*)\)/) {
		my %host;
		my $address = $1;
		my $hostname = $2;
		my $mac = '';

		open(ARP, "arp -an $address |") || die("caca");
		$line = <ARP>;
		if ($line =~ m/($RegExp::MACAddress)/) {
			$mac = $1;
		}
		close(ARP);
		
		$host{'address'} = $address;
		$host{'name'} = $hostname;
		$host{'mac'} = $mac;
		push(@hosts, ${%host});
		
		print("$address $mac $hostname\n");
	}
}
