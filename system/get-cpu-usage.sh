#!/bin/sh

print_help() {
	local script_name
	script_name=${0##*/}
	cat << EOF
Usage

 $script_name [-ah] time

Get CPU usage (in a time frame). Required programs: cat, printf, sleep, test. \
Required files: /proc/stat.

Options

 -a STRING  Set string to print at the end (after everything else).
 -b STRING  Set string to print at the beginning (before anything else).
 -f STRING  Set CPU information string format.
 -h         Show this message.
 -i         Subtract idle time from total time instead of adding user time, \
            nice time and system time together in order to calculate running \
            time. Not implemented.
 -s STRING  Set string to print between CPU information (as a separator).

Exit status

 0) Success.
 1) Required argument.
 2) Undefined option.
 3) Not enough arguments.
 4) Too many arguments.
 5) Invalid time interval. 
 6) Invalid format string.
EOF
}

error() {
	case $1 in
		1) printf "Option '%s' requires an argument.\n" "$2" >&2 ;;
		2) printf "Option '%s' is not defined.\n" "$2" >&2 ;;
		3) printf "There are not enough arguments.\n" >&2 ;;
		4) printf "There are too many arguments.\n" >&2 ;;
		5) printf "Invalid time interval.\n" >&2 ;;
		6) printf "Invalid format string.\n" >&2 ;;
	esac
	exit $1
}

# Fields: user, nice, system, idle, iowait, irq, softirq, steal, guest,
# guest_nice.

# Inclou el total de totes les CPU (primera línia).
cpu_usage_sample() {
	exec 3</proc/stat
	while read entry user nice system idle _ <&3 &&
	      test $(expr match + $entry cpu) -ne 0
	do printf '%s %s\n' $((user+nice+system)) $idle 
	done
	exec 3<&-
}

cpu_usage() {
	local fields running idle total
	fields=$((($#-1)/2))
	shift $(($1*2+1))
	running=$1
	idle=$2
	shift $fields
	running=$(($1-running))
	idle=$(($2-idle))
	total=$((running+idle))
	echo $(((1000*running/total+5)/10))
}

count_arguments() {
	printf '%s\n' $#
}

after='\n'
before=''
format='%d%%'
idle=false
separator=' '

while getopts ":f:hi" option
do
	case $option in
		a) after=$OPTARG ;;
		b) before=$OPTARG ;;
		f) format=$OPTARG ;;
		h) print_help; exit 0 ;;
		i) idle=true ;;
		s) separator=$OPTARG ;;
		:) error 1 "$OPTARG" ;;
		?) error 2 "$OPTARG" ;;
	esac
done

shift $((OPTIND-1))

if test $# -lt 1
then error 3
elif test $# -gt 1
then error 4
else time=$1
fi

sample_1=$(cpu_usage_sample)
sleep "$time" 2> /dev/null || error 5
sample_2=$(cpu_usage_sample)

printf -- "$before" 2> /dev/null || error 6
cores=$(($(count_arguments $sample_1 $sample_2)/2/2))
core=1 # Ingora el total de totes les CPU (core = 0).
while test $core -lt $cores
do
	if test $core -gt 1
	then printf -- "$separator" 2> /dev/null || error 6
	fi
	printf -- "$format" $(cpu_usage $core $sample_1 $sample_2) 2> /dev/null ||
	error 6
	core=$((core+1))
done
printf -- "$after" 2> /dev/null || error 6

exit 0
