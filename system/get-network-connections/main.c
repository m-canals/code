#include <stdio.h>
#include <pwd.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/tcp.h>
#include <errno.h>
#include <stdlib.h>

#define FILENAME "/proc/net/tcp"
#define ADDRESS(a, b, c, d) (a) | (b) << 8 | (c) << 16 | (d) << 24
#define ADDRESS_BYTE(address, byte) ((address) >> ((byte) << 3)) & 0xFF
#define NONE 0

/*enum status
{
  TCP_ESTABLISHED = 0x01,
  TCP_SYN_SENT    = 0x02,
  TCP_SYN_RECV    = 0x03,
  TCP_FIN_WAIT1   = 0x04,
  TCP_FIN_WAIT2   = 0x05,
  TCP_TIME_WAIT   = 0x06,
  TCP_CLOSE       = 0x07,
  TCP_CLOSE_WAIT  = 0x08,
  TCP_LAST_ACK    = 0x09,
  TCP_LISTEN      = 0x0A,
  TCP_CLOSING     = 0x0B
};*/

struct data
{
	u_int32_t local_address;
	u_int16_t local_port;
	u_int32_t remote_address;
	u_int16_t remote_port;
	u_int8_t status;
	uid_t uid;
};

enum rule_flags
{
	INCLUDE              = 0b0000000,
	EXCLUDE              = 0b0000001,
	CHECK_LOCAL_ADDRESS  = 0b0000010,
	CHECK_LOCAL_PORT     = 0b0000100,
	CHECK_REMOTE_ADDRESS = 0b0001000,
	CHECK_REMOTE_PORT    = 0b0010000,
	CHECK_STATUS         = 0b0100000,
	CHECK_UID            = 0b1000000,
};

struct rule
{
	enum rule_flags flags;
	struct data data;
};

int excluded(struct data * data, struct rule * rules, size_t length)
{
	for (size_t i = 0; i < length; i++) {
		// This rule doesn't apply: continue.
		if ((rules[i].flags & CHECK_LOCAL_ADDRESS
		 &&  rules[i].data.local_address != data->local_address)
		 || (rules[i].flags & CHECK_LOCAL_PORT
		 &&  rules[i].data.local_port != data->local_port)
		 || (rules[i].flags & CHECK_REMOTE_ADDRESS
		 &&  rules[i].data.remote_address != data->remote_address)
		 || (rules[i].flags & CHECK_REMOTE_PORT
		 &&  rules[i].data.remote_port != data->remote_port)
		 || (rules[i].flags & CHECK_STATUS
		 &&  rules[i].data.status != data->status)
		 || (rules[i].flags & CHECK_UID
		 &&  rules[i].data.uid != data->uid)) {
			continue;
		}
		
		// This rule applies: include or exclude.
		return rules[i].flags & EXCLUDE;
	}
	
	// None of the rules apply: include.
	return 0;
}

int scan_data(struct data * data, FILE * stream)
{
	return fscanf(stream, "%*d: %x:%hx %x:%hx %hhx %*x:%*x %*x:%*x %*x %u %*[^\n]s",
		&(data->local_address),
		&(data->local_port),
		&(data->remote_address),
		&(data->remote_port),
		&(data->status),
		&(data->uid)
	) == 6;
}

void print_address_info(u_int32_t address, u_int16_t port, char * protocol)
{
	struct servent * service = getservbyport(htons(port), protocol);

	printf("%d.%d.%d.%d %d (%s) ",
		ADDRESS_BYTE(address, 0),
		ADDRESS_BYTE(address, 1),
		ADDRESS_BYTE(address, 2),
		ADDRESS_BYTE(address, 3),
		port,
		service == NULL ? "" : service->s_name
	);
}

void print_data(struct data * data)
{
	struct passwd * pw = getpwuid(data->uid);

	print_address_info(data->local_address, data->local_port, "tcp");
	print_address_info(data->remote_address, data->remote_port, "tcp");
	printf("%02X %d (%s)\n",
		data->status,
		data->uid,
		pw == NULL ? "" : pw->pw_name
	);
}

void print_rules(struct rule * rules, size_t length)
{
	printf("\n");
	
	for (size_t i = 0; i < length; i++) {
		print_data(&rules[i].data);
	}
}

int main(int argc, char ** argv)
{
	FILE * stream;
	struct data data;
	struct rule rules[] = {
		#include "rules.c"
	};

	if ((stream = fopen(FILENAME, "r")) == NULL) {
		perror("An error occurred while openening the file");
		exit(EXIT_FAILURE);
	}

	fscanf(stream, "%*[^\n]s");
	
	if (ferror(stream) != 0) {
		perror("An error occurred while reading the header");
		exit(EXIT_FAILURE);
	}

	while (scan_data(&data, stream)) {
		if (! excluded(&data, rules, sizeof(rules) / sizeof(struct rule))) {
			print_data(&data);
		}
	}

	if (ferror(stream) != 0) {
		perror("An error occurred while reading the data");
		exit(EXIT_FAILURE);
	}
	
	if (fclose(stream) != 0) {
		perror("An error occurred while closing the file");
		exit(EXIT_FAILURE);
	}

	// print_rules(rules, sizeof(rules) / sizeof(struct rule));

	exit(EXIT_SUCCESS);
}
