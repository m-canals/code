// The following rules are read at compile time.
//
// Each rule has two fields:
// 
//  1. A bit field (action and checked data): INCLUDE / EXCLUDE,
//     CHECK_LOCAL_ADDRESS, CHECK_LOCAL_PORT, CHECK_REMOTE_ADDRESS,
//     CHECK_REMOTE_PORT, CHECK_STATUS, CHECK_UID.
//
//  2. A list (checked data values): local address, local port,
//     remote address, remote port, status, UID. Non-checked values
//     can be set to NONE and trailing values can be omitted.
//
// If no rule matches it defaults to inclusion.

{
	EXCLUDE | CHECK_LOCAL_ADDRESS, { ADDRESS(127,0,0,1) }
}
,
{
	EXCLUDE | CHECK_REMOTE_ADDRESS, { NONE, NONE, ADDRESS(127,0,0,1) }
}

