#!/usr/bin/env python3

# http://man7.org/linux/man-pages/man5/proc.5.html
# https://stackoverflow.com/questions/11987495/linux-proc-loadavg

# Get CPU statistics (user, nice, system, idle, ...) for all CPUs
# (first statistics) and for each CPU (remaining statistics).
def get_cpu_statistics(path = '/proc/stat'):
	statistics = []
	with open(path, 'r') as stream:
		for line in stream:
			if line.startswith('cpu'):
				fields = [float(field) for field in line.split()[1:]]
				statistics.append(fields)
	return statistics

# Get CPU usage for each statistics.
def get_cpu_usage(old_statistics, new_statistics, include_all = True):
	statistics = [y - x for x, y in zip(old_statistics, new_statistics)]
	total = sum(statistics) if include_all else sum(statistics[0:4])
	idle = statistics[3]
	running = total - idle
	return 0 if total == 0 else running / total

def _parse_arguments():
	def non_negative_float(value):
		value = float(value)
		if value < 0:
			raise ValueError
		return value
	parser = ArgumentParser(
		add_help = False,
		description = "Get CPU usage.")
	parser.add_argument('-n', '--not-all',
		dest = 'include_all',
		action = 'store_false',
		default = True,
		help = "Only use time spent in user, nice, system and idle modes.")
	parser.add_argument('-f', '--format',
		metavar = "FORMAT",
		type = str,
		default = "%d%%\n",
		help = "Set output format. Default: %%d%%%%\\n.")
	parser.add_argument('-t', '--time',
		metavar = "TIME",
		dest = 'time',
		type = non_negative_float,
		default = 1.0,
		help = "Wait TIME seconds between samplings. Default: 1.")
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")
	return parser.parse_args()

def _main():
	arguments = _parse_arguments()
	old_statistics = get_cpu_statistics()[1:]
	time.sleep(arguments.time)
	new_statistics = get_cpu_statistics()[1:]
	statistics = zip(old_statistics, new_statistics)
	for old_statistics, new_statistics in statistics:
		usage = get_cpu_usage(old_statistics, new_statistics, arguments.include_all)
		sys.stdout.write(arguments.format % (100 * usage))

if __name__ == "__main__":
	import sys
	import time
	from argparse import ArgumentParser
	_main()
