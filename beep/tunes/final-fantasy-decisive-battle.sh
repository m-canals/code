#!/bin/sh

# The Decisive Battle from Final Fantasy VI
# BeepBeep v0.08 Songs

GN4="-f 392.00"
GS4="-f 415.30"
AN4="-f 440.00"
BN4="-f 493.88"
CN5="-f 523.25"
DN5="-f 587.33"
EN5="-f 659.26"
FN5="-f 698.46"
GN5="-f 783.99"
GS5="-f 830.61"
AN5="-f 880.00"
BN5="-f 987.77"
CN6="-f 1046.50"
DN6="-f 1174.66"
EN6="-f 1318.51"
FN6="-f 1396.91"
FS6="-f 1479.98"
GN6="-f 1567.98"
AN6="-f 1760.00"
CN7="-f 2093.00"

whole='-l 2048'
half_slide_eighth='-l 1280'
half_slide_sixteenth='-l 1152'
half='-l 1024'
dottedquarter='-l 768'
quarter='-l 512'
dottedeighth='-l 384'
eighth='-l 256'
sixteenth='-l 128'

sixteenth_rest='-D 128'
eighth_rest='-D 256'

beep \
	$sixteenth $EN6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $AN5 -n \
	$sixteenth $FN6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $AN5 -n \
	$sixteenth $FS6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $AN5 -n \
	$sixteenth $FN6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $AN5 -n \
	$sixteenth $EN6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $BN5 -n \
	$sixteenth $DN6 -n \
	$sixteenth $EN6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $AN5 -n \
	$sixteenth $FN6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $AN5 -n \
	$sixteenth $FS6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $AN5 -n \
	$sixteenth $FN6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $AN5 -n \
	$sixteenth $EN6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $BN5 -n \
	$sixteenth $DN6 -n \
	$sixteenth $EN6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $AN5 -n \
	$sixteenth $FN6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $AN5 -n \
	$sixteenth $FS6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $AN5 -n \
	$sixteenth $FN6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $AN5 -n \
	$sixteenth $EN6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $BN5 -n \
	$sixteenth $DN6 -n \
	$sixteenth $EN6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $AN5 -n \
	$sixteenth $FN6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $AN5 -n \
	$sixteenth $FS6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $AN5 -n \
	$sixteenth $FN6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $AN5 -n \
	$sixteenth $EN6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $BN5 -n \
	$sixteenth $DN6 -n \
	$eighth $AN5 -n \
	$eighth $EN6 -n \
	$sixteenth $DN6 -n \
	$eighth $CN6 -n \
	$eighth $BN5 -n \
	$sixteenth $CN6 -n \
	$eighth $DN6 -n \
	$eighth $BN5 -n \
	$eighth $GN5 -n \
	$quarter $AN4 -n \
	$quarter $BN4 -n \
	$quarter $CN5 -n \
	$quarter $EN5 -n \
	$eighth $AN5 -n \
	$eighth $EN6 -n \
	$sixteenth $DN6 -n \
	$eighth $CN6 -n \
	$eighth $BN5 -n \
	$sixteenth $CN6 -n \
	$eighth $DN6 -n \
	$eighth $BN5 -n \
	$eighth $GN5 -n \
	$eighth $AN4 $sixteenth_rest -n \
	$sixteenth $AN4 $eighth_rest -n \
	$eighth $AN4 $sixteenth_rest -n \
	$sixteenth $AN4 $eighth_rest -n \
	$eighth $AN4 -n \
	$eighth $GN4 -n \
	$eighth $AN4 $sixteenth_rest -n \
	$sixteenth $AN4 $eighth_rest -n \
	$eighth $AN4 $sixteenth_rest -n \
	$sixteenth $AN4 $eighth_rest -n \
	$eighth $AN4 -n \
	$eighth $GN4 -n \
	$sixteenth $EN5 -n \
	$sixteenth $AN5 -n \
	$sixteenth $BN5 -n \
	$sixteenth $CN6 -n \
	$sixteenth $BN5 -n \
	$sixteenth $AN5 -n \
	$sixteenth $EN5 -n \
	$sixteenth $CN5 -n \
	$eighth $BN4 -n \
	$sixteenth $CN5 -n \
	$eighth $AN4 -n \
	$sixteenth $CN5 -n \
	$sixteenth $BN4 -n \
	$sixteenth $CN5 -n \
	$eighth $DN5 -n \
	$sixteenth $EN5 -n \
	$eighth $CN5 -n \
	$sixteenth $EN5 -n \
	$sixteenth $DN5 -n \
	$sixteenth $EN5 -n \
	$sixteenth $FN5 -n \
	$sixteenth $BN4 -n \
	$sixteenth $CN5 -n \
	$sixteenth $FN5 -n \
	$sixteenth $AN5 -n \
	$sixteenth $DN5 -n \
	$sixteenth $GS5 -n \
	$sixteenth $AN5 -n \
	$sixteenth $AN5 -n \
	$sixteenth $EN5 -n \
	$sixteenth $CN6 -n \
	$sixteenth $BN5 -n \
	$sixteenth $AN5 -n \
	$sixteenth $EN5 -n \
	$sixteenth $CN5 -n \
	$sixteenth $AN4 -n \
	$eighth $BN4 -n \
	$sixteenth $CN5 -n \
	$eighth $AN4 -n \
	$sixteenth $CN5 -n \
	$sixteenth $BN4 -n \
	$sixteenth $CN5 -n \
	$eighth $DN5 -n \
	$sixteenth $EN5 -n \
	$eighth $CN5 -n \
	$sixteenth $EN5 -n \
	$sixteenth $DN5 -n \
	$sixteenth $EN5 -n \
	$sixteenth $FN5 -n \
	$sixteenth $DN5 -n \
	$sixteenth $GN5 -n \
	$sixteenth $EN5 -n \
	$sixteenth $AN5 -n \
	$sixteenth $FN5 -n \
	$sixteenth $BN5 -n \
	$sixteenth $GS5 -n \
	$dottedquarter $FN6 $supper_small -n \
	$eighth $CN6 $supper_small -n \
	$dottedeighth $BN5 $supper_small -n \
	$dottedeighth $CN6 $supper_small -n \
	$eighth $DN6 $supper_small -n \
	$dottedeighth $EN6 -n \
	$dottedeighth $FN6 -n \
	$eighth $GN6 -n \
	$quarter $CN6 -n \
	$dottedeighth $BN5 -n \
	$half $AN5 $super_smal -n \
	$dottedeighth $FN6 -n \
	$eighth $CN6 -n \
	$dottedeighth $BN5 -n \
	$dottedeighth $AN5 -n \
	$eighth $GN5 -n \
	$half_slide_eighth $AN5 -n \
	$eighth $BN5 -n \
	$eighth $CN6 -n \
	$eighth $EN6 -n \
	$dottedquarter $FN6 -n \
	$eighth $CN6 -n \
	$dottedeighth $BN5 -n \
	$dottedeighth $CN6 -n \
	$eighth $DN6 -n \
	$dottedeighth $EN6 -n \
	$dottedeighth $FN6 -n \
	$eighth $GN6 -n \
	$dottedeighth $FS6 -n \
	$dottedeighth $GN6 -n \
	$eighth $AN6 -n \
	$half_slide_sixteenth $CN7 -n \
	$sixteenth $AN6 -n \
	$sixteenth $GN6 -n \
	$sixteenth $FN6 -n \
	$sixteenth $CN6 -n \
	$sixteenth $AN5 -n \
	$sixteenth $GN5 -n \
	$sixteenth $FN5 -n \
	$whole $CN5 -n \
	$dottedeighth $BN4 -n \
	$dottedeighth $DN5 -n \
	$eighth $BN4 -n \
	$dottedeighth $DN5 -n \
	$dottedeighth $GN5 -n \
	$eighth $AN5 -n \
	$dottedeighth $EN5 -n \
	$dottedeighth $GN5 -n \
	$eighth $EN5 $small_space -n \
	$sixteenth $FN5 -n \
	$sixteenth $EN5 -n \
	$sixteenth $DN5 -n \
	$sixteenth $CN5 -n \
	$sixteenth $BN4 -n \
	$sixteenth $AN4 -n \
	$sixteenth $GS4 

