#!/bin/sh

# SQUAWK Space 1
# https://wiki.mikrotik.com/wiki/A_Bit_of_Sounds
# SQUAWK © 1987 Merlin R. Null.

for i in $(seq 1000 -20 40)
do beep -f $i -l 11 -D 11
done

