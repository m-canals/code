#!/bin/sh

# SQUAWK Chang
# https://wiki.mikrotik.com/wiki/A_Bit_of_Sounds
# SQUAWK © 1987 Merlin R. Null.

for j in $(seq 1 1 4)
do
	for i in $(seq 2000 -400 50)
	do beep -f $i -l 11 -D 11
	done
	for i in $(seq 800 400 2000)
	do beep -f $i -l 11 -D 11
	done
done

