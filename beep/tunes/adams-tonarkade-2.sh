#!/bin/sh

# Adams Tonarkade 1
# https://wiki.mikrotik.com/wiki/A_Bit_of_Sounds
# "Grafik, Animation, Simulation für Personalcomputer"
#  (ISBN 3890905722, Markt & Technik, 1989).

for i in $(seq 1250 -8 600)
do beep -f $i -l 11 -D 11
done

