#!/bin/sh

# Beepathon
# https://github.com/adamrees89/Beep-Songs/blob/master/Song%20Pack/beepathon

beep -f 2000 -n -f 1000 -n -f 500 -n -f 250 -n -f 125 -n -f 250 -n -f 500 -n -f 1000 -n -f 2000

