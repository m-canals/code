#!/bin/sh

# Adams Tonarkade 1
# https://wiki.mikrotik.com/wiki/A_Bit_of_Sounds
# "Grafik, Animation, Simulation für Personalcomputer"
#  (ISBN 3890905722, Markt & Technik, 1989).

for n in $(seq 1 1 8)
do
	for i in $(seq 600 8 750)
	do beep -f $i -l 11 -D 11
	done
done

