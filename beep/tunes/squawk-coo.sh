#!/bin/sh

# SQUAWK Coo
# https://wiki.mikrotik.com/wiki/A_Bit_of_Sounds
# SQUAWK © 1987 Merlin R. Null.

for i in $(seq 0 10 150)
do beep -f $((1295 - i)) -l 22 -D 22 -n -f $((1095 + i)) -l 22 -D 22
done

