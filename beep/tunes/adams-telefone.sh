#!/bin/sh

# Adams Telefone
# https://wiki.mikrotik.com/wiki/A_Bit_of_Sounds
# "Grafik, Animation, Simulation für Personalcomputer"
#  (ISBN 3890905722, Markt & Technik, 1989).

for n in $(seq 1 1 4)
do
	for i in $(seq 1 1 25)
	do beep -f 540 -l 33 -D 33 -n -f 650 -l 27 -D 27
	done
	sleep 2
done

