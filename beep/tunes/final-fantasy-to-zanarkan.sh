#!/bin/sh

# To Zanarkan from Final Fantasy X
# BeepBeep v0.08 Songs

GN3="-f 196.00"
BN3="-f 246.94"
CN4="-f 261.63"
DN4="-f 293.66"
EN4="-f 329.63"
FN4="-f 349.23"
FS4="-f 369.99"
GN4="-f 392.00"
AN4="-f 440.00"
BN4="-f 493.88"
CN5="-f 523.25"
CS5="-f 554.37"
DN5="-f 587.33"
EN5="-f 659.26"
FS5="-f 739.99"
GN5="-f 783.99"
GS5="-f 830.61"
AN5="-f 880.00"
BN5="-f 987.77"
CN6="-f 1046.50"
DN6="-f 1174.66"
DS6="-f 1244.51"
EN6="-f 1318.51"
FS6="-f 1479.98"
GN6="-f 1567.98"

dottedhalf='-l 1536'
half='-l 1024'
dottedquarter='-l 768'
quarter='-l 512'
eighth='-l 256'

beep \
	$eighth $EN5 -n \
	$eighth $EN4 -n \
	$eighth $GN4 -n \
	$eighth $BN4 -n \
	$eighth $EN5 -n \
	$eighth $FS5 -n \
	$dottedhalf $GN5 -n \
	$eighth $DN5 -n \
	$eighth $DN4 -n \
	$eighth $FS4 -n \
	$eighth $AN4 -n \
	$eighth $DN5 -n \
	$eighth $EN5 -n \
	$dottedhalf $FS5 -n \
	$quarter $BN4 -n \
	$quarter $BN4 -n \
	$quarter $BN4 -n \
	$quarter $BN4 -n \
	$dottedquarter $AN4 -n \
	$eighth $DN5 -n \
	$dottedhalf $GN4 -n \
	$quarter $EN5 -n \
	$quarter $EN5 -n \
	$quarter $EN5 -n \
	$quarter $EN5 -n \
	$dottedquarter $DN5 -n \
	$eighth $GN5 -n \
	$dottedhalf $CN5 -n \
	$eighth $FN4 -n \
	$eighth $CN4 -n \
	$eighth $FN4 -n \
	$eighth $GN4 -n \
	$eighth $AN4 -n \
	$eighth $CN5 -n \
	$dottedquarter $EN5 -n \
	$eighth $BN4 -n \
	$eighth $EN5 -n \
	$eighth $FS5 -n \
	$quarter $GN5 -n \
	$quarter $FS5 -n \
	$quarter $EN5 -n \
	$dottedquarter $DN5 -n \
	$eighth $EN5 -n \
	$quarter $DN5 -n \
	$dottedhalf $BN4 -n \
	$dottedquarter $EN5 -n \
	$eighth $BN4 -n \
	$eighth $EN5 -n \
	$eighth $FS5 -n \
	$quarter $GN5 -n \
	$quarter $FS5 -n \
	$quarter $GN5 -n \
	$dottedquarter $AN5 -n \
	$eighth $GN5 -n \
	$quarter $AN5 -n \
	$dottedhalf $BN5 -n \
	$dottedquarter $EN5 -n \
	$eighth $BN4 -n \
	$eighth $EN5 -n \
	$eighth $FS5 -n \
	$quarter $GN5 -n \
	$quarter $FS5 -n \
	$quarter $EN5 -n \
	$dottedquarter $DN5 -n \
	$eighth $EN5 -n \
	$quarter $DN5 -n \
	$half $BN4 -n \
	$eighth $GN4 -n \
	$eighth $AN4 -n \
	$quarter $BN4 -n \
	$quarter $BN4 -n \
	$quarter $BN4 -n \
	$quarter $BN4 -n \
	$quarter $AN4 -n \
	$quarter $DN5 -n \
	$dottedquarter $GN4 -n \
	$eighth $GN4 -n \
	$eighth $FS4 -n \
	$eighth $DN4 -n \
	$dottedhalf $EN4 -n \
	$eighth $GN4 -n \
	$eighth $GN3 -n \
	$eighth $BN3 -n \
	$eighth $EN4 -n \
	$eighth $GN4 -n \
	$eighth $BN4 -n \
	$half $AN5 -n \
	$quarter $EN6 -n \
	$quarter $EN6 -n \
	$quarter $DN6 -n \
	$quarter $CN6 -n \
	$half $BN5 -n \
	$quarter $GN6 -n \
	$dottedhalf $GN6 -n \
	$half $BN5 -n \
	$quarter $FS6 -n \
	$dottedhalf $FS6 -n \
	$half $AN5 -n \
	$quarter $EN6 -n \
	$quarter $EN6 -n \
	$half $DS6 -n \
	$half $BN4 -n \
	$quarter $FS5 -n \
	$quarter $FS5 -n \
	$quarter $EN5 -n \
	$quarter $DN5 -n \
	$half $CS5 -n \
	$quarter $AN5 -n \
	$dottedhalf $AN5 -n \
	$quarter $GS5 -n \
	$quarter $EN5 -n \
	$quarter $CS5 -n \
	$half $BN4 -n \
	$quarter $FS5 -n \
	$dottedhalf $AN4 -n \
	$eighth $FS4 -n \
	$quarter $AN4 -n \
	$half $BN4

