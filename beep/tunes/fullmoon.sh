#!/bin/sh

# https://www.kirrus.co.uk/2010/09/linux-beep-music/#comment-245007
# Fullmoon Amiga Demo Theme

LG=196
LA1=233.1
C=261.6
C1=277.2
D=293.7
D1=311.1
E=329.6
F=349.2
F1=370.0
G=392.0
G1=415.3
A=440.0
A1=466.2
B=493.9
C2=523.2
C22=554.3
D2=587.33
D12=622.2
E2=659.26
F2=698.46
F22=739.99
G2=783.99
G22=830.61
A2=880.00
A22=932.33
B2=987.77
C3=1046.50

beep -f $C -l 50 -n -f 20 -l 150 -n -f $C1 -l 50 -n -f 20 -l 150 -n -f $F1 -l 50 -n -f 20 -l 50 -n -f $G -l 50 -n -f 20 -l 150 -n -f $LA1 -l 50 -n -f 20 -l 50 -n -f $E -l 50 -n -f 20 -l 150 -n -f $LG -l 50 -n -f 20 -l 150 -n -f $C -l 50 -n -f 20 -l 50 -n -f $LA1 -l 50 -n -f 20 -l 50 -n -f $C1 -l 50 -n -f 20 -l 150 -n -f $C1 -l 50 -n -f 20 -l 150 -n -f $F1 -l 50 -n -f 20 -l 150 -n -f $G1 -l 50 -n -f 20 -l 50 -n -f $LA1 -l 50 -n -f 20 -l 150 -n -f $E -l 50 -n -f 20 -l 50 -n -f $G -l 50 -n -f 20 -l 150 -n -f $F -l 50 -n -f 20 -l 150 -n -f $LA1 -l 50 -n -f 20 -l 50 -n -f $C1 -l 50 -n -f $D -l 50 -n -f $C1 -l 50 -n -f 20 -l 150

