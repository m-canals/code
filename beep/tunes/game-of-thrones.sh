#!/bin/sh

# Game of Thrones Theme
# https://www.instructables.com/id/Game-Of-Thrones-Theme-on-Arduino/

F3=175
G3=196
GS3=208
AS3=233
C4=262
D4=294
DS4=311
E4=330
F4=349
G4=392
  
beep -f $G4 -l 500 -n -f $C4 -l 500 -n -f $DS4 -l 250 -n -f $F4 -l 250 -n -f $G4 -l 500 -n -f $C4 -l 500 -n -f $DS4 -l 250 -n -f $F4 -l 250 -n -f $G4 -l 500 -n -f $C4 -l 500 -n -f $DS4 -l 250 -n -f $F4 -l 250 -n -f $G4 -l 500 -n -f $C4 -l 500 -n -f $DS4 -l 250 -n -f $F4 -l 250 -n -f $G4 -l 500 -n -f $C4 -l 500 -n -f $E4 -l 250 -n -f $F4 -l 250 -n -f $G4 -l 500 -n -f $C4 -l 500 -n -f $E4 -l 250 -n -f $F4 -l 250 -n -f $G4 -l 500 -n -f $C4 -l 500 -n -f $E4 -l 250 -n -f $F4 -l 250 -n -f $G4 -l 500 -n -f $C4 -l 500 -n -f $E4 -l 250 -n -f $F4 -l 250 -n -f $G4 -l 500 -n -f $C4 -l 500 -n -f $DS4 -l 250 -n -f $F4 -l 250 -n -f $D4 -l 500 -n -f $G3 -l 500 -n -f $AS3 -l 250 -n -f $C4 -l 250 -n -f $D4 -l 500 -n -f $G3 -l 500 -n -f $AS3 -l 250 -n -f $C4 -l 250 -n -f $D4 -l 500 -n -f $G3 -l 500 -n -f $AS3 -l 250 -n -f $C4 -l 250 -n -f $D4 -l 500 -n -f $G3 -l 500 -n -f $AS3 -l 250 -n -f $C4 -l 250 -n -f $D4 -l 1000 -n -f $F4 -l 1000 -n -f $AS3 -l 1000 -n -f $DS4 -l 250 -n -f $D4 -l 250 -n -f $F4 -l 1000 -n -f $AS3 -l 1000 -n -f $DS4 -l 250 -n -f $D4 -l 250 -n -f $C4 -l 500 -n -f $GS3 -l 250 -n -f $AS3 -l 250 -n -f $C4 -l 500 -n -f $F3 -l 500 -n -f $GS3 -l 250 -n -f $AS3 -l 250 -n -f $C4 -l 500 -n -f $F3 -l 500 -n -f $GS3 -l 250 -n -f $AS3 -l 250 -n -f $C4 -l 500 -n -f $F3 -l 500 -n -f $G4 -l 1000 -n -f $C4 -l 1000 -n -f $DS4 -l 250 -n -f $F4 -l 250 -n -f $G4 -l 1000 -n -f $C4 -l 1000 -n -f $DS4 -l 250 -n -f $F4 -l 250 -n -f $D4 -l 500 -n -f $G3 -l 500 -n -f $AS3 -l 250 -n -f $C4 -l 250 -n -f $D4 -l 500 -n -f $G3 -l 500 -n -f $AS3 -l 250 -n -f $C4 -l 250 -n -f $D4 -l 500 -n -f $G3 -l 500 -n -f $AS3 -l 250 -n -f $C4 -l 250 -n -f $D4 -l 500 -n -f $G3 -l 500 -n -f $AS3 -l 250 -n -f $C4 -l 250 -n -f $D4 -l 500

