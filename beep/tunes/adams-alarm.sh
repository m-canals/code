#!/bin/sh

# Adams Larm
# https://wiki.mikrotik.com/wiki/A_Bit_of_Sounds
# "Grafik, Animation, Simulation für Personalcomputer"
#  (ISBN 3890905722, Markt & Technik, 1989).

for n in $(seq 1 1 10)
do
	for i in $(seq 300 40 1800)
	do beep -f $i -l 11 -D 11
	done
done

