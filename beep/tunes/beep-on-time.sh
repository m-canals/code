#!/bin/sh

# Beep on time
# BeepBeep v0.08 Songs

h=$(date +%I)
m=$(($(date +%M) / 15))

beep -D 500 -l 500 -r $h -f 440 -n -D 1000 -l 0 -n -D 800 -l 200 -f 880 -r $m

