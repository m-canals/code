#!/bin/sh

# SQUAWK Siren
# https://wiki.mikrotik.com/wiki/A_Bit_of_Sounds
# SQUAWK © 1987 Merlin R. Null.

for n in $(seq 1 1 3)
do beep -f 550 -l 494 -D 494 -n -f 400 -l 494 -D 494
done

