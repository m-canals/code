#!/bin/sh

# Adams Fanfare
# https://wiki.mikrotik.com/wiki/A_Bit_of_Sounds
# "Grafik, Animation, Simulation für Personalcomputer"
#  (ISBN 3890905722, Markt & Technik, 1989).

for i in $(seq 1200 -50 350)
do beep -f $i -l 33 -D 33
done

