#!/bin/sh

# SQUAWK Oh!
# https://wiki.mikrotik.com/wiki/A_Bit_of_Sounds
# SQUAWK © 1987 Merlin R. Null.

for i in $(seq 800 100 2000)
do beep -f $i -l 11 -D 11
done

for i in $(seq 2000 -100 50)
do beep -f $i -l 11 -D 11
done

