#!/bin/sh

# Deluge Torrent Completion
# http://ubuntuforums.org/showthread.php?t=1157670&page=4
# BeepBeep v0.08 Songs

for f in $(seq 21 40 3500)
do beep -f $f -d 20 -l 20
done

for f in $(seq 3500 -40 21)
do beep -f $f -d 20 -l 20
done

