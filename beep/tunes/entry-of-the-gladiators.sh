#!/bin/sh

# Entry of the Gladiators by Julius Fucik
# BeepBeep v0.08 Songs

CS4="-f 277.63"
DN4="-f 293.66"
DS4="-f 311.13"
Eb4="-f 311.13"
EN4="-f 329.63"
FN4="-f 349.23"
FS4="-f 369.99"
Gb4="-f 369.99"
GN4="-f 392.00"
GS4="-f 415.30"
Ab4="-f 415.30"
AN4="-f 440.00"
AS4="-f 466.16"
Bb4="-f 466.16"
BN4="-f 493.88"
CN5="-f 523.25"
CS5="-f 554.37"
Db5="-f 554.37"
DN5="-f 587.33"
DS5="-f 622.25"
Eb5="-f 622.25"
EN5="-f 659.26"
FN5="-f 698.46"
FS5="-f 739.99"
GN5="-f 783.99"
GS5="-f 830.61"
Ab5="-f 830.61"
AN5="-f 880.00"
Bb5="-f 932.33"
BN5="-f 987.77"
CN6="-f 1046.50"
CS6="-f 1108.73"
DN6="-f 1174.66"

dottedquarter='-l 768'
quarter='-l 512'
eighth='-l 256'
sixteenth='-l 128'

longspace='-D 128'

beep \
	$dottedquarter $EN5 -n \
	$sixteenth $EN5 -n \
	$sixteenth $EN5 -n \
	$dottedquarter $EN5 -n \
	$sixteenth $EN5 -n \
	$sixteenth $EN5 -n \
	$sixteenth $EN5 -n \
	$sixteenth $Eb5 -n \
	$sixteenth $DN5 -n \
	$sixteenth $Db5 -n \
	$sixteenth $CN5 -n \
	$sixteenth $BN4 -n \
	$sixteenth $Bb4 -n \
	$sixteenth $AN4 -n \
	$sixteenth $Ab4 -n \
	$sixteenth $GN4 -n \
	$sixteenth $Gb4 -n \
	$sixteenth $FN4 -n \
	$sixteenth $EN4 -n \
	$sixteenth $Eb4 -n \
	$sixteenth $DN4 -n \
	$sixteenth $CS4 -n \
	$dottedquarter $FS5 -n \
	$sixteenth $FS5 -n \
	$sixteenth $FS5 -n \
	$dottedquarter $FS5 -n \
	$sixteenth $FS5 -n \
	$sixteenth $FS5 -n \
	$sixteenth $FS5 -n \
	$sixteenth $FN5 -n \
	$sixteenth $EN5 -n \
	$sixteenth $Eb5 -n \
	$sixteenth $DN5 -n \
	$sixteenth $Db5 -n \
	$sixteenth $CN5 -n \
	$sixteenth $BN4 -n \
	$sixteenth $Bb4 -n \
	$sixteenth $AN4 -n \
	$sixteenth $Ab4 -n \
	$sixteenth $GN4 -n \
	$sixteenth $FS4 -n \
	$sixteenth $FN4 -n \
	$sixteenth $EN4 -n \
	$sixteenth $DS4 -n \
	$dottedquarter $GN5 -n \
	$sixteenth $GN5 -n \
	$sixteenth $GN5 -n \
	$dottedquarter $GN5 -n \
	$sixteenth $GN5 -n \
	$sixteenth $GN5 -n \
	$sixteenth $GN4 -n \
	$sixteenth $GS4 -n \
	$sixteenth $AN4 -n \
	$sixteenth $AS4 -n \
	$sixteenth $BN4 -n \
	$sixteenth $CN5 -n \
	$sixteenth $CS5 -n \
	$sixteenth $DN5 -n \
	$sixteenth $DS5 -n \
	$sixteenth $EN5 -n \
	$sixteenth $FN5 -n \
	$sixteenth $FS5 -n \
	$sixteenth $GN5 -n \
	$sixteenth $GS5 -n \
	$sixteenth $AN5 -n \
	$sixteenth $BN5 -n \
	$eighth $CN6 -n \
	$eighth $BN5 -n \
	$sixteenth $Bb5 -n \
	$sixteenth $BN5 -n \
	$sixteenth $Bb5 -n \
	$sixteenth $AN5 -n \
	$eighth $Ab5 -n \
	$eighth $GN5 -n \
	$eighth $FS5 -n \
	$eighth $GN5 -n \
	$eighth $AN5 -n \
	$eighth $Ab5 -n \
	$sixteenth $GN5 -n \
	$sixteenth $Ab5 -n \
	$sixteenth $GN5 -n \
	$sixteenth $FS5 -n \
	$eighth $FN5 -n \
	$eighth $EN5 -n \
	$eighth $DS5 -n \
	$eighth $EN5 -n \
	$sixteenth $GN5 $longspace -n \
	$sixteenth $FN5 -n \
	$sixteenth $FN5 -n \
	$eighth $CS5 -n \
	$eighth $DN5 -n \
	$sixteenth $GN5 $longspace -n \
	$sixteenth $FN5 -n \
	$sixteenth $FN5 -n \
	$eighth $CS5 -n \
	$eighth $DN5 -n \
	$sixteenth $BN4 -n \
	$sixteenth $CN5 -n \
	$sixteenth $CS5 -n \
	$sixteenth $DN5 -n \
	$sixteenth $DS5 -n \
	$sixteenth $EN5 -n \
	$sixteenth $FN5 -n \
	$sixteenth $FS5 -n \
	$sixteenth $GN5 -n \
	$sixteenth $GS5 -n \
	$sixteenth $AN5 -n \
	$sixteenth $BN5 -n \
	$eighth $AN5 -n \
	$eighth $GN5 -n \
	$eighth $CN6 -n \
	$eighth $BN5 -n \
	$sixteenth $Bb5 -n \
	$sixteenth $BN5 -n \
	$sixteenth $Bb5 -n \
	$sixteenth $AN5 -n \
	$eighth $Ab5 -n \
	$eighth $GN5 -n \
	$eighth $FS5 -n \
	$eighth $GN5 -n \
	$eighth $AN5 -n \
	$eighth $Ab5 -n \
	$sixteenth $GN5 -n \
	$sixteenth $Ab5 -n \
	$sixteenth $GN5 -n \
	$sixteenth $FS5 -n \
	$eighth $FN5 -n \
	$eighth $EN5 -n \
	$eighth $DS5 -n \
	$eighth $EN5 -n \
	$eighth $DS5 -n \
	$sixteenth $DS5 -n \
	$sixteenth $DS5 -n \
	$eighth $FS5 -n \
	$eighth $BN4 -n \
	$sixteenth $GN5 -n \
	$sixteenth $AN5 -n \
	$sixteenth $GN5 -n \
	$sixteenth $FS5 -n \
	$eighth $EN5 -n \
	$eighth $BN4 -n \
	$eighth $BN5 -n \
	$sixteenth $BN5 -n \
	$sixteenth $BN5 -n \
	$eighth $BN5 -n \
	$eighth $BN5 -n \
	$sixteenth $BN5 -n \
	$sixteenth $BN5 -n \
	$sixteenth $BN5 -n \
	$sixteenth $BN5 -n \
	$sixteenth $BN5 -n \
	$sixteenth $BN5 -n \
	$eighth $BN5 -n \
	$eighth $CN6 -n \
	$eighth $BN5 -n \
	$sixteenth $Bb5 -n \
	$sixteenth $BN5 -n \
	$sixteenth $Bb5 -n \
	$sixteenth $AN5 -n \
	$eighth $Ab5 -n \
	$eighth $GN5 -n \
	$eighth $FS5 -n \
	$eighth $GN5 -n \
	$eighth $AN5 -n \
	$eighth $Ab5 -n \
	$sixteenth $GN5 -n \
	$sixteenth $Ab5 -n \
	$sixteenth $GN5 -n \
	$sixteenth $FS5 -n \
	$eighth $FN5 -n \
	$eighth $EN5 -n \
	$eighth $DS5 -n \
	$eighth $EN5 -n \
	$sixteenth $GN5 $longspace -n \
	$sixteenth $FN5 -n \
	$sixteenth $FN5 -n \
	$eighth $CS5 -n \
	$eighth $DN5 -n \
	$sixteenth $GN5 $longspace -n \
	$sixteenth $FN5 -n \
	$sixteenth $FN5 -n \
	$eighth $CS5 -n \
	$eighth $DN5 -n \
	$sixteenth $BN4 -n \
	$sixteenth $CN5 -n \
	$sixteenth $CS5 -n \
	$sixteenth $DN5 -n \
	$sixteenth $DS5 -n \
	$sixteenth $EN5 -n \
	$sixteenth $FN5 -n \
	$sixteenth $FS5 -n \
	$sixteenth $GN5 -n \
	$sixteenth $GS5 -n \
	$sixteenth $AN5 -n \
	$sixteenth $BN5 -n \
	$eighth $AN5 -n \
	$eighth $GN5 -n \
	$eighth $CN6 -n \
	$eighth $BN5 -n \
	$sixteenth $Bb5 -n \
	$sixteenth $BN5 -n \
	$sixteenth $Bb5 -n \
	$sixteenth $AN5 -n \
	$eighth $Ab5 -n \
	$eighth $GN5 -n \
	$eighth $FS5 -n \
	$eighth $GN5 -n \
	$eighth $AN5 -n \
	$eighth $Ab5 -n \
	$sixteenth $GN5 -n \
	$sixteenth $Ab5 -n \
	$sixteenth $GN5 -n \
	$sixteenth $FS5 -n \
	$eighth $FN5 -n \
	$eighth $EN5 -n \
	$eighth $DS5 -n \
	$eighth $EN5 -n \
	$eighth $DN5 -n \
	$sixteenth $DN5 -n \
	$sixteenth $DN5 -n \
	$eighth $AN5 -n \
	$eighth $DN5 -n \
	$eighth $Eb5 -n \
	$sixteenth $Eb5 -n \
	$sixteenth $Eb5 -n \
	$quarter $Bb5 -n \
	$sixteenth $BN5 -n \
	$sixteenth $DN6 -n \
	$sixteenth $CS6 -n \
	$sixteenth $CN6 -n \
	$eighth $BN5 -n \
	$eighth $AN5 -n \
	$eighth $GN5

