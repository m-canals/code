#!/bin/sh

# Final countdown
# BeepBeep v0.08 Songs

FN5="-f 698.46"
GS5="-f 830.61"
AS5="-f 932.33"
CN6="-f 1046.50"
CS6="-f 1108.73"

half='-l 1024'
dottedeighth='-l 384'
eighth='-l 256'
sixteenth='-l 128'

beep \
	$half $FN5 -n \
	$sixteenth $GS5 -n \
	$sixteenth $AS5 -n \
	$dottedeighth $CN6 -n \
	$half $FN5 -n \
	$eighth $AS5 -n \
	$eighth $CN6 -n \
	$eighth $CS6 -n \
	$eighth $CN6 -n \
	$half $AS5 -n \
	$sixteenth $AS5 -n \
	$sixteenth $CN6 -n \
	$dottedeighth $CS6 -n \
	$half $FN5 -n \
	$eighth $GS5 -n \
	$eighth $AS5 -n \
	$eighth $CN6 -n \
	$eighth $AS5 -n \
	$eighth $GS5 -n \
	$eighth $AS5

