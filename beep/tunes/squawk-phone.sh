#!/bin/sh

# SQUAWK Phone
# https://wiki.mikrotik.com/wiki/A_Bit_of_Sounds
# SQUAWK © 1987 Merlin R. Null.

for n in $(seq 1 1 10)
do beep -f 1195 -l 22 -D 22 -n -f 2571 -l 22 -D 22
done

