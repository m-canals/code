#!/usr/bin/env python
#
# https://github.com/tomas-teijeiro/wavtobeep
#
# ffmpeg -i input.mp3 output.wav
#

from scipy.io import wavfile
import numpy
import sys

def print_beep_command(data):

	sys.stdout.write('beep')
	first = True
	
	for msec, freq in data:
	
		if (first):
			first = False
		else:
			sys.stdout.write(' -n')
			
		sys.stdout.write(' -l {0} -f {1}'.format(msec,freq))
	
	sys.stdout.write('\n')


def print_arduino_tone(data):

	for msec, freq in data:
	
		sys.stdout.write('tone(4, {0}, {1});\n'.format(freq, msec))
		sys.stdout.write('delay({0});\n'.format(msec))

def frequency(note, octave):

	return (440.0 * numpy.exp(((octave - 4) + (note - 10) / 12.0) * numpy.log(2)))

#Accepted frequencies
FREQS = numpy.array([1] + [frequency(n,o) for o in xrange(10) for n in xrange(1,13)])

#Maximum permitted length of the processed data (in seconds)
MAX_LEN = 40

#Resolution of the time window for frequency analysis.
CH_MS = 25

fs, data = wavfile.read("/dev/stdin")
data = data[:MAX_LEN * fs]

#Window size and overlap definition for the spectral analysis
w = int(fs/1000.0 * CH_MS)
overlap = w/2

#Number of chunks with the selected parameters
n = len(data)/(w-overlap)

#Duration of each beep
dur = int(1000.0*(len(data)/float(fs))/n)

#Array of frequencies
freq = numpy.arange(0,fs/2.0,(fs/2.0)/(w/2))

#We truncate the data array to be a multiplo of the step
data = data[:n*(w-overlap)]
blw = numpy.blackman(w)
freql = []
for i in xrange(0, len(data), w-overlap):
	chunk = data[i:i+w]
	if len(chunk) != w:
		chunk = chunk.copy()
		chunk.resize(w)
	chunk = chunk * blw
	chunk = chunk-numpy.mean(chunk)
	ft = numpy.fft.fft(chunk)
	ft = ft[:w/2]
	#Get the most representative frequency of the chunk
	hz = freq[numpy.absolute(ft).argmax()]
	#We check the frequency table to get the closest
	hz = FREQS[numpy.abs(FREQS-hz).argmin()]
	if freql and freql[-1][1] == hz:
		freql[-1] = (freql[-1][0]+dur, hz)
	else:
		freql.append((dur, hz))

print_beep_command(freql)
print_arduino_tone(freql)
