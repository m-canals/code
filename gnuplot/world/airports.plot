#!/usr/bin/env gnuplot
# Author: Hagen Wierstorf
# GNUPLOT Version: 5.2 (patchlevel 2)

width = 1680
height = width / 2
point_scale_factor = width / 4.2

set terminal pngcairo size width, height enhanced
set output 'airports.png'
set bmargin 0
set lmargin 0
set rmargin 0
set tmargin 0
set border 0 linewidth 0
set style line 2 linecolor rgb 'black' linetype 1 linewidth 1
set style line 3 linecolor rgb 'red' pointtype 7 pointsize variable
unset key
unset tics
set xrange [-180:180]
set yrange [-60:74]

pointsize(pagerank) = point_scale_factor * pagerank

# Contorn dels continents, contorn de mars interiors i punts.
plot 'world-110m.data'   with filledcurves linecolor rgb 'gray',  \
     'world-110m.data'   with lines linestyle 2, \
     'world-110m-2.data' with filledcurves linecolor rgb 'white', \
     'world-110m-2.data' with lines linestyle 2, \
     'airports.data' using 1:2:(pointsize($3)) with points linestyle 3  

