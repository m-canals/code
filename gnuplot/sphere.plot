#!/usr/bin/env gnuplot

unset key
unset tics
unset border

set parametric
set urange [-pi : pi]
set vrange [-pi : pi]
set isosamples 25, 25
set hidden3d

# Radius.
r = 1

# Sphere function.
fx(u, v) = r * cos(u) * cos(v)
fy(u, v) = r * cos(u) * sin(v)
fz(u) = r * sin(u)

set arrow from 0, 0, 0 to r / sqrt(2), 0, r / sqrt(2) front nohead

splot fx(u, v), fy(u, v), fz(u)

pause mouse close
