#!/usr/bin/env gnuplot

set dgrid3d splines
set pm3d map

$data << EOF
0 0 1
0 1 1
1 0 1
1 1 1
0.5 0.5 0
EOF

splot $data using 1:2:3

pause mouse close
