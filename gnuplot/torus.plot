#!/usr/bin/env gnuplot

unset key
unset tics
unset border

set parametric
set urange [-pi : pi]
set vrange [-pi : pi]
set isosamples 50, 50
set hidden3d

# Minor and major radius.
r = 0.5
R = 1

# Torus function.
fx(u, v) = (R + r * cos(v)) * cos(u)
fy(u, v) = (R + r * cos(v)) * sin(u)
fz(u) = r * sin(u)

splot fx(u, v), fy(u, v), fz(v)

pause mouse close
