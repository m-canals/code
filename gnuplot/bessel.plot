#!/usr/bin/env gnuplot

output = 'bessel.gif'

set terminal pngcairo

unset key
unset border
unset tics
set palette rgb 3,9,9
set lmargin at screen 0.05
set bmargin at screen 0
set rmargin at screen 0.95
set tmargin at screen 1

set parametric

# Bessel function, which is moving in time.

bessel(x, t) = besj0(x) * cos(2 * pi * t)

# Calculate the zeros for the bessel function (see Watson,
# A Treatise on the Theory of Bessel Functions, 1966, page 505).

n = 6 # Number of zeros.
k = (n * pi - pi / 4.0)
u_0 = k + 1 / (8 * k) - 31 / (384 * k) ** 3 + 3779 / (15360 * k) ** 5

set urange [0 : u_0]
set vrange [0 : 1.5 * pi]
set cbrange [-1 : 1]
set zrange [-1 : 1]

set isosamples 200, 100
set pm3d depthorder
set view 40, 200

directory = system('mktemp --directory')

do for [t = 0 : 50] {
	set output sprintf('%s/%02d', directory, t)
	splot u * sin(v), u * cos(v), bessel(u, t / 50.0) with pm3d
}

system(sprintf('convert %s/* gif:%s', directory, output))
