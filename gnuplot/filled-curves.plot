#!/usr/bin/env gnuplot

unset key
unset tics
unset border
set size square

# Alternativament, podem fer servir la declaració global:
#
# set style function filledcurves ...
# set style fill ...

set multiplot layout 2,5

$data << EOF
0.5 0.5
0 0
0 1
1 1
1 0
EOF
plot $data using 1:2 with filledcurves closed \
linetype 1 fill transparent pattern 1 border

plot x**2 with filledcurves x1 \
linetype 2 fill transparent pattern 2 border

plot x**2 with filledcurves x2 \
linetype 3 fill transparent pattern 3 border

plot x**2 with filledcurves y=50 \
linetype 4 fill transparent pattern 4 border

plot x**2 with filledcurves above y=50 \
linetype 5 fill transparent pattern 5 border

plot x**2 with filledcurves below y=50 \
linetype 6 fill transparent pattern 6 border

plot x**2 with filledcurves xy=0,50 \
linetype 7 fill transparent pattern 7 border

set parametric
plot cos(t), sin(t) with filledcurves \
linetype 8 fill transparent pattern 8 border
unset parametric

set polar
unset raxis
plot sin(6 * t) + 2 with filledcurves above r=0.5 \
linetype 9 fill transparent solid 1.0 noborder
unset polar

set polar
unset raxis
plot  sin(6 * t) + 2  with filledcurves below r=4 \
linetype 10 fill transparent solid 0.5 noborder
unset polar

unset multiplot

pause mouse close
