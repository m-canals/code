#!/usr/bin/env gnuplot

# Tangent line function.
l(x, p) = f(p) + d(p) * (x - p)

# Parabolic function.
f(x) = 1 * x ** 2
d(x) = 2 * x

# Point of tangency.
p = -10

set label 1 at p, f(p) " (p, f(p))" point pointtype 7 front
k = abs(p)
plot [-k : k] f(x), l(x, p)

pause mouse close
