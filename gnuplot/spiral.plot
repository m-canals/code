#!/usr/bin/env gnuplot
# TODO
#
#  - Calculate data directly.

output = 'spiral.gif'

set terminal pngcairo

unset key
unset border
unset tics

set style line 1 lc rgb '#0060ad' lt 1 lw 2 pt 7 ps 2
set view 342, 0

set xrange [-1 : 1]
set yrange [0 : 20]
set zrange [-1 : 1]

directory = system('mktemp --directory')
n = 0

# Upwards.
do for [ii = 1 : 99] {
    n = n + 1
    set output sprintf('%s/spiral%03d.png', directory, n)
    splot 'spiral.data' every ::1::ii w l ls 1, \
          'spiral.data' every ::ii::ii w p ls 1
}

# Downwards.
do for [ii = 99 : 1 : -1] {
    n = n + 1
    set output sprintf('%s/spiral%03d', directory, n)
    splot 'spiral.data' every ::1::ii w l ls 1, \
          'spiral.data' every ::ii::ii w p ls 1
}

system(sprintf('convert %s/* gif:%s', directory, output))
