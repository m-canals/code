#!/usr/bin/env gnuplot

e = 1 # Amplitude (E)
b = 1 # Amplitude (B)
l = 5 # Wavelength
f = 1 # Frequency
p = 0 # Phase
t = 1 # Time

k = 2 * pi / l
w = 2 * pi * f
E(x) =   e * sin(k * x - w * t + p)
B(x) = - b * sin(k * x - w * t + p)

splot '+' using 1:(0):(E($1)) title 'E(t)' with lines, \
      '+' using 1:(B($1)):(0) title 'B(t)' with lines

pause mouse close
