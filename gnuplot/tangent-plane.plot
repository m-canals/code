#!/usr/bin/env gnuplot

# Hide non-visible parts.
set hidden3d 

max(a, b) = a > b ? a : b

# Tangent plane function.
p(x, y, a, b) = dx(a, b) * (x - a) + dy(a, b) * (y - b) + f(a, b)

# Hyperbolic paraboloid function.
f(x, y) = x ** 2 / 1 - y ** 2 / 1
dx(x, y) = 2 * x / 1
dy(x, y) = - 2 * y / 1

# Point of tangency.
a = 10
b = -5

set label 1 at a, b, f(a, b) " (a, b, f(a, b))" point pointtype 7 front
k = max(abs(a), abs(b))
splot [-k : k] [-k : k] f(x, y), p(x, y, a, b)

pause mouse close
