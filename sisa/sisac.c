// SISA Compiler
// ===================================================================
//
// A line follows the following syntax:
//
//  separator* instruction (separator+ argument)* separator* comment?
// 
// Where a separator is any combination of " ,()\t\r\n"
// and a comment is any text that begins with ';'.
// Immediate arguments are limited to INT_MIN and INT_MAX.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define ARG_MAX 3

struct argument {
	int min;
	int max;
	int mask;
	int position;
};

struct instruction {
	char * name;
	int code;
	size_t arguments_size;
	struct argument arguments[ARG_MAX];
};

// Instruction Syntax
// ===================================================================
//
// and    Rd, Ra, Rb | addi   Rd, Ra, N6 | in     Rd, N8
// or     Rd, Ra, Rb |                   | out    N8, Ra
// xor    Rd, Ra, Rb | ld     Rd, N6(Ra) |
// not    Rd, Ra     | st     N6(Ra), Rb |
// add    Rd, Ra, Rb | ldb    Rd, N6(Ra) |
// sub    Rd, Ra, Rb | stb    N6(Ra), Rb |
// sha    Rd, Ra, Rb |                   |
// shl    Rd, Ra, Rb | jalr   Rd, Ra     |
//                   |                   |
// cmplt  Rd, Ra, Rb | bz     Ra, N8     |
// cmple  Rd, Ra, Rb | bnz    Ra, N8     |
// cmpeq  Rd, Ra, Rb |                   |
// cmpltu Rd, Ra, Rb | movi   Rd, N8     |
// cmpleu Rd, Ra, Rb | movhi  Rd, N8     |

const struct instruction instructions[] = {
	{"AND",    0x0000, 3, {{   0,   7,   7,   3}, {   0,   7,   7,   9}, {   0,   7,   7,   6}}},
	{"OR",     0x0001, 3, {{   0,   7,   7,   3}, {   0,   7,   7,   9}, {   0,   7,   7,   6}}},
	{"XOR",    0x0002, 3, {{   0,   7,   7,   3}, {   0,   7,   7,   9}, {   0,   7,   7,   6}}},
	{"NOT",    0x0003, 2, {{   0,   7,   7,   3}, {   0,   7,   7,   9}                       }},
	{"ADD",    0x0004, 3, {{   0,   7,   7,   3}, {   0,   7,   7,   9}, {   0,   7,   7,   6}}},
	{"SUB",    0x0005, 3, {{   0,   7,   7,   3}, {   0,   7,   7,   9}, {   0,   7,   7,   6}}},
	{"SHA",    0x0006, 3, {{   0,   7,   7,   3}, {   0,   7,   7,   9}, {   0,   7,   7,   6}}},
	{"SHL",    0x0007, 3, {{   0,   7,   7,   3}, {   0,   7,   7,   9}, {   0,   7,   7,   6}}},
	{"CMPLT",  0x1000, 3, {{   0,   7,   7,   3}, {   0,   7,   7,   9}, {   0,   7,   7,   6}}},
	{"CMPLE",  0x1001, 3, {{   0,   7,   7,   3}, {   0,   7,   7,   9}, {   0,   7,   7,   6}}},
	{"CMPEQ",  0x1003, 3, {{   0,   7,   7,   3}, {   0,   7,   7,   9}, {   0,   7,   7,   6}}},
	{"CMPLTU", 0x1004, 3, {{   0,   7,   7,   3}, {   0,   7,   7,   9}, {   0,   7,   7,   6}}},
	{"CMPLEU", 0x1005, 3, {{   0,   7,   7,   3}, {   0,   7,   7,   9}, {   0,   7,   7,   6}}},
	{"ADDI",   0x2000, 3, {{   0,   7,   7,   6}, {   0,   7,   7,   9}, { -32,  31,  63,   0}}},
	{"LD",     0x3000, 3, {{   0,   7,   7,   6}, { -32,  31,  63,   0}, {   0,   7,   7,   9}}},
	{"ST",     0x4000, 3, {{ -32,  31,  63,   0}, {   0,   7,   7,   9}, {   0,   7,   7,   6}}},
	{"LDB",    0x5000, 3, {{   0,   7,   7,   6}, { -32,  31,  63,   0}, {   0,   7,   7,   9}}},
	{"STB",    0x6000, 3, {{ -32,  31,  63,   0}, {   0,   7,   7,   9}, {   0,   7,   7,   6}}},
	{"JALR",   0x7000, 2, {{   0,   7,   7,   6}, {   0,   7,   7,   9}                       }},
	{"BZ",     0x8000, 2, {{   0,   7,   7,   9}, {-128, 127, 255,   0}                       }},
	{"BNZ",    0x8100, 2, {{   0,   7,   7,   9}, {-128, 127, 255,   0}                       }},
	{"MOVI",   0x9000, 2, {{   0,   7,   7,   9}, {-128, 127, 255,   0}                       }},
	{"MOVHI",  0x9100, 2, {{   0,   7,   7,   9}, {-128, 127, 255,   0}                       }},
	{"IN",     0xA000, 2, {{   0,   7,   7,   9}, {-128, 127, 255,   0}                       }},
	{"OUT",    0xA100, 2, {{-128, 127, 255,   0}, {   0,   7,   7,   9}                       }}
};

const size_t instructions_size = sizeof (instructions) / sizeof (struct instruction);

// Case-insensitive match.
int match(const char * a, const char * b) {
	while (*a && *b) {
		if (*a != *b && toupper(*a) != toupper(*b)) {
			return 0;
		}
		
		a++;
		b++;
	}

	return ! (*a || *b);
}

const struct instruction * search_instruction_by_name(
		const struct instruction * instructions,
		size_t size,
		const char * name) {
	size_t i;
		
	for (i = 0; i < size; i++) {    
		if (match(instructions[i].name, name)) {
			return &instructions[i];
		}
	}
	
	return NULL;
}

int is_type(const char * string, int (* function)(int)) {
	if (! *string) {
		return 0;
	}
	
	while (*string) {
		if (! (*function)(*string)) {
			return 0;
		}
		
		string++;
	}
	
	return 1;
}

int is_register(const char * string) {
	return 'R' == toupper(string[0])
	    && '0' <= string[1]
	    && '7' >= string[1]
	    &&  0  == string[2];
}

int is_ximmediate(const char * string) {
	return '0' == string[0]
	    && 'X' == toupper(string[1])
	    && is_type(&string[2], isxdigit);
}

int is_immediate(const char * string) {
	if (string[0] == '-') {
		string++;
	}
	
	return is_type(string, isdigit);
}

int argument_overflows(int n, const struct argument * argument) {
	return n < argument->min || n > argument->max;
}

int xargument_overflows(int n, const struct argument * argument) {
	return n > argument->mask;
}

const char field_separator[] = " ,()\t\r\n";
const char comment_delimiter[] = ";";

int parse_argument(char * string, const struct argument * argument, int * data) {
	int n;
	char * format;
	char * strptr;
	int (* overflows)(int, const struct argument *);
	
	if (is_register(string)) {
		strptr = string + 1;
		format = "%d";
		overflows = argument_overflows;
	} else if (is_immediate(string)) {
		strptr = string;
		format = "%d";
		overflows = argument_overflows;
	} else if (is_ximmediate(string)) {
		strptr = string;
		format = "%x";
		overflows = xargument_overflows;
	} else {
		fprintf(stderr, "Invalid argument: %s\n", string);
		return 1;
	}

	sscanf(strptr, format, &n);

	if (overflows(n, argument)) {
		fprintf(stderr, "Immediate argument overflows: %s\n", string);
		return 1;
	}

	n &= argument->mask;
	n <<= argument->position;
	*data |= n;

	return 0;
}

int parse_line(char * line, int * data) {
	char * saveptr;
	char * token;
	
	token = strtok_r(line, field_separator, &saveptr);
	
	// Ignore empty lines.
	if (token == NULL) {
		return 1;
	}
		
	// Ignore lines which are only comments.
	if (token[0] == comment_delimiter[0]) {
		return 1;
	}

	size_t length;
	char * saveptr2;
	char * token2;

	// Thus far, it is a non-empty line which is not only a comment,
	// so token2 is not null.
	length = strlen(token);
	token2 = strtok_r(token, comment_delimiter, &saveptr2);

	// Is it a instruction?
	const struct instruction * instruction = search_instruction_by_name(
		instructions, instructions_size, token2);

	if (instruction == NULL) {
		fprintf(stderr, "Undefined instruction: %s\n", token2);
		return 1;
	}
	
	* data = instruction->code;

	size_t argument = 0;
	int error = 0;

	// There's not any comment, so there may be arguments.
	if (strlen(token2) == length) {
		while ((token = strtok_r(NULL, field_separator, &saveptr)) != NULL) {
			// A comment begins at the beginning.
			if (token[0] == comment_delimiter[0]) {
				break;
			}

			// If a comment begins immediatly after an argument
			// it just looks at the argument, which is not null.
			length = strlen(token);
			token2 = strtok_r(token, comment_delimiter, &saveptr2);

			// Are there too many arguments?
			if (argument >= instruction->arguments_size) {
				if (argument == instruction->arguments_size) {
					fprintf(stderr, "Too many arguments: %s\n", instruction->name);
				}
				
				fprintf(stderr, "Unexpected argument: %s\n", token);
				error = 1;
			} else if (parse_argument(token2, &instruction->arguments[argument], data) != 0) {
				error = 1;
			}
			
			argument += 1;
   
			// A comment begins somewhere.
			if (strlen(token2) < length) {
				break;
			}
		}
	}
	
	// Are there enough arguments?
	if (argument < instruction->arguments_size) {
		fprintf(stderr, "Not enough arguments: %s\n", instruction->name);
		return 1;
	}

	return error;
}

enum output_method {
	BINARY,
	TEXT
};

void write_binary_output(FILE * file, int encoded, char * format) {
	fwrite(&encoded, 2, 1, stdout);
}

void write_text_output(FILE * file, int encoded, char * format) {
	fprintf(file, format, encoded);
}

struct input {
	FILE * file;
};

struct output {
	FILE * file;
	enum output_method method;
	char * format;
};

int parse(struct input * input, struct output * output) {
	void (* write_output_data)(FILE *, int, char *);
	
	switch (output->method) {
		case BINARY:
			write_output_data = write_binary_output;
			break;
		case TEXT:
			write_output_data = write_text_output;
			break;
	}

	int error = 0;
	char * line = NULL;
	size_t length = 0;
	
	while (getline(&line, &length, input->file) != EOF) {
		int data;
		
		if (parse_line(line, &data) == 0) {
			write_output_data(output->file, data, output->format);
		} else {
			error = 1;
		}
	}
	
	free(line);
	
	return error;
}

int main(int argc, char ** argv) {
	struct input input;

	input.file = stdin;
	
	struct output output;
	
	output.file = stdout;
	output.method = BINARY;
	output.format = "0x%04x\n";

	return parse(&input, &output);
}
