Simple Instruction Set Architecture (SISA) Compiler and Interpreter
===================================================================

Compile SISA code and run it.

Usage
-----

	make && ./sisac < code > code.x && ./sisai code.x
