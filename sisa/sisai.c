// SISA Interpreter
// ===================================================================

#include <stdio.h>

#define KEY_STATUS   0x00
#define KEY_DATA     0x01
#define PRINT_STATUS 0x02
#define PRINT_DATA   0x03

typedef int word;

#define WORD_SIZE 16
#define WORD_MAX  0xFFFF

word sign_extension(word n, int size) {
	if (n & (1 << (size - 1))) {
		n |= -1 << size;
	} else {
		n &= WORD_MAX;
	}

	return n;
}

word pc = 0;
word regfile[0x8];
word memory[0x10000];

void function_and(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	size_t b = (instruction >> 6) & 0x0007;
	size_t d = (instruction >> 3) & 0x0007;
	
	regfile[d] = sign_extension(regfile[a] & regfile[b], WORD_SIZE);
}

void function_or(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	size_t b = (instruction >> 6) & 0x0007;
	size_t d = (instruction >> 3) & 0x0007;
	
	regfile[d] = sign_extension(regfile[a] | regfile[b], WORD_SIZE);
}

void function_xor(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	size_t b = (instruction >> 6) & 0x0007;
	size_t d = (instruction >> 3) & 0x0007;
	
	regfile[d] = sign_extension(regfile[a] ^ regfile[b], WORD_SIZE);
}

void function_not(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	size_t d = (instruction >> 3) & 0x0007;
	
	regfile[d] = sign_extension(~ regfile[a], WORD_SIZE);
}

void function_add(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	size_t b = (instruction >> 6) & 0x0007;
	size_t d = (instruction >> 3) & 0x0007;
	
	regfile[d] = sign_extension(regfile[a] + regfile[b], WORD_SIZE);
}

void function_sub(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	size_t b = (instruction >> 6) & 0x0007;
	size_t d = (instruction >> 3) & 0x0007;
	
	regfile[d] = sign_extension(regfile[a] - regfile[b], WORD_SIZE);
}

void function_sha(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	size_t b = (instruction >> 6) & 0x0007;
	size_t d = (instruction >> 3) & 0x0007;
	
	int sign = regfile[a] & (1 << (WORD_SIZE - 1));
	int n = sign_extension(regfile[b], 5);

	if (n < 0) {
		n = -n;
		regfile[d] = regfile[a] >> n;
		
		if (sign) { // Negative
			regfile[d] |= -1 << (WORD_SIZE - n);
		}
	} else {
		regfile[d] |= sign | (regfile[a] << n) & (WORD_MAX >> 1);
	}
	
	regfile[d] = sign_extension(regfile[d], WORD_SIZE);
}

void function_shl(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	size_t b = (instruction >> 6) & 0x0007;
	size_t d = (instruction >> 3) & 0x0007;
	
	int n = sign_extension(regfile[b], 5);

	if (n < 0) {
		regfile[d] = regfile[a] >> -n;
	} else {
		regfile[d] = regfile[a] << n;
	}
	
	regfile[d] = sign_extension(regfile[d], WORD_SIZE);
}

void function_cmplt(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	size_t b = (instruction >> 6) & 0x0007;
	size_t d = (instruction >> 3) & 0x0007;
	
	regfile[d] = regfile[a] < regfile[b];
}

void function_cmple(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	size_t b = (instruction >> 6) & 0x0007;
	size_t d = (instruction >> 3) & 0x0007;
	
	regfile[d] = regfile[a] <= regfile[b];
}

void function_cmpeq(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	size_t b = (instruction >> 6) & 0x0007;
	size_t d = (instruction >> 3) & 0x0007;
	
	regfile[d] = regfile[a] == regfile[b];
}

void function_cmpltu(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	size_t b = (instruction >> 6) & 0x0007;
	size_t d = (instruction >> 3) & 0x0007;
	
	regfile[d] = (unsigned int) regfile[a] < (unsigned int) regfile[b];
}

void function_cmpleu(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	size_t b = (instruction >> 6) & 0x0007;
	size_t d = (instruction >> 3) & 0x0007;
	
	regfile[d] = (unsigned int) regfile[a] <= (unsigned int) regfile[b];
}

void function_addi(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	size_t d = (instruction >> 6) & 0x0007;
	int    n = (instruction >> 0) & 0x003F;
	
	regfile[d] = sign_extension(
		regfile[a] + sign_extension(n, 6), WORD_SIZE);
}

void function_ld(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	size_t d = (instruction >> 6) & 0x0007;
	int    n = (instruction >> 0) & 0x003F;
	
	word address = (regfile[a] + sign_extension(n, 6)) & WORD_MAX;
	regfile[d] = sign_extension(memory[address], WORD_SIZE);
}

void function_st(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	size_t b = (instruction >> 6) & 0x0007;
	int    n = (instruction >> 0) & 0x003F;
	
	word address = (regfile[a] + sign_extension(n, 6)) & WORD_MAX;
	memory[address] = regfile[b];
}

void function_ldb(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	size_t d = (instruction >> 6) & 0x0007;
	int    n = (instruction >> 0) & 0x003F;
	
	word address = (regfile[a] + sign_extension(n, 6)) & WORD_MAX;
	regfile[d] = sign_extension(memory[address] >> (address % 2), 8);
}

void function_stb(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	size_t b = (instruction >> 6) & 0x0007;
	int    n = (instruction >> 0) & 0x003F;
	
	word address = (regfile[a] + sign_extension(n, 6)) & WORD_MAX;
	memory[address] = ((regfile[b] & 0xFF) << ((address % 2 == 1) * 8)) |
		(memory[address] & (0xFF << ((address % 2 == 0) * 8)));
}

void function_jalr(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	size_t d = (instruction >> 6) & 0x0007;
}

void function_bz(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	int    n = (instruction >> 0) & 0x00FF;
	
	if (! regfile[a]) {
		pc += sign_extension(n, 8);
	}
}

void function_bnz(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	int    n = (instruction >> 0) & 0x00FF;
	
	if (regfile[a]) {
		pc += sign_extension(n, 8);
	}
}

void function_movi(word instruction) {
	size_t d = (instruction >> 9) & 0x0007;
	int    n = (instruction >> 0) & 0x00FF;
	
	regfile[d] = sign_extension(n, 8);
}

void function_movhi(word instruction) {
	size_t d = (instruction >> 9) & 0x0007;
	int    n = (instruction >> 0) & 0x00FF;
	
	regfile[d] = sign_extension(n << 8 |
		(regfile[d] & 0x00FF), WORD_SIZE);
}

void function_in(word instruction)
{
	size_t d = (instruction >> 9) & 0x0007;
	int    n = (instruction >> 0) & 0x00FF;
	
	if (n == KEY_STATUS || n == PRINT_STATUS) {
		regfile[d] = 1;
	} else if (n == KEY_DATA) {
		printf("Input: ");
		
		int done = 0;
		
		while (! done) {
			if (scanf("%d", &regfile[d]) == 0) {
				printf("Not a number\n");
			} else if ((unsigned int) regfile[d] > WORD_MAX) {
				printf("Out of range\n");
			} else {
				done = 1;
			}
		}
	}
}

void function_out(word instruction) {
	size_t a = (instruction >> 9) & 0x0007;
	int    n = (instruction >> 0) & 0x00FF;
	
	printf("Output: %d\n", regfile[a]);
}

struct instruction {
	word id_mask;
	word id;
	void (* function)(word);
};

const struct instruction instructions[] = {
	{0xF007, 0x0000, function_and},
	{0xF007, 0x0001, function_or},
	{0xF007, 0x0002, function_xor},
	{0xF007, 0x0003, function_not},
	{0xF007, 0x0004, function_add},
	{0xF007, 0x0005, function_sub},
	{0xF007, 0x0006, function_sha},
	{0xF007, 0x0007, function_shl},
	{0xF007, 0x1000, function_cmplt},
	{0xF007, 0x1001, function_cmple},
	{0xF007, 0x1003, function_cmpeq},
	{0xF007, 0x1004, function_cmpltu},
	{0xF007, 0x1005, function_cmpleu},
	{0xF000, 0x2000, function_addi},
	{0xF000, 0x3000, function_ld},
	{0xF000, 0x4000, function_st},
	{0xF000, 0x5000, function_ldb},
	{0xF000, 0x6000, function_stb},
	{0xF000, 0x7000, function_jalr},
	{0xF100, 0x8000, function_bz},
	{0xF100, 0x8100, function_bnz},
	{0xF100, 0x9000, function_movi},
	{0xF100, 0x9100, function_movhi},
	{0xF100, 0xA000, function_in},
	{0xF100, 0xA100, function_out}
};

const size_t instructions_size = sizeof(instructions) / sizeof(struct instruction);

size_t load_memory(const char * filename, word * memory) {
	FILE * file = fopen(filename, "r");
	
	if (file == NULL) {
		return 0;
	}

	size_t offset = 0;

	while (fread(memory+offset, 2, 1, file)) {
		offset += 1;
	}

	fclose(file);
	
	return offset;
}

int main(int argc, char ** argv) {
	if (argc < 2) {
		printf("Usage: %s FILE\n", argv[0]);
		return 1;
	}
	
	char * filename = argv[1];
	size_t read_words = load_memory(filename, memory);
	
	printf("%zu words read\n", read_words);
	
	int halt = 0;
	
	while (! halt) {
		word instruction = memory[pc];
		pc = pc + 1;

		for (size_t i = 0; i < instructions_size; i++) {
			if ((instruction & instructions[i].id_mask) == instructions[i].id)
			{
				printf("Next instruction: %zu (0x%04x) @%d\n", i, instruction, pc-1);
				(*instructions[i].function)(instruction);
				break;
			}
		}
		
		if (pc == read_words) {
			break;
		}
	}

	return 0;
}
