class SquareGridGraphEdgeType
{
	constructor(graph)
	{
		this._graph = graph;
	}

	_add(vertex)
	{
		vertex += this._offset;
		
		const bit = vertex % 4;
		const byte = (vertex - bit) / 4;
		
		this._graph._array[byte] |= (1 << bit) << this._shift;
	}
	
	_has(vertex)
	{
		vertex += this._offset;
		
		const bit = vertex % 4;
		const byte = (vertex - bit) / 4;
		
		return this._graph._array[byte] & ((1 << bit) << this._shift);
	}
	
	_remove(vertex)
	{
		vertex += this._offset;
		
		const bit = vertex % 4;
		const byte = (vertex - bit) / 4;
		
		this._graph._array[byte] &= ~ ((1 << bit) << this._shift);
	}
}

class SquareGridGraphTopEdgeType extends SquareGridGraphEdgeType
{
	get _offset() { return -this._graph._outerWidth; }
	get _shift() { return 4; }
	get x() { return 0; }
	get y() { return -1; }
}

class SquareGridGraphRightEdgeType extends SquareGridGraphEdgeType
{
	get _offset() { return 0; }
	get _shift() { return 0; }
	get x() { return 1; }
	get y() { return 0; }
}

class SquareGridGraphBottomEdgeType extends SquareGridGraphEdgeType
{
	get _offset() { return 0; }
	get _shift() { return 4; }
	get x() { return 0; }
	get y() { return 1; }
}

class SquareGridGraphLeftEdgeType extends SquareGridGraphEdgeType
{
	get _offset() { return -1; }
	get _shift() { return 0; }
	get x() { return -1; }
	get y() { return 0; }
}

class SquareGridGraph
{
	// Given two positive integers, width and height, create a square grid
	// graph of that size.
	constructor(width, height)
	{
		this._width = width;
		this._height = height;
		this._size = this._width * this._height;
		this._outerWidth = this._width + 2;
		this._outerHeight = this._height + 2;
		this._outerSize = this._outerWidth * this._outerHeight;
		this._arraySize = 1 + (this._outerSize - this._outerSize % 4) / 4;
		this._array = new Uint8Array(this._arraySize);
		this._TopEdge = new SquareGridGraphTopEdgeType(this);
		this._RightEdge = new SquareGridGraphRightEdgeType(this);
		this._BottomEdge = new SquareGridGraphBottomEdgeType(this);
		this._LeftEdge = new SquareGridGraphLeftEdgeType(this);
	}
	
	// Get width, height and size.
	
	get width()
	{
		return this._width;
	}
	
	get height()
	{
		return this._height;
	}
	
	get size()
	{
		return this._size;
	}
	
	// Get edge types for this graph. Edge types have `x` and `y`
	// properties.
	
	get TopEdge()
	{
		return this._TopEdge;
	}
	
	get RightEdge()
	{
		return this._RightEdge;
	}
	
	get BottomEdge()
	{
		return this._BottomEdge;
	}
	
	get LeftEdge()
	{
		return this._LeftEdge;
	}

	// Given a vertex position (x, y), get the vertex at that position.
	vertex(x, y)
	{
		return this._outerWidth * (y + 1) + (x + 1);
	}
	
	// Given a vertex and an edge type, add the edge between that vertex
	// and the vertex adjacent throught that edge type. If it already
	// exists, do nothing.
	addEdge(vertex, edgeType)
	{
		edgeType._add(vertex)
	}
	
	// Given a vertex and an edge type, remove the edge between that
	// vertex and the vertex adjacent throught that edge type. If it
	// already exists, do nothing.
	removeEdge(vertex, edgeType)
	{
		edgeType._remove(vertex)
	}

	// Given a vertex, get an incident edge type or none if its degree is
	// zero.
	incidentEdge(vertex)
	{
		     if (this._TopEdge._has(vertex))    return this._TopEdge;
		else if (this._RightEdge._has(vertex))  return this._RightEdge;
		else if (this._BottomEdge._has(vertex)) return this._BottomEdge;
		else if (this._LeftEdge._has(vertex))   return this._LeftEdge;
		else                                    return null;
	}
	
	// Given a vertex and an edge type, return the vertex adjacent through
	// that edge type.
	adjacentVertex(vertex, edgeType)
	{
		return vertex + edgeType.x + edgeType.y * this._outerWidth;
	}
}

// =====================================================================

class Walk
{
	// Given a pair of coordinates, create a walk beginning at those
	// coordinates.
	constructor(x, y)
	{
		this._X = [x]
		this._Y = [y];
		this._length = 1;
	}

	// Get the number of vertices of this walk.
	get size()
	{
		return this._length;
	}

	// Given an index of a vertex, get the x coordinate of that vertex. 
	x(i)
	{
		return this._X[i];
	}
	
	// Given an index of a vertex, get the y coordinate of that vertex.
	y(i)
	{
		return this._Y[i];
	}
	
	// Get a `Path2D` object containing this walk as a line path.
	get path2D()
	{
		const path = new Path2D();

		path.moveTo(this._X[0], this._Y[0]);
		
		for (let i = 1; i < this._length; i++) {
			path.lineTo(this._X[i], this._Y[i]);
		}
		
		return path;
	}
	
	// Get an SVG path data string containing this walk as a line path.
	get pathSVG()
	{
		let path = `M ${this._X[0]} ${this._Y[0]}`;

		for (let i = 1; i < this._length; i++) {
			path += ` l ${this._X[i]} ${this._Y[i]}`;
		}
		
		return path;
	}
	
	// Given a pair of offsets, add a vertex whose coordinates are those
	// offsets added to the coordinates of the last added vertex.
	addEdge(Dx, Dy)
	{
		this._X.push(this._X[this._length - 1] + Dx);
		this._Y.push(this._Y[this._length - 1] + Dy);
		this._length += 1;
	}
}

// =====================================================================

class WalkCollection
{
	// Create a collection of walks.
	constructor()
	{
		this._walks = new Array();
	}
	
	// Get the number of walks in this collection.
	get size()
	{
		return this._walks.length;
	}
	
	// Get a generator of the walks in this collection.
	* walks()
	{
		for (const walk of this._walks) {
			yield walk;
		}
	}

	// Get a `Path2D` object containing the walks in this collection as
	// line paths.
	get path2D()
	{
		const path = new Path2D();
		
		for (const walk of this._walks) {
			path.addPath(walk.path2D);
		}
		
		return path;
	}
	
	// Get an SVG path data string containing the walks in this collection
	// as line paths.
	get pathSVG()
	{
		let path = new String();
		
		for (const walk of this._walks) {
			path += path.length > 0 ? ` ${walk.pathSVG}` : `${walk.pathSVG}`;
		}
		
		return path;
	}
	
	// Add a walk to this collection.
	addWalk(walk)
	{
		this._walks.push(walk);
	}
}
