Style.defineDynamicViewportVariables()

window.addEventListener('resize', Style.defineDynamicViewportVariables);

window.addEventListener('load', event => {
	const message = document.querySelector('#allow-script-message');
	
	message.style.display = 'none';
	new Application(Images);
});

