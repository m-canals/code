class Application
{
	constructor(imageData)
	{
		this.book = new ApplicationBook(this, imageData);
		this.page = new ApplicationPage(this);		
		this.page.open();
		this.page.draw(new Image());
	}
}
