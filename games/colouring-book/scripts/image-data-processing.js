// We assume points in clipping paths are top-left corners of pixels,
// since it looks like Firefox and Chrome behave like this. This should
// be ascertained.

class ImageDataProcessing
{
	// Given a function mapping RGBA values to boolean values, apply it to
	// each pixel and return an array containing returned values.
	static boolean(imageData,
		value = (r, g, b, a) => a < 255 || Math.max(r, g, b) > 0)
	{
		const data = imageData.data;
		const boolean = new Array(data.length);
		
		for (let i = 0; i < data.length; i += 4) {
			boolean[i / 4] = value(
				data[i], data[i + 1], data[i + 2], data[i + 3]);
		}
		
		return boolean;
	}
	
	static ITU601 = [0.2990, 0.5870, 0.1140];
	static ITU709 = [0.2126, 0.7152, 0.0722];
	
	// Given RGB coefficients, convert RGB values to grayscale values.
	//
	// https://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale
	//
	static grayscale(imageData, [r, g, b] = ImageDataProcessing.ITU601)
	{
		const data = imageData.data;
		
		for (let i = 0; i < data.length; i += 4) {
			const l = r * data[i] + g * data[i + 1] + b * data[i + 2];
			
			data[i] = l;
			data[i + 1] = l;
			data[i + 2] = l;
		}
	}
	
	// Given RGB thresholds, convert RGB values to 3-bit values. On each
	// channel, assign black if its value is below its threshold,
	// otherwise, white.
	//
	// https://en.wikipedia.org/wiki/Thresholding_(image_processing)
	//
	static threshold(imageData, r, g = r, b = g)
	{
		const data = imageData.data;
		
		for (let i = 0; i < data.length; i += 4) {
			data[i] = data[i] < r ? 0 : 255;
			data[i + 1] = data[i + 1] < g ? 0 : 255;
			data[i + 2] = data[i + 2] < b ? 0 : 255;
		}
	}

	// Given an array mapping pixel indices to `true` (interior) or `false`
	// (exterior), return a square grid graph of edges located between 
	// pixels belonging to different regions.
	//
	// Note that:
	//
	//  - Graph vertices are top-left vertices of pixels.
	//
	//  - There is an additional column of vertices for right vertices of
	//    pixels from the last column and an additional row of vertices
	//    for bottom vertices of pixels from the last row.
	//
	//  - Pixels outside the image belong to the exterior.
	//
	static edges(imageData,
		interior = ImageDataProcessing.boolean(imageData))
	{ 
		const width = imageData.width;
		const height = imageData.height;
		const graph = new SquareGridGraph(width + 1, height + 1);

		for (let y = 0; y < graph.height; y++) {
			for (let x = 0; x < graph.width; x++) {
				const index = width * y + x;
				const vertex = graph.vertex(x, y);
				const group = x < width && y < height && interior[index];
				const topGroup = x < width && y > 0 && interior[index - width];
				const leftGroup = x > 0 && y < height && interior[index - 1];
				
				if (group != topGroup) {
					graph.addEdge(vertex, graph.RightEdge);
				}

				if (group != leftGroup) {
					graph.addEdge(vertex, graph.BottomEdge);
				}
			}
		}
		
		return graph;
	}

	// Given an array mapping pixel indices to `true` (interior) or
	// `false` (exterior), return an array containing disjoint boundaries
	// of edges located between pixels belonging to different regions.
	// 
	// Note that:
	//
	//  - Boundary vertices are top-left vertices of pixels.
	//
	//  - There is an additional column of vertices for right vertices of
	//    pixels from the last column and an additional row of vertices
	//    for bottom vertices of pixels from the last row.
	//
	//  - Pixels outside the image belong to the exterior.
	//
	//     ░░░░░░░░░░
	//     ░░░░░█░░░░
	//     ░░░░█░░░░░
	//     ░░░░░█░░░░
	//     ░░░░░░░░░░
	//
	//  - A boundary is a closed walk or circuit, a sequence of distinct
	//    edges beginning and ending at the same vertex that can contain
	//    cycles, like in the example.
	//
	//  - An array of boundaries can not be minimal nor maximal, since it
	//    can contain boundaries that can be merged, like in the example,
	//    which produces two boundaries, one around the top black
	//    rectangle and another one around the remaining ones, when it
	//    could produce one or three boundaries.
	//
	static boundaries(imageData,
		interior = ImageDataProcessing.boolean(imageData))
	{
		const graph = ImageDataProcessing.edges(imageData, interior);
		const boundaries = new Array();

		for (let y = 0; y < graph.height; y++) {
			for (let x = 0; x < graph.width; x++) {
				let vertex = graph.vertex(x, y);
				let edge = graph.incidentEdge(vertex);
				
				if (edge) {
					const boundary = new Walk(x, y);

					do {
						boundary.addEdge(edge.x, edge.y);
						graph.removeEdge(vertex, edge);
						vertex = graph.adjacentVertex(vertex, edge);
						edge = graph.incidentEdge(vertex)
					} while (edge);
					
					boundaries.push(boundary);
				}
			}
		}
		
		return boundaries;
	}

	// Given an array mapping pixel indices to `true` (interior) or
	// `false` (exterior), return an array mapping each index of a pixel
	// to the boundaries of its region.
	//
	// Note that:
	//
	// 	- Pixels outside the image belong to the exterior.
	//
	//  - The path of each boundary is an even-odd clipping path.
	//
	static regions(imageData,
		interior = ImageDataProcessing.boolean(imageData))
	{
		const data = imageData.data;
		const width = imageData.width;
		const height = imageData.height;
		const regions = new Array(width * height).fill(null);
		const boundaries = ImageDataProcessing.boundaries(
			imageData, interior);
		
		// Since two distinct boundaries cannot begin at the same vertex,
		// otherwise they would have been merged, we can use their starting
		// vertices as keys.

		const boundaryMap = new Map();
		
		for (const boundary of boundaries) {
			const index = boundary.y(0) * (width + 1) + boundary.x(0);
			boundaryMap.set(index, boundary);
		}

		function addAdjacentBoundaries(region, x, y)
		{
			for (let i = 0; i < 2; i++) {
				for (let j = 0; j < 2; j++) {
					const index = (width + 1) * (y + i) + (x + j);
			
					if (boundaryMap.has(index)) {
						region.addWalk(boundaryMap.get(index));
						boundaryMap.delete(index);
					}
				}
			}
		}
		
		function addAdjacentPixels(stack, i)
		{
			const j = i - width;
			const k = i + width;
			
			stack.push(j, j + 1, i + 1, k + 1, k, k - 1, i - 1, j - 1);
		}
		
		function valid(x, y)
		{
			return x >= 0 && x < width && y >= 0 && y < height;
		}
		
		const nullRegion = new WalkCollection();

		for (let i = 0; i < regions.length; i++) {
			if (regions[i] == null) {
				if (interior[i]) {
					const region = new WalkCollection();
					const stack = [i];
					
					while (stack.length > 0) {
						const i = stack.pop();
						const x = i % width;
						const y = (i - x) / width;
							
						if (valid(x, y) && regions[i] == null && interior[i]) {
							regions[i] = region;
							addAdjacentBoundaries(region, x, y);
							addAdjacentPixels(stack, i);
						}
					}
				} else {
					regions[i] = nullRegion;
				}
			}
		}

		return regions;
	}
}
