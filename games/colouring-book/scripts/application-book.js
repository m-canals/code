class ApplicationBook extends ApplicationContent
{
	constructor(application, imageData)
	{
		super(application, document.querySelector('#book'));
		
		this.imageData = imageData
		this.elements = {
			imageTemplate: document.querySelector('#book-image-template'),
			imageList: document.querySelector('#book-images'),
			scrollBackButton: document.querySelector('#book-scroll-back-button'),
			scrollForthButton: document.querySelector('#book-scroll-forth-button'),
			closeButton: document.querySelector("#book-close-button")
		};

		this.addImages()
		this.addButtons();
	}

	addImages()
	{
		const content = this.elements.imageTemplate.content;
		
		for (const data of this.imageData) {
			const fragment = content.cloneNode(true);
			const image = fragment.children[0].children[0];
			
			image.addEventListener('load', this.handleImageLoad.bind(this));
			image.src = data;
			
			this.elements.imageList.appendChild(fragment);
		}
	}
	
	addButtons()
	{
		this.elements.scrollBackButton.addEventListener('click',
			this.handleScrollBackClick.bind(this));
		this.elements.scrollForthButton.addEventListener('click',
			this.handleScrollForthClick.bind(this));
		this.elements.closeButton.addEventListener('click',
			this.handleOpenPageClick.bind(this));
	}
	
	handleImageLoad(event)
	{
		event.target.addEventListener('click',
			this.handleImageClick.bind(this));
	}

	handleImageClick(event)
	{
		this.close();
		this.application.page.open();
		this.application.page.draw(event.target);
	}

	handleOpenPageClick(event)
	{
		this.close();
		this.application.page.open();
	}
	
	handleScrollBackClick(event)
	{
		const element = this.elements.imageList;
		const style = window.getComputedStyle(element);
		
		if (style.overflowX == "scroll")
			element.scrollLeft -= parseFloat(style.width);
		else
			element.scrollTop -= parseFloat(style.height);
	}
	
	handleScrollForthClick(event)
	{
		const element = this.elements.imageList;
		const style = window.getComputedStyle(element);
		
		if (style.overflowX == "scroll")
			element.scrollLeft += parseFloat(style.width);
		else
			element.scrollTop += parseFloat(style.height);
	}
}
