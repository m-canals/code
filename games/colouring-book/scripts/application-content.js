class ApplicationContent
{
	constructor(application, element)
	{
		this.application = application;
		this.element = element;
	}
	
	close()
	{
		this.element.classList.remove('open');
	}
	
	open()
	{
		this.element.classList.add('open');
	}
}
