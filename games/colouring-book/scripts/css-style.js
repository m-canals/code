class Style
{
	static resizeToBoundingRectangle(element,
		elementWidth, elementHeight,
		rectangleWidth, rectangleHeight)
	{
		const elementRatio = elementWidth / elementHeight;
		const boxRatio = rectangleWidth / rectangleHeight;
		const width = rectangleHeight * Math.min(elementRatio, boxRatio);
		const height = rectangleWidth / Math.max(elementRatio, boxRatio);
		
		element.style.setProperty('--width', `${width}px`);
		element.style.setProperty('--height', `${height}px`);
	}

	static resizeObjectToFitParent(element)
	{
		const rectangle = element.parentElement.getBoundingClientRect();

		Style.resizeToBoundingRectangle(element,
			element.width, element.height,
			rectangle.width, rectangle.height);
	}
	
	static divideGrid(element,
		itemCount = element.children.length,
		itemRatio = 1)
	{
		const rectangle = element.getBoundingClientRect();
		const ratio = rectangle.width / rectangle.height;
		
		// rows * colums >= itemCount 
		// (columns / ratio * itemRatio) * columns >= itemCount
		// columns >= sqrt(itemCount * ratio / itemRatio) =>
		// columns >= ceil(sqrt(itemCount * ratio / itemRatio))

		const columns = Math.ceil(Math.sqrt(itemCount * ratio / itemRatio));
		const rows = Math.ceil(itemCount / columns);

		element.style.setProperty('--rows', rows);
		element.style.setProperty('--columns', columns);
	}

	static defineDynamicViewportVariables()
	{
		const root = document.documentElement;
		const dvw = root.clientWidth / 100;
		const dvh = root.clientHeight / 100;
		const dvmin = Math.min(dvw, dvh);
		const dvmax = Math.max(dvw, dvh);
		
		root.style.setProperty('--1dvw', `${dvw}px`);
		root.style.setProperty('--1dvh', `${dvh}px`);
		root.style.setProperty('--1dvmin', `${dvmin}px`);
		root.style.setProperty('--1dvmax', `${dvmax}px`);
	}
}
