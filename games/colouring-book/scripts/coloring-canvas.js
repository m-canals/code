// Depends: boolean-clipping/clipping.js.

// If we use compositing, we need to compute a region on every touch start
// or compute and store all regions on initialization, so we may need to
// compute or store too much. Therefore, we use clipping, so we only need
// to compute and store regions on initialization, which should be more
// efficient.

class ColoringCanvas
{
	constructor(canvas)
	{
		this.canvas = canvas;
		this.context = this.canvas.getContext("2d");
		this.canvas.width = 0;
		this.canvas.height = 0;
		this.regions = null;
		this.controller = null;
		
		this.path = null;
		this.touchedPaths = new Map();
		this.touchedRegions = new Map();
		
		this.size = 0.2;
		this.opacity = 1.0;
		this.color = 'black'
		this.radius = null;
		
		this.canvas.addEventListener('mousedown',
			this.handleMouseDown.bind(this));
		this.canvas.addEventListener('touchstart',
			this.handleTouchStart.bind(this));
		this.canvas.addEventListener('touchmove',
			this.handleTouchMove.bind(this));
		this.canvas.addEventListener('touchend',
			this.handleTouchEnd.bind(this));
	}

	setColor(color)
	{
		this.color = color;
		this.applyColor()
	}
	
	setOpacity(opacity)
	{
		this.opacity = opacity;
		this.applyOpacity()
	}
	
	setSize(size)
	{
		this.size = size;
		this.applySize()
	}

	fill(position, path)
	{
		this.context.save();
		this.context.clip(path, "evenodd");
		this.context.beginPath();
		this.context.arc(position.x, position.y, this.radius, 0, 2 * Math.PI);
		this.context.fill();
		this.context.restore();
	}
	
	// Draw an image onto the canvas. Optional arguments are:
	//
	//  - Canvas size (width, height)
	//
	//     Two numbers. By default, it is that of the image.
	//
	//  - Canvas size scale factor
	//
	//     A number. By default, it is the minimum one needed to contain
	//     the image. It is used to scale up the canvas instead of scaling
	//     down the image (lowering its resolution).
	//
	//  - Image translation distances (x, y)
	//
	//     Two numbers. By default, they are those needed to center the
	//     image.
	//
	draw(image,
		width = image.width,
		height = image.height,
		scale = Math.max(1, image.width / width, image.height / height),
		x = (parseInt(width * scale) - image.width) / 2,
		y = (parseInt(height * scale) - image.height) / 2)
	{
		this.canvas.width = parseInt(width * scale);
		this.canvas.height = parseInt(height * scale);
		this.context.drawImage(image, x, y);

		const imageData = this.context.getImageData(0, 0,
			this.canvas.width, this.canvas.height);
		
		//ImageDataProcessing.grayscale(imageData);
		//ImageDataProcessing.threshold(imageData, 128);

		console.time('main');
		this.regions = ImageDataProcessing.regions(imageData);
		console.timeEnd('main');
		console.log(this.regions)
		const R = new Set(this.regions);
		const S = Array.from(R).map(W => Math.min(...[...W.walks()].map(w => w.size)));
		const s = Math.min(...S); // should always be 5
		
		console.log(this.canvas.width, this.canvas.height, R.size, s)

		this.context.putImageData(imageData, 0, 0);

		this.applySize()
		this.applyOpacity()
		this.applyColor()
	}
	
	getEventPosition(event)
	{
		const x = event.clientX;
		const y = event.clientY;
		const rect = event.target.getBoundingClientRect();

		return {
			x: Math.floor((x - rect.left) / rect.width * this.canvas.width),
			y: Math.floor((y - rect.top) / rect.height * this.canvas.height)
		};
	}
	
	getRegion(position)
	{
		const index = this.canvas.width * position.y + position.x;
		const region = this.regions[index];
		
		return region;
	}

	// ---------------------------------------------------------------------

	applyColor()
	{
		this.context.fillStyle = this.color;
	}
	
	applyOpacity()
	{
		this.context.globalAlpha = this.opacity;
	}
	
	applySize()
	{
		this.radius = this.size * 0.25 * Math.min(
			this.canvas.width, this.canvas.height);
	}
	
	// ---------------------------------------------------------------------

	handleMouseDown(event)
	{
		const leftButton = event.button == 0;
		
		if (leftButton && this.controller == null && this.regions != null) {
			const position = this.getEventPosition(event);
			const index = this.canvas.width * position.y + position.x;
			const region = this.regions[index];
			const path = region.path2D;
			const controller = new AbortController();
			
			// Paths are computed in real time, but it doesn't look like an
			// issue. If it was one, they could be cached in `Walk` and
			// `Region` classes.

			this.path = region.path2D;
			this.controller = controller;
			this.fill(position, this.path);
	
			this.canvas.addEventListener('mousemove',
				this.handleMouseMove.bind(this), {signal: controller.signal});
			window.addEventListener('mouseup',
				this.handleMouseUp.bind(this), {signal: controller.signal});
		}
	}
	
	handleMouseMove(event)
	{
		const position = this.getEventPosition(event);
		
		this.fill(position, this.path);
	}

	handleMouseUp(event)
	{
		const leftButton = event.button == 0;
		
		if (leftButton) {
			this.controller.abort();
			this.controller = null;
		}
	}
	
	// ---------------------------------------------------------------------
	
	handleTouchStart(event)
	{
		if (this.regions != null) {
			for (const touch of event.changedTouches) {
				const position = this.getEventPosition(touch);
				const region = this.getRegion(position);

				if (region != null) {
					const path = region.path2D;
					this.touchedPaths.set(touch.identifier, path);
					this.touchedRegions.set(touch.identifier, region)
					this.fill(position, path);
				}
			}
		}
		
		event.preventDefault();
	}
	
	handleTouchMove(event)
	{
		for (const touch of event.changedTouches) {
			const position = this.getEventPosition(touch);
			const region = this.getRegion(position);
			
			if (region == this.touchedRegions.get(touch.identifier)) {
				const path = this.touchedPaths.get(touch.identifier);
				this.fill(position, path);
			}
		}
		
		event.preventDefault();
	}
	
	handleTouchEnd(event)
	{
		for (const touch of event.changedTouches) {
			this.touchedPaths.remove(touch.identifier);
			this.touchedRegions.remove(touch.identifier);
		}
	}
	
	// ---------------------------------------------------------------------
}
