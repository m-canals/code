class ApplicationPage extends ApplicationContent
{
	constructor(application)
	{
		super(application, document.querySelector('#page'));
		
		this.elements = {
			image: document.querySelector("#page-image"),
			canvas: document.querySelector("#page-image-canvas"),
			size: document.querySelector('#page-size'),
			opacity: document.querySelector('#page-opacity'),
			color: document.querySelector('#page-color-template'),
			colors: document.querySelector('#page-colors'),
			openBook: document.querySelector('#open-book')
		};
		
		this.canvas = new ColoringCanvas(this.elements.canvas);
		
		this.addColors();
		this.addEventListeners();
		this.setCanvasProperties()
	}
	
	open()
	{
		super.open()
		this.handleResize() // Closed elements miss resize events.
	}
	
	draw(image)
	{
		this.canvas.draw(image,
			this.elements.image.clientWidth,
			this.elements.image.clientHeight);
		
		// If this method is called after having opened the page,
		// the ratio of the canvas may differ.
		Style.resizeObjectToFitParent(this.elements.canvas);
	}
	
	addColors()
	{
		const content = this.elements.color.content;
		const values = [0, 128, 255];
		
		for (const r of values) {
			for (const g of values) {
				for (const b of values) {
					const value = `${r},${g},${b}`;
					const fragment = content.cloneNode(true);
					const label = fragment.children[0];
					const input = label.children[0];
					
					label.style.setProperty('--color', value);
					input.addEventListener('input', this.handleColorChange.bind(this));
					input.value = value;
					input.checked = r + g + b == 0;
					
					this.elements.colors.appendChild(fragment);
				}
			}
		}
	}
	
	addEventListeners()
	{
		window.addEventListener('resize',
			this.handleResize.bind(this));
		this.elements.openBook.addEventListener('click',
			this.handleOpenBookClick.bind(this));
		this.elements.size.addEventListener('input',
			this.handleSizeChange.bind(this));
		this.elements.opacity.addEventListener('input',
			this.handleOpacityChange.bind(this));
	}
	
	handleOpenBookClick(event)
	{
		this.close();
		this.application.book.open();
	}
	
	handleSizeChange(event)
	{
		this.canvas.setSize(event.target.value);
		this.elements.colors.style.setProperty('--size',
			event.target.value);
	}
	
	handleOpacityChange(event)
	{
		this.canvas.setOpacity(event.target.value);
		this.elements.colors.style.setProperty('--opacity',
			event.target.value);
	}
	
	handleColorChange(event)
	{
		this.canvas.setColor(`rgb(${event.target.value})`);
	}

	handleResize(event)
	{
		Style.divideGrid(this.elements.colors);
		Style.resizeObjectToFitParent(this.elements.canvas);
	}
	
	setCanvasProperties()
	{
		this.elements.size.dispatchEvent(new Event("input"));
		this.elements.opacity.dispatchEvent(new Event("input"));
		this.elements.colors.querySelectorAll('input:checked').forEach(
			input => input.dispatchEvent(new Event('input')));
	}
}
