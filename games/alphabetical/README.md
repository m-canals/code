Alphabetical
============

Implementation of a single-player final round of the game.

![Screenshot of the game.](screenshot.png "Screenshot of the game.")

Usage
-----

Open `index.html` in a web browser.
