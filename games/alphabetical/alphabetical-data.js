class Data {
	constructor(letter, answer, question) {
		this.letter = letter;
		this.answer = answer;
		this.question = question;
	}
}

data = [
	new Data('A', "Aneto", "Quina muntanya dels Pirineus rep el sobrenom de La Maleïda?"),
	new Data('B', "Barcelona", "Quina ciutat rep el sobrenom de Ciutat Comtal?"),
	new Data('C', "Canigó", "Quina muntanya és la més emblemàtica dels Pirineus?"),
	new Data('D', "Daurada", "Com s'anomena la costa del Camp de Tarragona?"),
	new Data('E', "Espanya", "Quin país limita amb Catalunya?"),
	new Data('F', "Forca", "De què te forma el Pedraforca?"),
	new Data('G', "Garrotxa", "Quina comarca és popular pels seus volcans?"),
	new Data('I', "Illa", "Quin tipus de territori és Cabrera?"),
	new Data('J', "Jaume", "Com s'anomena el conqueridor de València?"),
	new Data('L', "Llobregat", "Quin és el nom d'un dels principals rius catalans?"),
	new Data('M', "Montseny", "A quin massís pertany el Turó de l'Home?"),
	new Data('N', "Negre", "De quin color és la Mare de Déu de Montserrat?"),
	new Data('O', "Orxata", "Quina beguda fem amb xufa?"),
	new Data('P', "Panellets", "Què mengem per la Castanyada?"),
	new Data('R', "Roses", "Què regalem per Sant Jordi?"),
	new Data('S', "Senyera", "Com en diem de la bandera?"),
	new Data('T', "Tombs", "De què se'n fan tres a les festes de Sant Antoni?"),
	new Data('U', "Urgell", "A quina comarca pertany Agramunt?"),
	new Data('V', "Vinya", "Quin conreu predomina al Penedès?"),
	new Data('X', "Xixona", "Quina població és coneguda per ser el bressol del torró?")
]
