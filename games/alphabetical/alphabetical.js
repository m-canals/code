class Alphabetical {
	constructor(alphabet, question, data) {
		this.letters = this.createLetters(data, alphabet);
		this.question = question;
		this.data = data;
		this.played = data.length - 1;
		this.remaining = data.length;
		this.advance();
	}
	
	arrangeLetters(elements) {
		var radians_per_element = 2 * Math.PI / elements.length;
		var R = 200;
		var r = 12.5;
		for (var i = 0; i < elements.length; i++) {
			var radians = radians_per_element * i - Math.PI / 2.0;
			var x = R - r + (R - r * 2) * Math.cos(radians);
			var y = R - r + (R - r * 2) * Math.sin(radians);
			elements[i].style.marginLeft = x + 'px';
			elements[i].style.marginTop = y + 'px';
		}
	}
	
	createLetters(data, alphabet) {
		var letters = [];
		
		for (var i = 0; i < data.length; i++) {
			var letter = document.createElement('div');
			letter.innerText = data[i].letter;
			alphabet.appendChild(letter);
			letters.push(letter);
		}
		
		this.arrangeLetters(letters);
		
		return letters;
	}
	
	advance() {
		if (this.remaining > 0) {
			do this.played = (this.played + 1) % this.letters.length;
			while (this.letters[this.played].className !== "");
			this.letters[this.played].className = "played"
			this.question.innerText = this.data[this.played].question;
		}
	}
	
	pass() {
		this.letters[this.played].className = "";
		this.advance();
	}
	
	answer(answer) {
		answer = answer.toLowerCase();
		
		var rightAnswer = this.data[this.played].answer.toLowerCase();
		var className = answer == rightAnswer ? 'right' : 'wrong';
		this.remaining -= 1;
		this.letters[this.played].className = className;
		this.letters[this.played].title = this.data[this.played].answer;
		this.advance();
	}
	
	play(answer) {
		if (this.remaining > 0) {
			answer = answer.trim();
			
			if (answer.length == 0) this.pass();
			else this.answer(answer);
		}
	}
}
