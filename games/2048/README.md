2048
====

Text-based implementation of the game.

![Screenshot of the game.](screenshot.png "Screenshot of the game.")

Usage
-----

	make && ./2048
