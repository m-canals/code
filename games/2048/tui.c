#include "common.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>

#define HideCursor()             printf("\033[?25l")
#define ShowCursor()             printf("\033[?25h")
#define EnablePrivateMode()	     printf("\033[?1049h")
#define DisablePrivateMode()     printf("\033[?1049l")
#define MoveCursor(row, column)  printf("\033[%d;%dH", row, column)

static const int KeyUp = 256;
static const int KeyDown = 257;
static const int KeyRight = 258;
static const int KeyLeft = 259;

int GetKey() {
	struct termios attributes;
	tcgetattr(0, &attributes);
	tcflag_t flags = attributes.c_lflag;
	attributes.c_lflag &= ~ (ICANON | ECHO);	   
	tcsetattr(0, TCSANOW, &attributes);
	int character = getchar();
	
	if (character == 27) {
		character = getchar();
		
		if (character == 91) {
			character = getchar();
			
			switch (character) {
				case 65:
					character = KeyUp;
					break;
				case 66:
					character = KeyDown;
					break;
				case 67:
					character = KeyRight;
					break;
				case 68:
					character = KeyLeft;
					break;	
			}
		}
	}
	
	attributes.c_lflag = flags;
	tcsetattr(0, TCSANOW, &attributes);
	return character;
}

void DisplayGame(Game * game) {
	MoveCursor(1, 1);
	printf("Score: %d", game->score);

	MoveCursor(2, 1);
	putchar('+');
	for (int j = 0; j < game->columns; j++)
		putchar('-');
	putchar('+');

	for (int i = 0; i < game->rows; i++) {
		MoveCursor(i + 3, 1);
		putchar('|');
		for (int j = 0; j < game->columns; j++) {
			TileValue value = GetTileValue(game, i, j);
			if (value == EmptyTileValue)
				putchar(' ');
			else if (MinTileValue <= value && value <= MinTileValue + 8)
				putchar('1' + value - MinTileValue);
			else if (MinTileValue + 9 <= value && value <= MaxTileValue)
				putchar('A' + value - MinTileValue - 9);
			else
				putchar('?');
		}
		putchar('|');
	}
	
	MoveCursor(game->rows + 3, 1);
	putchar('+');
	for (int j = 0; j < game->columns; j++)
		putchar('-');
	putchar('+');
	
	MoveCursor(game->rows + 4, 1);
		printf("Use WASD or arrow keys to move.");
	MoveCursor(game->rows + 5, 1);
		printf("Press Q to quit.");
	
	if (game->gameover) {
		MoveCursor(game->rows + 6, 1);
		printf("Game over");
	}
}

void PrintError(Error error) {
	if (error == NotEnoughRowsOrColumns)
		puts("Error: Not enough rows or columns.");
	else if (error == TooManyRowsOrColumns)
		puts("Error: Too many rows or columns.");
	else if (error == NotEnoughMemory)
		puts("Error: Not enough memory.");
	else
		puts("Error: Unknown error.");
}

int main(int argc, char ** argv) {
	Game game;
	int rows = 4;
	int columns = 4;
	int key;
	
	if (argc == 3) {
		rows = atoi(argv[1]);
		columns = atoi(argv[2]);
	}

	Error error = InitializeGame(&game, rows, columns, time(NULL));

	if (error != None) {
		PrintError(error);
		return 1;
	}
	
	EnablePrivateMode();
	HideCursor();

	while (! game.gameover) {
		DisplayGame(&game);
		key = GetKey();
		if (key == KeyUp || key == 'w' || key == 'W')
			SlideTiles(&game, Up);
		else if (key == KeyDown || key == 's' || key == 'S')
			SlideTiles(&game, Down);
		else if (key == KeyRight || key == 'd' || key == 'D')
			SlideTiles(&game, Right);
		else if (key == KeyLeft || key == 'a' || key == 'A')
			SlideTiles(&game, Left);
		else if (key == 'q' || key == 'Q')
			break;
	}
	
	if (game.gameover) {
		DisplayGame(&game);
		do key = GetKey();
		while (key != 'q' && key != 'Q');
	}
	
	ShowCursor();
	DisablePrivateMode();
	
	FinalizeGame(&game);
	return 0;
}
