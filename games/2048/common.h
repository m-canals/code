#ifndef COMMON_H
#define COMMON_H

typedef char TileValue;

static const TileValue EmptyTileValue = 0;
static const TileValue MinTileValue = 1;
static const TileValue MaxTileValue = 35;

typedef struct game {
	TileValue * tiles;
	int * positions;
	int rows;
	int columns;
	int size;
	int score;
	int gameover;
} Game;

typedef enum {
	None,
	NotEnoughRowsOrColumns,
	TooManyRowsOrColumns,
	NotEnoughMemory,
} Error;

typedef enum {
	Right,
	Left,
	Up,
	Down
} Direction;

// May set gameover.
Error InitializeGame(Game * game, int rows, int columns, unsigned int seed);

// May set gameover.
void SlideTiles(Game * game, Direction direction);

TileValue GetTileValue(Game * game, int row, int column);

void FinalizeGame(Game * game);

#endif
