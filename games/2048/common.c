#include "common.h"
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>

TileValue * GetTile(Game * game, int row, int column) {
	return & game->tiles[row * game->columns + column];
}

TileValue GetTileValue(Game * game, int row, int column) {
	return game->tiles[row * game->columns + column];
}

int TilesHaveSameValue(Game * game, int row1, int column1, int row2,
int column2) {
	TileValue value1 = * GetTile(game, row1, column1);
	TileValue value2 = * GetTile(game, row2, column2);
	return value1 == value2;
}

int TileIsEmpty(Game * game, int row, int column) {
	TileValue value = * GetTile(game, row, column);
	return value == EmptyTileValue;
}

void MoveTile(Game * game, int destinationRow, int destinationColumn,
int sourceRow, int sourceColumn) {
	TileValue value = * GetTile(game, sourceRow, sourceColumn);
	* GetTile(game, destinationRow, destinationColumn) = value;
	* GetTile(game, sourceRow, sourceColumn) = EmptyTileValue;
}

void MergeTiles(Game * game, int destinationRow,
int destinationColumn, int sourceRow, int sourceColumn) {
	TileValue value = * GetTile(game, destinationRow, destinationColumn);
	* GetTile(game, destinationRow, destinationColumn) =
		(value == MaxTileValue) ? (MinTileValue) : (value + 1);
	* GetTile(game, sourceRow, sourceColumn) = EmptyTileValue;
	game->score++;
}

int GameIsOver(Game * game) {
	for (int i = 0; i < game->rows; i++)
		for (int j = 0; j < game->columns; j++)
			if (TileIsEmpty(game, i, j) || (i > 0 &&
			    TilesHaveSameValue(game, i, j, i - 1, j)) || (j > 0 &&
			    TilesHaveSameValue(game, i, j, i, j - 1)))
				return 0;

	return 1;
}

void PlaceNewTile(Game * game) {
	for (int i = 0, placed = 0; i < game->size && placed == 0; i++) {
		int j = rand() % (game->size - i) + i;
		int k = game->positions[j];
		
		if (game->tiles[k] == EmptyTileValue) {
			game->tiles[k] = MinTileValue;
			placed = 1;
		} else {
			game->positions[j] = game->positions[i];
			game->positions[i] = k;
		}
	}
	
	game->gameover = GameIsOver(game);
}

void SlideTilesHorizontally(Game * game, int first, int p) {
	for (int i = 0; i < game->rows; i++) {
		int afterLast = first + game->columns * p;
		int k = first;
		for (int j = k + p; j != afterLast; j += p) {
			if (! TileIsEmpty(game, i, j)) {
				if (TileIsEmpty(game, i, k)) {
					MoveTile(game, i, k, i, j);
				} else if (TilesHaveSameValue(game, i, k, i, j)) {
					MergeTiles(game, i, k, i, j);
					k += p;
				} else {
					k += p;
					if (j != k)
						MoveTile(game, i, k, i, j);
				}
			}
		}
	}
}

void SlideTilesVertically(Game * game, int first, int p) {
	for (int j = 0; j < game->columns; j++) {
		int afterLast = first + game->rows * p;
		int k = first;
		for (int i = k + p; i != afterLast; i += p) {
			if (! TileIsEmpty(game, i, j)) {
				if (TileIsEmpty(game, k, j)) {
					MoveTile(game, k, j, i, j);
				} else if (TilesHaveSameValue(game, k, j, i, j)) {
					MergeTiles(game, k, j, i, j);
					k += p;
				} else {
					k += p;
					if (i != k)
						MoveTile(game, k, j, i, j);
				}
			}
		}
	}
}

void FinalizeGame(Game * game) {
	// If pointers do not point to allocated memory they must be NULL
	// so no unallocated memory is freed on premature finalization.
	free(game->tiles);
	free(game->positions);
}

Error InitializeGame(Game * game, int rows, int columns, unsigned int seed) {
	game->rows = rows;
	game->columns = columns;
	game->size = game->rows * game->columns;
	game->score = 0;
	game->tiles = NULL;     
	game->positions = NULL;
	game->gameover = 0;
	
	if (game->rows < 1 || game->columns < 1) {
		FinalizeGame(game);
		return NotEnoughRowsOrColumns;
	}
	
	if (game->rows > INT_MAX / game->columns) {
		FinalizeGame(game);
		return TooManyRowsOrColumns;
	}
	
	if (game->size > RAND_MAX){
		FinalizeGame(game);
		return TooManyRowsOrColumns;
	}
	
	// FIXME SIZE_MAX may overflow on comparison.
	if (game->size > SIZE_MAX) {
		FinalizeGame(game);
		return TooManyRowsOrColumns;
	}
	
	game->tiles = calloc(game->size, sizeof * game->tiles);
	game->positions = calloc(game->size, sizeof * game->positions);
	
	if (game->tiles == NULL || game->positions == NULL) {
		FinalizeGame(game);
		return NotEnoughMemory;
	}

	for (int i = 0; i < game->size; i++) {
		game->positions[i] = i;
	}
	
	srand(seed);
	
	PlaceNewTile(game);
	
	return None;
}

void SlideTiles(Game * game, Direction direction) {
	if (direction == Up)
		SlideTilesVertically(game, 0, 1);
	else if (direction == Down)
		SlideTilesVertically(game, game->rows-1, -1);
	else if (direction == Left)
		SlideTilesHorizontally(game, 0, 1);
	else if (direction == Right)
		SlideTilesHorizontally(game, game->columns-1, -1);
		
	PlaceNewTile(game);
}
