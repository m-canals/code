class Game {
	constructor(tiles, table, userRack) {
		this.tiles = tiles
		this.table = table
		this.tableGroup = new DraggableElementTableGroup()
		this.tableGroup.addTable(table)
		this.tableGroup.addTable(userRack)
		this.tableTiles = null
		this.poolTiles = null
		this.players = {
			user: new User(userRack),
			computer: new Computer()
		}
		this.initialTiles = 14
		this.initialMeldPoints = 25
	}
	_placeSet(set, row, cell) {
		for (var i = 0; i < set.length; i++) {
			this.tableGroup.moveElement(this.table.rows[row].cells[cell+i], set[i])
		}
	}
	_arrangeTiles(sets) {
		var rows = this.table.rows.length
		var columns = this.table.rows[0].cells.length
		
		// First available column (for each row).
		var column = new Array(rows)
		for (var i = 0; i < column.length; i++) {
			column[i] = 0
		}
		
		sets.sort((a, b) => { return b.length - a.length })
		
		for (var i = 0; i < sets.length; i++) {
			var set = sets[i]
			var row = 0;
			while (columns - column[row] < set.length) {
				row++
			}
			this._placeSet(set, row, column[row])
			column[row] += set.length + 1
		}
	}
	_draw(player, howMany) {
		for (let i = 0; i < howMany; i++) {
			player.drawTile(this.poolTiles.pop())
		}
	}
	_removeBadTileClass() {
		this.players.user.tiles.forEach(tile => { tile.classList.remove('bad-tile') } )
		this.tableTiles.forEach(tile => { tile.classList.remove('bad-tile') } )
	}
	_addBadTileClass(tiles) {
		tiles.forEach(tile => { tile.classList.add('bad-tile') } )
	}
	// TODO Potser caldira crear les classes TileSet i TileSets, tenint en compte
	// la diferència entre els conjunts de fitxes i la llistes de fitxes.
	_replaceJokerTile(group, index, offset) {
		group[index].number = group[index + offset].number - offset
		group[index].color = group[index + offset].color
	}
	// Precondició: un grup té com a mínim tres fitxes.
	_replaceJokerTiles(set) {
		for (var i = 0; i < set.length - 2; i++) {
			if (set[i].isJoker()) {
				var offset = 2 - (! set[i+1].isJoker())
				this._replaceJokerTile(set, i, offset)
			}
		}
		
		for (var i = set.length - 2; i < set.length; i++) {
			if (set[i].isJoker()) {
				this._replaceJokerTile(set, i, -1)
			}
		}
	}
	_countDistinctColors(set) {
		return countDistinct(set.map(tile => { return tile.color }))
	}
	_isConsecutive(set) {
		for (var i = 1; i < set.length; i++) {
			if (set[i].number != set[i-1].number + 1) {
				return false
			}
		}
		return true
	}
	_isInRange(set) {
		for (var i = 0; i < set.length; i++) {
			if (set[i].number < 1 || set[i].number > 13) {
				return false
			}
		}
		return true
	}
	// Precondició: no hi ha jòquers.
	_isRun(set) {
		return set.length >= 3 && this._countDistinctColors(set) == 1 &&
		 this._isConsecutive(set) && this._isInRange(set)
	}
	// Precondició: no hi ha jòquers.
	_isGroup(set) {
		return set.length >= 3 && this._countDistinctColors(set) == set.length
	}
	_isSet(set) {
		if (set.length < 3) {
			return false
		} else {
			this._replaceJokerTiles(set)
			return this._isRun(set) || this._isGroup(set)
		}
	}
	_areSets(sets) {
		var badTiles = new Array()
		for (var i = 0; i < sets.length; i++) {
			var set = sets[i]
			if (! this._isSet(set)) {
				badTiles.push(...set)
			}
		}
		return badTiles
	}
	_getPoints(tiles) {
		return tiles.reduce((points, tile) => { return points + tile.number }, 0)
	}
	// FIXME Tot més ordenat.
	_end(tiles) {
		this.tableTiles = tiles
		this.tableGroup.clearMovements()
		if (this.players.user.tiles.length == 0) {
			return 'user'
		} else {
			var sets = this.table.getHorizontalGroups()
			sets = this.players.computer.play(sets)
			var tiles = sets.flat()
			var newTiles = difference(tiles, this.tableTiles)
			if (newTiles.length == 0) {
				console.log('computer draws')
				this._arrangeTiles(sets)
				this._draw(this.players.computer, 1)
				return 'none'
			} else {
				console.log('computer plays')
				this._arrangeTiles(sets)
				if (this.players.computer.tiles.length == 0) {
					return 'computer'
				} else {
					return 'none'
				}
			}
		}
	}
	initialize() {
		this.tableTiles = new Array()
		this.poolTiles = [...this.tiles]
		shuffle(this.poolTiles)
		this._draw(this.players.user, this.initialTiles)
		this._draw(this.players.computer, this.initialTiles)
	}
	undo() {
		this._removeBadTileClass()
		this.tableGroup.undoMovements()
	}
	play() {
		var sets = this.table.getHorizontalGroups()
		var tiles = sets.flat()
		var badTiles = difference(this.tableTiles, tiles)
		var newTiles = difference(tiles, this.tableTiles)
		this._removeBadTileClass()
		// Alguna fitxa ara no hi és i abans hi era.
		if (badTiles.length > 0) {
			this._addBadTileClass(badTiles)
			return 'none'
		// Alguna fitxa ara hi és i abans no hi era.
		} else if (newTiles.length > 0) {
				badTiles = this._areSets(sets)
				if (badTiles.length > 0) {
					this._addBadTileClass(badTiles)
					return 'none'
				} else if (this.players.user.canMeld) {
						return this._end(tiles)
				} else if (this._getPoints(newTiles) >= this.initialMeldPoints) {
						this.players.user.canMeld = true
						return this._end(tiles)
				} else {
					this._addBadTileClass(newTiles)
					return 'none'
				}
		// Hi ha les mateixes fitxes.
		} else {
			badTiles = this._areSets(sets)
			if (badTiles.length > 0) {
				this._addBadTileClass(badTiles)
				return 'none'
			} else {
				this._draw(this.players.user, 1)
				return this._end(tiles)
			}
		}
	}
}
