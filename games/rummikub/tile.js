const TileColor = {
	Blue: 'blue',
	Red: 'red',
	Yellow: 'yellow',
	Black: 'black'
}

// TODO Potser caldria posar els atributs amb el prefix «data». Però amb els
// mètodes què fem?
class Tile extends DraggableElement {
	constructor(id, color, number) {
		super(id)
		this.classList.add('tile')
		this.number = number
		this.color = color
	}
	isJoker() {
		return false
	}
}

customElements.define('draggable-tile', Tile, {extends: 'div'})

class NormalTile extends Tile {
	constructor(id, color, number) {
		super(id, color, number)
		this.innerHTML = number
		this.classList.add(color)
	}
}

customElements.define('draggable-normal-tile', NormalTile, {extends: 'div'})

class JokerTile extends Tile {
	constructor(id) {
		super(id, null, null)
		this.innerHTML = '😷'
		this.classList.add('joker')
	}
	isJoker() {
		return true
	}
}

customElements.define('draggable-joker-tile', JokerTile, {extends: 'div'})

function createTiles(idPrefix) {
	var id = 2
	var tiles = [new JokerTile(idPrefix + 0), new JokerTile(idPrefix + 1)]
	for (var tileColor in TileColor) {
		var color = TileColor[tileColor]
		for (var number = 1; number <= 13; number++) {
			for (var i = 0; i < 2; i++) {
				tiles.push(new NormalTile(idPrefix + id, color, number))
				id++
			}
		}
	}
	return tiles
}

