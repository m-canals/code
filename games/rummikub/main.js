var MyGame

function main() {
	var table = document.getElementById('table')
	var rack = document.getElementById('rack')
	var tiles = createTiles('tile-')
	// Hi ha 106 fitxes, per tant, hi ha com a molt 36 grups de fitxes, 
	// 35 de 3 i 1 de 4. Si a la primera fila hi ha el grup de 4 i 3 grups de 3
	// (16 espais) i a cadascuna de les restants hi ha 4 grups de 3 (15 espais),
	// llavors hi ha coma mínim 9 files de 16 espais. En una fila hi cap un grup
	// de 13 fitxes.
	insertCells(table, 11, 13)
	// Hi ha 78 fitxes a la pila i cada jugador en té 14. Després de 13 torns hi
	// ha 65 fitxes a la pila, l'usuari en té 27 i l'ordinador 1. Després de 14
	// torns, hi ha 63 fitxes a la pila, l'usuari en té 28 i l'ordinador 2.
	// Després de 15 torns, hi ha 62 fitxes a la pila, l'usuari en té 29 i
	// l'ordinador 1. Després de 57 torns, hi ha 0 fitxes a la pila, l'usuari en
	// té 71 i l'ordinador 2. Llavors hi ha com a mínim 71 espais.
	insertCells(rack, 6, 13)
	MyGame = new Game(tiles, table, rack)
	MyGame.initialize()
}
