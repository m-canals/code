function shuffle(array) {
	for(let i = array.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * i)
		const temp = array[i]
		array[i] = array[j]
		array[j] = temp
	}
}

function getDistinct(values) {
	var set = new Set()
	values.forEach(value => { set.add(value) })
	return set
}

function countDistinct(values) {
	return getDistinct(values).size
}

function insertCells(table, rows, columns) {
	for (var i = 0; i < rows; i++) {
		var row = table.insertRow(i)
		for (var j = 0; j < columns; j++) {
			row.insertCell(j)
		}
	}
}

function difference(minuendArray, subtrahendArray) {
	var array = new Set(subtrahendArray);
	return [...new Set([...minuendArray].filter(x => ! array.has(x)))]
}

