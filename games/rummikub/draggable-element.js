// TODO Seleccionar elements adjacents amb shift o clic llarg.

class DraggableElementEvent {
	static _drag(event) {
		event.dataTransfer.setData('id', event.target.id)
	}
	static _dragOver(event) {
		event.preventDefault()
	}
	static _drop(event, tableGroup) {
		event.preventDefault()
		var data = event.dataTransfer.getData('id')
		var element = document.getElementById(data)
		tableGroup.saveMovement(element)
		tableGroup.moveElement(event.target, element)
	}
	static drag() {
		return function (event) { DraggableElementEvent._drag(event) }
	}
	static dragOver() {
		return function (event) { DraggableElementEvent._dragOver(event) }
	}
	static drop(tableGroup) {
		return function (event) { DraggableElementEvent._drop(event, tableGroup) }
	}
}

class DraggableElement extends HTMLDivElement {
	constructor(id) {
		super()
		this.id = id
		this.draggable = true
		this.ondragstart = DraggableElementEvent.drag()
	}
}

customElements.define('draggable-element', DraggableElement, {extends: 'div'})

class DraggableElementTable extends HTMLTableElement {
	constructor(rows, columns) {
		super()
		this.tableGroup = null
	}
	getFirstEmptyCell() {
		for (var i = 0; i < this.rows.length; i++) {
			var row = this.rows[i]
			for (var j = 0; j < row.cells.length; j++) {
				var cell = row.cells[j]
				if (cell.children.length == 0) {
					return cell
				}
			}
		}
	}
	getHorizontalGroups() {
		var groups = new Array()
		for (var i = 0; i < this.rows.length; i++) {
			var row = this.rows[i]
			var group = new Array()
			for (var j = 0; j < row.cells.length; j++) {
				var cell = row.cells[j]
				if (cell.children.length == 0) {
					if (group.length > 0) {
						groups.push(group)
						group = new Array()
					}
				} else {
					group.push(cell.children[0])
				}
			}
			if (group.length > 0) {
				groups.push(group)
			}
		}
		return groups
	}
}

customElements.define('draggable-element-table', DraggableElementTable, {extends: 'table'})

class DraggableElementTableGroup {
	constructor() {
		this.draggedTiles = new Array()
	}
	addTable(table) {
		table.tableGroup = this
		for (var i = 0; i < table.rows.length; i++) {
			var row = table.rows[i]
			for (var j = 0; j < row.cells.length; j++) {
				var cell = row.cells[j]
				cell.ondrop = DraggableElementEvent.drop(this)
				cell.ondragover = DraggableElementEvent.dragOver()
			}
		}
	}
	moveElement(target, element) {
		// El lloc d'on es mou, si n'hi ha, s'hi podrà moure un altre element.
		if (element.parentNode !== null) {
			element.parentNode.ondrop = DraggableElementEvent.drop(this)
			element.parentNode.ondragover = DraggableElementEvent.dragOver()
		}
		// El lloc a on es mou no s'hi podrà moure un altre element.
		target.appendChild(element)
		target.ondrop = undefined
		target.ondragover = undefined
	}
	saveMovement(element) {
		if (element.parentNode !== null) {
			this.draggedTiles.push([element, element.parentNode])
		}
	}
	undoMovements() {
		while (this.draggedTiles.length > 0) {
			var move = this.draggedTiles.pop()
			var element = move[0]
			var target = move[1]
			this.moveElement(target, element)
		}
	}
	clearMovements() {
		this.draggedTiles = new Array()
	}
}
