Rummikub
========

Quick and incomplete implementation of the game using the HTML Drag and Drop API.

![Screenshot of the game.](screenshot.png "Screenshot of the game.")

Usage
-----

Open `index.html` in a web browser.
