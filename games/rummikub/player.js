class Player {
	constructor() {
		this.tiles = new Array() // TODO Potser hauria de ser a la classe «Game».
		this.canMeld = true // TODO Hauria de ser a la classe «Game».
	}
	drawTile(tile) {
		this.tiles.push(tile)
	}
}

class User extends Player {
	constructor(table) {
		super()
		this.rack = table
	}
	drawTile(tile) {
		this.tiles.push(tile)
		this.rack.tableGroup.moveElement(this.rack.getFirstEmptyCell(), tile)
	}
}

class Computer extends Player {
	// TODO Això hauria de ser en una classe per a les fitxes.
	_removeTiles(tiles) {
		for (var i = 0; i < tiles.length; i++) {
			var index = this.tiles.indexOf(tiles[i])
			this.tiles.splice(index, 1)
		}
	}
	_runSort(tiles) {
		tiles.sort((a, b) => {
			if (a.isJoker()) return true
			else if (b.isJoker()) return false
			else if (b.color < a.color) return true
			else if (a.color < b.color) return false
			else return parseInt(b.number) < parseInt(a.number)
		})
	}
	_groupSort(tiles) {
		tiles.sort((a, b) => {
			if (a.isJoker()) return true
			else if (b.isJoker()) return false
			else if (parseInt(b.number) < parseInt(a.number)) return true
			else if (parseInt(a.number) < parseInt(b.number)) return false
			else return a.color < b.color
		})
	}
	_getRuns() {
		return []
	}
	_considerGroup(map, groups, jokerTiles, joker, four) {
		if (map.size == 2 && joker && jokerTiles.length > 0) { 
			var group = new Array()
			map.forEach(tile => { group.push(tile) })
			group.push(jokerTiles[0])
			groups.push(group)
		} else if (map.size == 3) {
			var group = new Array()
			map.forEach(tile => { group.push(tile) })
			groups.push(group)
		} else if (map.size == 4) {
			var group = new Array()
			map.forEach(tile => { group.push(tile) })
			if (! four) group.pop()
			groups.push(group)
		}
	}
	_getGroups(joker, four) {
		var groups = new Array()
		if (this.tiles.length > 0) {
			var tiles = this.tiles.filter(tile => ! tile.isJoker())
			var jokerTiles = this.tiles.filter(tile => tile.isJoker())
			this._groupSort(tiles)
			var map = new Map() // No volem repetits d'un color.
			map.set(tiles[0].color, tiles[0])
			for (var i = 1; i < tiles.length; i++) {
				if (tiles[i].number != tiles[i-1].number) {
					this._considerGroup(map, groups, jokerTiles, joker, four)
					map.clear()
				}
				map.set(tiles[i].color, tiles[i])
			}
			this._considerGroup(map, groups, jokerTiles, joker, four)
		}
		return groups
	}
	play(sets) {
		var playedSets = new Array()
		if (this.canMeld) {
			var runs = this._getRuns()
			if (runs.length > 0) {
				playedSets.push(runs[0])
			} else {
				var groups = this._getGroups(false, false)
				if (groups.length > 0) {
					playedSets.push(groups[0])
				}
			}
		} else {
			/*if (tenim un grup de initialMeld o mes o mes grups) {
				this.canMeld = true
				...
			} else {
				return sets
			}*/
		}
		playedSets.forEach(set => { this._removeTiles(set) })
		console.log(playedSets)
		return sets.concat(playedSets)
	}
}
