Word Search Generator
=====================

Generate a word search and save it as an HTML file.

![Generated word search (converted to PNG).](output.png "Generated word search (converted to PNG).")

Usage
-----

	php generate-word-search.php > output.html
