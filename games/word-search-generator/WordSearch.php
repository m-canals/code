<?php

// TODO
//
//  - Return string instead of echoing it.

class WordSearch {
	public $rows;             // Number of rows.
	public $columns;          // Number of columns.
	public $fill_characters;  // Characters to use.
	public $characters;       // Matrix of characters.
	public $isWord;           // Does a character belong to a word?
	private $positions;       // Vector of positions to choose from.
	
	public const Directions = [
		'N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'
	];
	
	private const DirectionSteps = array(
			'NW' => [-1, -1], 'N'  => [-1, 0], 'NE' => [-1, 1],
			'W'  => [ 0, -1],                  'E'  => [ 0, 1],
			'SW' => [ 1, -1], 'S'  => [ 1, 0], 'SE' => [ 1, 1]
	);

	private function bad_position($row, $column) {
		return $row < 0
		 || $column < 0
		 || $row    >= $this->rows
		 || $column >= $this->columns;
	}

	public function __construct($rows, $columns) {
		$this->rows = $rows;
		$this->columns = $columns;
		$this->characters = array_fill(0, $rows, array_fill(0, $columns, ''));
		$this->isWord = array_fill(0, $rows, array_fill(0, $columns, false));
		$this->fill_characters = range('a', 'z');
		$this->positions = range(0, $rows * $columns);

	}

	public function html($id, $class = '', $word_class = 'word') {
		echo '<table id="' . $id . '" class="' . $class . '">' . "\n";
		
		for ($row = 0; $row < $this->rows; $row++) {
			echo '<tr>' . "\n";
			
			for ($column = 0; $column < $this->columns; $column++) {
				$character = $this->characters[$row][$column];
				$class = $this->isWord[$row][$column] ? $word_class : '';
				echo '<td class="' . $class . '">' . htmlentities($character) . '</td>' . "\n";
			}
			
			echo '</tr>' . "\n";
		}
		
		echo '</table>' . "\n";
	}

	public function fill() {
		for ($row = 0; $row < $this->rows; $row++) {
			for ($column = 0; $column < $this->columns; $column++) {
				$character = & $this->characters[$row][$column];
				if (empty($character)) {
					$key = array_rand($this->fill_characters);
					$character = $this->fill_characters[$key];
				}
			}
		}
	}

	public function place_word_at_towards($word, $row, $column, $direction) {
		$first_row = $row;
		$first_column = $column;
		$row_step = self::DirectionSteps[$direction][0];
		$column_step = self::DirectionSteps[$direction][1];
		$length = mb_strlen($word);

		// Can it be placed?
		for ($i = 0; $i < $length; $i++) {
			if ($this->bad_position($row, $column))
				return false;
			
			$character = & $this->characters[$row][$column];
			if (! empty($character) && $character != mb_substr($word, $i, 1))
				return false;
			
			$row += $row_step;
			$column += $column_step;
		}
		
		// Place it.
		$row = $first_row;
		$column = $first_column;
		for ($i = 0; $i < $length; $i++) {
			$this->characters[$row][$column] = mb_substr($word, $i, 1);
			$this->isWord[$row][$column] = true;
			$row += $row_step;
			$column += $column_step;
		}
		
		return true;
	}
	
	public function place_word_at($word, $row, $column, $directions = self::Directions) {
		shuffle($directions);
		
		foreach ($directions as $direction)
			if ($this->place_word_at_towards($word, $row, $column, $direction))
				return true;
		
		return false;
	}
	
	public function place_word($word, $directions = self::Directions) {
		$length = mb_strlen($word);
	
		if ($length > $this->rows)
			$directions = array_intersect($directions, ['E', 'W']);
	
		if ($length > $this->columns)
			$directions = array_intersect($directions, ['N', 'S']);
	
		if (! empty($directions)) {
			shuffle($this->positions);
			
			foreach ($this->positions as $position) {
				$row    = $position / $this->columns;
				$column = $position % $this->columns;
				
				if ($this->place_word_at($word, $row, $column, $directions))
					return true;
			}
		}
		
		return false;
	}
}
?>
