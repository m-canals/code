#include "../common/game.h"
#include "../common/field.h"
#include "tui.h"
#include <time.h>
#include <stdio.h>

void Flag(Game * game, Position * position)
{
	Size index = ComputeFieldIndex(game, position->row, position->column);
	Field * field = GetGameField(game, index);
	
	if (FieldHasProperties(field, Flagged)) {
		ClearFieldProperties(field, Flagged);
		SetFieldProperties(field, MaybeFlagged);
	} else if (FieldHasProperties(field, MaybeFlagged)) {
		ClearFieldProperties(field, MaybeFlagged);
	} else {
		SetFieldProperties(field, Flagged);
	}
	
	MoveCursor(position);
	DisplayField(field, position);
}

void Sweep(Game * game, Position * position)
{
	Size index = ComputeFieldIndex(game, position->row, position->column);
	Field * field = GetGameField(game, index);
	SweepFields(game, field);
	GameStatus status = GetGameStatus(game);
	
	if (status == Lost) {
		GetKey();
		Cleanup();
		printf("You have lost.\n");
		exit(EXIT_SUCCESS);
	} else if (status == Won) {
		GetKey();
		Cleanup();
		printf("You have won.\n");
		exit(EXIT_SUCCESS);
	}
}

void Quit()
{
	Cleanup();
	exit(EXIT_SUCCESS);
}

void Move(Game * game, Position * position, Position * offset)
{
	Size index;
	Field * field;
	
	index = ComputeFieldIndex(game, position->row, position->column);
	field	= GetGameField(game, index);
	MoveCursor(position);
	position->row += offset->row;
	position->column += offset->column;
	DisplayField(field, position);
	index = ComputeFieldIndex(game, position->row, position->column);
	field = GetGameField(game, index);
	MoveCursor(position);
	DisplayField(field, position);
}

void MoveUp(Game * game, Position * position)
{
	Move(game, position, &((Position) {-1, 0}));
}

void MoveDown(Game * game, Position * position)
{
	Move(game, position, &((Position) {1, 0}));
}

void MoveRight(Game * game, Position * position)
{
	Move(game, position, &((Position) {0, 1}));
}

void MoveLeft(Game * game, Position * position)
{
	Move(game, position, &((Position) {0, -1}));
}

void DisplaySweptField(Field * field, void * data)
{
	Position * activePosition = data;
	Position position = {GetFieldRowNumber(field), GetFieldColumnNumber(field)};
	MoveCursor(&position);
	DisplayField(field, activePosition);
}

int main(int argc, char ** argv)
{
	Game game;
	Size rows, columns, mines;
	Position position;
	InitializationResult initializationResult;
	Key key;
	
	if (GetWindowSize(&rows, &columns) == Failure) {
		fprintf(stderr, "Error: can not get window size. Exiting.\n");
		exit(EXIT_FAILURE);
	}

	if (argc > 1) {
		mines = atoi(argv[1]);
	} else {
		mines = rows * columns / 10;
	}

	if (argc > 2) {
		columns = atoi(argv[2]);
	}

	if (argc > 3) {
		rows = atoi(argv[3]);
	}

	srand(time(NULL));

	fprintf(stderr, "Initializing game with %zu rows, %zu columns and %zu mines...\n",
		rows, columns, mines);
	initializationResult = InitializeGame(&game, rows, columns, mines, DisplaySweptField, &position);
	
	switch (initializationResult) {
		case NotEnoughRows:
		case NotEnoughColumns:
			fprintf(stderr, "Error: there are not enough rows or columns. Exiting.\n");
			exit(EXIT_FAILURE);
		case TooManyRowsOrColumns:
			fprintf(stderr, "Error: there are too many rows or columns. Exiting.\n");
			exit(EXIT_FAILURE);
		case TooManyMines:
			fprintf(stderr, "Error: there are too many mines. Exiting.\n");
			exit(EXIT_FAILURE);;
		case NotEnoughMemory:
			fprintf(stderr, "Error: there is not enough memory. Exiting.\n");
			exit(EXIT_FAILURE);
		case SuccessfulInitialization:
			fprintf(stderr, "Done.\n");
	}

	position.row = 0;
	position.column = 0;
	
	Setup();
	MoveCursor(&position);
	DisplayGame(&game, &position);
	
	do {
		// TODO Handle position escape sequence.
		// (if key == LEFT_MOUSE_BUTTON) MoveTo(&game, &position);).
		key = GetKey();
    
		if (key == ArrowUpKey) {
			if (position.row > 0) MoveUp(&game, &position);
		} else if (key == ArrowDownKey) {
			if (position.row < rows - 1) MoveDown(&game, &position);
		} else if (key == ArrowLeftKey) {
			if (position.column > 0)  MoveLeft(&game, &position);
		} else if (key == ArrowRightKey) {
			if (position.column < columns - 1) MoveRight(&game, &position);
		} else if (key == SpaceKey) {
			Flag(&game, &position);
		} else if (key == EnterKey) {
			Sweep(&game, &position);
		} else if (key == 'q' || key == 'Q') {
			Quit();
		}
	} while (1);
}
