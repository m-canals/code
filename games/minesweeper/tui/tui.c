#include "../common/field.h"
#include "../common/game.h"
#include "tui.h"
#include <stdio.h>
#include <sys/ioctl.h>
#include <termios.h>

Result GetWindowSize(Size * rows, Size * columns)
{
	struct winsize ws;
	
	if (ioctl(fileno(stdout), TIOCGWINSZ, &ws) != 0) 
		return Failure;

	* rows = ws.ws_row;
	* columns = ws.ws_col;

	return Success;
}

Key GetKey()
{
    struct termios attributes;
    tcflag_t flags;
    int character;
    
    tcgetattr(0, &attributes);
    flags = attributes.c_lflag;
    attributes.c_lflag &= ~ (ICANON | ECHO);       
    tcsetattr(0, TCSANOW, &attributes);
    character = getchar();
    
    if (character == 27) {
        character = getchar();
        
        if (character == 91) {
            character = getchar();
            
            switch (character) {
                case 65:
                    character = ArrowUpKey;
                    break;
                case 66:
                    character = ArrowDownKey;
                    break;
                case 67:
                    character = ArrowRightKey;
                    break;
                case 68:
                    character = ArrowLeftKey;
                    break;    
            }
        }
    }
    
    attributes.c_lflag = flags;
    tcsetattr(0, TCSANOW, &attributes);
    
    return character;
}

void Setup()
{
	EnableAlternativeScreen();
	HideCursor();
}

void Cleanup()
{
	ResetFormat();
	ShowCursor();
	DisableAlternativeScreen();
}

void ResetFormat()
{
	fputs("\033[0m", stdout);
}

void HideCursor()
{
	fputs("\033[?25l", stdout);
}

void ShowCursor()
{
	fputs("\033[?25h", stdout);
}

void EnableAlternativeScreen()
{
	fputs("\033[?1049h", stdout);
}

void DisableAlternativeScreen()
{
	fputs("\033[?1049l", stdout);
}

void MoveCursor(Position * position)
{
	printf("\033[%zu;%zuH", position->row + 1, position->column + 1);
}

void DisplayField(Field * field, Position * activePosition)
{
	Size mines = GetFieldNumberOfAdjacentMines(field);
	Boolean active = activePosition->row == GetFieldRowNumber(field)
		&& activePosition->column == GetFieldColumnNumber(field);
	
	if (FieldHasProperties(field, Swept | Mined))
		printf(active ? ActiveMinedFieldFormat : MinedFieldFormat);
	else if (FieldHasProperties(field, Swept) && mines == 0)
		printf(active ? ActiveSweptFieldFormat : SweptFieldFormat, ' '); 
	else if (FieldHasProperties(field, Swept))
		printf(active ? ActiveSweptFieldFormat : SweptFieldFormat, '0' + (int) mines); 
	else if (FieldHasProperties(field, Flagged))
		printf(active ? ActiveFlaggedFieldFormat : FlaggedFieldFormat); 
	else if (FieldHasProperties(field, MaybeFlagged))
		printf(active ? ActiveMaybeFlaggedFieldFormat : MaybeFlaggedFieldFormat); 
	else
		printf(active ? ActiveNonSweptFieldFormat : NonSweptFieldFormat); 
}

void DisplayGame(Game * game,  Position * activePosition)
{
	for (Size row = 0; row < GetGameNumberOfRows(game); row++) {
		if (row > 0)
			printf("\n");
		
		for (Size column = 0; column < GetGameNumberOfColumns(game); column++) {
			Size index = ComputeFieldIndex(game, row, column);
			Field * field = GetGameField(game, index);
			DisplayField(field, activePosition);
		}
	}
}
