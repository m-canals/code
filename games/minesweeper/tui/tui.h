#ifndef MINESWEEPER_TUI_H
#define MINESWEEPER_TUI_H

#include "../common/game.h"

typedef struct {
	Size row, column;
} Position;

typedef enum Result {
	Success,
	Failure
} Result;

Result GetWindowSize(Size * rows, Size * columns);

typedef int Key;

static const Key EnterKey = 10;
static const Key SpaceKey = 32;
static const Key ArrowUpKey = 275;
static const Key ArrowDownKey = 276;
static const Key ArrowRightKey = 277;
static const Key ArrowLeftKey = 278;

Key GetKey();

void Setup();

void Cleanup();

void ResetFormat();

void HideCursor();

void ShowCursor();

void EnableAlternativeScreen();

void DisableAlternativeScreen();

void MoveCursor(Position * position);

static const char MinedFieldFormat[]              = "\033[30;47mx";
static const char SweptFieldFormat[]              = "\033[30;47m%c";
static const char FlaggedFieldFormat[]            = "\033[30;100m!";
static const char MaybeFlaggedFieldFormat[]       = "\033[30;100m?";
static const char NonSweptFieldFormat[]           = "\033[30;100m ";
static const char ActiveMinedFieldFormat[]        = "\033[97;40mx";
static const char ActiveSweptFieldFormat[]        = "\033[97;40m%c";
static const char ActiveFlaggedFieldFormat[]      = "\033[97;40m!";
static const char ActiveMaybeFlaggedFieldFormat[] = "\033[97;40m?";
static const char ActiveNonSweptFieldFormat[]     = "\033[97;40m ";

void DisplayField(Field * field, Position * activePosition);

void DisplayGame(Game * game, Position * activePosition);

#endif
