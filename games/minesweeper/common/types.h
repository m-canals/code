#ifndef TYPES_H
#define TYPES_H

#include <stdlib.h>
#include <stdint.h>

typedef int Boolean;
typedef size_t Size;

static const Size MaximumSize = SIZE_MAX;

#endif
