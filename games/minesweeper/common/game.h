#ifndef GAME_H
#define GAME_H

#include "types.h"
#include "field.h"

typedef enum GameStatus {
	Lost,
	Won,
	Unfinished
} GameStatus;

typedef struct Game {
	Field * fields;
	Size numberOfRows, numberOfColumns;
	Size numberOfFields, numberOfMinedFields, numberOfSweptFields;
	GameStatus status;
	FieldFunction sweepFunction;
	void * sweepFunctionData;
} Game;

Size ComputeFieldRowNumber(Game * game, Size index);

Size ComputeFieldColumnNumber(Game * game, Size index);

Size ComputeFieldIndex(Game * game, Size row, Size column);

Field * GetGameField(Game * game, Size index);

Size GetGameNumberOfRows(Game * game);

Size GetGameNumberOfColumns(Game * game);

GameStatus GetGameStatus(Game * game);

void ApplyOnAdjacentFields(Game * game, Field * field, FieldFunction function);

void InitializeFields(Game * game);

typedef enum InitializationResult {
	NotEnoughRows,
	NotEnoughColumns,
	TooManyRowsOrColumns,
	TooManyMines,
	NotEnoughMemory,
	SuccessfulInitialization
} InitializationResult;

InitializationResult InitializeGame(Game * game, Size rows, Size columns, Size mines, FieldFunction sweepFunction, void * sweepFunctionData);

Boolean AllNonMinedFieldsAreSwept(Game * game);

void SweepField(Field * field, void * data);

void SweepFields(Game * game, Field * field);

#endif
