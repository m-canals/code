#include "field.h"

Size GetFieldRowNumber(Field * field)
{
	return field->rowNumber;
}

Size GetFieldColumnNumber(Field * field)
{
	return field->columnNumber;
}

Size GetFieldNumberOfAdjacentMines(Field * field)
{
	return field->numberOfAdjacentMines;
}

FieldProperties GetFieldProperties(Field * field, FieldProperties properties)
{
	return field->properties & properties;
}

Boolean FieldHasProperties(Field * field, FieldProperties properties)
{
	return GetFieldProperties(field, properties) == properties;
}

void SetFieldProperties(Field * field, FieldProperties properties)
{
	field->properties |= properties;
}

void ClearFieldProperties(Field * field, FieldProperties  properties)
{
	field->properties &= ~ properties;
}

void ToggleFieldProperties(Field * field, FieldProperties  properties)
{
	field->properties ^= properties;
}

void SwapFieldProperties(Field * firstField, Field * secondField, FieldProperties properties)
{
	FieldProperties firstFieldProperties = firstField->properties;
	FieldProperties secondFieldProperties = secondField->properties;
	
	firstField->properties = (firstField->properties & (~ properties))
		| (secondFieldProperties & properties);
	secondField->properties = (secondField->properties & (~ properties))
		| (firstFieldProperties & properties);
}

void IncreaseNumberOfAdjacentMines(Field * field, void * data)
{
	field->numberOfAdjacentMines++;    
}
