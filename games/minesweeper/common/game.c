#include "field.h"
#include "game.h"

Size ComputeFieldRowNumber(Game * game, Size index) {
	return index / game->numberOfColumns;
}

Size ComputeFieldColumnNumber(Game * game, Size index) {
	return index % game->numberOfColumns;
}

Size ComputeFieldIndex(Game * game, Size row, Size column) {
	return row * game->numberOfColumns + column;
}

Field * GetGameField(Game * game, Size index) {
	return &game->fields[index];
}

Size GetGameNumberOfRows(Game * game)
{
	return game->numberOfRows;
}

Size GetGameNumberOfColumns(Game * game)
{
	return game->numberOfColumns;
}

GameStatus GetGameStatus(Game * game)
{
	return game->status;
}

void ApplyOnAdjacentFields(Game * game, Field * field, FieldFunction function)
{
	Size row = field->rowNumber;
	Size column = field->columnNumber;

	for (Size i = 0; i < 3; i++) {
		for (Size j = 0; j < 3; j++) {
			Boolean adjacent = (i != 1 || j != 1);
			Boolean tooSmall = (i == 0 && row < 1) ||
			                   (j == 0 && column < 1);
			Boolean tooLarge = (i == 2 && row >= game->numberOfRows - 1) ||
			                   (j == 2 && column >= game->numberOfColumns - 1);
			                
			if (adjacent && ! tooSmall && ! tooLarge) {
				
				Size index = ComputeFieldIndex(game, row + i - 1, column + j - 1);
				function(GetGameField(game, index), (void *) game);
			}
		}
	}
}

void InitializeFields(Game * game)
{
	// Initialize fields.
  for (Size i = 0; i < game->numberOfFields; i++) {  	
  	Field * field = GetGameField(game, i);
  	field->rowNumber = ComputeFieldRowNumber(game, i);
  	field->columnNumber = ComputeFieldColumnNumber(game, i);
		ClearFieldProperties(field, All);
		field->numberOfAdjacentMines = 0;	
  }

	// TODO Make it solvable without guessing.
	// Place mines.
  for (Size i = 0; i < game->numberOfMinedFields; i++) {
  	SetFieldProperties(GetGameField(game, i), Mined);
  }
 
  // Shuffle mines.
  //
  // With an unreasonably large number of fields the distribution
	// isn't uniform: before RAND_MAX, small values are more frequent;
	// after RAND_MAX, values larger than RAND_MAX are impossible.
	for (Size i = 0; i < game->numberOfFields; i++) {
    Size j = (rand() % (game->numberOfFields - i)) + i;
    SwapFieldProperties(GetGameField(game, i), GetGameField(game, j), Mined);
  }

	// Update the number of adjacent mines to each field.
	for (Size i = 0; i < game->numberOfFields; i++) {
		Field * field = GetGameField(game, i);

		if (FieldHasProperties(field, Mined)) {
			ApplyOnAdjacentFields(game, field, IncreaseNumberOfAdjacentMines);
		}
	}
}

InitializationResult InitializeGame(Game * game, Size rows, Size columns,
	Size mines, FieldFunction sweepFunction, void * sweepFunctionData)
{
	if (rows < 1)
		return NotEnoughRows;
	else if (columns < 1)
		return NotEnoughColumns;
	else if (rows > MaximumSize / columns) // Prevent overflow.
		return TooManyRowsOrColumns;
	else if (mines > rows * columns)
		return TooManyMines;

	game->status = Unfinished;
	game->numberOfRows = rows;
	game->numberOfColumns = columns;
	game->numberOfFields = rows * columns;
	game->numberOfSweptFields = 0;
	game->numberOfMinedFields = mines;
	game->fields = calloc(game->numberOfFields, sizeof(Field));
	game->sweepFunction = sweepFunction;
	game->sweepFunctionData = sweepFunctionData;
	
	if (game->fields == NULL)
		return NotEnoughMemory;
	
	InitializeFields(game);
	  
	return SuccessfulInitialization;
}

Boolean AllNonMinedFieldsAreSwept(Game * game) {
	Size numberOfNonMinedFields = game->numberOfFields - game->numberOfMinedFields;
	return game->numberOfSweptFields == numberOfNonMinedFields;
}

void SweepField(Field * field, void * data)
{
	Game * game = data;
	
	if (! FieldHasProperties(field, Swept)) {
		SetFieldProperties(field, Swept);
		game->numberOfSweptFields++;
		
		if (game->sweepFunction != NULL)
			game->sweepFunction(field, game->sweepFunctionData);
		
		if (! FieldHasProperties(field, Mined) && field->numberOfAdjacentMines == 0)
			ApplyOnAdjacentFields(game, field, SweepField);
	}
}
// TODO Generalize auto-sweeping:
// if the number of adjacent mines equals the number of adjacent flags
// then sweep all non-flagged adjacent fields (if an adjacent mine is not
// flagged the game is over).
void SweepFields(Game * game, Field * field)
{
	if (game->status == Unfinished) {
		SweepField(field, (void *) game);
		
		if (FieldHasProperties(field, Mined)) {
			game->status = Lost;
		} else if (AllNonMinedFieldsAreSwept(game)) {
			game->status = Won;
		}
	}
}
