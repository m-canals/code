#ifndef FIELD_H
#define FIELD_H

#include "types.h"

typedef enum FieldProperties {
	Mined        = 0b0001,
	Swept        = 0b0010,
	Flagged      = 0b0100,
	MaybeFlagged = 0b1000,
	All          = 0b1111
} FieldProperties;

typedef struct Field {
	Size rowNumber, columnNumber;
	Size numberOfAdjacentMines;
	FieldProperties properties;	
} Field;

Size GetFieldRowNumber(Field * field);

Size GetFieldColumnNumber(Field * field);

Size GetFieldNumberOfAdjacentMines(Field * field);

FieldProperties GetFieldProperties(Field * field, FieldProperties properties);

Boolean FieldHasProperties(Field * field, FieldProperties properties);

void SetFieldProperties(Field * field, FieldProperties properties);

void ClearFieldProperties(Field * field, FieldProperties  properties);

void ToggleFieldProperties(Field * field, FieldProperties  properties);

void SwapFieldProperties(Field * firstField, Field * secondField, FieldProperties properties);

typedef void (* FieldFunction)(Field * field, void * data);

void IncreaseNumberOfAdjacentMines(Field * field, void * data);

#endif
