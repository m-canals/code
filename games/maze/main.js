function generateMaze(maze)
{
	const canvas = document.getElementById('maze');
	const firstColor = document.getElementById('first-color').value;
	const secondColor = document.getElementById('second-color').value;
	const size = document.getElementById('size').value / 1000;
	const bounds = canvas.getBoundingClientRect();
	const height = Math.max(1, Math.round(bounds.height * size));
	const width = Math.max(1, Math.round(bounds.width * size));
	const method = document.getElementById('method').value == 'mst' ?
		InteractiveCanvasMazeGame.randomMST :
		InteractiveCanvasMazeGame.randomDFS;

	maze.setPathTypeColor(1, firstColor);
	maze.setPathTypeColor(2, secondColor);
	maze.generate(width, height, method);
}

function main()
{
	const canvas = document.getElementById('maze');
	const create = document.getElementById('create');
	const maze = new InteractiveCanvasMazeGame(canvas);
	
	generateMaze(maze);
	maze.run();
	create.addEventListener('click', event => generateMaze(maze));
}

window.addEventListener('load', main);
