// TODO Optional loops.
// TODO Variable path distance.

class RandomMSTGraph extends SquareGridGraph
{
	minimumSpanningTree(vertex)
	{
		const edgePrecedenceFunction = function(edge, otherEdge) {
			// Does it matter that this function is multivalued i.e. edge
			// weights are variable instead of constant?
			return Math.round(Math.random());
		}
		
		return super.minimumSpanningTree(vertex, edgePrecedenceFunction);
	}
}

class RandomDFSGraph extends SquareGridGraph
{
	incidentEdges(vertex)
	{
		const edges = Array.from(super.incidentEdges(vertex));
		let i = edges.length;
		
		// Shuffle edges.
		while (i > 0) {
			const j = Math.floor(Math.random() * i);
			i--;
			[edges[i], edges[j]] = [edges[j], edges[i]];
		}
		
		return edges;
	}
}

class Maze
{
	static randomDFS(width, height)
	{
		const graph = new RandomDFSGraph(width, height);
		const tree = graph.depthFirstSearchTree(0);
		
		return tree;
	}
	
	static randomMST(width, height)
	{
		const graph = new RandomMSTGraph(width, height);
		const tree = graph.minimumSpanningTree(0);
		
		return tree;
	}

	constructor()
	{
		this.tree = null;
	}

	generate(width, height, method)
	{
		this.tree = method(width, height);
	}
}

class CanvasMaze extends Maze
{
	constructor(canvas)
	{
		super();
		this.context = canvas.getContext('2d');
		this.context.imageSmoothingEnabled = false;
		this.pathWidth = 4;
		this.pathColor = '#ffffff';
		this.wallWidth = 1;
		this.wallColor = '#000000';
		this.vertexSize = this.pathWidth + 2 * this.wallWidth;
	}
	
	drawPath(vertex)
	{
		const x = this.x(this.tree.vertexColumn(vertex));
		const y = this.y(this.tree.vertexRow(vertex));
		const left = ! this.tree.hasLeftEdge(vertex);
		const top = ! this.tree.hasTopEdge(vertex);
		const right = ! this.tree.hasRightEdge(vertex);
		const bottom = ! this.tree.hasBottomEdge(vertex);

		this.context.beginPath();
		this.context.rect(
			x + this.wallWidth,
			y + top * this.wallWidth,
			this.pathWidth,
			this.vertexSize - (top + bottom) * this.wallWidth);
		this.context.rect(
			x + left * this.wallWidth,
			y + this.wallWidth,
			this.vertexSize - (left + right) * this.wallWidth,
			this.pathWidth);
		this.context.fillStyle = this.pathColor;
		this.context.fill();
	}
	
	drawWall(vertex)
	{
		const x = this.x(this.tree.vertexColumn(vertex));
		const y = this.y(this.tree.vertexRow(vertex));
		const left = ! this.tree.hasLeftEdge(vertex);
		const top = ! this.tree.hasTopEdge(vertex);
		const right = ! this.tree.hasRightEdge(vertex);
		const bottom = ! this.tree.hasBottomEdge(vertex);
		const w = this.wallWidth;
		const l = this.vertexSize + 2 * this.wallWidth;

		this.context.beginPath();
		this.context.rect(x - w, y, l, w * top);
		this.context.rect(x - w, y + this.vertexSize - w, l, w * bottom);
		this.context.rect(x, y - w, w, l * left);
		this.context.rect(x + this.vertexSize - w, y - w, w, l * right);
		this.context.fillStyle = this.wallColor;
		this.context.fill();
	}
	
	x(column)
	{
		return this.vertexSize * column;
	}
	
	y(row)
	{
		return this.vertexSize * row;
	}
	
	column(x)
	{
		return (x - (x % this.vertexSize)) / this.vertexSize;
	}
	
	row(y)
	{
		return (y - (y % this.vertexSize)) / this.vertexSize;
	}

	setPathWidth(width)
	{
		this.pathWidth = width;
	}
	
	setPathColor(color)
	{
		this.pathColor = color;
	}

	setWallColor(color)
	{
		this.wallColor = color;
	}
	
	setWallWidth(width)
	{
		this.wallWidth = width;
	}
	
	generate(width, height, method)
	{
		super.generate(width, height, method);
		this.context.canvas.width = this.x(width);
		this.context.canvas.height = this.y(height);
	}
}

class CanvasMazeGame extends CanvasMaze
{
	constructor(canvas)
	{
		super(canvas);
		this.roots = null;
		this.vertexPathTypes = null;
		this.pathColors = ['#ffffff', '#ff8080', '#8080ff'];
	}
	
	color(vertex)
	{
		const type = this.vertexPathTypes[vertex];
		const color = this.pathColors[type];

		return color;
	}
	
	setPathTypeColor(pathType, color)
	{
		this.pathColors[pathType] = color;
	}
	
	generate(width, height, method)
	{
		super.generate(width, height, method);

		const path = this.tree.longestPath();
		const firstRoot = this.tree.firstVertex(path);
		const secondRoot = this.tree.secondVertex(path);
		
		this.roots = [firstRoot, secondRoot];
		this.vertexPathTypes = new Array(width * height).fill(0);
		this.vertexPathTypes[firstRoot] = 1;
		this.vertexPathTypes[secondRoot] = 2;

		for (const vertex of this.tree.vertices()) {
			this.setPathColor(this.color(vertex));
			this.drawPath(vertex);
			this.drawWall(vertex);
		}
	}

	move(row, column)
	{
		const vertex = this.tree.vertex(row, column);
		const type = this.vertexPathTypes;
		
		if (type[vertex] === 0) {
			for (const adjacentVertex of this.tree.adjacentVertices(vertex)) {
				if (type[adjacentVertex] !== 0) {
					type[vertex] = type[adjacentVertex];
					this.setPathColor(this.color(vertex));
					this.drawPath(vertex);
					return true;
				}
			}
		}
		
		return false;
	}
}

class InteractiveCanvasMazeGame extends CanvasMazeGame
{
	constructor(canvas)
	{
		super(canvas);
		this.mouseAbortController = null;
		this.abortController = null;
	}

	run()
	{
		const controller = new AbortController();
		
		this.context.canvas.addEventListener('mousedown',
			this.handleMouseDown.bind(this), {signal: controller.signal});
		this.context.canvas.addEventListener('touchstart',
			this.handleTouchStart.bind(this), {signal: controller.signal});
		this.context.canvas.addEventListener('touchmove',
			this.handleTouchMove.bind(this), {signal: controller.signal});
		
		this.abortController = controller;
	}

	stop()
	{
		if (this.abortController !== null) {
			this.abortController.abort();
			
			if (this.mouseAbortController !== null) {
				this.mouseAbortController.abort();
			}
		}
	}
	
	move(event)
	{
		const bounds = this.context.canvas.getBoundingClientRect();
		const width = this.context.canvas.width;
		const height = this.context.canvas.height;
		const x = (event.clientX - bounds.left) / bounds.width * width;
		const y = (event.clientY - bounds.top) / bounds.height * height;
		const row = this.row(Math.floor(y));
		const column = this.column(Math.floor(x));
		
		super.move(row, column);
	}

	handleMouseDown(event)
	{
		const controller = new AbortController();
		
		this.move(event);
			
		this.context.canvas.addEventListener('mousemove',
			this.handleMouseMove.bind(this), {signal: controller.signal});
		window.addEventListener('mouseup',
			this.handleMouseUp.bind(this), {signal: controller.signal});
		
		this.mouseAbortController = controller;
	}
	
	handleMouseMove(event)
	{
		this.move(event);
	}
	
	handleMouseUp(event)
	{
		this.mouseAbortController.abort();
	}

	handleTouchStart(event)
	{
		for (const touch of event.changedTouches)
			this.move(touch);
		
		event.preventDefault();
	}
	
	handleTouchMove(event)
	{
		for (const touch of event.changedTouches)
			this.move(touch);
		
		event.preventDefault();
	}
}
