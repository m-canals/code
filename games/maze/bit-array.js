// Representation of a bit array.

class BitArray
{
	// Given a length, create an array of this length.
	
	constructor(length)
	{
		this.arrayLength = length;
		this.array = new Uint8Array(Math.ceil(length / 8));
	}
	
	// Return the length of this array.
	
	get length()
	{
		return this.arrayLength;
	}
	
	// Given an index, return the bit located at this index.
	
	get(index)
	{
		const bit = index % 8;
		const byte = (index - bit) / 8;
		
		return (this.array[byte] >> bit) & 1;
	}
	
	// Given an index, set the bit located at this index.
	
	set(index)
	{
		const bit = index % 8;
		const byte = (index - bit) / 8;
		
		this.array[byte] |= 1 << bit;
	}
	
	// Given an index, clear the bit located at this index.
	
	clear(index)
	{
		const bit = index % 8;
		const byte = (index - bit) / 8;
		
		this.array[byte] &= ~ (1 << bit);
	}
	
	// Given an index, toggle the bit located at this index.
	
	toggle(index)
	{
		const bit = index % 8;
		const byte = (index - bit) / 8;
		
		this.array[byte] ^= 1 << bit;
	}
}
