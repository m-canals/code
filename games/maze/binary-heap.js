// Representation of a binary heap, a binary tree where no node precedes
// its parent for some total order on their value. The root of the tree
// is the top of the heap and the leaves of the tree are the bottom of
// the heap.
//
// A complete binary tree, where nodes are added left-to-right, top-to-
// bottom, and removed in the opposite direction is used, so it can be
// conveniently stored in an array.

class BinaryHeap
{
	// Given a function returning whether a node with a value precedes a
	// node with another value, create a binary heap.
	
	constructor(f = (a, b) => a < b)
	{
		this.array = new Array();
		this.f = f;
	}

	// Return the number of nodes.
	
	get size()
	{
		return this.array.length;
	}
	
	// Return the value of the root.
	
	get top()
	{
		return this.array[0];
	}

	// Given a value, add a leaf with this value and sift it up. Return
	// the number of nodes after insertion.
	
	push(value)
	{
		let i = this.array.push(value) - 1;

		while (true) {
			let p = Math.floor((i - 1) / 2);

			if (p >= 0 && this.f(value, this.array[p])) {
				this.array[i] = this.array[p];
				i = p;
			} else {
				this.array[i] = value;
				break;
			}
		}

		return this.size;
	}

	// Replace the root with a leaf and sift it down. Return the value of
	// the root before deletion.
	
	pop()
	{
		const top = this.top;
		const value = this.array.pop();

		if (this.size > 0) {
			let i = 0;

			while (true) {
				let l = 2 * i + 1;
				let r = l + 1;

				if (l < this.size && this.f(this.array[l], value)) {
					if (r < this.size && this.f(this.array[r], value)) {
						if (this.f(this.array[l], this.array[r])) {
							this.array[i] = this.array[l];
							i = l;
						} else {
							this.array[i] = this.array[r];
							i = r;
						}
					} else {
						this.array[i] = this.array[l];
						i = l;
					}
				} else if (r < this.size && this.f(this.array[r], value)) {
					this.array[i] = this.array[r];
					i = r;
				} else {
					this.array[i] = value;
					break;
				}
			}
		}

		return top;
	}
}
