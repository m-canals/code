// Representation of an undirected simple square grid graph allowing
// loops.

class SquareGridGraph
{
	constructor(width, height)
	{
		this.width = width;
		this.height = height;
		this.size = this.width * this.height;
		this.attributes = new Object();
	}

	vertexRow(vertex)
	{
		return (vertex - this.vertexColumn(vertex)) / this.width;
	}
	
	vertexColumn(vertex)
	{
		return vertex % this.width;
	}

	relativeEdge(vertex, otherVertex)
	{
		if (vertex < otherVertex)
			return 2 * vertex + (otherVertex - vertex) % this.width;
		else
			return 2 * otherVertex + (vertex - otherVertex) % this.width;
	}
	
	relativeEdgeType(edge)
	{
		return edge % 2;
	}
	
	relativeEdgeVertex(edge)
	{
		return (edge - this.relativeEdgeType(edge)) / 2;
	}

	// Methods returning vertices
	// ---------------------------------------------------------------------
	
	vertex(row, column)
	{
		return this.width * row + column;
	}
	
	firstVertex(edge)
	{
		return (edge - this.secondVertex(edge)) / this.size;
	}
	
	secondVertex(edge)
	{
		return edge % this.size;
	}
	
	bottomVertex(vertex)
	{
		return vertex + this.width;
	}
	
	rightVertex(vertex)
	{
		return vertex + 1;
	}
	
	leftVertex(vertex)
	{
		return vertex - 1;
	}

	topVertex(vertex)
	{
		return vertex - this.width;
	}
	
	* incidentVertices(edge)
	{
		yield this.firstVertex(edge);
		yield this.secondVertex(edge);
	}

	* adjacentVertices(vertex)
	{
		if (this.hasBottomEdge(vertex)) yield this.bottomVertex(vertex);
		if (this.hasRightEdge(vertex)) yield this.rightVertex(vertex);
		if (this.hasLeftEdge(vertex)) yield this.leftVertex(vertex);
		if (this.hasTopEdge(vertex)) yield this.topVertex(vertex);
	}

	* vertices()
	{
		for (let i = 0; i < this.size; i++) yield i;
	}

	// Methods returning absolute edges
	// ---------------------------------------------------------------------

	edge(firstVertex, secondVertex)
	{
		return this.size * firstVertex + secondVertex;
	}

	bottomEdge(vertex)
	{
		return this.edge(vertex, this.bottomVertex(vertex));
	}
	
	rightEdge(vertex)
	{
		return this.edge(vertex, this.rightVertex(vertex));
	}
	
	leftEdge(vertex)
	{
		return this.edge(vertex, this.leftVertex(vertex));
	}
	
	topEdge(vertex)
	{
		return this.edge(vertex, this.topVertex(vertex));
	}
	
	* incidentEdges(vertex)
	{
		for (const adjacentVertex of this.adjacentVertices(vertex))
			yield this.edge(vertex, adjacentVertex);
	}
	
	* adjacentEdges(edge)
	{
		for (const incidentVertex of this.incidentVertices(edge))
			for (const adjacentVertex of this.adjacentVertices(incidentVertex))
				if (! this.hasIncidentEdge(adjacentVertex, edge))
					yield this.edge(incidentVertex, adjacentVertex);
	}

	* edges()
	{
		for (let i = 0; i < this.size; i++)
			for (let j = i; j < this.size; j++)
				yield this.edge(i, j);
	}
	
	// Methods returning boolean values
	// -------------------------------------------------------------------
	
	hasBottomVertex(vertex)
	{
		return vertex < this.size - this.width;
	}
	
	hasBottomEdge(vertex)
	{
		return this.hasBottomVertex(vertex);
	}
	
	hasRightVertex(vertex)
	{
		return vertex % this.width < this.width - 1;
	}
	
	hasRightEdge(vertex)
	{
		return this.hasRightVertex(vertex);
	}
	
	hasLeftVertex(vertex)
	{
		return vertex % this.width > 0;
	}
	
	hasLeftEdge(vertex)
	{
		return this.hasLeftVertex(vertex);
	}

	hasTopVertex(vertex)
	{
		return vertex >= this.width;
	}
	
	hasTopEdge(vertex)
	{
		return this.hasTopVertex(vertex);
	}
	
	hasIncidentVertex(edge, vertex)
	{
		for (const incidentVertex of this.incidentVertices(edge))
			if (vertex == incidentVertex)
				return true;
		
		return false;
	}
	
	hasIncidentEdge(vertex, edge)
	{
		return this.hasIncidentVertex(edge, vertex);
	}
	
	hasAdjacentVertex(vertex, otherVertex)
	{
		for (const adjacentVertex of this.adjacentVertices(vertex))
			if (otherVertex == adjacentVertex)
				return true;
		
		return false;
	}

	hasAdjacentEdge(edge, otherEdge)
	{
		const firstVertex = this.firstVertex(edge);
		const secondVertex = this.secondVertex(edge);
		const otherFirstVertex = this.firstVertex(otherEdge);
		const otherSecondVertex = this.secondVertex(otherEdge);
		
		return firstVertex  == otherFirstVertex
		    && secondVertex != otherSecondVertex
		    || firstVertex  == otherSecondVertex
		    && secondVertex != otherFirstVertex
		    || secondVertex == otherFirstVertex
		    && firstVertex  != otherSecondVertex
		    || secondVertex == otherSecondVertex
		    && firstVertex  != otherFirstVertex;
	}

	// Algorithms
	// ---------------------------------------------------------------------

	* depthFirstSearch(vertex)
	{
		const visitedVertices = new Set([vertex]);
		const edgeStack = new Array();
		
		for (const edge of this.incidentEdges(vertex))
			edgeStack.push(edge);

		while (edgeStack.length > 0) {
			const edge = edgeStack.pop();
			const vertex = this.secondVertex(edge);
			
			if (! visitedVertices.has(vertex)) {
				visitedVertices.add(vertex);
				yield edge;
				
				for (const edge of this.incidentEdges(vertex))
					edgeStack.push(edge);
			}
		}
	}
	
	* breadthFirstSearch(vertex)
	{
		const visitedVertices = new Set([vertex]);
		const edgeQueue = new Array();
		
		for (const edge of this.incidentEdges(vertex))
			edgeQueue.push(edge);

		while (edgeQueue.length > 0) {
			const edge = edgeQueue.shift();
			const vertex = this.secondVertex(edge);
			
			if (! visitedVertices.has(vertex)) {
				visitedVertices.add(vertex);
				yield edge;
				
				for (const edge of this.incidentEdges(vertex))
					edgeQueue.push(edge);
			}
		}
	}

	depthFirstSearchTree(vertex)
	{
		const tree = new SquareGridSpanningTree(this.width, this.height);
		
		for (const edge of this.depthFirstSearch(vertex))
			tree.addEdge(this.firstVertex(edge), this.secondVertex(edge));
		
		return tree;
	}

	minimumSpanningTree(vertex, edgePrecedenceFunction)
	{
		const tree = new SquareGridSpanningTree(this.width, this.height);
		const visitedVertices = new Set([vertex]);
		const edgeHeap = new BinaryHeap(edgePrecedenceFunction);
		
		for (const edge of this.incidentEdges(vertex))
			edgeHeap.push(edge);

		while (edgeHeap.size > 0) {
			const edge = edgeHeap.pop();
			const vertex = this.secondVertex(edge);

			if (! visitedVertices.has(vertex)) {
				visitedVertices.add(vertex);
				tree.addEdge(this.firstVertex(edge), vertex);

				for (const edge of this.incidentEdges(vertex))
					edgeHeap.push(edge);
			}
		}
		
		return tree;
	}
}

// Representation of a undirected simple square grid spanning subgraph
// allowing loops.

class SquareGridSpanningSubgraph extends SquareGridGraph
{
	constructor(width, height)
	{
		super(width, height);
		this.edgeArray = new BitArray(this.size * 2);
	}

	addEdge(vertex, otherVertex)
	{
		this.edgeArray.set(this.relativeEdge(vertex, otherVertex));
	}

	* edges()
	{
		for (const vertex of this.vertices)
			for (const adjacentVertex of this.adjacentVertices(vertex))
				if (vertex < adjacentVertex)
					yield this.edge(vertex, adjacentVertex);
	}
	
	hasLeftEdge(vertex)
	{
		return super.hasLeftEdge(vertex) &&
		       this.vertexIsAdjacent(vertex, vertex - 1);
	}
	
	hasRightEdge(vertex)
	{
		return super.hasRightEdge(vertex) &&
		       this.vertexIsAdjacent(vertex, vertex + 1);
	}
	
	hasTopEdge(vertex)
	{
		return super.hasTopEdge(vertex) &&
		       this.vertexIsAdjacent(vertex, vertex - this.width);
	}
	
	hasBottomEdge(vertex)
	{
		return super.hasBottomEdge(vertex) &&
		       this.vertexIsAdjacent(vertex, vertex + this.width);
	}

	vertexIsAdjacent(vertex, otherVertex)
	{
		return this.edgeArray.get(this.relativeEdge(vertex, otherVertex));
	}
	
	edgeIsIncident(vertex, edge)
	{
		return this.vertexIsAdjacent(vertex, this.firstVertex(edge)) ||
		       this.vertexIsAdjacent(vertex, this.secondVertex(edge));
	}

	* adjacentVertices(vertex)
	{
		for (const otherVertex of super.adjacentVertices(vertex))
			if (this.vertexIsAdjacent(vertex, otherVertex))
				yield otherVertex;
	}

	* incidentEdges(vertex)
	{
		for (const edge of super.incidentEdges(vertex))
			if (this.edgeIsIncident(vertex, edge))
				yield edge;
	}
}

// Representation of a undirected simple square grid spanning tree.

class SquareGridSpanningTree extends SquareGridSpanningSubgraph
{
	farthestVertex(vertex)
	{
		const visitedVertices = new Set();
		const distanceStack = [0];
		const vertexStack = [vertex];
		let farthestVertex = vertex;
		let farthestDistance = 0;
		
		while (vertexStack.length > 0) {
			const vertex = vertexStack.pop();
			const distance = distanceStack.pop();
			let leaf = true;

			for (const adjacentVertex of this.adjacentVertices(vertex)) {
				if (! visitedVertices.has(adjacentVertex)) {
					visitedVertices.add(adjacentVertex);
					vertexStack.push(adjacentVertex);
					distanceStack.push(distance + 1);
					leaf = false;
				}
			}
			
			if (leaf && distance > farthestDistance) {
				farthestVertex = vertex;
				farthestDistance = distance;
			}
		}
		
		return farthestVertex;
	}

	vertexDistances(vertex)
	{
		const distances = new Array(this.size).fill(Infinity);
		const stack = [vertex];
		
		distances[vertex] = 0;
		
		while (stack.length > 0) {
			const vertex = stack.pop();
			
			for (const adjacentVertex of this.adjacentVertices(source)) {
				if (! visitedVertices.has(adjacentVertex)) {
					visitedVertices.add(adjacentVertex);
					distances[adjacentVertex] = distances[vertex] + 1;
				}
			}
		}
		
		return distances;
	}
	
	distances()
	{
		const distances = new Array(this.size ** 2).fill(Infinity);

		for (let i = 0; i < this.size; i++)
			distances[this.edge(i, i)] = 0;
		
		for (const edge of this.edgeSubset) {
			const source = this.firstVertex(edge);
			const target = this.secondVertex(edge);

			for (let i = 0; i < this.size; i++) {
				const toTarget = this.edge(i, target);
				const toSource = this.edge(i, source);
				const fromTarget = this.edge(target, i);
				const fromSource = this.edge(source, i);
				
				if (i == source) {
					// source -> target
					distances[toTarget] = 1;
				} else if (i == target) {
					// target -> source
					distances[toSource] = 1;
				} else {
					// source -> target -> ... -> i
					if (Number.isFinite(distances[fromTarget]))
						distances[fromSource] = distances[fromTarget] + 1;
					// source <- target <- ... <- i
					if (Number.isFinite(distances[toTarget]))
						distances[toSource] = distances[fromTarget] + 1;
					// i -> ... -> source -> target
					if (Number.isFinite(distances[toSource]))
						distances[toTarget] = distances[toSource] + 1;
					// i <- ... <- source <- target
					if (Number.isFinite(distances[fromSource]))
						distances[fromTarget] = distances[fromSource] + 1;
				}
			}
		}
	}

	longestPath()
	{
		if (this.size <= 0)
			return undefined;
		
		const vertex = this.farthestVertex(0);
		const otherVertex = this.farthestVertex(vertex);
		
		return this.edge(vertex, otherVertex);
	}
	
	// Naive algorithm (quadratic time complexity).
	/*longestPath()
	{
		const distances = this.distances();
		let longestPath = undefined;
		let maximumDistance = -1;
		
		for (let path = 0; path < distances.length; path++) {
			if (distances[path] > maximumDistance) {
				longestPath = path;
				maximumDistance = distances[path];
			}
		}
		
		return longestPath;
	}*/
}
