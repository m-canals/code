class Shapes
{
	constructor(...shapes)
	{
		this.shapes = [...shapes];
	}
	
	* [Symbol.iterator]()
	{
		for (const shape of this.shapes)
			yield shape;
	}
	
	filter(condition)
	{
		this.shapes = this.shapes.filter(condition);
		
		return this;
	}
}

class Point
{
	static epsilon = Number.EPSILON;

	// Origin point (0,0).

	static origin = new Point(0, 0);
	
	// Given two points, return the one that is less than the other one.
	// See `Point.isLessThan`.
	
	static min(a, b)
	{
		return a.isLessThan(b) ? a : b;
	}
	
	// Given two points, return the one that is greater than the other one.
	// See `Point.isGreaterThan`.
	
	static max(a, b)
	{
		return a.isGreaterThan(b) ? a : b;
	}

	// Return a random point in [0,1)².
	
	static random()
	{
		return new Point(Math.random(), Math.random());
	}
	
	// Given some points, return the arithmetic mean point of these points.
	
	static arithmeticMean(...points)
	{
		const x = 0;
		const y = 0;
		
		for (const point of points) {
			x += point.x;
			y += point.y;
		}
		
		return new Point(x / points.length, y / points.length);
	}
	
	// Given two coordinates, create a point located at these coordinates.
	// If no coordinates are given, create a point located at (0,0).
	
	constructor(x = 0, y = 0)
	{
		this.x = x;
		this.y = y;
	}
	
	// Return whether this point is finite.
	
	isFinite()
	{
		return Number.isFinite(this.x) && Number.isFinite(this.y);
	}

	// Given a point, return whether this point is less than this other
	// point, i.e. its x coordinate is less or its x coordinate is equal and
	// its y coordinate is less.
	
	isLessThan(point)
	{
		return this.x < point.x || (this.x == point.x && this.y < point.y);
	}
	
	// Given a point, return whether this point is equal to this other
	// point, i.e. its x coordinate is equal and its y coordinate is equal.
	
	isEqualTo(point)
	{
		return this.x == point.x && this.y == point.y;
	}

	// Given a point, return whether this point is greater than this other
	// point, i.e. its x coordinate is greater or its x coordinate is equal
	// and its y coordinate is greater.
	
	isGreaterThan(point)
	{
		return this.x > point.x || (this.x == point.x && this.y > point.y);
	}
	
	// Given a point, return whether this point is approximate to this other
	// point, i.e. its x coordinate is approximate and its y coordinate is
	// approximate. A ccoordinate is approximate to another if the distance
	// between them is less than or equal to `Point.epsilon`.
	
	isApproximateTo(point)
	{
		return Math.abs(this.x - point.x) <= Point.EPSILON &&
		       Math.abs(this.y - point.y) <= Point.EPSILON;
	}
	
	// Given a point, return the radial coordinate of this point in a
	// polar coordinate system centered at this other point. Given no point,
	// return the radial coordinate of this point in a polar coordinate
	// system centered at (0,0).

	distance(point = Point.origin)
	{
		return Math.sqrt((this.x - point.x) ** 2 + (this.y - point.y) ** 2);
	}
	
	// Given a point, return the angular coordinate of this point in a
	// polar coordinate system centered at this other point. Given no point,
	// return the angular coordinate of this point in a polar coordinate
	// system centered at (0,0).
	
	angle(point = Point.origin)
	{
		return Math.atan2(this.y - point.y, this.x - point.x);
	}

	// Given a shape, return the intersection of this point and this shape.

	intersection(shape)
	{
		return shape.pointIntersection(this);
	}
	
	// Given a point, return the intersection of this point and this other
	// point, which can be null or a point.
	
	pointIntersection(point)
	{
		if (this.x == point.x && this.y == point.y)
			return new Point(this.x, this.y);
		else
			return null;
	}

	// Given two distances, translate this point by adding these distances
	// to its x and y coordinates and return this point, allowing method
	// chaining.
	
	translate(x, y)
	{
		this.x += x;
		this.y += y;
		
		return this;
	}
	
	// Given two factors, scale this point by multiplying its x and y
	// coordinates by these factors and return this point, allowing method
	// chaining.
	
	scale(x, y)
	{
		this.x *= x;
		this.y *= y;
		
		return this;
	}
	
	// Given an angle in radians and a point, rotate this point around this
	// other point by this angle and return this point, allowing method
	// chaining.
	
	rotate(theta, point = Point.origin)
	{
		const distance = this.distance();
		const angle = this.angle(point) + theta;
		
		this.x = distance * Math.cos(angle) + point.x;
		this.y = distance * Math.sin(angle) + point.y;
		
		return this;
	}
}

class Line
{
	// Return a line with characteristic points in [0,1)².

	static random()
	{
		return new Line(
			Math.random(), Math.random(),
			Math.random(), Math.random());
	}

	// Given the reasonably-sized (see `Number.MAX_VALUE`) finite
	// coordinates of two distinct points, create a line containing these
	// points.
	
	constructor(x1, y1, x2, y2)
	{
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}

	// Return the slope of this line.
	//
	// - If this line is parallel to the y-axis it is infinite.
	// - If this line is not parallel to the y-axis it is finite.

	get slope()
	{
		return (this.y2 - this.y1) / (this.x2 - this.x1);
	}
	
	// Return the y-intercept of this line.
	//
	// - If this line does not intersect the y-axis it is infinite.
	// - If this line intersects the y-axis once it is finite.
	// - If this line intersects the y-axis more than once it is NaN.
	
	get intercept()
	{
		return (this.x2 * this.y1 - this.y2 * this.x1) / (this.x2 - this.x1);
	}
	
	x(y)
	{
		const a = this.slope;
		const b = this.intercept;
		const x = (y - b) / a;
		
		// if slope inf this.x
		// if slope 0 nan
		// else return x;
	}
	
	y(x)
	{
		const a = this.slope;
		const b = this.intercept;
		const y = a * x + b;
		
		// if slope inf nan
		// if slope 0 this.y
		// else return y
	}
	
	// Given a shape, return the intersection of this line and this shape,
	
	intersection(shape)
	{
		return shape.lineIntersection(this);
	}
	
	// Given a point, return the intersection of this line and this point,
	// which can be null or a point.
	
	pointIntersection(point)
	{
		const a = this.slope;
		const b = this.intercept;
		const f = a * point.x + b - point.y;
		
		if (Number.isFinite(f))
			return Math.abs(f) <= Point.epsilon ? new Point(point.x, point.y) : null;
		else if (Math.abs(point.x - (this.x1 + this.x2) / 2) <= Point.epsilon)
			return new Point(point.x, point.y)
		else
			return null;
	}

	// Given a line, return the intersection of this line and this other
	// line, which can be null, a point or a line.
	
	lineIntersection(line)
	{
		const a1 = this.slope;
		const a2 = line.slope;
		const b1 = this.intercept;
		const b2 = line.intercept;

		// Both lines are parallel to the y-axis.
		if (! Number.isFinite(b1) && ! Number.isFinite(b2)) {
			// Intersecting and parallel to each other.
			if (this.x1 == line.x1) {
				return new Line(this.x1, this.y1, this.x2, this.y2);
			// Not intersecting and parallel to each other.
			} else {
				return null;
			}
		// Some line is parallel to the y-axis.
		} else if (! Number.isFinite(b1) && Number.isFinite(b2)) {
			return new Point(this.x1, a2 * this.x1 + b2);
		} else if (Number.isFinite(b1) && ! Number.isFinite(b2)) {
			return new Point(line.x1, a1 * line.x1 + b1);
		// Any line is not parallel to the y-axis.
		} else {
			// Intesecting and not parallel to each other.
			if (a1 !== a2) {
				// Let (x, y) be the point of intersection, since both lines
				// contain this point, we can solve the system of equations
				// a1 * x + b1 = y and b1 * x + b2 = y.
				const x = (b2 - b1) / (a1 - a2);
				const y = a1 * x + b1;
				return new Point(x, y);
			// Intersecting and parallel to each other.
			} else if (b1 === b2) {
				return new Line(this.x1, this.y1, this.x2, this.y2);
			// Not intersecting and parallel to each other.
			} else {
				return null;
			}
		}
	}
	
	// Given a circle, return the intersection of this line and this circle,
	// which can be null, a point or two points.
	
	circleIntersection(circle)
	{
		return circle.lineIntersection(this);
	}
	
	// Given two distances, translate this line by adding these distances
	// to its characteristic points and return this line, allowing method
	// chaining.
	
	translate(x, y)
	{
		this.x1 += x;
		this.y1 += y;
		this.x2 += x;
		this.y2 += y;
		
		return this;
	}
	
	// Given two factors, scale this line by scaling its characteristic
	// points by these factors and return this line, allowing method
	// chaining.
	
	scale(x, y)
	{
		this.x1 *= x;
		this.y1 *= y;
		this.x2 *= x;
		this.y2 *= y;
		
		return this;
	}
}

class Segment extends Line
{
	// Return a segment with random points in [0,1)².

	static random()
	{
		return new Segment(
			Math.random(), Math.random(),
			Math.random(), Math.random());
	}

	// Return the minimum x coordinate of this segment.
	
	get left()
	{
		return Math.min(this.x1, this.x2);
	}
	
	// Return the maximum x coordinate of this segment.
	
	get right()
	{
		return Math.max(this.x1, this.x2);
	}
	
	// Return the minimum y coordinate of this segment.
	
	get bottom()
	{
		return Math.min(this.y1, this.y2);
	}
	
	// Return the maximum y coordinate of this segment.
	
	get top()
	{
		return Math.max(this.y1, this.y2);
	}
	
	// Return the minimum point of this segment. See `Point.min`.
	
	get minimum()
	{
		return Point.min(
			new Point(this.x1, this.y1), new Point(this.x2, this.y2));
	}
	
	// Return the minimum point of this segment. See `Point.max`.
	
	get maximum()
	{
		return Point.max(
			new Point(this.x1, this.y1), new Point(this.x2, this.y2));
	}
	
	// Given a number, return whether this segment contains a point
	// whose x coordinate is this number.
	
	isInDomain(x)
	{
		return this.left <= x && x <= this.right;
	}
	
	// Given a number, return whether this segment contains a point
	// whose y coordinate is this number.
	
	isInImage(y)
	{
		return this.bottom <= y && y <= this.top;
	}
	
	// Given a point, return whether the bounding box of this segment
	// contains this point.
	
	isInBoundingRectangle(point)
	{
		return this.isInDomain(point.x) && this.isInImage(point.y);
	}
	
	// Given a shape, return the intersection of this segment and this
	// shape.
	
	intersection(shape)
	{
		return shape.segmentIntersection(this);
	}
	
	// Given a point, return the intersection of this segment and this
	// point, which can be null or a point.
	
	pointIntersection(point)
	{
		const intersection = super.pointIntersection(point);
		
		if (intersection instanceof Point) {
			const isInThis = this.isInBoundingRectangle(intersection);
			if (isInThis) return intersection;
			else return null;
		} else {
			return null;
		}
	}
	
	// Given a line, return the intersection of this segment and this line,
	// which can be null, a point or a segment.
	
	lineIntersection(line)
	{
		const intersection = super.lineIntersection(line);

		if (intersection instanceof Point) {
			const isInThis = this.isInBoundingRectangle(intersection);
			
			if (isInThis) return intersection;
			else return null;
		}
		else if (intersection instanceof Line) {
			// TODO
		}
		else {
			return null;
		}
	}
	
	// Given a segment, return the intersection of this segment and this
	// other segment, which can be null, a point or a segment.

	segmentIntersection(segment)
	{
		const intersection = super.lineIntersection(segment);
		
		if (intersection instanceof Point) {
			const isInThis = this.isInBoundingRectangle(intersection);
			const isInThisOther= segment.isInBoundingRectangle(intersection);
			
			if (isInThis && isInThisOther) return intersection;
			else return null;
		}
		else if (intersection instanceof Line) {
			const a = Point.max(this.minimum, segment.minimum);
			const b = Point.min(this.maximum, segment.maximum);

			if (a.isLessThan(b)) return new Segment(a.x, a.y, b.x, b.y);
			else return null;
		}
		else {
			return null;
		}
	}
	
	// Given a circle, return the intersection of this segment and this
	// circle, which can be null, a point, two points or a circle.

	circleIntersection(circle)
	{
		const intersection = super.circleIntersection(circle);
		
		if (intersection instanceof Point) {
			if (this.isInBoundingRectangle(intersection)) return intersection;
			else return null;
		} else if (intersection instanceof Shapes) {
			return intersection.filter(this.isInBoundingRectangle.bind(this));
		} else {
			return null;
		}
	}
}

class Circle
{
	// Return a circle of a random size in [0,1) centered at a random point
	// in [0,1)².

	static random()
	{
		return new Circle(Math.random(), Math.random(), Math.random());
	}
	
	// Given two coordinates and a radius, create a circle of this radius
	// centered at these coordinates. If no coordinates are given, create a
	// circle centered at (0,0). If no radius is given, create a circle of
	// radius 1.
	
	constructor(x = 0, y = 0, r = 1)
	{
		this.center = new Point(x, y);
		this.r = r;
	}

	// Given a shape, return the intersection of this circle and this shape.

	intersection(shape)
	{
		return shape.circleIntersection(this);
	}
	
	// Given a point, return the intersection of this circle and this point,
	// which can be null or a point.
	
	pointIntersection(point)
	{
		if (Math.abs(point.distance(this.center) - this.r) <= Point.epsilon)
			return new Point(point.x, point.y);
		else
			return null;
	}
	
	// Given a line, return the intersection of this circle and this line,
	// which can be null, a point or two points.
	
	lineIntersection(line)
	{
		// Let us assume this circle is centered at the origin. First, we need
		// to translate the line accordingly.

		line = Object.create(line).translate(-this.center.x, -this.center.y);
		
		const a = line.slope;
		const b = line.intercept;
		const r = this.r;
		
		// Next, we find the distance d0 between the origin and the point
		// (x0, y0) that belongs to the line and is closest to the origin.
		//
		//  d0 = min {sqrt(x ** 2 + y ** 2) : a * x + b = y}
		//  d0 = min {sqrt(x ** 2 + (a * x + b) ** 2)}
		//  d0 = min {f(x)}
		//  d0 = f(x), df / dx = 0

		let x0, y0, d0 = b / Math.sqrt(a ** 2 + 1);
		
		// Line parallel to y-axis.
		if (! Number.isFinite(d0)) {
			d0 = (line.x1 + line.x2) / 2;
			x0 = d0;
			y0 = 0;
		// Line not parallel to y-axis.
		} else {
			x0 = - a * b / (a ** 2 + 1);
			y0 = b / (a ** 2 + 1);
		}
		
		// Now, we find the distance d between (x0, y0) and a point of
		// intersection (x, y). Note that:
		//
		//  - If d0 < r they intersect twice (distinct points).
		//  - If d0 = r they intersect once (same points).
		//  - If d0 > r they do not intersect (non-finite points).

		let d = Math.sqrt(r ** 2 - d0 ** 2);

		// Finally, we multiply d1 and the normalized vector of the slope of
		// the line in order to get the coordinate absolute differences that
		// we then add to and subtract from (x0, y0).
		//
		//  (dx, dy) = d * (1, a) / |(1, a)|
		//           = d * (1, a) / sqrt(1 ** 2 + a ** 2)
		//           = (d / sqrt(a ** 2 + 1), a * d / sqrt(a ** 2 + 1))
		//
		// Note that we need to translate the points to its original position.

		let dx = d / Math.sqrt(a ** 2 + 1);
		let dy = a * dx;
		let p1 = new Point(x0 + dx + this.center.x, y0 + dy + this.center.y);
		let p2 = new Point(x0 - dx + this.center.x, y0 - dy + this.center.y);

		if (! p1.isFinite()) return null;
		if (p1.isApproximateTo(p2)) return Points.arithmeticMean(p1, p2);
		else return new Shapes(p1, p2);
	}
	
	// Given a segment, return the intersection of this circle and this
	// segment, which can be null, a point or two points.
	
	segmentIntersection(segment)
	{
		return segment.circleIntersection(this);
	}
	
	// Given a circle, return the intersection of this circle and this
	// other circle, which can be null, a point, two points or a circle.
	
	circleIntersection(circle)
	{
		// Let us assume this circle is centered at the origin and the other
		// circle is centered in the positive y axis, so we can easily solve
		// the following equations, where (x,±y) are the points belonging to 
		// both circles, r1 and r2 are the radii of the circles and d is the
		// distance between the centers of the circles.
		//
		//  (x - 0) ** 2 + (y - 0) ** 2 = r1 ** 2
		//  (x - d) ** 2 + (y - 0) ** 2 = r2 ** 2
		//
		// We get:
		//
		//  x = d / 2 + (r1 + r2) / 2 * (r1 - r2) / d
		//  y = sqrt(r1 ** 2 - x ** 2)
		//
		// Note that:
		//
		//  - If d = 0 and r1 = r2 they are the same.
		//  - If d = 0 and r1 ≠ r2 they do not intersect.
		//  - If d > 0 and x = r1 (y = 0) they intersect once.
		//  - If d > 0 and x < r1 (y > 0) they intersect twice.
		//  - If d < 0 and x > r1 they do not intersect.
		//
		// Finally, we rotate and translate the points to their actual
		// positions.

		const x1 = this.center.x;
		const y1 = this.center.y;
		const r1 = this.r;
		const r2 = circle.r;
		const d = circle.center.distance(this.center);
		const a = circle.center.angle(this.center);

		let x = d / 2 + (r1 + r2) / 2 * (r1 - r2) / d;
		let y = Math.sqrt(r1 ** 2 - x ** 2);
		let p1 = new Point(x, y).rotate(a).translate(x1, y1);
		let p2 = new Point(x, -y).rotate(a).translate(x1, y1);
		
		if (! p1.isFinite()) return r1 == r2 ? new Circle(x1, y1, r1) : null;
		if (p1.isApproximateTo(p2)) return Points.arithmeticMean(p1, p2);
		else return new Shapes(p1, p2);
	}

	// Given two distances, translate the center of this circle by adding
	// these distances to its x and y coordinates and return this circle,
	// allowing method chaining.
	
	translate(x, y)
	{
		this.center.translate(x, y);
		
		return this;
	}

	// TODO
	scale(xy, point = Point.origin)
	{
		return this;
	}
	
	// Given an angle in radians, rotate the center of this circle around
	// (0,0) by adding this angle to its angular coordinate and return this
	// circle, allowing method chaining.
	
	rotate(angle, point = Point.origin)
	{
		this.center.rotate(angle, point);
		
		return this;
	}
}
