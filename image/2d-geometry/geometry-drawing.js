

class GeometryDrawing
{
	constructor(context)
	{
		this.context = context;
		this.pointColor = 'black';
		this.pointRadius = 0.05;
		this.lineColor = 'black';
		this.lineWidth = 0.02;
		// TODO Pass transofmratin
	}

	draw(shape)
	{
		this.context.save();
		this.context.beginPath();
		
		if (shape instanceof Point) {
			this.context.fillStyle = this.pointColor;
			this.context.arc(shape.x, shape.y, this.pointRadius, 0, Math.PI * 2);
			this.context.fill();
		} else if (shape instanceof Segment) {
			this.context.lineWidth = this.lineWidth;
			this.context.strokeStyle = this.lineColor;
			this.context.moveTo(shape.x1, shape.y1);
			this.context.lineTo(shape.x2, shape.y2);
			this.context.stroke();
		} else if (shape instanceof Line) {
			this.context.lineWidth = this.lineWidth;
			this.context.strokeStyle = this.lineColor;
			// TODO
			this.context.stroke();
		} else if (shape instanceof Circle) {
			this.context.lineWidth = this.lineWidth;
			this.context.strokeStyle = this.lineColor;
			this.context.arc(shape.center.x, shape.center.y, shape.r, 0, Math.PI * 2);
			this.context.stroke();
		} else if (shape instanceof Shapes) {
			for (const subshape of shape) this.draw(subshape);
		}
		
		this.context.restore();
	}
}
