#!/usr/bin/env python3

class SierpinskiTriangleTurtle:
	def __init__(self, turtle, iterations, size):
		self.turtle = turtle
		self.iterations = iterations
		self.size = size

class SubstitutiveSierpinskiTriangleTurtle(SierpinskiTriangleTurtle):
	def run(self):
		string = "+x"
		for i in range(self.iterations):
			string = string.translate({ord("x"): "y+x+y", ord("y"): "x-y-x"})
		for sign in string[0::2]:
			self.turtle.right(60) if sign == '+' else turtle.left(60)
			self.turtle.forward(self.size)

class RecursiveSierpinskiTriangleTurtle(SierpinskiTriangleTurtle):
	def __x(self, iteration, direction):
		if iteration < self.iterations:
			self.__y(iteration + 1, direction)
			self.__x(iteration + 1, self.turtle.right)
			self.__y(iteration + 1, self.turtle.right)
		elif iteration == self.iterations:
			direction(60)
			self.turtle.forward(self.size)
	def __y(self, iteration, direction):
		if iteration < self.iterations:
			self.__x(iteration + 1, direction)
			self.__y(iteration + 1, self.turtle.left)
			self.__x(iteration + 1, self.turtle.left)
		elif iteration == self.iterations:
			direction(60)
			self.turtle.forward(self.size)
	def run(self):
		self.__x(0, self.turtle.right)

if __name__ == '__main__':
	import turtle
	turtle.setworldcoordinates(-1, -1, 1, 1)
	turtle.color('black')
	turtle.speed(0)
	turtle.hideturtle()
	turtle.penup()
	turtle.goto(0,0)
	turtle.pendown()
	RecursiveSierpinskiTriangleTurtle(turtle, 5, 0.025).run()
	turtle.done()
