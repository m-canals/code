Sierpinski Arrowhead Curve Generator
====================================

Generate a rotated [Sierpiński arrowhead curve](https://en.wikipedia.org/wiki/Sierpi%C5%84ski_curve#Arrowhead_curve) with Python turtle module.

![Generated Sierpiński arrowhead curve.](screenshot.png "Generated Sierpiński arrowhead curve.")

Usage
-----

	python3 generate-sierpinski-arrowhead-curve.py

