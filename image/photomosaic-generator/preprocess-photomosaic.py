#!/usr/bin/env python3

# TODO
#
#  - Use directories instead of files as arguments.

from photomosaic import Data
from sys import stderr
from os import path

def handle_error(error, exit):

	message = str(error)
	
	if len(message) == 0:
		print("Unknown error.", file = stderr)
	else:
		print(message[0].upper() + message[1:] + '.', file = stderr)
	
	if exit:
		print("Exiting.", file = stderr)
		exit(1)

def _parse_arguments():

	parser = ArgumentParser(
		add_help = False,
		description = "Preprocess photomosaic data so multiple "
			"generations with the same set of images can be done. Data "
			"must be preprocessed even if only one photomosaic is "
			"generated. Currently it computes the mean color of each image " 
			"for each image mode (the original mode if there is not any "
			"mode) and saves it along with the filename, "
			"the image mode and the original image mode.")
	parser.add_argument(
		metavar = "IMAGE-FILE",
		dest = 'image_paths',
		nargs = '*',
		type = str,
		default = [],
		help = "Preprocess input image from file IMAGE-FILE.")
	parser.add_argument('-o', '--output',
		metavar = "OUTPUT-FILE",
		dest = 'output_path',
		type = str,
		required = True,
		help = "Save output data to file OUTPUT-FILE.")
	parser.add_argument('-m', '--modes',
		metavar = "MODE",
		dest = 'image_modes',
		nargs = '*',
		type = str,
		choices = Data.Modes,
		default = [],
		required = False,
		help = "Compute image metadata for mode MODE (L, RGB, RGBA, CMYK, HSV).")
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")
	
	return parser.parse_args()

def _main():

	arguments = _parse_arguments()
	
	try:
		print("Preprocessing data... ")
		data = Data()

		for image_path in arguments.image_paths:
		
			image_filename = path.basename(image_path)

			if len(arguments.image_modes) > 0:
				image_modes = set(arguments.image_modes)
			else:
				image_modes = {None}

			for image_mode in image_modes:
			
				if image_mode == None:
					image_mode_name = "default"
				else:
					image_mode_name = image_mode
				
				try:
					data.add_image_metadata(image_path, image_mode)
				except OSError as error:
					handle_error(error, False)
					print("Ingnoring %s mode for image %s."
						%(image_mode_name, image_filename), file = stderr)
				except Data.ParameterValueError as error:
					handle_error(error, False)
					print("Ingnoring %s mode for image %s."
						%(image_mode_name, image_filename), file = stderr)
				except Data.ConversionError as error:
					handle_error(error, False)
					print("Ingnoring %s mode for image %s."
						%(image_mode_name, image_filename), file = stderr)
				except Exception as error:
					handle_error(error, True)
				
		print("Done.")

		print("Saving preprocessed data... ")
		data.save(arguments.output_path)
		print("Done.")
	except Exception as error:
		handle_error(error, True)

if __name__ == '__main__':
	from argparse import ArgumentParser
	_main()
