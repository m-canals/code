#!/usr/bin/env python3

# TODO
#
#  - Look for a better image matching algorithm (maybe try to
#    subdivide each image).
#  - Try a faster library for minimum calculation (maybe NumPy).
#  - Improve progress indicator: number of progress updates as
#    progress parameter, other types of updates, etc.
#  - Handle KeyboardInterrupt exceptions appropriately.
#  - Add threading to preprocessing.

from PIL import Image, ImageDraw
from xml.etree import ElementTree
from random import shuffle, seed
from threading import Thread, Semaphore

class MathUtilities:

	def get_square_distance(a, b):

		return sum((x - y) * (x - y) for x, y in zip(a, b))

class ImageUtilities:

	def get_image_band_mean_value(band_index, band_size, histogram):

		first = band_size * band_index
		last = band_size * (band_index + 1)
		frequencies = histogram[first:last]
		weighted_sum = sum(value * frequency for value, frequency in enumerate(frequencies))
		mean_value = weighted_sum / sum(frequencies)
		
		return round(mean_value)

	def get_image_mean_color(image, mask = None):

		histogram = image.histogram(mask = mask)
		bands = image.getbands()
		band_count = len(bands)
		# We assume all bands are the same size.
		band_size = len(histogram) // band_count
		
		return tuple(ImageUtilities.get_image_band_mean_value(band_index, band_size, histogram)
			for band_index in range(band_count))

	def get_image_cell_box(image, rows, columns, row, column):

		input_width, input_height = image.size
		input_column_width = input_width / columns
		input_row_height = input_height / rows
		
		x0 = round(column * input_column_width)
		x1 = round((column + 1) * input_column_width) - 1
		y0 = round(row * input_row_height)
		y1 = round((row + 1) * input_row_height) - 1
		
		return (x0, y0, x1, y1)
	
	def get_image_cell_mask(image, rows, columns, row, column):
	
		mask = Image.new('1', image.size, color = 0)
		draw = ImageDraw.Draw(mask)
		box = ImageUtilities.get_image_cell_box(image, rows, columns, row, column)
		draw.rectangle(box, fill = 1)
		return mask

class ImageMetadata:

	def __init__(self, file = None, mode = None, convert_mode = None,
		mean_color = None):

		self.file = file
		self.mode = mode
		self.convert_mode = convert_mode
		self.mean_color = mean_color
	
	def __repr__(self):
	
		return "ImageMetadata(%s, %s, %s)" % (
			repr(self.file), repr(self.mode), repr(self.mean_color))

class Data:

	Modes = Image.MODES
	
	class ParameterValueError(Exception):
		
		def __init__(self, name, value):
			
			self.name = name
			self.value = value
		
		def __str__(self):
		
			return f"invalid value for parameter {self.name}: {self.value}"
	
	class AttributeValueError(Exception):
		
		def __init__(self, name, value):
			
			self.name = name
			self.value = value
		
		def __str__(self):
		
			return f"invalid value for attribute {self.name}: {self.value}"

	class ConversionError(Exception):
	
		def __init__(self, mode, convert_mode):
		
			self.mode = mode
			self.convert_mode = convert_mode
		
		def __str__(self):
		
			return f"invalid image conversion from {self.mode} to {self.convert_mode}"

	class AttributeNotFoundError(Exception):
		
		def __init__(self, name):
	
			self.name = name
		
		def __str__(self):
		
			return f"missing attribute: {self.name}"

	DATA_TAG = 'data'
	IMAGE_METADATA_LIST_TAG = 'images'
	IMAGE_METADATA_TAG = 'image'
	FILE_ATTRIBUTE = 'file'
	MODE_ATTRIBUTE = 'mode'
	MEAN_COLOR_ATTRIBUTE = 'mean-color'
	CONVERT_MODE_ATTRIBUTE = 'convert-mode'

	def __init__(self):
	
		self.image_metadata_list = list()
	
	def add_image_metadata(self, image_path, image_mode = None):
		if image_mode != None and image_mode not in Data.Modes:
			raise Data.ParameterValueError('image_mode', image_mode)

		with Image.open(image_path) as image:
		
			try:
				if image_mode != None and image_mode != image.mode:
					image = image.convert(image_mode)
			except ValueError:
				raise Data.ConversionError(image.mode, image_mode)

			convert_mode = image.mode if image_mode == None else image_mode
			mean_color = ImageUtilities.get_image_mean_color(image)
			image_metadata = ImageMetadata(image_path, image.mode, convert_mode, mean_color)
			self.image_metadata_list.append(image_metadata)
	
	def save(self, output_path):
	
		data_element = ElementTree.Element(
			Data.DATA_TAG)
		data_element.text = "\n"
		data_element.tail = "\n"
		
		images_element = ElementTree.SubElement(data_element,
			Data.IMAGE_METADATA_LIST_TAG)
		images_element.text = "\n"
		images_element.tail = "\n"
		
		for image_metadata in self.image_metadata_list:
			mean_color = ','.join(map(str, image_metadata.mean_color))
			image_element = ElementTree.SubElement(images_element,
				Data.IMAGE_METADATA_TAG, attrib = {
					Data.FILE_ATTRIBUTE: image_metadata.file,
					Data.MODE_ATTRIBUTE: image_metadata.mode,
					Data.CONVERT_MODE_ATTRIBUTE: image_metadata.convert_mode,
					Data.MEAN_COLOR_ATTRIBUTE: mean_color})
			image_element.tail = "\n"
		
		output = ElementTree.tostring(data_element)		
		
		with open(output_path, "wb") as output_file:
			output_file.write(output)

	def load(self, input_path):
	
		def nat(value):
			value = int(value)
			if value < 0:
				raise ValueError
			return value
		
		tree = ElementTree.parse(input_path)

		for element in tree.getroot():
			if element.tag == Data.IMAGE_METADATA_LIST_TAG:
				for subelement in element:
					if subelement.tag == Data.IMAGE_METADATA_TAG:
						try:
							file = subelement.attrib[Data.FILE_ATTRIBUTE]
						except KeyError:
							raise Data.AttributeNotFoundError(Data.FILE_ATTRIBUTE)
						
						try:
							mode = subelement.attrib[Data.MODE_ATTRIBUTE]
							if mode not in Data.Modes:
								raise Data.AttributeValueError(Data.MODE_ATTRIBUTE, mode)
						except KeyError:
							raise Data.AttributeNotFoundError(Data.MODE_ATTRIBUTE)
						
						try:
							convert_mode = subelement.attrib[Data.CONVERT_MODE_ATTRIBUTE]
							if convert_mode not in Data.Modes:
								raise Data.AttributeValueError(Data.CONVERT_MODE_ATTRIBUTE, convert_mode)
						except KeyError:
							raise Data.AttributeNotFoundError(Data.CONVERT_MODE_ATTRIBUTE)
						
						try:
							mean_color = subelement.attrib[Data.MEAN_COLOR_ATTRIBUTE]
							mean_color = tuple(map(nat, mean_color.split(',')))
						except KeyError:
							raise Data.AttributeNotFoundError(Data.MEAN_COLOR_ATTRIBUTE)
						except ValueError:
							raise Data.AttributeValueError(Data.MEAN_COLOR_ATTRIBUTE, mean_color)
						
						try:
							Image.new(mode, (0, 0)).convert(convert_mode)
						except ValueError:
							raise Data.ConversionError(mode, convert_mode)

						image_metadata = ImageMetadata(file, mode, convert_mode, mean_color)
						self.image_metadata_list.append(image_metadata)

class DummyGeneratorProgress:

	def __init__(self):
	
		return
	
	def start(self):
	
		return
	
	def update(self, thread_number, completed, total):
	
		return
	
	def end(self):
	
		return

class GeneratorProgress:

	def __init__(self):
	
		self.completed = dict()
		self.total = dict()
		self.update_semaphore = Semaphore()

	def start(self):
	
		self.completed.clear()
		self.total.clear()

	def update(self, thread_number, completed, total):
	
		self.update_semaphore.acquire()
		self.completed[thread_number] = completed
		self.total[thread_number] = total
		self.show()
		self.update_semaphore.release()

	def show(self):
	
		return
	
	def end(self):
	
		return

class Generator:

	class NotEnoughTesseraeError(Exception):
		
		def __init__(self, available, required):
	
			self.available = available
			self.required = required
		
		def __str__(self):
		
			return f"not enough tesserae: {self.available} available but {self.required} required"

	def __init__(self, data):
	
		self.data = data
		self.image_sempahore = Semaphore()
		self.paste_sempahore = Semaphore()
	
	def _generate(self, input_image, rows, columns, width, height,
		max_occurrences, progress, positions, occurrences, indices,
		output_image, thread_number):
	
		for i, (row, column) in enumerate(positions):
		
			mask = ImageUtilities.get_image_cell_mask(
				input_image, rows, columns, row, column)
			
			self.image_sempahore.acquire()
			
			mean_color = ImageUtilities.get_image_mean_color(input_image, mask)
			index = min(indices, key = lambda i: MathUtilities.get_square_distance(
					self.data.image_metadata_list[i].mean_color, mean_color))
			
			occurrences[index] += 1
			
			if occurrences[index] == max_occurrences:
				indices.remove(index)
				
			self.image_sempahore.release()
			
			image_metadata = self.data.image_metadata_list[index]
			image = Image.open(image_metadata.file)
			
			# Conversion takes almost 75% of the time and resizement takes
			# almost 25%. If no conversion is done because both modes match,
			# then resizement takes almost 100%; is this because of implicit
			# conversion?
			if image_metadata.mode != image_metadata.convert_mode:
				image = image.convert(image_metadata.convert_mode)
				
			image = image.resize((width, height))
			
			# It seems paste is thread-safe, at least with non-overlapping
			# images. Anyway, a sempahore is used in order to stay safe,
			# since it doesn't cause much overhead, if any.
			self.paste_sempahore.acquire()
			output_image.paste(image, box = (column * width, row * height))
			self.paste_sempahore.release()
			
			progress.update(thread_number, (i + 1), len(positions))

	def generate(self, input_path, rows, columns, width, height,
		max_occurrences = None, number_of_threads = 1, random_seed = None,
		mode = None, progress = None):
		
		with Image.open(input_path) as input_image:
			
			if progress == None:
				progress = DummyGeneratorProgress()
			
			if max_occurrences == None:
				max_occurrences = rows * columns
			
			if random_seed != None:
				seed(random_seed)
			
			if mode != None and mode != input_image.mode:
				input_image = input_image.convert(mode)
			
			occurrences = [0] * len(self.data.image_metadata_list)
			indices = {i for i, image_metadata
				in enumerate(self.data.image_metadata_list)
				if image_metadata.convert_mode == input_image.mode}
			
			available_tesserae = len(indices) * max_occurrences
			required_tesserae = rows * columns
			
			if available_tesserae < required_tesserae:
				raise Generator.NotEnoughTesseraeError(
					available_tesserae, required_tesserae)
			
			positions = [(i, j) for i in range(rows) for j in range(columns)]
			shuffle(positions)
			
			output_image_size = (columns * width, rows * height)
			output_image = Image.new(input_image.mode, output_image_size)
		
			threads = set()
			
			for thread_number in range(number_of_threads):
				first = round(rows * columns / number_of_threads * thread_number)
				last = round(rows * columns / number_of_threads * (thread_number + 1))
				thread = Thread(target = self._generate, args = (
					input_image, rows, columns, width, height, max_occurrences,
					progress, positions[first:last], occurrences, indices,
					output_image, thread_number))
				threads.add(thread)
			
			progress.start()
			
			for thread in threads:
				thread.start()
			
			for thread in threads:
				thread.join()
			
			progress.end()
			
		return output_image

