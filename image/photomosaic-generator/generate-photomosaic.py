#!/usr/bin/env python3

from photomosaic import Data, Generator, GeneratorProgress
import os

# Number of CPUs the current process is restricted to.
if 'sched_getaffinity' in dir(os):
	DefaultNumberOfThreads = len(os.sched_getaffinity(0))
else:
	# Number of CPUs in the system.
	cpu_count = os.cpu_count()
	if cpu_count != None:
		DefaultNumberOfThreads = cpu_count
	else:
		DefaultNumberOfThreads = 1

def handle_error(error):
	message = str(error)
	if len(message) == 0:
		print("Unknown error.")
	else:
		print(message[0].upper() + message[1:] + '.')
	print("Exiting.")
	exit(1)

class GeneratorTextProgress(GeneratorProgress):
	def show(self):
		completed = sum(self.completed.values())
		total = sum(self.total.values())
		print("\rGenerating photomosaic (%d / %d)..." % (completed, total), end = '')
	def end(self):
		print("\nDone.")

def _parse_arguments():
	def positive_integer(string):
		value = int(string)
		if value < 1:
			raise ValueError
		return value
	def non_negative_integer(string):
		value = int(string)
		if value < 0:
			raise ValueError
		return value
	parser = ArgumentParser(
		add_help = False,
		description = "Generate a photomosaic from an image and a set of "
			"images. Data must be processed before generation.")
	parser.add_argument('-i', '--input',
		metavar = "INPUT-FILE",
		dest = 'input_path',
		type = str,
		required = True,
		help = "Read input image from file INPUT-FILE.")
	parser.add_argument('-d', '--data',
		metavar = "DATA-FILE",
		type = str,
		required = True,
		dest = 'data_file',
		help = "Load preprocessed data from file DATA-FILE.")
	parser.add_argument('-o', '--output',
		metavar = "OUTPUT-FILE",
		dest = 'output_path',
		type = str,
		required = True,
		help = "Save output image to file OUTPUT-FILE.")
	parser.add_argument('-m', '--mode',
		metavar = "MODE",
		dest = 'mode',
		type = str,
		default = None,
		required = False,
		choices = Data.Modes,
		help = "Convert image to image mode MODE.")
	parser.add_argument('-r', '--rows',
		metavar = "ROWS",
		dest = 'rows',
		type = positive_integer,
		required = True,
		help = "Paste ROWS images per column.")
	parser.add_argument('-c', '--columns',
		metavar = "COLUMNS",
		dest = 'columns',
		type = positive_integer,
		required = True,
		help = "Paste COLUMNS images per row.")
	parser.add_argument('-x', '--width',
		metavar = "WIDTH",
		dest = 'width',
		type = positive_integer,
		required = True,
		help = "Resize images to width WIDTH.")
	parser.add_argument('-y', '--height',
		metavar = "HEIGHT",
		dest = 'height',
		type = positive_integer,
		required = True,
		help = "Resize images to height HEIGHT.")
	parser.add_argument('-R', '--repetitions',
		metavar = "REPETITIONS",
		dest = 'max_occurrences',
		type = non_negative_integer,
		required = False,
		default = None,
		help = "Repeat an image at most REPETITIONS times. "
			"Default: infinite.")
	parser.add_argument('-t', '--threads',
		metavar = "NUM-THREADS",
		dest = 'num_threads',
		type = positive_integer,
		required = False,
		default = DefaultNumberOfThreads,
		help = "Distribute the workload among NUM-THREAD threads. "
			"Default (execution-dependent): %d." % DefaultNumberOfThreads)
	parser.add_argument('-s', '--seed',
		metavar = "SEED",
		type = str,
		required = False,
		dest = 'seed',
		default = None,
		help = "Generate pseudorandom numbers using seed SEED.")
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")
	return parser.parse_args()

def _main():
	arguments = _parse_arguments()

	try:
		print("Loading preprocessed data...")
		data = Data()
		data.load(arguments.data_file)
		print("Done.")
		generator = Generator(data)
		output = generator.generate(
			arguments.input_path,
			arguments.rows,
			arguments.columns,
			arguments.width,
			arguments.height,
			arguments.max_occurrences + 1 if arguments.max_occurrences != None else None,
			arguments.num_threads,
			arguments.seed,
			arguments.mode,
			GeneratorTextProgress())
		print("Saving photomosaic...")
		output.save(arguments.output_path)
		print("Done.")
	except Exception as error:
		handle_error(error)
	
if __name__ == '__main__':
	from argparse import ArgumentParser
	_main()
	
