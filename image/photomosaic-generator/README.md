Photomosaic Generator
=====================

Generate a photomosaic.

![Generated photomosaic.](output.jpg "Generated photomosaic.")

Usage
-----

	python3 preprocess-photomosaic.py ...
	python3 generate-photomosaic.py ...

