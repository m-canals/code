Hitomezashi Generator
=====================

Generate a random [hitomezashi pattern](https://en.wikipedia.org/wiki/Sashiko).

![Generated pattern.](output.png "Generated pattern.")

Usage
-----

Set variables `size` (number of rows and columns) and `output`
(output filename) and run:

	R -f generate-hitomezashi.R

