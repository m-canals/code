#include <vector>
#include <queue>
#include <cstdlib>
#include <cstdio>

class Maze {
	typedef int Weight;
	typedef std::vector <Weight> Weights;
	typedef std::vector <Weights> WeightsVector;
	typedef std::vector <WeightsVector> WeightsMatrix;
	typedef std::vector <bool> BoolVector;
	typedef std::vector <BoolVector> BoolMatrix;
	typedef int Coordinate;
	typedef std::pair <Coordinate, Coordinate> Position;
	typedef Position AbsolutePosition;
	typedef Position RelativePosition;
	typedef std::pair <AbsolutePosition, RelativePosition> Edge;
	typedef std::pair <Weight, Edge> WeightedEdge;
	typedef std::priority_queue <WeightedEdge> Queue;
	
	private:
		BoolMatrix maze;
		WeightsMatrix graph;
		int graphWidth;
		int graphHeight;
		int graphSize;
		int width;
		int height;
		int rows;
		int columns;
		int rowHeight;
		int columnWidth;
		
		void pushEdges(Queue & queue, int row, int column);
		bool positionIsValid(int row, int column);
		void generateMinimumSpanningTree(int row, int column);
		void generateDirectedGraph();
		void generateUndirectedGraph();
	
	public:
		Maze(int graphWidth, int graphHeight, int columnWidth, int rowHeight);
		void generate(bool directed, unsigned int seed);
		void write(FILE * stream);
};

