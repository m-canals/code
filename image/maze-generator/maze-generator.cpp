#include "maze-generator.hpp"

// TODO
//
//  - Separate column width and row height into
//    black / white column width and row height.
//  - Generate solution.

void Maze::pushEdges(Queue & queue, int row, int column) {
	queue.push(WeightedEdge(graph[row][column][0],
		Edge(Position(row - 1, column), Position(1, 0))));	
	queue.push(WeightedEdge(graph[row][column][1],
		Edge(Position(row, column + 1), Position(0, -1))));
	queue.push(WeightedEdge(graph[row][column][2],
		Edge(Position(row + 1, column), Position(-1, 0))));
	queue.push(WeightedEdge(graph[row][column][3],
		Edge(Position(row, column - 1), Position(0, 1))));
}

bool Maze::positionIsValid(int row, int column) {
	return row >= 0 and    row < graphHeight and
	    column >= 0 and column < graphWidth;
}

void Maze::generateMinimumSpanningTree(int row, int column) {
	Queue queue;
	int size;
	
	maze = BoolMatrix(rows, BoolVector(columns, false));
	maze[row * 2][column * 2] = true;
	pushEdges(queue, row, column);
	size = 1;

	while (size < graphSize) {
		WeightedEdge item = queue.top();
		queue.pop();

		row = item.second.first.first;
		column = item.second.first.second;
		
		int mazeRow = row * 2;
		int mazeColumn = column * 2;
		int mazeEdgeRow = mazeRow + item.second.second.first;
		int mazeEdgeColumn = mazeColumn + item.second.second.second;

		if (positionIsValid(row, column) and not maze[mazeRow][mazeColumn]) {
			maze[mazeRow][mazeColumn] = true;
			maze[mazeEdgeRow][mazeEdgeColumn] = true;
			pushEdges(queue, row, column);
			size++;
		}
	}
}

void Maze::generateDirectedGraph() {
	for (int i = 0; i < graphHeight; i++)
		for (int j = 0; j < graphWidth; j++)
			for (int k = 0; k < 4; k++)
				graph[i][j][k] = rand();
}

void Maze::generateUndirectedGraph() {
	for (int i = 0; i < graphHeight; i++) {
		for (int j = 0; j < graphWidth; j++) {
			if (i > 0)
				graph[i-1][j][2] = graph[i][j][0] = rand();
			if (j > 0)
				graph[i][j-1][1] = graph[i][j][3] = rand();
		}
	}
}

Maze::Maze(int graphWidth, int graphHeight, int columnWidth, int rowHeight) {
	this->graphWidth = graphWidth;
	this->graphHeight = graphHeight;
	this->graphSize = graphWidth * graphHeight;
	this->rowHeight = rowHeight;
	this->columnWidth = columnWidth;
	this->rows = 2 * graphHeight - 1;
	this->columns = 2 * graphWidth - 1;
	this->width = this->columns * columnWidth;
	this->height = this->rows * rowHeight;
	this->graph = WeightsMatrix(graphHeight, WeightsVector(graphWidth, Weights(4)));
}

void Maze::generate(bool directed, unsigned int seed) {
	srand(seed);
	
	if (directed)
		generateDirectedGraph();
	else
		generateUndirectedGraph();
	
	generateMinimumSpanningTree(rand() % graphHeight, rand() % graphWidth);
}

void Maze::write(FILE * stream) {
	fprintf(stream, "P1 %d %d\n", width, height);
	
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < rowHeight; j++) {
			for (int k = 0; k < columns; k++) {
				for (int l = 0; l < columnWidth; l++) {
					fprintf(stream, "%d ", not maze[i][k]);
				}
			}
			
			fprintf(stream, "\n");
		}
	}
}

