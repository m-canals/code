Maze Generator
==============

Generate a maze using Prim's minimum spanning tree algorithm
on a random weighted lattice graph and save it as a PBM image.

[![Generated maze.](output.jpg "Generated maze (converted to JPG).")](output.pbm)

Usage
-----

	make && ./maze-generator [WIDTH] [HEIGHT] [COLUMN-WIDTH] [ROW-HEIGHT] [DIRECTED] [SEED]

Arguments
---------

  1. **Graph width:** determines the number of rows (2 * width - 1)
  of the resulting image. Values range from 1 to `INT_MAX`.
  Default value: 25.
  
  2. **Graph height:** determines the number of columns (2 * height - 1)
  of the resulting image. Values range from 1 to `INT_MAX`.
  Default value: 25.
  
  3. **Column width:** in pixels. Values range from 1 to `INT_MAX`.
  Default value: 5.
  
  4. **Row height:** in pixels. Values range from 1 to `INT_MAX`.
  Default value: 5.

  5. **Directed:** use a directed graph (1) or an undirected graph (0).
  Default value: 0.

  6. **Seed:** used for random weights. Values range from 0 to `UINT_MAX`.
  Default value: current time.

