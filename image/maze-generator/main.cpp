#include <iostream>
#include <cstdlib>
#include <ctime>
#include "maze-generator.hpp"

// TODO
//
//  - Use optional arguments and flags.

using namespace std;

int main(int argc, char ** argv) {
		int graphWidth = argc >= 2 ? atoi(argv[1]) : 25;
		int graphHeight = argc >= 3 ? atoi(argv[2]) : 25;
		int rowHeight = argc >= 4 ? atoi(argv[3]) : 5;
		int columnWidth = argc >= 5 ? atoi(argv[4]) : 5;
		bool directed = argc >= 6 ? atoi(argv[5]) : 0;
		unsigned int seed = argc >= 7 ? atoi(argv[6]) : time(NULL);
		Maze maze(graphWidth, graphHeight, columnWidth, rowHeight);
		maze.generate(directed, seed);
		maze.write(stdout);
		return 0;
}

