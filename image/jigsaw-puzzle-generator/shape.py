#!/usr/bin/env python3

from math import inf

class Shape:

	# Create a drawable shape.
	def __init__(self):
	
		pass

	# Translate shape (+). It takes precedence over scaling.
	def __add__(self, argument):
	
		return Shape()
	
	# Translate shape (-). It takes precedence over scaling.
	def __sub__(self, argument):
	
		return Shape()

	# Scale shape (*). It takes precedence over rotation.
	def __mul__(self, argument):
	
		return Shape()
	
	# Scale shape (/). It takes precedence over rotation.
	def __div__(self, argument):
	
		return Shape()
	
	# Rotate shape (<<). It takes precedence over reflection.
	def __lshift__(self, argument):
	
		return Shape()
	
	# Rotate shape (>>). It takes precedence over reflection.
	def __rshift__(self, argument):
	
		return Shape()
	
	# Reflect shape (|).
	def __or__(self, argument):
	
		return Shape()

	# Draw shape on `PIL.ImageDraw.ImageDraw` image using positional and
	# named arguments `arguments` and `named_arguments`.
	def __call__(self, draw, * arguments, ** named_arguments):

		pass

class Point(Shape):

	# Create a drawable point. Numbers `x`and `y` are its coordinates.
	def __init__(self, x = 0, y = 0):
	
		self.x = x
		self.y = y

	def __iter__(self):
	
		yield self.x
		yield self.y

	def __abs__(self):
	
		return Point(abs(self.x), abs(self.y))
	
	def __round__(self, * argument, ** named_arguments):
	
		return Point(
			round(self.x, * argument, ** named_arguments),
			round(self.y, * argument, ** named_arguments))
	
	def __lt__(self, argument):
	
		if isinstance(argument, Point):
			return self.x < argument.x and self.y < argument.y
		else:
			return self.x < argument and self.y < argument

	def __le__(self, argument):
	
		if isinstance(argument, Point):
			return self.x <= argument.x and self.y <= argument.y
		else:
			return self.x <= argument and self.y <= argument

	def __eq__(self, argument):
	
		if isinstance(argument, Point):
			return self.x == argument.x and self.y == argument.y
		else:
			return self.x == argument and self.y == argument

	def __gt__(self, argument):
	
		if isinstance(argument, Point):
			return self.x > argument.x and self.y > argument.y
		else:
			return self.x > argument and self.y > argument
	
	def __ge__(self, argument):
	
		if isinstance(argument, Point):
			return self.x >= argument.x and self.y >= argument.y
		else:
			return self.x >= argument and self.y >= argument

	def __add__(self, argument):
	
		if isinstance(argument, Point):
			return Point(self.x + argument.x, self.y + argument.y)
		else:
			return Point(self.x + argument, self.y + argument)
	
	def __sub__(self, argument):
	
		if isinstance(argument, Point):
			return Point(self.x - argument.x, self.y - argument.y)
		else:
			return Point(self.x - argument, self.y - argument)
	
	def __mul__(self, argument):
	
		if isinstance(argument, Point):
			return Point(self.x * argument.x, self.y * argument.y)
		else:
			return Point(self.x * argument, self.y * argument)
	
	def __div__(self, argument):
	
		if isinstance(argument, Point):
			return Point(self.x / argument.x, self.y / argument.y)
		else:
			return Point(self.x / argument, self.y / argument)

	def __or__(self, argument):
	
		if argument % 180 == 0:
			return Point(self.x, - self.y)
		elif argument % 180 == 45:
			return Point(self.y, self.x)
		elif argument % 180 == 90:
			return Point(- self.x, self.y)
		elif argument % 180 == 135:
			return Point(- self.y, - self.x)
		else:
			raise ValueError
	
	def __lshift__(self, argument):
	
		if argument % 360 == 0:
			return Point(self.x, self.y)
		elif argument % 360 == 90:
			return Point(- self.y, self.x)
		elif argument % 360 == 180:
			return Point(- self.x, - self.y)
		elif argument % 360 == 270:
			return Point(self.y, - self.x)
		else:
			raise ValueError
	
	def __rshift__(self, argument):
	
		return self << - argument

	def __call__(self, draw, * arguments, ** named_arguments):

		xy = [* round(self)]
		draw.point(xy, * arguments, ** named_arguments)

class Line(Shape):

	# Create a drawable line segment. `Point` points `A` and `B` are its
	# end points.
	def __init__(self, A, B):
	
		self.A = A
		self.B = B

	def __add__(self, argument):
	
		return Line(self.A + argument, self.B + argument)

	def __sub__(self, argument):
	
		return Line(self.A - argument, self.B - argument)

	def __mul__(self, argument):
	
		return Line(self.A * argument, self.B * argument)
	
	def __div__(self, argument):
	
		return Line(self.A / argument, self.B / argument)
	
	def __or__(self, argument):

		return Line(self.A | argument, self.B | argument)
	
	def __lshift__(self, argument):

		return Line(self.A << argument, self.B << argument)
	
	def __rshift__(self, argument):

		return self << - argument

	def __call__(self, draw, * arguments, ** named_arguments):
	
		xy = [* round(self.A), * round(self.B)]
		draw.line(xy, * arguments, ** named_arguments)

class Arc(Shape):

	# Create an elliptical arc. `Point` points `A` and `B` contain the
	# minimum and the maximum x and y coordinates of the corresponding
	# ellipse; numbers `start` and `end` are the angles, in degrees, at
	# which it starts and ends.
	def __init__(self, A, B, start, end):
	
		self.A = A
		self.B = B
		self.start = start
		self.end = end

	def __add__(self, argument):
	
		return Arc(self.A + argument, self.B + argument, self.start, self.end)

	def __sub__(self, argument):
	
		return Arc(self.A - argument, self.B - argument, self.start, self.end)

	def __mul__(self, argument):
	
		if argument >= 0:
			return Arc(self.A * argument, self.B * argument, self.start, self.end)
		elif argument <= 0:
			return self * abs(argument) >> 180
		elif argument <= Point(0, inf):
			return self * abs(argument) | 90
		elif argument <= Point(inf, 0):
			return self * abs(argument) | 0

	def __div__(self, argument):
	
		if argument >= 0:
			return Arc(self.A / argument, self.B / argument, self.start, self.end)
		elif argument <= 0:
			return self / abs(argument) >> 180
		elif argument <= Point(0, inf):
			return self / abs(argument) | 90
		elif argument <= Point(inf, 0):
			return self / abs(argument) | 0

	def __or__(self, argument):

		if argument % 180 == 0:
			return Arc(
				Point(self.A.x, - self.B.y),
				Point(self.B.x, - self.A.y),
				- self.end,
				- self.start)
		elif argument % 180 == 45:
			return Arc(
				Point(self.A.y, self.A.x),
				Point(self.B.y, self.B.x),
				90 - self.end,
				90 - self.start)
		elif argument % 180 == 90:
			return Arc(
				Point(- self.B.x, self.A.y),
				Point(- self.A.x, self.B.y),
				180 - self.end,
				180 - self.start)
		elif argument % 180 == 135:
			return Arc(
				Point(- self.B.y, - self.B.x),
				Point(- self.A.y, - self.A.x),
				- 90 - self.end,
				- 90 - self.start)
		else:
			raise ValueError

	def __lshift__(self, argument):

		if argument % 360 == 0:
			return Arc(
				Point(self.A.x, self.A.y),
				Point(self.B.x, self.B.y),
				self.start,
				self.end)
		elif argument % 360 == 90:
			return Arc(
				Point(- self.B.y, self.A.x),
				Point(- self.A.y, self.B.x),
				90 + self.start,
				90 + self.end)
		elif argument % 360 == 180:
			return Arc(
				Point(- self.B.x, - self.B.y),
				Point(- self.A.x, - self.A.y),
				180 + self.start,
				180 + self.end)
		elif argument % 360 == 270:
			return Arc(
				Point(self.A.y, - self.B.x),
				Point(self.B.y, - self.A.x),
				270 + self.start,
				270 + self.end)
		else:
			raise ValueError
	
	def __rshift__(self, argument):

		return self << - argument

	def __call__(self, draw, * arguments, ** named_arguments):

		xy = [* round(self.A), * round(self.B)]
		draw.arc(xy, self.start, self.end, * arguments, ** named_arguments)

# TODO
class BezierCurve(Shape):
	...

