#!/usr/bin/env python3

from jigsawizer import TriarcocircularJigsawizer
from argparse import ArgumentParser
from PIL import Image
from os import path, makedirs

def _parse_arguments():

	parser = ArgumentParser(
		add_help = False,
		description = "Jigsawize an image.")
	parser.add_argument('-i', '--input-file',
		metavar = 'INPUT-FILE',
		dest = 'input_file',
		required = True,
		help = "Load input image from INPUT-FILE.")
	parser.add_argument('-o', '--output-file',
		metavar = 'OUTPUT-FILE',
		dest = 'output_file',
		required = False,
		default = None,
		help = "Save ouput image to OUTPUT-FILE.")
	parser.add_argument('-d', '--output-directory',
		metavar = 'OUTPUT-DIRECTORY',
		dest = 'output_directory',
		required = False,
		default = None,
		help = "Save output piece images to OUTPUT-DIRECTORY.")
	parser.add_argument('-r', '--rows',
		metavar = 'ROWS',
		dest = 'rows',
		type = int,
		default = 10,
		help = "Cut image in ROW rows.")
	parser.add_argument('-c', '--columns',
		metavar = 'COLUMNS',
		dest = 'columns',
		type = int,
		default = 20,
		help = "Cut image in COLUMNS columns.")
	parser.add_argument('-C', '--line-color',
		metavar = 'COLOR',
		dest = 'color',
		default = 'white',
		help = "Draw lines of color COLOR.")
	parser.add_argument('-W', '--line-width',
		metavar = 'WIDTH',
		dest = 'width',
		type = int,
		default = 1,
		help = "Draw lines of width WIDTH.")
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")
	
	return parser.parse_args()

def _main():

	arguments = _parse_arguments()
	image = Image.open(arguments.input_file)
	jigsawizer = TriarcocircularJigsawizer(arguments.rows, arguments.columns)
	
	if arguments.output_directory != None:
		pieces = jigsawizer.crop_pieces(image)
		makedirs(arguments.output_directory, exist_ok = True)
		format = '%%0%dd' % len(str(len(pieces)))
		for i, piece in enumerate(pieces):
			filepath = arguments.output_directory + path.sep + (format % i) + '.png'
			piece.save(filepath)
	
	if arguments.output_file != None:
		jigsawizer.draw_pieces(image,
			color = arguments.color,
			width = arguments.width)
		image.save(arguments.output_file)

if __name__ == '__main__':
	_main()
