#!/usr/bin/env python3

from PIL import Image, ImageDraw
from random import random
from math import cos, sin, pi, degrees
from shape import *

# TODO Jigsawizers with normally distributed parameters for each piece.

class Jigsawizer:

	# Create a generator of jigsaw puzzles defined by 'Shape' shapes in
	#  iterable `shapes`.
	def __init__(self, shapes):
	
		self.shapes = shapes

	# Draw shapes on `PIL.Image.Image` image `image` with lines of color
	# `color` and width `width`.
	def draw_pieces(self, image, color = 'orange', width = 1):

		draw = ImageDraw.Draw(image)
		factors = Point(image.width, image.height)
		
		for shape in self.shapes:
			(shape * factors)(draw, fill = color, width = width)

	# Locate the first pixel in `PIL.Image.Image` image `image` whose value
	# is `value`, starting at `start` (x and y coordinates).
	def locate_value(self, image, value, start):

			x, y = start
			
			while y < image.height:
				while x < image.width:
					if image.getpixel((x, y)) == value:
						return (x, y)
					x = x + 1
				x = 0
				y = y + 1
				
			return None

	# Crop regions closed by shapes from `PIL.Image.Image` image `image`.
	def crop_pieces(self, image):
		
		map = Image.new('1', image.size, 0)
		mask = Image.new('1', image.size, 0)
		pieces = Image.new('RGBA', image.size, (0, 0, 0, 0))
		
		self.draw_pieces(map, color = 1, width = 1)

		queue = list()
		start = self.locate_value(map, 0, (0, 0))
		images = list()
		
		while start != None:
			queue.append(start)
			
			while len(queue) > 0:
				x, y = queue.pop()
				if 0 <= x < map.width and 0 <= y < map.height:
					mask.putpixel((x, y), 1)
					if map.getpixel((x, y)) == 0:
						map.putpixel((x, y), 1)
						queue.extend([(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)])
			
			box = mask.getbbox()
			pieces.paste(image, mask = mask)
			piece = pieces.crop(box = box)
			images.append(piece)
			pieces.paste((0, 0, 0, 0), box)
			mask.paste(0, (0, 0, mask.width, mask.height))
			start = self.locate_value(map, 0, start) # Skip visited positions.

		return images

class GridJigsawizer(Jigsawizer):

	# Create a generator of grid jigsaw puzzles. Lists of numbers `X` and
	# `Y` contain column and row image-relative offsets between 0 and 1;
	# sets of `Shape` shapes `top_blank`, `top_tab`, `left_blank` and
	# `left_tab` describe top and left blanks and tabs; numbers
	# `top_tab_probability` and `left_tab_probability` are probabilities
	# between 0 and 1 of a tab being at the top and at the left,
	# respectively.
	def __init__(self, X, Y,
		top_blank, top_tab, left_blank, left_tab,
		top_tab_probability = 0.5,
		left_tab_probability = 0.5):

		X = sorted(X + [1])
		Y = sorted(Y + [1])
		shapes = set()
		
		for row in range(0, len(Y) - 1):
			for column in range(0, len(X) - 1):
				scale = Point(X[column + 1] - X[column], Y[row + 1] - Y[row])
				offset = Point(X[column], Y[row])
				added_shapes = set()

				if column > 0:
					if random() < left_tab_probability:
						added_shapes.update(left_tab)
					else:
						added_shapes.update(left_blank)
				
				if row > 0:
					if random() < top_tab_probability:
						added_shapes.update(top_tab)
					else:
						added_shapes.update(top_blank)
				
				shapes.update({shape * scale + offset for shape in added_shapes})
		
		super().__init__(shapes)

class GridJigsawizerWithReflection(GridJigsawizer):
	
	# Create a generator of grid jigsaw puzzles with reflection. Lists of
	# numbers `X` and `Y` contain column and row image-relative offsets
	# between 0 and 1; set of `Shape` shapes `top_blank` describe top and
	# left blanks and tabs by reflection; numbers `top_tab_probability` and
	# `left_tab_probability` are probabilities between 0 and 1 of a tab
	# being at the top and at the left, respectively.
	def __init__(self, X, Y,
		top_blank,
		top_tab_probability = 0.5,
		left_tab_probability = 0.5):
	
		top_tab = {shape | 0 for shape in top_blank}
		left_blank = {shape | 45 for shape in top_blank}
		left_tab = {shape | 45 for shape in top_tab}
		
		super().__init__(X, Y, top_blank, top_tab, left_blank, left_tab,
			top_tab_probability, left_tab_probability)

class TriarcocircularJigsawizer(GridJigsawizerWithReflection):

	# Create a basic generator of jigsaw puzzles. Numbers `head_radius`,
	# `neck_radius` and `center` are tab and blank head and neck
	# piece-relative radiuses and center between 0 and 1; number `curvature`
	# is a  curvature coefficient (arc angle) between 0 and 1; numbers
	# `top_tab_probability` and `left_tab_probability` are probabilities
	# between 0 and 1 of a tab being at the top and at the left,
	# respectively.
	def __init__(self, rows, columns,
		center = 0.5,
		head_radius = 0.1,
		neck_radius = 0.05,
		curvature = 0.75,
		top_tab_probability = 0.5,
		left_tab_probability = 0.5):
	
		r_h = head_radius
		r_n = neck_radius
		t = curvature * pi
		
		A = Point(center - (r_n + r_h) * cos(- pi / 2 + t), r_n)
		B = Point(center, r_n + (r_n + r_h) * sin(- pi / 2 + t))
		C = Point(center + (r_n + r_h) * cos(- pi / 2 + t), r_n)

		top_blank = {
			Line(Point(0.0, 0.0), Point(A.x, 0.0)),
			Arc(A - r_n, A + r_n, degrees(- pi / 2), degrees(- pi / 2 + t)),
			Arc(B - r_h, B + r_h, degrees(pi / 2 - t), degrees(pi / 2 + t)),
			Arc(C - r_n, C + r_n, degrees(- pi / 2 - t), degrees(- pi / 2)),
			Line(Point(C.x, 0.0), Point(1.0, 0.0))}

		X = [column / columns for column in range(columns)]
		Y = [row / rows for row in range(rows)]

		super().__init__(X, Y, top_blank, top_tab_probability,
			left_tab_probability)

