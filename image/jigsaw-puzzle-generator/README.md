Jigsaw Puzzle Generator
=======================

Generate a jigsaw puzzle.

![Generated jigsaw puzzle (cropped and drawn pieces).](screenshot.png "Generated jigsaw puzzle (cropped and drawn pieces).")

Usage
-----

	python3 jigsawize.py -i input.jpg -o output.jpg -d output -C red -W 2

