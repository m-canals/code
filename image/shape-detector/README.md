Shape Detector
==============

Detect lines, circles and contours.

![Detected contours.](output.jpg "Detected cotours.")

Usage
-----

	python3 detect-shapes.py ...

