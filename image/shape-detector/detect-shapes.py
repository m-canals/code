#!/usr/bin/env python3
#
# Source:
#
#  - https://codewords.recurse.com/issues/six/image-processing-101

import cv2
import numpy

def draw_lines(image, lines,
	color = (255, 0, 0),
	thickness = 1):

	for r, t in lines:
		a = numpy.cos(t)
		b = numpy.sin(t)
		x = a * r
		y = b * r
		x1 = int(x - 1000 * b)
		y1 = int(y + 1000 * a)
		x2 = int(x + 1000 * b)
		y2 = int(y - 1000 * a)
		cv2.line(image, (x1, y1), (x2, y2), color[::-1], thickness)

def draw_circles(image, circles,
	color = (255, 0, 0),
	thickness = 1):

	for x, y, r in circles:
		cv2.circle(image, (int(x), int(y)), int(r), color[::-1], thickness)

def draw_contours(image, contours,
	color = (255, 0, 0),
	thickness = 1):

	contourId = -1 # All contours.
	cv2.drawContours(image, contours, contourId, color[::-1], thickness)

def draw_contour_bounding_rectangles(image, contours,
	color = (255, 0, 0),
	thickness = 1):

	for contour in contours:
		x, y, w, h = cv2.boundingRect(contour)
		cv2.rectangle(image, (x, y), (x + w, y + h), color[::-1], thickness)

# @param b  Blur kernel size in pixels (two positive odd integers).
#           The color of a blurred pixel depends on
#           the colors of the pixels of its sorrounding kernel.
# @param t  Weak edge threshold (float).
# @param T  Strong edge threshold (float).
# @param d  Distance resolution in pixels (positive float).
# @param a  Angle resolution in radians (positive float).
# @param l  Line relevance threshold (integer).
def detect_lines(image, b = (1, 1), t = 0.0, T = 0.0, d = 0.5, a = 2.0 * numpy.pi, l = 0.0):
	
	blur = cv2.GaussianBlur(image, b, 0, 0)
	gray = cv2.cvtColor(blur, cv2.COLOR_BGR2GRAY)
	edges = cv2.Canny(gray, threshold1 = t, threshold2 = T)
	lines = cv2.HoughLines(edges, d, a, threshold = l)
	
	return [] if lines is None else [line[0] for line in lines]

# @param b  Blur kernel size in pixels (two positive odd integers).
#           The color of a blurred pixel depends on
#           the colors of the pixels of its sorrounding kernel.
# @param t  Weak edge threshold (float).
# @param T  Strong edge threshold (float).
# @param s  Size resolution as inverse proportion to the image size (positive float).
# @param d  Minimum distance between circle centers (positive float).
# @param r  Minimum circle radius (non-negative integer).
# @param R  Maximum circle radius (non-negative integer).
def detect_circles(image, b = (1, 1), t = 0.0, T = 0.0, s = 1.0, d = 0.5, r = 0, R = 0):

	blur = cv2.GaussianBlur(image, b, 0, 0)
	gray = cv2.cvtColor(blur, cv2.COLOR_BGR2GRAY)
	edges = cv2.Canny(gray, threshold1 = t, threshold2 = T)
	circles = cv2.HoughCircles(edges, cv2.HOUGH_GRADIENT, s, d, minRadius = r, maxRadius = R)
	
	return [] if circles is None else circles[0]

# @param b  Blur kernel size in pixels (two positive odd integers).
#           The color of a blurred pixel depends on
#           the colors of the pixels of its sorrounding kernel.
# @param t  Minimum threshold in binary conversion (float).
# @param T  Maximum threshold in binary conversion (float).
# @param i  Invert binary image (boolean).
# @param a  Minimum contour area (float).
# @param A  Maximum contour area (float).
def detect_contours(image, b = (1, 1), t = 0, T = 255, i = False, a = 0, A = None):
	
	height, width, _ = image.shape
	A = width * height if A == None else A
	
	blurred = cv2.GaussianBlur(image, b, 0, 0)
	gray = cv2.cvtColor(blurred, cv2.COLOR_BGR2GRAY)
	binary = cv2.threshold(gray, t, T, cv2.THRESH_BINARY)[1]
	binary = cv2.bitwise_not(binary) if i else binary
	contours = cv2.findContours(binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]
	
	return [contour for contour in contours if a <= cv2.contourArea(contour) <= A]

def _parse_arguments():
	
	def non_negative_int(argument):
	
		value = int(argument)
		
		if value < 0:
			raise ValueError
		
		return value
	
	def non_negative_float(argument):
	
		value = float(argument)
		
		if value < 0.0:
			raise ValueError
		
		return value
	
	def positive_float(argument):
	
		value = float(argument)
		
		if value <= 0.0:
			raise ValueError
		
		return value
	
	def positive_odd_int(argument):
	
		value = int(argument)
		
		if value <= 0 or value % 2 == 0:
			raise ValueError
		
		return value
	
	parser = ArgumentParser(
		add_help = False,
		description = "Detect lines, circles and contours.")
	parser.add_argument('-i', '--input',
		metavar = "INPUT-FILE",
		dest = 'input_path',
		type = str,
		required = True,
		help = "Load input image from INPUT-FILE.")
	parser.add_argument('-o', '--output',
		metavar = "OUTPUT-FILE",
		dest = 'output_path',
		type = str,
		required = True,
		help = "Save output image to OUTPUT-FILE.")
	parser.add_argument('-c', '--color',
		metavar = ("R", "G", "B"),
		nargs = 3,
		dest = 'color',
		type = float,
		default = (255, 0, 0),
		help = "Draw shapes of this color.")
	parser.add_argument('-t', '--thickness',
		metavar = "THICKNESS",
		dest = 'thickness',
		type = int,
		default = 2,
		help = "Draw shapes of this thickness.")
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")
	parser.set_defaults(detection_type = None)
	subparsers = parser.add_subparsers(
		help = 'Detect shapes of this type.')
	
	subparser = subparsers.add_parser('line',
		help = 'Detect lines.')
	subparser.set_defaults(detection_type = 'line')
	subparser.add_argument('-b', '--blur-size',
		metavar = ("BLUR-KERNEL-WIDTH", "BLUR-KERNEL-HEIGHT"),
		nargs = 2,
		dest = 'b',
		type = positive_odd_int,
		default = (5, 5),
		help = "Set blur kernel size.")
	subparser.add_argument('-t', '--weak-threshold',
		metavar = ("WEAK-EDGE-THRESHOLD"),
		dest = 't',
		type = float,
		default = 90,
		help = "Set weak edge threshold.")
	subparser.add_argument('-T', '--strong-threshold',
		metavar = ("STRONG-EDGE-THRESHOLD"),
		dest = 'T',
		type = float,
		default = 110,
		help = "Set strong edge threshold.")
	subparser.add_argument('-d', '--distance-resolution',
		metavar = ("DISTANCE-RESOLUTION"),
		dest = 'd',
		type = positive_float,
		default = 1,
		help = "Set distance resolution.")
	subparser.add_argument('-a', '--angle-resolution',
		metavar = ("ANGLE-RESOLUTION"),
		dest = 'a',
		type = positive_float,
		default = 2.0 * numpy.pi / 360,
		help = "Set angle resolution.")
	subparser.add_argument('-l', '--line-threshold',
		metavar = ("LINE-RELEVANCE-THRESHOLD"),
		dest = 'l',
		type = int,
		default = 110,
		help = "Set line relevance threshold.")
	
	subparser = subparsers.add_parser('circle',
		help = 'Detect circles.')
	subparser.set_defaults(detection_type = 'circle')
	subparser.add_argument('-b', '--blur-size',
		metavar = ("BLUR-KERNEL-WIDTH", "BLUR-KERNEL-HEIGHT"),
		nargs = 2,
		dest = 'b',
		type = positive_odd_int,
		default = (5, 5),
		help = "Set blur kernel size.")
	subparser.add_argument('-t', '--weak-threshold',
		metavar = ("WEAK-EDGE-THRESHOLD"),
		dest = 't',
		type = float,
		default = 90,
		help = "Set weak edge threshold.")
	subparser.add_argument('-T', '--strong-threshold',
		metavar = ("STRONG-EDGE-THRESHOLD"),
		dest = 'T',
		type = float,
		default = 110,
		help = "Set strong edge threshold.")
	subparser.add_argument('-s', '--size-resolution',
		metavar = ("PROPORTION"),
		dest = 's',
		type = positive_float,
		default = 1.5,
		help = "Set size resolution as inverse proportion to image size.")
	subparser.add_argument('-d', '--min-distance',
		metavar = ("MIN-DISTANCE"),
		dest = 'd',
		type = positive_float,
		default = 0.5,
		help = "Set minimum distance between centers.")
	subparser.add_argument('-r', '--min-radius',
		metavar = ("MIN-RADIUS"),
		dest = 'r',
		type = non_negative_int,
		default = 0,
		help = "Set minimum radius.")
	subparser.add_argument('-R', '--max-radius',
		metavar = ("MAX-RADIUS"),
		dest = 'R',
		type = non_negative_int,
		default = 0,
		help = "Set maximum radius.")
	
	subparser = subparsers.add_parser('contour',
		help = 'Detect contours.')
	subparser.set_defaults(detection_type = 'contour')
	subparser.add_argument('-r', '--bounding-rectangle',
		dest = 'draw_bounding_rectangle',
		action = 'store_true',
		default = False,
		help = "Draw bounding rectangle instead.")
	subparser.add_argument('-b', '--blur-size',
		metavar = ("BLUR-KERNEL-WIDTH", "BLUR-KERNEL-HEIGHT"),
		nargs = 2,
		dest = 'b',
		type = positive_odd_int,
		default = (5, 5),
		help = "Set blur kernel size.")
	subparser.add_argument('-t', '--min-threshold',
		metavar = ("MIN-THRESHOLD"),
		dest = 't',
		type = float,
		default = 130,
		help = "Set minimum threshold.")
	subparser.add_argument('-T', '--max-threshold',
		metavar = ("MAX-THRESHOLD"),
		dest = 'T',
		type = float,
		default = 255,
		help = "Set maximum threshold.")
	subparser.add_argument('-i', '--invert',
		dest = 'i',
		action = 'store_true',
		default = False,
		help = "Invert detection.")
	subparser.add_argument('-a', '--min-area',
		metavar = ("MIN-AREA"),
		dest = 'a',
		type = float,
		default = 0,
		help = "Set minimum area.")
	subparser.add_argument('-A', '--max-area',
		metavar = ("MAX-AREA"),
		dest = 'A',
		type = float,
		default = None,
		help = "Set maximum area.")

	return parser.parse_args()

def _main():

	arguments = _parse_arguments()
	image = cv2.imread(arguments.input_path)
	print(arguments.color)
	if arguments.detection_type == 'line':
		lines = detect_lines(image, tuple(arguments.b), arguments.t,
			arguments.T, arguments.d, arguments.a, arguments.l)
		print(f"Detected lines: {len(lines)}.")
		draw_lines(image, lines,
			arguments.color, arguments.thickness)
	elif arguments.detection_type == 'circle':
		circles = detect_circles(image, tuple(arguments.b), arguments.t,
			arguments.T, arguments.s, arguments.d, arguments.r, arguments.R)
		print(f"Detected circles: {len(circles)}.")
		draw_circles(image, circles,
			arguments.color, arguments.thickness)
	elif arguments.detection_type == 'contour':
		contours = detect_contours(image, tuple(arguments.b), arguments.t,
			arguments.T, arguments.i, arguments.a, arguments.A)
		print(f"Detected contours: {len(contours)}.")
		
		if arguments.draw_bounding_rectangle:
			draw_contour_bounding_rectangles(image, contours,
				arguments.color, arguments.thickness)
		else:
			draw_contours(image, contours,
				arguments.color, arguments.thickness)
		
	
	cv2.imwrite(arguments.output_path, image)

if __name__ == '__main__':
	from argparse import ArgumentParser
	_main()
