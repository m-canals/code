#!/usr/bin/env python3

# TODO 3D rotation.

from PIL import Image
from math import radians
from point import Point2D as Point
from bounds import Bounds2D as Bounds
from sys import argv, stderr

def rotation(point, center, angle):

	p = ((point - center) >> angle) + center
	p.x = int(round(p.x))
	p.y = int(round(p.y))
	
	return p

def rotate(image, mode, angle, center = None, color = None, expand = False):

	b0 = Bounds(Point(0, 0), Point(image.width, image.height))

	if expand or center == None:
		center = b0.center
	else:
		center = Point(* center)
	
	if expand:
		b1 = b0.transformed(lambda p: rotation(p, center, angle))
	else:
		b1 = b0

	output = Image.new(mode, b1.size, color)

	# Compute what goes where instead of where goes what, so there is
	# something everywhere, since everything being somewhere does not
	# guarantee so.
	for x1 in range(b1.width):
		for y1 in range(b1.height):
			p1 = Point(x1, y1)
			p0 = rotation(b1.lower + p1, center, -angle)
			if p0 in b0:
				pixel = image.getpixel(p0.xy)
				output.putpixel(p1.xy, pixel)

	return output

def _parse_arguments():

	parser = ArgumentParser(
		add_help = False,
		description = "Rotate images.")
	parser.add_argument('-i', '--input',
		metavar = "INPUT-FILE",
		dest = 'input_path',
		type = str,
		required = True,
		help = "Load input image from INPUT-FILE.")
	parser.add_argument('-o', '--output',
		metavar = "OUTPUT-FILE",
		dest = 'output_path',
		type = str,
		required = True,
		help = "Save output image to OUTPUT-FILE.")
	parser.add_argument('-a', '--angle',
		metavar = 'ANGLE',
		dest = 'angle',
		type = float,
		default = 0,
		help = "Rotate ANGLE degrees or radians (see -r).")
	parser.add_argument('-c', '--center',
		metavar = ("X", "Y"),
		nargs = 2,
		dest = 'center',
		type = float,
		default = (0.5, 0.5),
		help = "Rotate around an axis located at (X, Y), "
		       "where X and Y are ratios to the image size or pixels (see -p). "
		       "The coordinate system is centered at the top left corner. ")
	parser.add_argument('-r', '--radians',
		dest = 'radians',
		action = 'store_true',
		default = False,
		help = "Interpret ANGLE as radians "
		       "instead of degrees.")
	parser.add_argument('-p', '--pixels',
		dest = 'pixels',
		action = 'store_true',
		default = False,
		help = "Interpret X and Y as pixels instead of ratios.")
	parser.add_argument('-b', '--background-color',
		nargs = '*',
		metavar = 'COLOR',
		dest = 'color',
		default = None,
		help = "Fill background with a grayscale, RGB or RGBA color. "
		       "Examples: black, #000, #000F, #000000, #000000FF, "
		       "0, 0 0 0, 0 0 0 255.")
	parser.add_argument('-e', '--expand',
		dest = 'expand',
		action = 'store_true',
		default = False,
		help = "Enlarge output image as necessary.")
	parser.add_argument('-m', '--mode',
		metavar = 'MODE',
		dest = 'mode',
		default = None,
		choices = Image.MODES,
		help = "Use mode MODE on output image. "
		       "Available modes: %s." %(', '.join(Image.MODES)))
	parser.add_argument('-h', '--help',
		action = 'help',
		help = "Show this help message and exit.")

	return parser.parse_args()

def parse_color(color):

	if color != None:
	
		for i, element in enumerate(color):
			try:
				color[i] = int(element)
			except ValueError:
				pass
		
		color = color[0] if len(color) == 1 else tuple(color)
	
	return color

def _main():

	arguments = _parse_arguments()
	input_path = arguments.input_path
	output_path = arguments.output_path
	center = arguments.center
	angle = arguments.angle
	color = parse_color(arguments.color)
	mode = arguments.mode
	use_pixels = arguments.pixels
	use_radians = arguments.radians
	expand = arguments.expand

	try:
		image = Image.open(input_path)
	except (FileNotFoundError, OSError) as error:
		print(f'{argv[0]}: error: {error}', file = stderr)
		exit(1)

	if mode == None:
		mode = image.mode
	
	if not use_pixels:
		x, y = center
		w, h = image.size
		center = (x * w, y * h)
	
	if not use_radians:
		angle = radians(angle)

	try:
		output_image = rotate(image, mode, angle, center, color, expand)
	except (TypeError, ValueError) as error:
		print(f'{argv[0]}: error: {error}', file = stderr)
		exit(1)
	
	try:
		image.close()
	except (OSError) as error:
		print(f'{argv[0]}: error: {error}', file = stderr)
		exit(1)
	
	try:
		output_image.save(output_path)
	except (ValueError, OSError) as error:
		print(f'{argv[0]}: error: {error}', file = stderr)
		exit(1)
	

if __name__ == '__main__':
	from argparse import ArgumentParser
	_main()

