#!/usr/bin/env python3

from point import Point2D

class Bounds2D:
	
	def __init__(self, * arguments):
	
		if len(arguments) == 0:
			self.lower = Point2D()
			self.upper = Point2D()
		elif len(arguments) == 1 and isinstance(arguments[0], Bounds2D):
			self.lower = Point2D(arguments[0].lower)
			self.upper = Point2D(arguments[0].upper)
		elif len(arguments) == 2:
			self.lower = Point2D(arguments[0])
			self.upper = Point2D(arguments[1])
		else:
			raise TypeError
	
	@property
	def width(self):
	
		return self.upper.x - self.lower.x
	
	@property
	def height(self):
	
		return self.upper.y - self.lower.y

	@property
	def size(self):
	
		return (self.width, self.height)

	@property
	def center(self):
	
		return (self.lower + self.upper) / 2
	
	def transform(self, function):
	
		l, u = self.lower, self.upper
		points = {l, Point2D(l.x, u.y), u, Point2D(u.x, l.y)}
		points = {function(point) for point in points}
		self.lower.x = min(points, key = lambda p: p.x).x
		self.lower.y = min(points, key = lambda p: p.y).y
		self.upper.x = max(points, key = lambda p: p.x).x
		self.upper.y = max(points, key = lambda p: p.y).y

		return self

	def transformed(self, function):
	
		return Bounds2D(self).transform(function)

	def __iter__(self):
	
		return iter((self.lower, self.upper))
	
	def __str__(self):
	
		return '(%s, %s)' %(self.lower, self.upper)
	
	def __repr__(self):
	
		return f'Bounds2D({self.lower}, {self.upper})'

	def __contains__(self, argument):
	
		return self.lower.x <= argument.x < self.upper.x \
		   and self.lower.x <= argument.y < self.upper.y
