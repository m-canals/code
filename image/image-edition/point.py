#!/usr/bin/env python3

from math import cos, sin, hypot, atan2

class Point2D:

	def __init__(self, * arguments):
	
		if len(arguments) == 0:
			self.x = 0
			self.y = 0
		elif len(arguments) == 1 and isinstance(arguments[0], Point2D):
			self.x = arguments[0].x
			self.y = arguments[0].y
		elif len(arguments) == 2:
			self.x = arguments[0]
			self.y = arguments[1]
		else:
			raise TypeError
	
	@property
	def xy(self):
	
		return (self.x, self.y)
	
	def __iadd__(self, argument):
		
		if isinstance(argument, Point2D):
			self.x += argument.x
			self.y += argument.y
		else:
			self.x += argument
			self.y += argument
		
		return self
	
	def __isub__(self, argument):
		
		if isinstance(argument, Point2D):
			self.x -= argument.x
			self.y -= argument.y
		else:
			self.x -= argument
			self.y -= argument
	
		return self
	
	def __imul__(self, argument):
		
		if isinstance(argument, Point2D):
			self.x *= argument.x
			self.y *= argument.y
		else:
			self.x *= argument
			self.y *= argument
	
		return self
	
	def __itruediv__(self, argument):
		
		if isinstance(argument, Point2D):
			self.x /= argument.x
			self.y /= argument.y
		else:
			self.x /= argument
			self.y /= argument

		return self
	
	def __ipow__(self, argument):
	
		self.x **= argument
		self.y **= argument

		return self
	
	def __ilshift__(self, argument):
	
		radius = self.distance()
		angle = self.angle() + argument
		self.x = radius * cos(angle)
		self.y = radius * sin(angle)

		return self
	
	def __irshift__(self, argument):
	
		radius = self.distance()
		angle = self.angle() - argument
		self.x = radius * cos(angle)
		self.y = radius * sin(angle)

		return self
	
	def __iter__(self):
	
		return iter((self.x, self.y))

	def __hash__(self):
	
		return hash((self.x, self.y))

	def __str__(self):
	
		return f'({self.x}, {self.y})'
	
	def __repr__(self):
	
		return f'Point2D({self.x}, {self.y})'

	def __pos__(self):
	
		return Point2D(self)

	def __neg__(self):
		
		return Point2D(-self.x, -self.y)
	
	def __add__(self, argument):
		
		return Point2D(self).__iadd__(argument)
	
	def __sub__(self, argument):
		
		return Point2D(self).__isub__(argument)
	
	def __mul__(self, argument):
		
		return Point2D(self).__imul__(argument)

	def __truediv__(self, argument):
		
		return Point2D(self).__itruediv__(argument)

	def __pow__(self, argument):

		return Point2D(self).__ipow__(argument)

	def __lshift__(self, argument):

		return Point2D(self).__ilshift__(argument)

	def __rshift__(self, argument):
	
		return Point2D(self).__irshift__(argument)

	def dot(self, argument):
	
		return self.x * argument.x + self.y * argument.y

	def distance(self, argument = None):
	
		if argument == None:
			return hypot(self.x, self.y)
		else:
			return hypot(self.x - argument.x, self.y - argument.y)

	def angle(self):
		
		return atan2(self.y, self.x)

