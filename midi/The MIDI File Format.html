<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<HTML>
<HEAD>
<TITLE>The MIDI File Format</TITLE>
</HEAD>
<BODY>
<H1>The MIDI File Format</H1>
<H2>MIDI File Structure</H2>
<H3>Chunks</H3>
<P>MIDI files are structured into <EM>chunks</EM>.
<P>Each chunk consists of:
<P>
<TABLE BORDER=2 CELLPADDING=2>
<TR ALIGN=CENTER><TH>type<TH>length<TH>data
<TR ALIGN=CENTER>
	<TD ROWSPAN=1>4 bytes</EM>
	<TD ROWSPAN=1>4 bytes</EM>
	<TD><EM>length</EM> bytes
</TABLE>
<UL>
<LI>A 4-byte <EM>chunk type</EM> (ascii)
<LI>A 4-byte <EM>length</EM> (32 bits, msb first)
<LI><EM>length</EM> bytes of data
</UL>

<P>There are two types of chunks:
<DL>
<DT>Header Chunks
<DD>which have a chunk type of "MThd"
<DT>Track Chunks
<DD>which have a chunk type of "MTrk"
</DL>
<P>A MIDI file consists of a single header chunk followed by
one or more track chunks.
<P>Since the length-field is mandatory in the structure of chunks, it is
possible to accomodate chunks other than "MThd" or "MTrk" in a MIDI file, by
skipping over their contents.
The MIDI specification <EM>requires</EM> that software be able to
handle unexpected chunk-types by ignoring the entire chunk.
<P>
<TABLE BORDER=2 CELLPADDING=2>
<TR ALIGN=CENTER><TH><TH COLSPAN=5>&lt;---Chunk---&gt;
<TR ALIGN=CENTER><TH><TH>type<TH>length<TH COLSPAN=3>Data
<TR ALIGN=CENTER><TH ROWSPAN=7 VALIGN=TOP>MIDI<BR>File<BR>:
	<TD ROWSPAN=1><TT>MThd</TT>
	<TD ROWSPAN=1>6
	<TD><I>&lt;format&gt;</I>
	<TD><I>&lt;tracks&gt;</I>
	<TD><I>&lt;division&gt;</I>
<TR ALIGN=CENTER>
	<TD ROWSPAN=1><TT>MTrk</TT>
	<TD ROWSPAN=1><I>&lt;length&gt;</I>
	<TD COLSPAN=3><I>&lt;delta_time&gt;</I> <I>&lt;event&gt;</I>
	...
<TR ALIGN=CENTER>
	<TD COLSPAN=5>:
<TR ALIGN=CENTER>
	<TD ROWSPAN=1><TT>MTrk</TT>
	<TD ROWSPAN=1><I>&lt;length&gt;</I>
	<TD COLSPAN=3><I>&lt;delta_time&gt;</I> <I>&lt;event&gt;</I>
	...
</TABLE>


<H3>Value Representations</H3>
<P>Within a MIDI file, there is a variety of information in addition
to the basic MIDI data, such as delta-times and meta-events.
<P>Where such information contains numeric values, these
are generally represented in one
of two formats:
<UL>
<LI><A href=#binary>binary</A>
<LI><A href=#vlq>variable length quantity</A>
</UL>
<H4><A name=binary>Binary</A></H4>
<P>Binary values are stored:
<UL>
<LI>8-bits per byte
<LI>MSB first (Most Significant Byte first)
</UL>
(unless otherwise specified)
<H4><A name=vlq>Variable Length Quantities</A></H4>
<P>The <EM>variable-length quantity</EM> provides a convenient means
of representing arbitrarily large integers, without creating
needlessly large fixed-width integers.
<P>A <EM>variable-length quantity</EM> is a represented as a
series of 7-bit values, from most-significant to least-significant.
where the last byte of the series bit 7 (the most significant bit) set to 0,
and the preceding bytes have bit 7 set to 1.
<P>Examples:
<TABLE BORDER=2 CELLPADDING=2>
<TR ALIGN=CENTER><TH COLSPAN=3>Value<TH COLSPAN=2>Variable Length representation
</TH>
<TR ALIGN=CENTER><TD>Decimal<TD>Hex<TD>Binary<TD>Binary<TD>Hex
<TR ALIGN=CENTER><TD>-<TD>abcd<TD NOWRAP><TT>aaaabbbbccccdddd</TT><TD NOWRAP><TT>100000aa 1aabbbbc 0cccdddd</TT><TD>-
<TR ALIGN=CENTER><TD>0<BR>:<BR>127<TD>00<BR>:<BR>7F<TD NOWRAP><TT >0000 0000<BR>:<BR>0111 1111</TT><TD NOWRAP><TT>0000 0000<BR>:<BR>0111 1111</TT><TD>00<BR>:<BR>7F
<TR ALIGN=CENTER><TD>128<BR>:<BR>16383<TD>80<BR>:<BR>3FFF<TD NOWRAP><TT>00000000 10000000<BR>:<BR>00111111 11111111</TT><TD NOWRAP><TT>10000001 00000000<BR>:<BR>11111111 01111111</TT><TD>81 00<BR>:<BR>FF 7F
<TR ALIGN=CENTER><TD>1000<TD>03E8<TD NOWRAP><TT>11 1110 1000<TD NOWRAP><TT>10000111 01101000</TT><TD>87 68
<TR ALIGN=CENTER><TD>100000<TD>0F4240<TD NOWRAP><TT>1111 0100 0010 0100 0000<TD NOWRAP><TT>10111101 10000100 01000000</TT><TD NOWRAP>BD 84 40
</TABLE>
<P>As you can see from the above examples, small values (0-127) can be
represented by a single byte, while larger values are also accomodated.
<P>The largest value allowed within a MIDI file
is <TT>0FFFFFFF</TT>.
This limit is set to allow variable-length quantities
to be manipulated as 32-bit integers.


<H2>Header Chunks</H2>
<P>The data part of a header chunk contains three 16-bit fields.
These fields specify the format, number of tracks, and timing
for the MIDI file.
<P>The length of the header chunk is 6-bytes. However, software
which reads MIDI files is <EM>required</EM> to honour the length
field, even if it is greater than expected. Any unexpected
data must be ignored.

<P>
<TABLE BORDER=2 CELLPADDING=2>
<TR ALIGN=CENTER><TH COLSPAN=5>Header Chunk
<TR ALIGN=CENTER><TH>Chunk Type<TH>length<TH COLSPAN=3>Data
<TR ALIGN=CENTER>
		 <TD ROWSPAN=2>4 bytes
		 <BR>(ascii)
		 <TD ROWSPAN=2>4 bytes
		 <BR>(32-bit binary)
		 <TD COLSPAN=3>&lt;-- <I>length</I> (= 6 bytes) --&gt;
<TR ALIGN=CENTER>
		 <TD>16-bit<TD>16-bit<TD>16-bit 
<TR ALIGN=CENTER><TD><TT>MThd</TT><TD><I>&lt;length&gt;</I><TD><I>&lt;format&gt;</I>
<TD><I>&lt;tracks&gt;</I><TD><I>&lt;division&gt;</I>
</TABLE>
<DL>
<DT><I>&lt;length&gt;</I>
<DD>length in bytes of the chunk data part.
<BR>This is a 32-bit binary number, MSB first.
<BR>This will be exactly 6 (bytes) for any MIDI file created under
the MIDI&nbsp;1.0 specification.
Nevertheless, any MIDI file reader should be able to cope with larger header-chunks,
to allow for future expansion.
<P>
<DT><I>&lt;format&gt;</I>
<DD>The MIDI file format.
<BR>This is a 16-bit binary number, MSB first.
<BR>The only valid formats are <A href="#mff0">0</A>, 
<A href="#mff1">1</A> and <A href="#mff2">2</A>.
<P>
<DT><I>&lt;tracks&gt;</I>
<DD>The number of track chunks contained in this MIDI file.
<BR>This is a 16-bit binary number, MSB first.
<P>
<DT><I>&lt;division&gt;</I>
<DD>This defines the default unit of delta-time for this MIDI file.
<BR>This is a 16-bit binary value, MSB first.
<P>This may be in either of two formats, depending on the value of MS bit:
<P>
<TABLE BORDER=2 CELLPADDING=2>
<TR ALIGN=CENTER><TH>Bit:<TD><TT>15</TT><TD><TT>14  ...  8</TT><TD><TT>7  ...  0</TT></TD>
<TR ALIGN=CENTER><TH ROWSPAN=2><I>&lt;division&gt;</I><TD><CODE>0</CODE><TD COLSPAN=2><I>ticks per quarter note</I>
<TR ALIGN=CENTER><TD><CODE>1</CODE><TD><I>-frames/second</I><TD><I>ticks / frame</I>
</TABLE>
<P>
<DL>
<DT>bit 15 = 0:
<DD><DL>
<DT>bits 0-14
<DD>number of delta-time units
in each a quarter-note.
</DL>
<P>
<DT>bit 15 = 1:
<DD><DL>
<DT>bits 0-7<DD>number of delta-time units per SMTPE frame
<DT>bits 8-14<DD>form a negative number,
representing the number of SMTPE frames per second.
Valid values correspond to those in the
<A href="midi_system_common.html#mtc_qframe">MTC Quarter Frame</A> message.
<P>
<PRE>
   -24 = 24 frames per second
   -25 = 25 frames per second
   -29 = 30 frames per second, drop frame
   -30 = 30 frames per second, non-drop frame
</PRE>
</DL>
</DL>
</DL>

<H3>MIDI File Formats</H3>
<P>MIDI files come in 3 variations:
<UL>
<LI>Format 0
<BR>...which contain a single track
<P>
<LI>Format 1
<BR>... which contain one or more simultaneous tracks
<BR>(ie all tracks are to be played simultaneously).
<P>
<LI>Format 2
<BR>...which contain one or more independant tracks
<BR>(ie each track is to be played independantly of the others).
<P>
</UL>
<H4><A name=mff0>Format 0 MIDI Files</A></H4>
<P>Format 0 MIDI files consist of a header-chunk and a single
track-chunk.
<P>The single track chunk will contain all the note and tempo information.
<H4><A name=mff1>Format 1 MIDI Files</A></H4>
<P>Format 1 MIDI files consist of a header-chunk
and one or more track-chunks, with all tracks being played
simultaneously.
<P>The first track of a Format 1 file is special, and is also known
as the 'Tempo Map'. It should contain all meta-events of the
types 
<A href=#timesig>Time Signature</A>, and
<A href=#settempo>Set Tempo</A>.
The meta-events
<A href=#trackname>Sequence/Track Name</A>,
<A href=#seqnum>Sequence Number</A>,
<A href=#marker>Marker</A>,
and
<A href=#smtpeoff>SMTPE Offset</A>.
should also be on the first track of a Format 1 file.


<H4><A name=mff2>Format 2 MIDI Files</A></H4>
<P>Format 2 MIDI files consist of a header-chunk
and one or more track-chunks, where each track represents
an independant sequence.

<H2>Track Chunks</H2>
<P>The data part of a track chunk contains one or more
<I>&lt;delta_time&gt;</I> <I>&lt;event&gt;</I> pairs.
The <I>&lt;delta_time&gt;</I> is not optional,
but zero is a valid delta-time.
<P>
<TABLE BORDER=2 CELLPADDING=2>
<TR ALIGN=CENTER><TH COLSPAN=3>Track Chunk
<TR ALIGN=CENTER><TH>type<TH>length<TH>data
<TR ALIGN=CENTER>
	<TD>4 bytes
	<BR>(ascii)
	<TD>4 bytes
	<BR>(32-bit binary)
	<TD>&lt;-- <I>length</I> bytes --&gt;
	<BR>(binary data)
<TR ALIGN=CENTER>
	<TD><TT>MTrk</TT>
	<TD><I>&lt;length&gt;</I>
	<TD><I>&lt;delta_time&gt;</I> <I>&lt;event&gt;</I>
		     ...
</TABLE>
<P>
<DL>
<DT><I>&lt;delta_time&gt;</I>
<DD>is the number of 'ticks' from the previous event,
and is represented as a variable length quantity
<P>
<DT><I>&lt;event&gt;</I>
<DD>is one of:
<UL>
<LI><A href=#midi_event><I>&lt;midi_event&gt;</I></A>
<LI><A href=#sysex_event><I>&lt;sysex_event&gt;</I></A>
<LI><A href=#meta_event><I>&lt;meta_event&gt;</I></A>
</UL>
</DL>
<H3>Events</H3>
<P>Notice that there are no explicit delimiters between the
<I>&lt;delta_time&gt;</I> and <I>&lt;event&gt;</I> instances.
<BR>This is possible because both the delta-times and events
have clearly defined lengths:
<UL>
<LI>The last byte of a delta-time is identified by having MSbit=0;
<LI>MIDI Channel messages have a pre-defined length (even when running status
is used);
<LI>and sysex-events and meta-events contain an explicit length field.
</UL>
<H4><A name=midi_event>MIDI Events</A></H4>
<P>A <I>&lt;midi_event&gt;</I></A> is any MIDI Channel message.
This includes:
<UL>
<LI><A href=midi_channel_voice.html>Channel Voice messages</A>
<LI><A href=midi_channel_mode.html>Channel Mode messages</A>
</UL>
<P><A href="midi_messages.html#running">Running status</A>
is applicable within MIDI files in the same manner as usual.
Running status is cancelled by any <I>&lt;sysex_event&gt;</I>
or <I>&lt;meta_event&gt;</I>
<P>

<H4><A name=sysex_event>Sysex Events</A></H4>
<P>Where is is desirable to include messages other than
MIDI Channel messages in a MIDI file (System Exclusive messages in particular),
the <I>&lt;sysex_event&gt;</I>
can be used.
<P>Sysex events come in two flavors:
<TABLE BORDER=2 CELLPADDING=2>
<CAPTION><STRONG>Sysex Events</STRONG></CAPTION>
<TR VALIGN=TOP>
    <TD NOWRAP><TT>F0</TT> <I>&lt;length&gt;</I> <I>&lt;sysex_data&gt;</I>
    <TD COLSPAN=2><B>F0 Sysex Event</B>
<TR VALIGN=TOP><TD>
    <TD COLSPAN=2><P>This results in a <TT>F0</TT> being sent (Start-Of-Exclusive),
	followed by <I>&lt;sysex_data&gt;</I>
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER><I>&lt;length&gt;</I>
    <TD>is a variable length quantity, specifying the length of 
	<I>&lt;sysex_data&gt;</I>
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER><I>&lt;sysex_data&gt;</I>
    <TD><TT>F0</TT> <I>&lt;sysex_data&gt;</I>
	<BR>is sent as a MIDI message.
<TR VALIGN=TOP>
    <TD NOWRAP><TT>F7</TT> <I>&lt;length&gt;</I> <I>&lt;any_data&gt;</I>
    <TD COLSPAN=2><B>F7 Sysex Event</B> (or 'escape')
<TR VALIGN=TOP><TD>
    <TD COLSPAN=2>This results in exactly <I>&lt;any_data&gt;</I> being sent,
	without anything else being added.
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER><I>&lt;length&gt;</I>
    <TD>is a variable length quantity, specifying the length of
	<I>&lt;any_data&gt;</I>
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER><I>&lt;any_data&gt;</I>
    <TD><I>&lt;any_data&gt;</I>
	<BR>is sent as a MIDI message.
</TABLE>
<P>In both cases, the End-Of-Exclusive message, <TT>F7</TT>, is not sent
automatically. It must be  specified explicitly within <I>&lt;sysex_data&gt;</I>
or <I>&lt;any_data&gt;</I>.

<P>Most System Exclusive messages are quite simple, and are sent
as a single packet of bytes, starting with <TT>F0</TT> and ending with
<TT>F7</TT>. These are easily accomodated using the <TT>F0</TT> form of
the <I>&lt;sysex_event&gt;</I>
<P>However, some System Exclusive messages are used
to control device parameters in real-time, and what is syntactically
a single System Exclusive message may consist
of a series of small parts which must be sent with appropriate delays.
Hence it is necessary to be able to break up a single message into
a number of events, with appropriate delta-times.
This can be accomodated by using the <TT>F7</TT> form of the
<I>&lt;sysex_event&gt;</I>.
<P>In fact, the F7 Sysex Event can be used to included any
data into the MIDI stream, such as MIDI System Real-Time
messages (though this is generally not desirable).
<DL>
<DT>Example 1:
<DD>To generate the message:
<BR><TT>F0 7E 09 03 01 01 F7</TT>
<BR>(Sample Dump Request - Device 9, sample number 257)
<BR>the <I>&lt;sysex_event&gt;</I> would be:
<BR><TT>F0 06 7E 09 03 01 01 F7</TT>
<BR>or alternately:
<BR><TT>F7 07 F0 7E 09 03 01 01 F7</TT>
<DT>Example 2:
<DD>The MIDI System Real-Time message 'stop' can be inserted using
<BR>the <I>&lt;event&gt;</I> <TT>F7 01 FC</TT>, while 'continue'.
would be <TT>F7 01 FB</TT>.
<P>Suppose you wanted to pause an external drum machine by
sending a 'stop', followed 48 delta-time units later by 'continue'.
The complete delta-time/event sequence would look something like this:
<PRE>
  00 F7 01 FC 30 F7 01 FB
</PRE>
</DL>
<P>
<H4><A name=meta_event>Meta Events</A></H4>
<P>Meta Events are used for things like
track-names, lyrics and cue-points, which don't 
result in MIDI messages being sent,
but are still useful components of a MIDI file.
<P>Meta Events have the general form:
<DL>
<DT><TT>FF</TT> <I>&lt;type&gt;</I> <I>&lt;length&gt;</I> <I>&lt;data&gt;</I>
<DD><P>where:
<DL>
<P>
<DT><I>&lt;type&gt;</I>
<DD>is a single byte, specifying the type of meta-event.
<BR>The possible range is <TT>00-7F</TT>.
Not all values in this range are defined, but programs must be able
to cope with (ie ignore) unexpected values by examining the length and
skipping over the data portion.
<P>
<DT><I>&lt;length&gt;</I>
<DD>is the number of bytes of <I>&lt;data&gt;</I> following.
<BR>This is a variable length quantity
<BR>0 is a valid <I>&lt;length&gt;</I>
<P>
<DT><I>&lt;data&gt;</I>
<DD>zero or more bytes of data
</DL>
</DL>
<TABLE BORDER=2 CELLPADDING=2>
<CAPTION><STRONG>Meta Events</STRONG></CAPTION>
<TR VALIGN=TOP>
    <TD NOWRAP><TT>FF 00 02</TT> <I>ss ss</I>
    <TD COLSPAN=2><B><A name=seqnum>Sequence Number</A></B>
<TR VALIGN=TOP><TD>
    <TD COLSPAN=2>This is an optional event, which must occur only at the
        start of a track, before any non-zero delta-time.
    <P>For Format 2 MIDI files, this is used to identify each
        track. If omitted, the sequences are numbered sequentially
        in the order the tracks appear.
    <P>For Format 1 files, this event should occur on the
        first track only.
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER><I>ss ss</I>
    <TD>Sequence Number, 16 bit binary
<TR VALIGN=TOP>
    <TD NOWRAP><TT>FF 01</TT> <I>&lt;len&gt;</I> <I>&lt;text&gt;</I>
    <TD COLSPAN=2><B>Text Event</B>
<TR VALIGN=TOP><TD>
    <TD COLSPAN=2>This event is used for annotating the track with arbitrary text.
    <BR>Arbitrary 8-bit data (other than ascii text) is also permitted.
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER><I>&lt;len&gt;</I>
    <TD>length of <I>&lt;text&gt;</I> (variable length quantity)
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER><I>&lt;text&gt;</I>
    <TD><I>&lt;len&gt;</I> bytes of ascii text, or 8-bit binary data
<TR VALIGN=TOP>
    <TD NOWRAP><TT>FF 02</TT> <I>&lt;len&gt;</I> <I>&lt;text&gt;</I>
    <TD COLSPAN=2><B>Copyright Notice</B>
<TR VALIGN=TOP><TD>
    <TD COLSPAN=2>This event is for a Copyright notice in ascii text.
    <BR>This should be of the form "(C) 1850 J.Strauss"
    <P>This event should be the first event on the first track.
<TR VALIGN=TOP>
    <TD NOWRAP><TT>FF 03</TT> <I>&lt;len&gt;</I> <I>&lt;text&gt;</I>
    <TD COLSPAN=2><B><A name=trackname>Sequence/Track Name</A></B>
<TR VALIGN=TOP><TD>
    <TD COLSPAN=2>Name of the sequence or track
<TR VALIGN=TOP>
    <TD NOWRAP><TT>FF 04</TT> <I>&lt;len&gt;</I> <I>&lt;text&gt;</I>
    <TD COLSPAN=2><B>Instrument Name</B>
<TR VALIGN=TOP><TD>
    <TD COLSPAN=2>A description of the instrument(s) used on this track.
    <BR>This can also be used to describe instruments on a
        particular MIDI Channel within a track, by preceding
        this event with the meta-event MIDI Channel Prefix.
        (or specifying the channel(s) within the text).
<TR VALIGN=TOP>
    <TD NOWRAP><TT>FF 05</TT> <I>&lt;len&gt;</I> <I>&lt;text&gt;</I>
    <TD COLSPAN=2><B>Lyric</B>
<TR VALIGN=TOP><TD>
    <TD COLSPAN=2>Lyrics for the song.
    <BR>Normally, each syllable will have it's own lyric-event,
        which occurs at the time the lyric is to be sung.
<TR VALIGN=TOP>
    <TD NOWRAP><TT>FF 06</TT> <I>&lt;len&gt;</I> <I>&lt;text&gt;</I>
    <TD COLSPAN=2><B><A name=marker>Marker</A></B>
<TR VALIGN=TOP><TD>
    <TD COLSPAN=2>Normally on the first track of a format 1 or format 0 file.
    <BR>Marks a significant point in the sequence (eg "Verse 1")
<TR VALIGN=TOP>
    <TD NOWRAP><TT>FF 07</TT> <I>&lt;len&gt;</I> <I>&lt;text&gt;</I>
    <TD COLSPAN=2><B>Cue Point</B>
<TR VALIGN=TOP><TD>
    <TD COLSPAN=2>Used to include cues for events happening on-stage, such
        as "curtain rises", "exit, stage left", etc.
<TR VALIGN=TOP>
    <TD NOWRAP><TT>FF 20 01</TT> <I>cc</I>
    <TD COLSPAN=2><B>MIDI Channel Prefix</B>
<TR VALIGN=TOP><TD>
    <TD COLSPAN=2>Associate all following meta-events and sysex-events
        with the specified MIDI channel, until the next <I>&lt;midi_event&gt;</I>
        (which must contain MIDI channel information).
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER><I>cc</I>
    <TD>MIDI channel 1-16 <BR>Range: <TT>00-0F</TT>
<TR VALIGN=TOP>
    <TD NOWRAP><TT>FF 2F 00</TT>
    <TD COLSPAN=2><B>End of Track</B>
<TR VALIGN=TOP><TD>
    <TD COLSPAN=2>This event is <EM>not</EM> optional.
    <BR>It is used to give the track a clearly defined length, which is
        essential information if the track is looped or concatenated with
        another track.
<TR VALIGN=TOP>
    <TD NOWRAP><TT>FF 51 03</TT> <I>tt tt tt</I>
    <TD COLSPAN=2><B><A name=settempo>Set Tempo</A></B>
<TR VALIGN=TOP><TD>
    <TD COLSPAN=2>This sets the tempo in microseconds per quarter note.
	This means a change in the unit-length of a delta-time tick.
	<A href=#note1>(note 1)</A>

    <P>If not specified, the default tempo is 120 beats/minute,
        which is equivalent to <I>tttttt</I>=500000
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER><I>tt tt tt</I>
    <TD COLSPAN=2>New tempo, in us/quarter-note<BR>24-bit binary

<TR VALIGN=TOP>
    <TD NOWRAP><TT>FF 54 05</TT> <I>hh mm ss fr ff</I>
    <TD COLSPAN=2><B><A name=smtpeoff>SMTPE Offset</A></B>
<TR VALIGN=TOP><TD>
    <TD COLSPAN=2>This (optional) event specifies the SMTPE time
	at which the track is to start.
	<BR>This event must occur before any non-zero delta-times,
	and before any MIDI events.
	<P>In a format 1 MIDI file, this event must be on the first track
	(the tempo map).
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER NOWRAP><I>hh mm ss fr</I>
    <TD>hours/minutes/seconds/frames in SMTPE format
	<BR>this must be consistant with the message
	<A href=midi_system_common.html#mtc_qframe>MIDI Time Code Quarter Frame</A>
	(in a particular, the time-code type must be present in <I>hh</I>)
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER><I>ff</I>
    <TD>Fractional frame, in hundreth's of a frame

<TR VALIGN=TOP>
    <TD NOWRAP><TT>FF 58 04</TT> <I>nn dd cc bb</I>
    <TD COLSPAN=2><B><A name=timesig>Time Signature</A></B>
<TR VALIGN=TOP><TD>
    <TD COLSPAN=2>Time signature of the form:
	<BR><I>nn</I>/2^<I>dd</I>
	<BR>eg: 6/8 would be specified using <I>nn</I>=6, <I>dd</I>=3
	<P>The parameter <I>cc</I> is the number of MIDI Clocks
	per metronome tick.
	<P>Normally, there are 24 MIDI Clocks per quarter note.
	However, some software allows this to be set by the user.
	The parameter <I>bb</I> defines this in terms of the
	number of 1/32 notes which make up the usual 24 MIDI Clocks
	(the 'standard' quarter note).
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER><I>nn</I>
    <TD>Time signature, numerator 
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER><I>dd</I>
    <TD>Time signature, denominator expressed as a
	power of 2.
	<BR>eg a denominator of 4 is expressed as <I>dd</I>=2
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER><I>cc</I>
    <TD>MIDI Clocks per metronome tick
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER><I>bb</I>
    <TD>Number of 1/32 notes per 24 MIDI clocks (8 is standard)
<TR VALIGN=TOP>
    <TD NOWRAP><TT>FF 59 02</TT> <I>sf mi</I>
    <TD COLSPAN=2><B>Key Signature</B>
<TR VALIGN=TOP><TD>
    <TD COLSPAN=2>Key Signature, expressed as the number of
	sharps or flats, and a major/minor flag.
	<P>0 represents a key of C, negative numbers represent
	'flats', while positive numbers represent 'sharps'.
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER><I>sf</I>
    <TD>number of sharps or flats
	<BR>-7 = 7 flats
	<BR> 0 = key of C
	<BR>+7 = 7 sharps
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER><I>mi</I>
    <TD>0 = major key
	<BR>1 = minor key
<TR VALIGN=TOP>
    <TD NOWRAP><TT>FF 7F</TT> <I>&lt;len&gt;</I> <I>&lt;id&gt;</I> <I>&lt;data&gt;</I>
    <TD COLSPAN=2><B>Sequencer-Specific Meta-event</B>
<TR VALIGN=TOP><TD>
    <TD COLSPAN=2>This is the MIDI-file equivalent of the System Exclusive
	Message.
	<P>A manufacturer may incorporate sequencer-specific directives into
	a MIDI file using this event.
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER><I>&lt;len&gt;</I>
    <TD>length of <I>&lt;id&gt;</I>+<I>&lt;data&gt;</I> (variable length quantity)
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER><I>&lt;id&gt;</I>
    <TD>1 or 3 bytes representing the Manufacturer's ID
	<BR>This value is the same as is used for 
	<A href=midi_system_exclusive.html>MIDI System Exclusive messages</A>
<TR><TD>
    <TD VALIGN=TOP ALIGN=CENTER><I>&lt;data&gt;</I>
    <TD>8-bit binary data
</TABLE>










<HR>
<H2>Footnotes</H2>
<DL>
<DT><A name=note1>Note 1</A><DD>In the case where the value of 
<I>&lt;division&gt;</I> (in the header chunk) defines
delta-time units in 'ticks per quarter note' (MSbit=0),
a change in tempo means a change in the length of a unit of delta-time.
<P>In the case where <I>&lt;division&gt;</I> MSbit=1, and the 'ticks'
are defined in absolute terms (ticks/frame and frames/second),
it is not clear from the specification
what effect a new tempo should have.
<P>
</DL>

<HR>
<P><A HREF="midi_system_exclusive.html">
<IMG src=icons/left.gif ALIGN=top ALT=Previous>&nbsp;midi_system_exclusive.html</A>
<A HREF="contents.html"><IMG src=icons/up.gif ALIGN=top ALT=Up>
Contents</A>
<A HREF="further_reading.html">
<IMG src=icons/right.gif ALIGN=top ALT=Next>
further_reading.html</A>
<P>Please note this <A href=preface.html>Disclaimer</A>
<HR>
</BODY>
</HTML>
