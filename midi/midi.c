// References:
//  - https://www.csie.ntu.edu.tw/~r92092/ref/midi/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#define MTHD_TYPE 0x4d546864
#define MTRK_TYPE 0x4d54726b
#define FAILURE 0
#define SUCCESS 1

struct MThd {
	// Format 0: single track
	// Format 1: multiple simultaneously played tracks
	// Format 2: multiple independently played tracks
	uint16_t format;
	uint16_t tracks;
	uint16_t division;
};

struct MTrk {
	// TODO
};

int read_header(FILE * stream, struct MThd * header) {
	uint32_t type;
	unsigned long int length;
	unsigned int format, tracks, division;

	if (fread(&type, sizeof type, 1, stream) != 1) {
		fprintf(stderr, "ERROR: read error.\n");
		return FAILURE;
	}

	if ((type = be32toh(type)) != MTHD_TYPE) {
		fprintf(stderr, "ERROR: expected MThd chunk.\n");
		return FAILURE;
	}

	if (fread(&length, 4, 1, stream) != 1) {
		fprintf(stderr, "ERROR: read error.\n");
		return FAILURE;
	}
	
	if ((length = be32toh(length)) < 6) {
		fprintf(stderr, "ERROR: expected bigger MThd.\n");
		return FAILURE;
	}
	
	fread(&format, 2, 1, stdin);
	fread(&tracks, 2, 1, stdin);
	fread(&division, 2, 1, stdin);
	header->format = be16toh(format);
	header->tracks = be16toh(tracks);
	header->division = be16toh(division);
	fseek(stdin, length - 6, SEEK_CUR);
	return SUCCESS;
}

int display_header(struct MThd * header) {
	if (printf("Format: %u\n", header->format) < 0)
		return FAILURE;
	if (printf("Tracks: %u\n", header->tracks) < 0)
		return FAILURE;
	if (printf("Division: %u\n", header->division) < 0)
		return FAILURE;
	return SUCCESS;
}

int read_track(FILE * stream, struct MTrk * track) {
	uint32_t type;
	uint32_t length;
	
	if (fread(&type, sizeof type, 1, stream) != 1) {
		fprintf(stderr, "ERROR: read error.\n");
		return FAILURE;
	}

	if ((type = be32toh(type)) != MTRK_TYPE) {
		fprintf(stderr, "ERROR: expected MTrk chunk.\n");
		return FAILURE;
	}

	if (fread(&length, sizeof length, 1, stream) != 1) {
		fprintf(stderr, "ERROR: read error.\n");
		return FAILURE;
	}

	length = be32toh(length);

	/* TODO
	uint8_t byte;
	uint32_t delta_time;
	do {
		read byte
		byte = ...
	}	while (bit 8 == 1 and byte < 4)
	*/

	fseek(stream, length, SEEK_CUR);
	return SUCCESS;
}

int display_track(struct MTrk * track) {
	return SUCCESS;
}

int main() {
	FILE * stream = stdin;
	struct MThd header;
	uint16_t i;
	struct MTrk track;

	if (read_header(stream, &header) == FAILURE) {
		exit(EXIT_FAILURE);
	}
	
	if (display_header(&header) == FAILURE) {
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < header.tracks; i++) {
		if (read_track(stream, &track) == FAILURE) {
			exit(EXIT_FAILURE);
		}
		
		if (display_track(&track) == FAILURE) {
			exit(EXIT_FAILURE);
		}
	}

	exit(EXIT_SUCCESS);
}
