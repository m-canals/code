class Carousel {
	constructor(element, images) {
		this.images = images
		this.changeTime = 2000
		this.transitionTime = 25
		this.transitionOpacity = 0.05
		this.current = 0
		this.background = element;
		this.foreground = document.createElement('div')
		this.background.appendChild(this.foreground)
		this.createButtons()
		this.foreground.style.backgroundImage = "url('" + this.images[0] + "')";
		this.buttons[this.current].classList.add('active-slide-number');
		this.setOpacity(1);
	}
	
	createButtons() {
		this.buttons = new Array();
		var buttons = document.createElement('div');
		buttons.classList.add('slide-numbers');
		this.background.appendChild(buttons);
		for (var i = 0; i < this.images.length; i++ ) {
			var button = document.createElement('div')
			button.innerHTML = i + 1
			button.i = i
			button.classList.add('slide-number');
			buttons.appendChild(button)
			this.buttons.push(button)
			var that = this
			button.addEventListener("click", function() {
				that.change(this.i);
			});
		}
	}
	
	start() {
		var that = this;
		this.timeout = window.setTimeout(function() { that.change(); },
			this.changeTime);
	}
	
	stop() {
		window.clearTimeout(this.timeout);
	}
	
	play() {
		window.clearTimeout(this.timeout);
		this.transition();
	}
	
	setOpacity(opacity) {
		this.opacity = opacity;
		this.foreground.style.opacity = this.opacity;
	}
	
	change(next = this.current + 1) {
		this.background.style.backgroundImage = "url('" + this.images[this.current] + "')";
		this.buttons[this.current].classList.remove('active-slide-number');
		window.clearTimeout(this.timeout);
		this.current = next < this.images.length ? next : 0;
		this.setOpacity(0);
		this.foreground.style.backgroundImage = "url('" + this.images[this.current] + "')";
		this.buttons[this.current].classList.add('active-slide-number');
		this.transition();
	}
	
	transition() {
		if (this.opacity < 1) {
			this.setOpacity(this.opacity + this.transitionOpacity);
			var that = this;
			this.timeout = window.setTimeout(function() { that.transition(); },
				this.transitionTime);
		} else {
			var that = this;
			this.timeout = window.setTimeout(function() { that.change(); },
				this.changeTime);
		}
	}
}
