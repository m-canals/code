// http://diveintohtml5.info/canvas.html
// http://www.w3.org/TR/2010/WD-2dcontext-20100304/

class CanvasDrawable {
	constructor(element) {
		this.context = canvas.getContext("2d");
		this.drawing = new Drawing();
	}
	
	click(event) {
		this.drawing.click(event, this.context);
	}
	
	setDrawing(drawing) {
		this.drawing = drawing;
	}
}

class Drawing {
	constructor() {
		this.isFirstClick = true;
		this.strokeStyle = "#000";
		this.font = "bold 12px sans-serif";
	}

	click(event, context) {}
}

class Rectangle extends Drawing {
	click(event, context) {
		const x = event.offsetX;
		const y = event.offsetY;
		
		if (this.isFirstClick) {
			this.isFirstClick = false;
		} else {
			this.isFirstClick = true;
			const width = x - this.x + 1;
			const height = y - this.y + 1;
			context.strokeStyle = this.strokeStyle;
			context.strokeRect(this.x, this.y, width, height);
		}
		
		this.x = x;
		this.y = y;
	}
}

class Line extends Drawing {
	click(event, context) {
		const x = event.offsetX;
		const y = event.offsetY;
		
		if (this.isFirstClick) {
			this.isFirstClick = false;
		} else {
			this.isFirstClick = true;
			context.beginPath();
			context.moveTo(this.x, this.y);
			context.lineTo(x, y);
			context.strokeStyle = this.strokeStyle;
			context.stroke();
		}

		this.x = x;
		this.y = y;
	}
}

class Path extends Drawing {
	click(event, context) {
		const x = event.offsetX;
		const y = event.offsetY;

		if (this.isFirstClick) {
			this.isFirstClick = false;
		} else if (x == this.x && y == this.y) {
			this.isFirstClick = true;
		} else {
			context.beginPath();
			context.moveTo(this.x, this.y);
			context.lineTo(x, y);
			context.strokeStyle = this.strokeStyle;
			context.stroke();
		}

		this.x = x;
		this.y = y;
	}
}

class Text extends Drawing {
	click(event, context) {
		const x = event.offsetX;
		const y = event.offsetY;
		const text = prompt();
		
		if (text !== null) {
			context.font = this.font;
			context.fillText(text, x, y);
		}
	}
}
