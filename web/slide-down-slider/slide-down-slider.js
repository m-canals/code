class SlideDownSlider {
	constructor(element, start, stop, updateDistance = 1, updateTime = 20) {
		this.element = element;
		this.start = start;
		this.stop = stop;
		this.y = start;
		this.updateDistance = updateDistance;
		this.updateTime = updateTime;
		this.down = false;
		this.timeout = null;
		var button = this;
		element.addEventListener('mouseover', function() { button.invertMove(); });
		element.addEventListener('mouseout', function() { button.invertMove(); });
	}
	
	invertMove() {
		this.clearTimeout();
		this.down = ! this.down;
		this.move();
	}
	
	clearTimeout() {
		if (this.timeout !== null) {
			window.clearTimeout(this.timeout);
		}
	}
	
	setTimeout() {
		var button = this;
		this.element.timeoutId = window.setTimeout(
			function() { button.move(); },
			this.updateTime)
	}
	
	move() {
		if (this.down) {
			this.y = Math.min(this.y + this.updateDistance, this.stop)
			this.element.style.marginTop = this.y + 'px';
			if (this.y < this.stop) {
				this.setTimeout();
			}
		} else if (! this.down) {
			this.y = Math.max(this.y - this.updateDistance, this.start)
			this.element.style.marginTop = this.y + 'px';
			if (this.y > this.start) {
				this.setTimeout();
			}
		}
	}
}
