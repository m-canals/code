// TODO
//
//  - Fer submenús.
//  - Evitar que surti de la pantalla.

class Context1 {
	owns(event) {
		return event.target.id == "context-1"
	}
	getContent(event) {
		return '<a href="#1"><p>Acció 1</p></a><hr><a href="#2"><p>Acció 2</p></a><p class="disabled">Acció 3</p>'
	}
}

class ContextMenu {
	constructor(elementId) {
		this.element = document.getElementById(elementId)
		this.contexts = new Array()
	}
	bind(context) {
		this.contexts.push(context)
	}
	show(event) {
		event.preventDefault()
		for (var i = 0; i < this.contexts.length; i++) {
			if (this.contexts[i].owns(event)) {
				this.element.innerHTML = this.contexts[i].getContent(event)
				this.element.style.top = event.clientY + 'px'
				this.element.style.left = event.clientX + 'px'
				this.element.style.visibility = "visible"
			}
		}
	}
	hide(event) {
		// Si es fan submenús el parentNode s'haurà de revisar.
		if (event.target.id != this.element.id &&
		    event.target.parentNode.id != this.element.id) {
			this.element.style.visibility = "hidden"
		}
	}
}

window.addEventListener("load", function(event) {
	contextMenu = new ContextMenu("context-menu")
	context1 = new Context1()
	contextMenu.bind(context1)
	document.addEventListener("contextmenu", event => contextMenu.show(event))
	document.addEventListener("click", event => contextMenu.hide(event))
})
