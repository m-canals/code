class MCT
{
	constructor(table) //thead tbody
	{
		this.table = table;
				
		this.dragImage = document.createElement('span');
		this.dragImage.setAttribute(
			'style', 'position: absolute; display: block;');
		document.body.appendChild(this.dragImage);
		
		for (const header of table.rows.item(0).cells) {
			header.draggable = 'true';
			
			header.addEventListener('dragstart',
				event => this.dragStartListener(event));
			header.addEventListener('dragover',
				event => this.dragOverListener(event));
			header.addEventListener('dragleave',
				event => this.dragLeaveListener(event));
			header.addEventListener('drop',
				event => this.dropListener(event));
		}
	}
	
	getPosition(event)
	{
		const node = event.target;
		const rect = node.getBoundingClientRect();
		
		return (event.clientX - rect.left) / (rect.right - rect.left);
	}

	isOverLeftSide(event)
	{
		return this.getPosition(event) < 0.5;
	}

	dragStartListener(event)
	{
		const header = event.target;

		console.log('Drag start:', header.cellIndex);
		event.dataTransfer.setData('text/plain', header.cellIndex);
		event.dataTransfer.setDragImage(this.dragImage, 0, 0);
	}

	dragOverListener(event)
	{
		const header = event.target;
		const left = this.isOverLeftSide(event);
		
		console.log('Drag over:',  header.cellIndex);
		event.preventDefault();
		header.classList.add(left ? 'drag-over-left': 'drag-over-right');
		header.classList.remove(left ? 'drag-over-right': 'drag-over-left');
	}

	dragLeaveListener(event)
	{
		const header = event.target;
		
		console.log('Drag leave:', header.cellIndex);
		event.preventDefault();
		header.classList.remove('drag-over-left', 'drag-over-right');
	}

	dropListener(event)
	{
		const header = event.target;
		const index = parseInt(event.dataTransfer.getData('text/plain'));
		const left = this.isOverLeftSide(event);
		
		console.log('Drop:', index, header.cellIndex);
		event.preventDefault();
		header.classList.remove('drag-over-left', 'drag-over-right');
		
		if (left) this.moveColumnBefore(index, header.cellIndex);
		else this.moveColumnAfter(index, header.cellIndex);
	}
	
	moveColumnBefore(sourceIndex, destinationIndex)
	{
		for (const row of this.table.rows) {
			var source = row.cells.item(sourceIndex);
			var destination = row.cells.item(destinationIndex);
			row.insertBefore(source, destination);
		}
	}
	
	moveColumnAfter(sourceIndex, destinationIndex)
	{
		for (const row of this.table.rows) {
			var source = row.cells.item(sourceIndex);
			var destination = row.cells.item(destinationIndex);
			row.insertBefore(source, destination.nextSibling);
		}
	}
}

