// RCT resizing handle.
//
// Keep in mind that the same handle is used for every cell,
// so the event target, a cell, and its cell (column) index
// may be useful.
class RCTHandle
{
	// Constructor parameters are:
	//
	//  - Table (HTMLTableElement): table where this handle is used.
	//  - Size (number): width of the handle in pixels.
  //  - Speed (number): pixels per mouse movement unit.
	constructor(table, size, speed)
	{
		this.table = table;
		this.size = size;
		this.speed = speed;
	}

	// This method is called when the mouse moves over the table
	// where this handle is used as a left handle.
	overLeftHandle(event)
	{
		const left = event.target.getBoundingClientRect().left;
		
		return event.clientX < left + this.size;
	}

	// This method is called when the mouse moves over the table
	// where this handle is used as a right handle.
	overRightHandle(event)
	{
		const right = event.target.getBoundingClientRect().right;

		return event.clientX > right - this.size;
	}

	// This method is called when hovering starts (mouse moves into this handle).
	startHover(event) {}

	// This method is called while hovering (mouse moves over this handle).
	hover(event) {}

	// This method is called when hovering stops (mouse moves out of this handle)
	// and before resizing starts (mouse button is pressed inside a handle).
	stopHover(event) {}
	
	// This method is called when resizing starts
	// (mouse button is pressed inside this handle).
	startResize(event) {}
	
	// This method is called while resizing (mouse moves).
	resize(event) {}
	
	// This method is called when resizing stops (mouse button is released).
	stopResize(event) {}
}

// RCT basic resizing handle.
class RCTBasicHandle extends RCTHandle
{
	// Constructor parameters are:
	//
	//  - Table (HTMLTableElement): table where this handle is used.
	//  - Size (number): width of the handle in pixels.
	//  - Speed (number): pixels per mouse movement unit.
	//  - Cursor (string): value of the cursor style property of the table
	//    and the document body while hovering this handle or resizing with it,
	//    respectively.
	constructor(table, size, speed, cursor)
	{
		super(table, size, speed);
		this.cursor = cursor;
		this.replacedCursor = null;
	}

	startHover(event)
	{
		this.replacedCursor = this.table.style.cursor;
		this.table.style.cursor = this.cursor;
	}
	
	stopHover(event)
	{
		this.table.style.cursor = this.replacedCursor;
	}
	
	startResize(event)
	{
		this.replacedCursor = document.body.style.cursor;
		document.body.style.cursor = this.cursor;
	}
	
	stopResize(event)
	{
		document.body.style.cursor = this.replacedCursor;
	}
}

// RCT styled resizing handle.
class RCTStyledHandle extends RCTHandle
{
	// Constructor parameters are:
	//
	//  - Table (HTMLTableElement): table where this handle is used.
	//  - Size (number): width of the handle in pixels.
	//  - Speed (number): pixels per mouse movement unit.
	//  - Class (string): CSS class that is added to the table and the
	//    document body while hovering this handle or resizing with it,
	//    respectively.
	constructor(table, size, speed, className)
	{
		super(table, size, speed);
		this.className = className;
	}

	startHover(event)
	{
		this.table.classList.add(this.className);
	}
	
	stopHover(event)
	{
		this.table.classList.remove(this.className);
	}
	
	startResize(event)
	{
		document.body.classList.add(this.className);
	}
	
	stopResize(event)
	{
		document.body.classList.remove(this.className);
	}
}

// Resizable Column Table (RCT).
//
// Enables resizing of the columns of a table such that each row
// has the same number of cells. It does so by changing the width
// of the cells in the first row.
class RCT
{
	// Constructor parameters are:
	//
	//  - Table (HTMLTableElement): table used.
	//  - Left handle (RCTHandle): representation of the left resizing handle.
	//  - Right handle (RCTHandle): representation of the right resizing handle. 
	constructor(table, leftHandle, rightHandle)
	{
		this.headers = table.rows.item(0).cells;
		this.leftHandle = leftHandle;
		this.rightHandle = rightHandle;
		this.activeHandle = null;
		this.activeHeader = null;
		this.activeWidth = null;
		this.hoverController = null;
		this.resizeController = null;
		this.hoverOnMouseMove();
		this.startResizeOnMouseDown();
	}

	// ---------------------------------------------------------------- //

	getHoveredHandle(event)
	{

		const isCell = event.target instanceof HTMLTableCellElement;
		
		if (isCell && this.leftHandle.overLeftHandle(event))
			return this.leftHandle;
		else if (isCell && this.rightHandle.overRightHandle(event))
			return this.rightHandle;
		else
			return null;
	}
	
	getHoveredColumnHeader(event)
	{
		const isCell = event.target instanceof HTMLTableCellElement;
		
		if (isCell)
			return this.headers.item(event.target.cellIndex);
		else
			return null;
	}

	// ---------------------------------------------------------------- //

	hoverOnMouseMove()
	{
		this.hoverController = new AbortController();
		table.addEventListener('mousemove',
			this.hover.bind(this), {signal: this.hoverController.signal});
	}
	
	startResizeOnMouseDown()
	{
		table.addEventListener('mousedown',
			this.startResize.bind(this));
	}
	
	resizeOnMouseMove()
	{
		this.resizeController = new AbortController();
		document.addEventListener('mousemove',
			this.resize.bind(this), {signal: this.resizeController.signal});
	}
	
	stopResizeOnMouseUp()
	{
		document.addEventListener('mouseup',
			this.stopResize.bind(this), {once: true});
	}

	// ---------------------------------------------------------------- //

	hover(event)
	{
		const handle = this.getHoveredHandle(event);
		const header = this.getHoveredColumnHeader(event);
		
		if (handle === this.activeHandle && header === this.activeHeader) {
			if (this.activeHandle !== null) this.activeHandle.hover(event);
		} else {
			if (this.activeHandle !== null) this.activeHandle.stopHover(event);
			this.activeHandle = handle;
			this.activeHeader = header;
			if (this.activeHandle !== null) this.activeHandle.startHover(event);
		}
	}

	startResize(event)
	{
		const handle = this.getHoveredHandle(event);
		const header = this.getHoveredColumnHeader(event);

		if (handle !== null) {
			event.preventDefault();
			this.hoverController.abort();
			if (this.activeHandle !== null) this.activeHandle.stopHover(event);
			this.activeHandle = handle;
			this.activeHeader = header;
			// Get used width (all calculations performed) in pixels.
			this.activeWidth = parseFloat(window.getComputedStyle(header).width);
			this.activeHandle.startResize(event);
			this.resizeOnMouseMove();
			this.stopResizeOnMouseUp();
		}
	}

	resize(event)
	{
		this.activeWidth += this.activeHandle.speed * event.movementX;
		this.activeHeader.style.width = this.activeWidth.toString() + 'px';
		this.activeHandle.resize(event);
	}

	stopResize(event)
	{
		this.resizeController.abort();
		this.activeHandle.stopResize(event);
		this.activeHandle = null;
		this.hoverOnMouseMove();
	}
}
