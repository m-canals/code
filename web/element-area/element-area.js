// TODO
//
//  - Implement item resize, rotate, focus and blur.
//  - Improve touch event handling (partial ends, etc.)
//  - Test `ElementArea.getPosition`.
//
// =======================================================================

class Pan
{
	constructor(target, position)
	{
		this.target = target;
		this.previousPosition = null;
		this.position = position;
		this.x = null;
		this.y = null;
	}
	
	update(position)
	{
		// Works better than `event.movementX`and `event.movementY`.
		this.previousPosition = this.position;
		this.position = position;
		this.x = this.position.x - this.previousPosition.x;
		this.y = this.position.y - this.previousPosition.y;
	}
}

class MousePan extends Pan
{
	constructor(target, position)
	{
		super(target, position);
		this.abortController = new AbortController();
	}
}

class TouchPan extends Pan
{
	constructor(target, position, identifiers)
	{
		super(target, position);
		this.identifiers = new Set(identifiers);
	}
	
	getTouches(touches)
	{
		let usedTouches = new Array();
		
		for (const touch of touches) {
			if (this.identifiers.has(touch.identifier)) {
				usedTouches.push(touch);
			}
		}
		
		return usedTouches;
	}
}

class TouchPinch
{
	constructor(distance, identifiers)
	{
		this.previousDistance = distance;
		this.distance = distance;
		this.identifiers = new Set(identifiers);
		this.delta = 0.0;
	}
	
	getTouches(touches)
	{
		let usedTouches = new Array();
		
		for (const touch of touches) {
			if (this.identifiers.has(touch.identifier)) {
				usedTouches.push(touch);
			}
		}
		
		return usedTouches;
	}
	
	update(distance)
	{
		this.previousDistance = this.distance;
		this.distance = distance;
		this.delta = this.distance - this.previousDistance;
	}
}

// =======================================================================

class CreateEvent extends Event
{
	static type = "element-area-create";

	constructor(x, y, scale)
	{
		super(MoveEvent.type, {cancelable: true});
		this.x = x;
		this.y = y;
		this.scale = scale;
	}
}

class ScrollEvent extends Event
{
	static type = "element-area-scroll";

	constructor(x, y)
	{
		super(ScrollEvent.type, {cancelable: true});
		this.x = x;
		this.y = y;
	}
}

class ZoomEvent extends Event
{
	static type = "element-area-zoom";

	constructor(x, y, scale)
	{
		super(ZoomEvent.type, {cancelable: true});
		this.x = x;
		this.y = y;
		this.scale = scale;
	}
}

class AddEvent extends Event
{
	static type = "element-area-add";
	
	constructor(x, y, scale)
	{
		super(AddEvent.type, {cancelable: true});
		this.x = x;
		this.y = y;
		this.scale = scale;
	}
}

class DeleteEvent extends Event
{
	static type = "element-area-delete";

	constructor()
	{
		super(DeleteEvent.type, {cancelable: true});
	}
}

class MoveEvent extends Event
{
	static type = "element-area-move";

	constructor(x, y)
	{
		super(MoveEvent.type, {cancelable: true});
		this.x = x;
		this.y = y;
	}
}

class ScaleEvent extends Event
{
	static type = "element-area-scale";

	constructor(scale)
	{
		super(ScaleEvent.type, {cancelable: true});
		this.scale = scale;
	}
}

// =======================================================================

class ElementArea
{
	constructor()
	{
		this.mouseScaleConstant = 0.001;
		this.touchScaleConstant = 0.001;

		this.mousePan = null;
		this.touchPan = null;
		this.touchPinch = null;
		this.element = document.createElement('div');
		this.elements = new Set();

		this.create(0, 0, 1);

		this.element.addEventListener('wheel',
			this.handleWheelEvent.bind(this));
		this.element.addEventListener('mousedown',
			this.handleMouseDown.bind(this));
		this.element.addEventListener('touchstart',
			this.handleTouchStart.bind(this));
		this.element.addEventListener('touchmove',
			this.handleTouchMove.bind(this));
		this.element.addEventListener('touchend',
			this.handleTouchEnd.bind(this));
		this.element.addEventListener('touchcancel',
			this.handleTouchEnd.bind(this));
	}

	// =====================================================================
	
	createAction(element, x, y, scale)
	{		
		element.style.setProperty("--x", `${x}px`);
		element.style.setProperty("--y", `${y}px`);
		element.style.setProperty("--scale", `${scale}`);
	}
	
	scrollAction(element, x, y)
	{
		const style = window.getComputedStyle(element);
		const x0 = parseFloat(style.getPropertyValue("--x"));
		const y0 = parseFloat(style.getPropertyValue("--y"));
		
		element.style.setProperty("--x", `${x0 + x}px`);
		element.style.setProperty("--y", `${y0 + y}px`);
	}

	zoomAction(element, x, y, scale)
	{
		const style = window.getComputedStyle(element);
		const x0 = parseFloat(style.getPropertyValue("--x"));
		const y0 = parseFloat(style.getPropertyValue("--y"));
		const scale0 = parseFloat(style.getPropertyValue("--scale"));

		element.style.setProperty("--x", `${x + (x0 - x) * scale}px`);
		element.style.setProperty("--y", `${y + (y0 - y) * scale}px`);
		element.style.setProperty("--scale", `${scale0 * scale}`);
	}
	
	addAction(element, x, y, scale)
	{
		const style = window.getComputedStyle(this.element);
		const x0 = parseFloat(style.getPropertyValue("--x"));
		const y0 = parseFloat(style.getPropertyValue("--y"));
		const scale0 = parseFloat(style.getPropertyValue("--scale"));

		element.style.setProperty("--x", `${x0 + x}px`);
		element.style.setProperty("--y", `${y0 + y}px`);
		element.style.setProperty("--scale", `${scale0 * scale}`);
	}
	
	deleteAction(element)
	{
		
	}

	moveAction(element, x, y)
	{
		const style = window.getComputedStyle(element);
		const x0 = parseFloat(style.getPropertyValue("--x"));
		const y0 = parseFloat(style.getPropertyValue("--y"));
		
		element.style.setProperty("--x", `${x0 + x}px`);
		element.style.setProperty("--y", `${y0 + y}px`);
	}
	
	scaleAction(element, scale)
	{
		const style = window.getComputedStyle(element);
		const x0 = parseFloat(style.getPropertyValue("--x"));
		const y0 = parseFloat(style.getPropertyValue("--y"));
		const scale0 = parseFloat(style.getPropertyValue("--scale"));
		const width0 = parseFloat(style.getPropertyValue("width"));
		const height0 = parseFloat(style.getPropertyValue("height"));
		const x = (1 - scale) * width0 * scale0 / 2;
		const y = (1 - scale) * height0 * scale0 / 2;
		
		element.style.setProperty("--x", `${x0 + x}px`);
		element.style.setProperty("--y", `${y0 + y}px`);
		element.style.setProperty("--scale", `${scale0 * scale}`);
	}

	// =====================================================================

	create(x, y, scale)
	{
		const event = new CreateEvent(x, y, scale);
		
		if (this.element.dispatchEvent(event))
			this.createAction(this.element, x, y, scale);
	}

	scroll(x, y)
	{
		this.scrollElement(this.element, x, y);

		for (const element of this.elements)
			this.scrollElement(element, x, y);
	}
	
	scrollElement(element, x, y)
	{
		const event = new ScrollEvent(x, y);
		
		if (element.dispatchEvent(event))
			this.scrollAction(element, x, y);
	}

	zoom(x, y, scale)
	{
		scale = scale < 0 ? 1 / (1 - scale) : 1 + scale;
		
		this.zoomElement(this.element, x, y, scale);

		for (const element of this.elements)
			this.zoomElement(element, x, y, scale);
	}

	zoomElement(element, x, y, scale)
	{
		const event = new ZoomEvent(x, y, scale);
			
		if (element.dispatchEvent(event))
			this.zoomAction(element, x, y, scale);
	}

	add(element, x, y, scale)
	{
		this.elements.add(element);
		this.element.appendChild(element);
		
		const event = new AddEvent(x, y);
		
		if (element.dispatchEvent(event))
			this.addAction(element, x, y, scale);
	}
	
	delete(element)
	{
		this.element.removeChild(element);
		this.elements.delete(element);

		const event = new DeleteEvent();
		
		if (element.dispatchEvent(event))
			this.deleteAction(element);
	}
	
	move(element, x, y)
	{
		const event = new MoveEvent(x, y);
		
		if (element.dispatchEvent(event))
			this.moveAction(element, x, y);
	}
	
	scale(element, scale)
	{
		if (scale < 0) scale = 1 / (1 - scale);
		else scale = 1 + scale;
		
		const event = new ScaleEvent(scale);
			
		if (element.dispatchEvent(event))
			this.scaleAction(element, scale);
	}

	// =====================================================================
	
	getTarget(event)
	{
		let target = event.target;
		
		if (target !== this.element)
			while (target.parentElement !== this.element)
				target = target.parentElement;
		
		return target;
	}
	
	getPosition(event)
	{
		let x = event.clientX - this.element.offsetLeft;
		let y = event.clientY - this.element.offsetTop;

		if (event.target !== document && event.target !== this.element) {
			let element = event.target.offsetParent;
			
			while (element !== null && element !== this.element) {
				x += element.offsetLeft;
				y += element.offsetTop;
				element = element.offsetParent;
			}
		}
		
		return {'x': x, 'y': y};
	}
	
	getDistance(events)
	{
		const position1 = this.getPosition(events[0]);
		const position2 = this.getPosition(events[1]);
		
		return Math.sqrt((position1.x - position2.x) ** 2 +
			(position1.y - position2.y) ** 2);
	}
	
	getCenter(events)
	{
		let x = 0;
		let y = 0;
		
		for (const event of events) {
			const position = this.getPosition(event);
			x += position.x;
			y += position.y;
		}
		
		return {'x': x / events.length, 'y': y / events.length};
	}
	
	getIdentifiers(touches) {
		let identifiers = new Array();
		
		for (const touch of touches)
			identifiers.push(touch.identifier);
		
		return identifiers;
	}

	// =====================================================================

	handleMouseDown(event)
	{
		const target = this.getTarget(event);
		const position = this.getPosition(event);
	
		this.mousePan = new MousePan(target, position);

		window.addEventListener('mousemove', this.handleMouseMove.bind(this),
			{signal: this.mousePan.abortController.signal});
		window.addEventListener('mouseup', this.handleMouseUp.bind(this),
			{signal: this.mousePan.abortController.signal});
			
		event.preventDefault();
	}
	
	handleMouseMove(event)
	{
		const target = this.mousePan.target;
		const position = this.getPosition(event);
	
		this.mousePan.update(position);

		if (target === this.element)
			this.scroll(this.mousePan.x, this.mousePan.y);
		else
			this.move(target, this.mousePan.x, this.mousePan.y);
		
		event.preventDefault();
	}
	
	handleMouseUp(event)
	{
		this.mousePan.abortController.abort();
		this.mousePan = null;
	}
	
	handleWheelEvent(event)
	{
		const target = this.getTarget(event);
		const position = this.getPosition(event);
		const scale = - this.mouseScaleConstant * event.deltaY;

		if (target === this.element)
			this.zoom(position.x, position.y, scale);
		else
			this.scale(target, scale);
		
		event.preventDefault();
	}
	
	// =====================================================================
	
	handleTouchStart(event)
	{
		if (this.touchPan === null && this.touchPinch === null) {
			if (event.changedTouches.length == 1) {
				const touch = event.changedTouches[0];
				const identifiers = this.getIdentifiers([touch]);
				const target = this.getTarget(touch);
				const position = this.getPosition(touch);
				
				this.touchPan = new TouchPan(target, position, identifiers);
				
				event.preventDefault();
			}
			else if (event.changedTouches.length == 2) {
				const touches = event.changedTouches;
				const identifiers = this.getIdentifiers(touches);
				const distance = this.getDistance(touches);
				
				this.touchPinch = new TouchPinch(distance, identifiers);
				
				event.preventDefault();
			}
		}
		else if (this.touchPan !== null && this.touchPinch === null) {
			// Pinch touch start events aren't listened together (usual),
			// so we change the gesture.
			if (event.changedTouches.length == 1) {
				const newTouch = event.changedTouches[0];
				const oldTouch = this.touchPan.getTouches(event.touches)[0];
				const touches = [newTouch, oldTouch];
				const identifiers = this.getIdentifiers(touches);
				const distance = this.getDistance(touches);
				this.touchPan = null;
				
				this.touchPinch = new TouchPinch(distance, identifiers);
				
				event.preventDefault();
			}
		}
	}
	
	handleTouchMove(event)
	{
		if (this.touchPan !== null) {
			const touches = this.touchPan.getTouches(event.changedTouches);
			
			if (touches.length == 1) {
				const target = this.touchPan.target;
				const position = this.getPosition(touches[0]);

				this.touchPan.update(position);

				if (target === this.element)
					this.scroll(this.touchPan.x, this.touchPan.y);
				else
					this.move(target, this.touchPan.x, this.touchPan.y);
				
				event.preventDefault();
			}
		}
		
		if (this.touchPinch !== null) {
			const touches = this.touchPinch.getTouches(event.changedTouches);
			
			if (touches.length == 2) {
				const distance = this.getDistance(touches);
				const position = this.getCenter(touches);
				
				this.touchPinch.update(distance);
				this.zoom(position.x, position.y,
					this.touchScaleConstant * this.touchPinch.delta);

				event.preventDefault();
			}
		}
	}
	
	handleTouchEnd(event)
	{
		if (this.touchPan !== null &&
		    this.touchPan.getTouches(event.changedTouches).length > 0) {
				this.touchPan = null;
		}

		if (this.touchPinch !== null &&
		    this.touchPinch.getTouches(event.changedTouches).length > 0) {
				this.touchPinch = null;
		}
	}
}
