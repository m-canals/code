//
// FIXME
//
// A rotation matrix CSS variable, that would be updated on each
// move event, could be added in order to solve resizing issues:
//
//   transform: var(--matrix, identityMatrix) ... (CSS)
//   matrix = rotateX(...) * rotateY(...) * matrix (JS)
//

class MouseRotation
{
	constructor(elements)
	{
		this.elements = elements
		this.controller = null
		
		window.addEventListener('mousedown', this.begin.bind(this))
	}
	
	begin(event)
	{
		this.x = event.clientX
		this.y = event.clientY
		
		if (this.controller) {
			this.controller.abort()
		}
		
		this.controller = new AbortController();
		
		window.addEventListener('mouseup', this.end.bind(this), {
			signal: this.controller.signal})
		window.addEventListener('mousemove', this.move.bind(this), {
			signal: this.controller.signal})
	}
	
	move()
	{
		const x = event.clientX
		const y = event.clientY
		const θ = this.y - y
		const φ = x - this.x
		
		for (const element of this.elements) {
			const matrix = window.getComputedStyle(element).transform
			const transform = `rotateX(${θ}deg) rotateY(${φ}deg) ${matrix}`
			element.style.transform = transform
		}
	
		this.x = x
		this.y = y
	}
	
	end()
	{
		this.controller.abort()
		this.controller = null
	}
}
