class RelativeWeekDay
{
	static NAMES = [
		'Sunday',
		'Monday',
		'Tuesday',
		'Wednesday',
		'Thursday',
		'Friday',
		'Saturday',
	]
	
	static getName(day)
	{
		return RelativeWeekDay.NAMES[day - 1];
	}

	static getShortName(day)
	{
		return RelativeWeekDay.getName(day).substring(0, 3);
	}
}

class RelativeMonth
{
	static NAMES = [
		'January', 'February', 'March', 'April', 'May', 'June',
		'July', 'August', 'September', 'October', 'November', 'December'
	]
	
	static getName(month)
	{
		return RelativeMonth.NAMES[month - 1];
	}
	
	static getShortName(day)
	{
		return RelativeMonth.getName(day).substring(0, 3);
	}
}

class AbsoluteMonth
{
	static createElement(year, month)
	{
		const table = document.createElement('table');
		let date = new Date(year, month - 1, 1);
		let cell, row;
		
		// Month name.
		cell = table.insertRow().insertCell();
		cell.textContent = RelativeMonth.getName(month);
		cell.colSpan = 7;
		cell.style.textAlign = 'center';

		// Day names.
		row = table.insertRow();

		for (let day = 0; day < 7; day++) {
			row.insertCell().textContent = RelativeWeekDay.getShortName(day + 1);
		}
		
		// Previous month.
		row = table.insertRow();

		for (let day = 0; day < date.getDay(); day++) {
			row.insertCell();
		}
		
		// Month.
		while (date.getMonth() == month - 1) {
			const monthDay = date.getDate(); // 1 - 31
			const weekDay = date.getDay();   // 0 - 6
			
			if (weekDay == 0 && row.cells.length > 0) {
				row = table.insertRow();
			}
			
			row.insertCell().textContent = monthDay;
			date.setDate(monthDay + 1);
		}
		
		// Next month.
		for (let day = date.getDay(); day > 0 && day < 7; day++) {
			row.insertCell();
		}
		
		return table;
	}
	
	static * range(startYear, startMonth, endYear, endMonth)
	{
		let year = startYear;
		let month = startMonth;

		while (year < endYear || year == endYear && month <= endMonth) {
			yield [year, month];
			
			month = month % 12 + 1;
			year += month == 1;
		}
	}
}
