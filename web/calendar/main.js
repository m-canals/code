function main(event)
{
	const year = new Date().getFullYear();
	const months = AbsoluteMonth.range(year, 1, year, 12);

	for (const month of months) {
		const element = AbsoluteMonth.createElement(...month);
		document.body.append(element);
	}
}

window.addEventListener('load', main);
