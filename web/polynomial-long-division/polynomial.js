class Polynomial
{
	// Construct a polynomial from a list of numerical coefficients
	// (arguments) where each index denotes the degree of the term
	// of the indexed coefficient.
	constructor(...coefficients)
	{
		this.coefficients = coefficients;
		this.normalize();
	}
	
	// Return coefficients.
	getCoefficients()
	{
		return Array.from(this.coefficients);
	}
		
	// Return numerical coefficient of a term.
	// of non-negative integer degree.
	getCoefficient(degree)
	{
		if (degree < this.coefficients.length)
			return this.coefficients[degree];
		else
			return 0;
	}
	
	// Return the polynomial quotient of the leading term
	// of the polynomial and the leading term of another polynomial.
	// The degree of the divisor term must not be higher than that of
	// the dividend term.
	leadingQuotient(polynomial)
	{
		const A = this.coefficients;
		const B = polynomial.coefficients;
		const degree = this.degree() - polynomial.degree();
		const C = Array(degree + 1).fill(0);
		
		C[degree] = A.at(-1) / B.at(-1)
		const result = new Polynomial();
		result.coefficients = C;

		return result;
	}
	
	// Set the numerical coefficient of a term
	// of non-negative integer degree.
	setCoefficient(degree, coefficient) {
		for (var i = this.coefficients.length; i < degree; i++)
			this.coefficients[i] = 0;
	
		this.coefficients[degree] = coefficient;
		this.normalize();
	}
	
	// Return true if it is a zero polynomial or false otherwise.
	isZero()
	{
		return this.coefficients.length == 1 && this.coefficients[0] == 0;
	}
	
	// Return non-negative integer degree of the polynomial.
	degree()
	{
		return this.coefficients.length - 1;
	}
	
	// Normalize the polynomial.
	normalize()
	{
		if (this.coefficients.length == 0)
			this.coefficients.push(0);
		else
			while (this.coefficients.length > 1 && this.coefficients.at(-1) == 0)
				this.coefficients.pop();
	}

	// Return polynomial product of the polynomial
	// and another polynomial.
	product(polynomial)
	{
		const A = this.coefficients;
		const B = polynomial.coefficients;
		const degree = this.degree() + polynomial.degree();
		var C = Array(degree + 1).fill(0);

		for (let i = 0; i < A.length; i++)
			for (let j = 0; j < B.length; j++)
				C[i + j] += A[i] * B[j]; 

		return new Polynomial(...C);
	}

	// Return polynomial sum of the polynomial
	// and another polynomial.
	sum(polynomial)
	{
		const A = this.coefficients;
		const B = polynomial.coefficients;
		var degree = Math.max(this.degree(), polynomial.degree());
		var C = Array(degree + 1).fill(0);

		for (let i = 0; i < A.length; i++)
			C[i] += A[i];
		
		for (let i = 0; i < B.length; i++)
			C[i] += B[i];
		
		return new Polynomial(...C);
	}

	// Return polynomial difference of the polynomial
	// and another polynomial.
	difference(polynomial)
	{
		const A = this.coefficients;
		const B = polynomial.coefficients;
		var degree = Math.max(this.degree(), polynomial.degree());
		var C = Array(degree + 1).fill(0);

		for (let i = 0; i < A.length; i++)
			C[i] += A[i];
		
		for (let i = 0; i < B.length; i++)
			C[i] -= B[i];
		
		return new Polynomial(...C);
	}

	// Return textual representation of the polynomial.
	toString()
	{
		function formatFloat(value)
		{
			return value.toLocaleString('en-EN',
				{minimumFractionDigits: 0, maximumFractionDigits: 3});
		}
		
		function replaceExhaustively(string, replaced, replacement)
		{
			do {
				var oldString = string;
				string = string.replaceAll(replaced, replacement);
			} while (oldString != string);
			
			return string;
		}

		function toSuperscript(number)
		{
			var string = '';
			
			do {
				string = '⁰¹²³⁴⁵⁶⁷⁸⁹'[number % 10] + string;
				number = Math.floor(number / 10);
			} while (number > 0);
			
			return string;
		}

		const A = this.coefficients;
		var expression = '';

		for (let i = 0; i < A.length; i++) {
			expression += '+' + formatFloat(A[i]) + '⋅x' + toSuperscript(i);
		}

		// Simplify terms (preceded by + sign).
		expression = expression.replaceAll(/[x][⁰]/g, '1');
		expression = expression.replaceAll(/[x][¹][+]/g, 'x+');
		expression = expression.replaceAll(/[x][¹]$/g, 'x');
		expression = expression.replaceAll(/[+][0][⋅][^+]+/g, '+0');
		expression = expression.replaceAll(/[+][-][0][⋅][^+]+/g, '+-0');
		expression = expression.replaceAll(/[+][1][⋅]/g, '+');
		expression = expression.replaceAll(/[+][-][1][⋅]/g, '+-');
		expression = expression.replaceAll(/[⋅][1][+]/g, '+');
		expression = expression.replaceAll(/[⋅][1]$/g, '');

		// Remove terms (preceded by + sign).
		// Each replacement consumes the second + sign,
		// thus the need for exhaustive replacement.
		expression = replaceExhaustively(expression, /[+][0][+]/g, '+');
		expression = replaceExhaustively(expression, /[+][-][0][+]/g, '+');
		expression = expression.replaceAll(/[+][0]$/g, '');
		expression = expression.replaceAll(/[+][-][0]$/g, '');

		// Remove operators.
		expression = expression.replaceAll(/[⋅]/g, '');
		expression = expression.replaceAll(/[+][-]/g, '-');
		
		// Space operators.
		expression = expression.replaceAll(/[+]/g, ' + ');
		expression = expression.replaceAll(/[-]/g, ' - ');
		expression = expression.replaceAll(/^[ ][+][ ]/g, '');
		expression = expression.replaceAll(/^[ ][-][ ]/g, '-');
		
		return expression.length == 0 ? '0' : expression;
	}
}
