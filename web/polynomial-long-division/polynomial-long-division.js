function enlargeCoefficientTable(times)
{
	const table = document.getElementById('coefficient-table');
	
	for (let i = 0; i < times; i++) {
		const degree = table.rows[0].cells.length - 1;
		var cell;
		
		cell = table.rows[0].insertCell();
		cell.innerText = degree;
		cell = table.rows[1].insertCell()
		cell.contentEditable = "true";
		cell = table.rows[2].insertCell();
		cell.contentEditable = "true";
	}
}

function clearCoefficientTable()
{
	var table = document.getElementById('coefficient-table');
	
	for (var i = 1; i <= 2; i++) {
		var cells = table.rows[i].cells;
		
		for (var j = 1; j < cells.length; j++) {
			cells[j].innerText = '';
		}
	}
}

function getCoefficientTableRow(rowIndex)
{
	var table = document.getElementById('coefficient-table');
	var row = [];
	var cells = table.rows[rowIndex].cells;

	for (var j = 1; j < cells.length; j++) {
		row.push(cells[j].textContent);
	}

	return row;
}

function getCoefficientTableRows()
{
	var table = document.getElementById('coefficient-table');

	return [getCoefficientTableRow(1), getCoefficientTableRow(2)];
}

function clearDivisionTable()
{
	var table = document.getElementById('division-table');
	
	while (table.rows.length > 0)
		table.deleteRow(-1);
}

function enlargeDivisionTable(dividend, divisor, subtrahend, quotient, remainder) 
{
	var table = document.getElementById('division-table');
	var row, cell;
	
	if (table.rows.length == 0) {
		let row = table.insertRow();
		
		for (let i = 0; i <= 3; i++)
			row.insertCell(i);
	}
	
	row = table.rows[table.rows.length - 1];
	cell = row.cells[1];
	cell.innerText = String(dividend);
	cell.className = "dividend";
	cell = row.cells[3];
	cell.innerText = String(divisor);
	cell.className = "divisor";

	row = table.insertRow();
	cell = row.insertCell();
	cell.innerText = '−';
	cell.className = "minus-sign";
	cell = row.insertCell();
	cell.innerText = String(subtrahend);
	cell.className = "subtrahend";
	cell = row.insertCell();
	cell.innerText = '';
	cell.className = "separator";
	cell = row.insertCell();
	cell.innerText = String(quotient);
	cell.className = "quotient";
	
	row = table.insertRow();
	cell = row.insertCell();
	cell.innerText = '';
	cell = row.insertCell();
	cell.innerText = String(remainder);
	cell.className = "remainder";
	cell = row.insertCell();
	cell.innerText = '';
	cell.className = "separator";
	cell = row.insertCell();
	cell.innerText = '';
}

function computeDivisionTable()
{
	var [dividend, divisor] = getCoefficientTableRows().map(row =>
		new Polynomial(...row.map(cell => parseFloat(cell)).map(cell => isNaN(cell) ? 0 : cell)));
	
	clearDivisionTable();
	
	var quotient = new Polynomial();
	var remainder = new Polynomial(...dividend.getCoefficients());
	
	if (! divisor.isZero()) {
		if (remainder.isZero() || remainder.degree() < divisor.degree()) {
			enlargeDivisionTable(dividend, divisor, quotient, quotient, remainder);
		} else {
			// Set an interation limit so it doesn't
			// iterate infinitely in case of error.
			var i = 0;
			const n = remainder.degree();
			
			while (! remainder.isZero() && remainder.degree() >= divisor.degree() && i++ <= n) {
				const term = remainder.leadingQuotient(divisor);
				const subtrahend = divisor.product(term);
				quotient = quotient.sum(term);
				remainder = remainder.difference(subtrahend);
				enlargeDivisionTable(dividend, divisor, subtrahend, quotient, remainder);
				dividend = remainder;
			}
		}
	}
}

function loadAndComputeExample()
{
	var table = document.getElementById('coefficient-table');
	
	clearCoefficientTable();
	
	table.rows[1].cells[1].innerText = -4;
	table.rows[1].cells[3].innerText = -2;
	table.rows[1].cells[4].innerText = 1;
	table.rows[2].cells[1].innerText = -3;
	table.rows[2].cells[2].innerText = 1;
	
	computeDivisionTable();
}
