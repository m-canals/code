Web File Manager
================

List directory contents, upload files and download files. It uses the
CGI interface.

![Screenshot of the application.](screenshot.png "Screenshot of the application.")

Usage
-----

It can be run easily on a Python HTTP server:

	python3 -m http.server --cgi [--bind ADDRESS] [PORT]

