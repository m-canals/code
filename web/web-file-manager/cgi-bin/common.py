#!/usr/bin/env python3

import os
import cgi
import urllib
import sys
import builtins
import mimetypes

storage = cgi.FieldStorage()

def get_value(name, default_value = None):
	return storage.getvalue(name, default_value)

def get_files():
	if 'files' in storage:
		files = storage['files']
		
		if type(files) == cgi.FieldStorage:
			# Empty upload.
			if len(files.filename) == 0:
				return []
			# Single file upload.
			else:
				return [files]
		# Multiple file upload.
		elif type(files) == list:
			return files	
	# Empty upload.
	else:
		return []

def print(*args, **kwargs):
	if kwargs.pop('binary', False):
		with os.fdopen(sys.stdout.fileno(), "wb", closefd = False) as stdout:
			sys.stdout.flush()
			for arg in args:
				stdout.write(arg, **kwargs)
			stdout.flush()
	else:
		builtins.print(*args, **kwargs)

def escape(object):
	return cgi.escape(str(object), quote = True)

def url_escape(object):
	return urllib.parse.quote(str(object), safe = '')

class FileSize:
	UNITS = ('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB')

	def __init__(self, size_in_bytes):
		self.size_in_bytes = size_in_bytes

	def __str__(self):
		value, unit = self.convert()
		return f'{value:.1f} {unit}'
	
	def convert(self, target_unit = None):
		if target_unit != None and target_unit not in self.UNITS:
			raise ValueError
	
		value = self.size_in_bytes
		
		for unit in self.UNITS:
			if (target_unit == None and value < 1024 or
			    target_unit != None and unit == target_unit):
				break
			value /= 1024
		
		return (value, unit)

class Entry:
	def __init__(self, path):
		self.path = os.path.normpath(path)
		self.directory = os.path.dirname(path)
		self.filename = os.path.basename(path)
		self.html_path = escape(self.path)
		self.html_url_path = escape(url_escape(self.path))
		self.html_filename = escape(self.filename)
	
	def __lt__(self, entry):
		if self.is_directory() and not entry.is_directory():
			return True
		elif not self.is_directory() and entry.is_directory():
			return False
		else:
			return self.filename.lower() < entry.filename.lower()
		
	def is_directory(self):
		return False
	
	def is_file(self):
		return False
	
	def is_other(self):
		return False

class DirectoryEntry(Entry):
	HTML_LIST_ITEM_CLASS = 'directory'

	def is_directory(self):
		return True

	def is_root(self):
		return os.path.dirname(self.path) == self.path

	def get_entries(self):
		entries = []
		filenames = os.listdir(self.path) + [os.path.curdir]
		
		if not self.is_root():
			filenames.append(os.path.pardir)

		for entry_filename in filenames:
			entry_path = os.path.join(self.path, entry_filename)
			
			if os.path.isdir(entry_path):
				entry = DirectoryEntry(entry_path)
			elif os.path.isfile(entry_path):
				entry = FileEntry(entry_path)
			else:
				entry = OtherEntry(entry_path)
			entries.append(entry)
		return entries
	
	def html_path_breadcrumbs(self):
		html_breadcrumbs = ''
		breadcrumbs = []
		path = self.path
		done = False
		
		while not done:
			head, tail = os.path.split(path)

			if len(tail) == 0:
				breadcrumbs.insert(0, (path, head))
				done = True
			else:
				breadcrumbs.insert(0, (path, tail))
				path = head
		
		
		
		for i, (path, directory) in enumerate(breadcrumbs):
			html_directory = escape(directory)
			html_url_path = escape(url_escape(path))
			html_separator = escape(os.path.sep) if i >= 2 else ''
			html_breadcrumbs += f'{html_separator}<a href="list?path={html_url_path}">{html_directory}</a>'

		return html_breadcrumbs

	def html_entry_item(self):
		return (
			f'<li class="{self.HTML_LIST_ITEM_CLASS}">\n'
			f'<input type="checkbox" name="path" value="{self.html_path}"/>\n'
			f'<a href="list?path={self.html_url_path}">{self.html_filename}</a>\n'
			f'</li>\n')
	
	def html_entry_list(self):
		entries = self.get_entries()
		entries.sort()
		html_entries = ''.join(entry.html_entry_item() for entry in entries)
		return f'<ul class="entries">\n{html_entries}</ul>\n'

	def html_upload_form(self):
		return (
			f'<fieldset class="upload">\n'
			f'<form action="upload" method="post" enctype="multipart/form-data">\n'
			f'<input type="hidden" name="path" value="{self.html_path}"/>\n'
			f'<input type="file" name="files" multiple/>\n'
			f'<input type="submit" value="Upload"/>\n'
			f'</form>\n'
			f'</fieldset>\n')
	
	def upload(self, files):
		uploaded_files = []
		errors = []
		
		for storage in files:
			path = os.path.join(self.path, storage.filename)

			try:
				with open(path, 'wb') as file:
					file.write(storage.value)
				uploaded_files.append(storage)
			except OSError as error:
				errors.append(error)
		
		return uploaded_files, errors

class FileEntry(Entry):
	HTML_LIST_ITEM_CLASS = 'file'
	
	def __init__(self, *args, **kwargs):
		super().__init__(*args, *kwargs)
		self.size = FileSize(os.path.getsize(self.path))
		self.html_size = self.size

	def is_file(self):
		return True

	def html_entry_item(self):
		return (
			f'<li class="{self.HTML_LIST_ITEM_CLASS}">\n'
			f'<input type="checkbox" name="path" value="{self.html_path}"/>\n'
			f'<a href="view?path={self.html_url_path}">{self.html_filename}</a> ({self.html_size})\n'
			f'</li>\n')
	
	def download(self):
		content_type, charset = mimetypes.guess_type(self.path)
		content_type = 'application/octet-stream' if content_type == None else content_type
		charset = 'binary' if charset == None else charset
		
		try:
			with open(self.path, 'rb') as file:
				content = file.read()
				print(f'Content-Type: {content_type}; charset={charset}\r')
				print(f'Content-Disposition: inline; filename={url_escape(self.filename)}\r')
				print(f'\r')
				print(content, binary = True)
			return None
		except OSError as error:
			return error

class OtherEntry(Entry):
	HTML_LIST_ITEM_CLASS = 'other'

	def is_other(self):
		return True

	def html_entry_item(self):
		return (
			f'<li class="{self.HTML_LIST_ITEM_CLASS}">\n'
			f'<input type="checkbox" name="path" value="{self.html_path}"/>\n'
			f'<span>{self.html_filename}</span>\n'
			f'</li>\n')
