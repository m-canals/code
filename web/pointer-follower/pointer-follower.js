class Follower {
	constructor(element, x, y, xOffset, yOffset, updateTime = 16, updateDistance = 1) {
		this.element = element;
		this.x = x; // Central position.
		this.y = y;
		this.X = x; // Followed position.
		this.Y = y;
		this.xOffset = xOffset; // Distance from the top-left corner
		this.yOffset = yOffset; // of the bounding box to the centre.
		this.updateTime = updateTime;
		this.updateDistance = updateDistance;
		this.element.style.position = 'absolute';
		this.updatePosition();
		this.move();
	}
	
	updatePosition() {
		this.element.style.top = (this.y - this.yOffset) + 'px';
		this.element.style.left = (this.x - this.xOffset) + 'px';
	}
	
	move() {
		var dx = this.X - this.x;
		var dy = this.Y - this.y;
		if (Math.abs(dx) >= this.updateDistance ||
		    Math.abs(dy) >= this.updateDistance) {
			var a = Math.atan(dy / dx);
			var x = Math.cos(a) * this.updateDistance;
			var y = Math.sin(a) * this.updateDistance;
			if (dx > 0) {
				this.x += x;
				this.y += y;
			} else {
				this.x -= x;
				this.y -= y;
			}
			this.updatePosition();
		}
		var follower = this;
		window.setTimeout(function() { follower.move() }, this.updateTime);
	}
	
	goTo(x, y) {
		this.X = x;
		this.Y = y;
	}
}
